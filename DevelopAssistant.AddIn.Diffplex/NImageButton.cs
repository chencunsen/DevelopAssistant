﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.AddIn.Diffplex
{
    [ToolboxItem(false)]
    public class NImageButton : System.Windows.Forms.PictureBox
    {
        protected ToolTip toolTip = null;

        protected bool mouserInside = false;

        private string _tooltipText;
        public string ToolTipText
        {
            get { return _tooltipText; }
            set
            {
                _tooltipText = value;
                toolTip.SetToolTip(this, _tooltipText);
            }
        }

        private Image _iconImage;
        public Image IconImage
        {
            get { return _iconImage; }
            set { _iconImage = value; }
        }

        public NImageButton()
        {
            toolTip = new ToolTip();
        }

        protected override void OnPaint(PaintEventArgs paintEventArg)
        {
            base.OnPaint(paintEventArg);
            DrawPaint(paintEventArg.Graphics);
        }

        private void DrawPaint(System.Drawing.Graphics g)
        {
            if (_iconImage != null)
            {
                Rectangle imageRect = new Rectangle(4, 4, 16, 16);
                g.DrawImage(_iconImage, imageRect);
            }

            if (mouserInside)
            {
                Rectangle rect = new Rectangle(0, 0, this.Width - 1, this.Height - 1);
                g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(100, 142, 190, 139)), this.ClientRectangle);
                g.DrawRectangle(new Pen(SystemColors.ControlDark), rect);
            }
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            this.mouserInside = true;
            this.Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            this.mouserInside = false;
            this.Invalidate();
        }

    }
}
