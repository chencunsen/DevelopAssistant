﻿using DevelopAssistant.AddIn.Screenshot.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DevelopAssistant.AddIn.Screenshot
{
    public class ScreenshotAddIn : DevelopAssistant.AddIn.WindowAddIn
    {
        public ScreenshotAddIn()
        {
            this.IdentityID = "60b44cf5-1056-492a-a9a3-8e7127a74e38";
            this.Name = "获取屏幕截图";
            this.Text = "获取屏幕截图";
            this.Tooltip = "获取屏幕截图";            
            this.Icon = Resources.plus_shield;
        }

        public override void Initialize(string AssemblyName, string Winname)
        {
            //
        }

        public override object Execute(params object[] Parameter)
        {
            MainForm f = new MainForm((Form)Parameter[0],this){ 
               // WindowFloat = new DelegateWindowFloatHandler(FloatParentCenter) 
            };         
            return f;             
        }
    }
}
