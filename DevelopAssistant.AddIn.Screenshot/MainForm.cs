﻿using DevelopAssistant.AddIn;
using DevelopAssistant.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DevelopAssistant.AddIn.Screenshot
{
    public partial class MainForm : ICSharpCode.WinFormsUI.Forms.BaseForm
    {
        protected bool h;
        protected int x1;
        protected int y1;
        protected int x2;
        protected int y2; 

        private ProfessionalCaptureImageToolColorTable _colorTable =
        new ProfessionalCaptureImageToolColorTable();

        public Form HostForm;

        public MainForm()
        {
            h = false;
            InitializeComponent();
        }

        public MainForm(Form HostWindow, WindowAddIn AddIn)
            : this()
        {
            this.HostForm = HostWindow;
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.XTheme = ((ICSharpCode.WinFormsUI.Forms.BaseForm)HostWindow).XTheme;
            this.NToolBar.BorderColor = XTheme.FormBorderOutterColor;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new EventArgs());
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (h) HostForm.Location = new Point(x1, y1);  
        }

        private void BtnScreenshot_Click(object sender, EventArgs e)
        {
            x2 = this.Location.X;
            y2 = this.Location.Y;
            this.Location = new Point(-this.Width - 20, -this.Height - 20);

            CaptureImageTool capture = new CaptureImageTool();
            capture.ColorTable = _colorTable;
            capture.SelectCursor = CursorManager.ArrowNew;
            capture.DrawCursor = CursorManager.CrossNew;


            if (DialogResult.OK.Equals(capture.ShowDialog()))
            {
                this.toolStripLabel3.Text = "完成";
                this.Preview.Image = capture.Image;
            }

            this.Location = new Point(x2, y2);           

        }

        private void BtnHideAssistant_Click(object sender, EventArgs e)
        {
            x1 = HostForm.Location.X;
            y1 = HostForm.Location.Y;
            HostForm.Location = new Point(-HostForm.Width - 20, -HostForm.Height - 20);
            h = true;
        }

        private void toolStripButtonDrag_Click(object sender, EventArgs e)
        {
            this.Preview.SetDrag();
            if (this.Preview.Drag)
                toolStripButtonDrag.Checked = true;
            else
                toolStripButtonDrag.Checked = false;
        }

        private void toolStripButtonSave_Click(object sender, EventArgs e)
        {
            this.Preview.SaveToFile();
        }

        private void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color toolBackColor = SystemColors.Control;
            Color textColor = SystemColors.Window;
            Color linkColor = System.Drawing.Color.Blue;
            Color toolStripBackColor = SystemColors.ControlLight;
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    linkColor = System.Drawing.Color.Blue;
                    toolBackColor = SystemColors.Control;
                    textColor = SystemColors.Window;
                    toolStripBackColor = Color.FromArgb(246, 248, 250);
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(045, 045, 048);
                    linkColor = Color.FromArgb(051, 153, 153);
                    toolBackColor = Color.FromArgb(038, 038, 038);
                    textColor = Color.FromArgb(038, 038, 038);
                    toolStripBackColor = Color.FromArgb(038, 038, 038);
                    break;
            }

            panel1.ForeColor = foreColor;
            panel1.BackColor = backColor;
            panel2.ForeColor = foreColor;
            panel2.BackColor = backColor;
            Preview.BackColor = backColor;
            NToolBar.ForeColor = foreColor;
            NToolBar.BackColor = toolBackColor;
            toolStrip1.ForeColor = foreColor;
            toolStrip1.BackColor = toolStripBackColor;
            btnScreenShot.ForeColor = foreColor;
            btnScreenShot.BackColor = toolBackColor;
            btnHidenAssistant.ForeColor = foreColor;
            btnHidenAssistant.BackColor = toolBackColor;           
        }

    }
}
