﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace DevelopAssistant.AddIn.Screenshot
{    
    public enum DrawStyle
    {
        None = 0,
        Rectangle,
        Ellipse,
        Arrow,
        Text,
        Line
    }

    internal enum SizeGrip
    {
        None = 0,
        Top,
        Bottom,
        Left,
        Right,
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight,
        All
    }

    internal enum OperateType
    {
        None = 0,
        DrawRectangle,
        DrawEllipse,
        DrawArrow,
        DrawLine,
        DrawText
    }

    internal class OperateObject
    {
        private OperateType _operateType;
        private Color _color;
        private object _data;

        public OperateObject() { }

        public OperateObject(
            OperateType operateType, Color color, object data)
        {
            _operateType = operateType;
            _color = color;
            _data = data;
        }

        public OperateType OperateType
        {
            get { return _operateType; }
            set { _operateType = value; }
        }

        public Color Color
        {
            get { return _color; }
            set { _color = value; }
        }

        public object Data
        {
            get { return _data; }
            set { _data = value; }
        }
    }
}
