﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Core
{
    public class FindAndReplaceRequest
    {
        public string CommandText { get; set; }
        public string LookFor { get; set; }
        public string ReplaceWith { get; set; }
        public bool MatchCase { get; set; }
        public int Position { get; set; }

        public StringComparison StringComparison
        {
            get
            {
                StringComparison sc = StringComparison.CurrentCultureIgnoreCase;
                if (MatchCase)
                {
                    sc = StringComparison.CurrentCulture;
                }
                return sc;
            }
        }
    }
}
