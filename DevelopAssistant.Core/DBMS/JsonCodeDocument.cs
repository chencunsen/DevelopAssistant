﻿using DevelopAssistant.Service;
using ICSharpCode.TextEditor.Document;
using ICSharpCode.WinFormsUI.Docking;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing; 
using System.Text; 
using System.Windows.Forms;

namespace DevelopAssistant.Core.DBMS
{
    public partial class JsonCodeDocument : DockContent, IDocumentContent
    {
        ICSharpCode.TextEditor.TextEditorControl textEditorControl1 = null;

        public JsonCodeDocument()
        {
            InitializeComponent();
        }

        public JsonCodeDocument(MainForm form) : this()
        {
           
        }

        public void InitializeEditor(string content,string highlightingName)
        {
            textEditorControl1 = new ICSharpCode.TextEditor.TextEditorControl();
            textEditorControl1.ShowGuidelines = true;
            textEditorControl1.Dock = DockStyle.Fill;
            this.Controls.Add(textEditorControl1);            

            textEditorControl1.Document.FormattingStrategy = new CSharpFormattingStrategy();
            textEditorControl1.Document.FoldingManager.FoldingStrategy = new IndentFoldingStrategy(); //new IndentFoldingStrategy();
            textEditorControl1.Document.FoldingManager.UpdateFoldings();

            OnThemeChanged(new EventArgs());
        }

        public void SetJsonCodeText(string content)
        {
            textEditorControl1.Text = content;            
        }

        public void FindAndReplace(FindAndReplaceRequest request)
        {
             
        }

        private void JsonCodeDocument_Load(object sender, EventArgs e)
        {
            
        }

        public override void OnThemeChanged(EventArgs e)
        {
            switch (AppSettings.EditorSettings
                .TSQLEditorTheme)
            {
                case "Default":
                    textEditorControl1.LineViewerStyle = LineViewerStyle.None;
                    BackColor = SystemColors.Control;
                    break;
                case "Black":
                    textEditorControl1.LineViewerStyle = LineViewerStyle.FullRow;
                    BackColor = Color.FromArgb(045, 045, 048);
                    break;
            }

            textEditorControl1.Font = AppSettings.EditorSettings.EditorFont;
            textEditorControl1.SetHighlighting(AppSettings.EditorSettings
                .TSQLEditorTheme, "JavaScript");
        }

    }
}
