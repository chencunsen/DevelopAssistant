﻿using DevelopAssistant.Core.ToolBar;
using DevelopAssistant.Core.ToolBox;
using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Controls;
using ICSharpCode.WinFormsUI.Docking;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing; 
using System.Text; 
using System.Windows.Forms;

namespace DevelopAssistant.Core.DBMS
{
    public partial class DeleteTableDocument : DockContent, IDocumentContent
    {
        NTreeNode TNode = null;
        MenuBar OwnerForm = null;
        DataBaseServer SServer = null;

        public DeleteTableDocument()
        {
            InitializeComponent();
            InitializeControls();
        }

        public DeleteTableDocument(NTreeNode Node, DataBaseServer Server, MenuBar leftBar)
            : this()
        {
            TNode = Node;
            SServer = Server;
            OwnerForm = leftBar;
            txtTableName.Text = TNode.Text;
        }

        public void InitializeControls()
        {
            switch (AppSettings.WindowTheme.Name)
            {
                case "Mac":
                    this.btnApply.Radius = 4;
                    this.btnCancel.Radius = 4;
                    break;
                case "Shadow":
                    this.btnApply.Radius = 2;
                    this.btnCancel.Radius = 2;
                    break;
                case "VS2012":
                    this.btnApply.Radius = 0;
                    this.btnCancel.Radius = 0;
                    break;
            }
        }

        public void RefreshDocument(NTreeNode Node, DataBaseServer Server)
        {
            TNode = Node;
            SServer = Server;
            txtTableName.Text = TNode.Text;
            txtTableName.ApplyStyles();
            BindDependencies();
        }

        public void FindAndReplace(FindAndReplaceRequest request)
        {
            
        }

        private void BindDependencies()
        {
            using (var db = Utility.GetAdohelper(SServer))
            {
                this.listBox1.Items.Clear();
                var dt = db.GetDependencies(txtTableName.Text, 0);
                foreach (DataRow row in dt.Rows)
                {
                    string name = row["ObjectName"] + "(" + row["ObjectType"] + ")";
                    this.listBox1.Items.Add(name);
                }
                this.listBox1.Invalidate();
            }
        }

        private void DeleteTableDocument_Load(object sender, EventArgs e)
        {
            groupBox1.Title = "要删除的对象";
            groupBox2.Title = "所有依赖它的项";
            OnThemeChanged(new EventArgs());
        }

        private void btnApply_Click(object sender, EventArgs e)
        {            
            if(new MessageDialog(DevelopAssistant.Core.Properties.Resources.warning_32px, "确定要删除 " + this.txtTableName.Text + " 吗?", MessageBoxButtons.OKCancel)
                .ShowDialog().Equals(DialogResult.OK))
            {
                string strSql = "DROP TABLE ";
                strSql +=SnippetBase.GetTableName(this.txtTableName.Text, SServer.ProviderName) + ";";

                using (var db = Utility.GetAdohelper(SServer))
                {
                    db.Execute(CommandType.Text, strSql, null);
                }

                OwnerForm.SetSelectedNode(TNode.Parent);
                OwnerForm.RefreshDataBase();
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public override void OnThemeChanged(EventArgs e)
        {
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;

            Color listBoxColor = SystemColors.Window;
            Color foreColor = SystemColors.ControlText;
            Color backColor = SystemColors.Control;

            switch (themeName)
            {
                case "Black":
                    listBoxColor = Color.FromArgb(048, 048, 048);
                    foreColor = Color.FromArgb(240, 240, 248);
                    backColor = Color.FromArgb(030, 030, 030);
                    break;
                case "Default":
                    listBoxColor = SystemColors.Window;
                    foreColor = SystemColors.ControlText;
                    backColor = SystemColors.Control;
                    break;
            }
           
            BackColor = backColor;
            ForeColor = foreColor;
            groupBox1.ForeColor = foreColor;
            groupBox1.BackColor = backColor;
            groupBox2.ForeColor = foreColor;
            groupBox2.BackColor = backColor;
            listBox1.ForeColor = foreColor;
            listBox1.BackColor = listBoxColor;          
            txtTableName.XForeColor = foreColor;
            txtTableName.XBackColor = listBoxColor;
            btnApply.ForeColor = foreColor;
            btnApply.BackColor = backColor;
            btnCancel.ForeColor = foreColor;
            btnCancel.BackColor = backColor;            

        }

    }
}
