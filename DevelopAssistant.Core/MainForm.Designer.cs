﻿ 
namespace DevelopAssistant.Core
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.statusBar = new ICSharpCode.WinFormsUI.Controls.NStatusBar();
            this.SysNewVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.SysStatusInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.SysTodoInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.contextMenuStrip1 = new ICSharpCode.WinFormsUI.Controls.NContextMenuStrip(this.components);
            this.关闭ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.关闭其它ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.关闭所有ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.刷新ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.contextMenuStrip2 = new ICSharpCode.WinFormsUI.Controls.NContextMenuStrip(this.components);
            this.关闭所有ToolStripMenuItem1 = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.dockPanel = new ICSharpCode.WinFormsUI.Docking.DockPanel();
            this.MainToolBar = new ICSharpCode.WinFormsUI.Controls.NToolStrip();
            this.toolBarServers = new ICSharpCode.WinFormsUI.Controls.NToolStripDropDownButton();
            this.连接Sql数据库ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.连接sqlite数据库ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.postgresql数据库ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.access数据库ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.mySql数据库ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.toolBarButtonTheme = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarButtonToolBox = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarTodo = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarButtonLogger = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarButtonSeparator1 = new ICSharpCode.WinFormsUI.Controls.NToolStripSeparator();
            this.toolButtonNotepad = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarButtonNewQuery = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarButtonOpen = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarButtonSave = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarButtonFind = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarButtonDocument = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripSeparator2 = new ICSharpCode.WinFormsUI.Controls.NToolStripSeparator();
            this.toolBarUndo = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarRedo = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarFinder = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripSeparator5 = new ICSharpCode.WinFormsUI.Controls.NToolStripSeparator();
            this.toolBarAddComment = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarRemoveComment = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarIndent = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarOutIndent = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripSeparator7 = new ICSharpCode.WinFormsUI.Controls.NToolStripSeparator();
            this.toolBarFormat = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarMinum = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripSeparator6 = new ICSharpCode.WinFormsUI.Controls.NToolStripSeparator();
            this.toolBarRollback = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarCommit = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarStop = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarExcecute = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarButtonSeparator2 = new ICSharpCode.WinFormsUI.Controls.NToolStripSeparator();
            this.toolBarButtonRestore = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarButtonTorestore = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarLogout = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolBarExit = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.MainMenu = new ICSharpCode.WinFormsUI.Controls.NMenuStrip();
            this.menuItemMenu = new ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem();
            this.新建文档ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.打开文件ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.设置主题ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.MacThemeToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.WindowsThemeToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.BootstrapThemeToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.toolStripSeparator4 = new ICSharpCode.WinFormsUI.Controls.NToolStripSeparator();
            this.生成文档ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.文本保存ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.toolStripSeparator3 = new ICSharpCode.WinFormsUI.Controls.NToolStripSeparator();
            this.退出ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.menuItemFunction = new ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem();
            this.我的待办ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.menuItemView = new ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem();
            this.menuItemMaxSize = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.menuItemNormalSize = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.menuItemDefaultWindow = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.toolStripSeparator1 = new ICSharpCode.WinFormsUI.Controls.NToolStripSeparator();
            this.menuItemClose = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.menuItemCloseAll = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.menuItemCloseAllButThisOne = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.menuItemFile = new ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem();
            this.menuItemTools = new ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem();
            this.menuItemOptions = new ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem();
            this.编辑器设置ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.时间格式设置ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.menuItemWindow = new ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem();
            this.menuItemNewWindow = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.menuItemHelp = new ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem();
            this.menuItemFind = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.menuItemExecute = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.menuItemFormat = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.menuItemEdit = new ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem();
            this.帮助ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.关于ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.contextMenuStrip3 = new ICSharpCode.WinFormsUI.Controls.NContextMenuStrip(this.components);
            this.连接服务器ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.断开连接ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.删除连接ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.编辑属性ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.新增表ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.打开前100行ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.打开表ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.设计表ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.编辑数据ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.删除表ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.查看表对象ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.生成脚本ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.createSQLToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.insertSQLToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.updateSQLToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.clearSQLToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.生成代码ToolStripMenuItem = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.刷新ToolStripMenuItem1 = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.SystemContextMenu = new ICSharpCode.WinFormsUI.Controls.NContextMenuStrip(this.components);
            this.toolStripMenuItem3 = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.toolStripMenuItem2 = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.toolStripMenuItem1 = new ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem();
            this.statusBar.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.MainToolBar.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this.contextMenuStrip3.SuspendLayout();
            this.SystemContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusBar
            // 
            this.statusBar.AutoSize = false;
            this.statusBar.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.statusBar.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.statusBar.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SysNewVersion,
            this.SysStatusInfo,
            this.SysTodoInfo});
            this.statusBar.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.statusBar.Location = new System.Drawing.Point(6, 668);
            this.statusBar.Name = "statusBar";
            this.statusBar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.statusBar.Size = new System.Drawing.Size(1084, 26);
            this.statusBar.TabIndex = 9;
            // 
            // SysNewVersion
            // 
            this.SysNewVersion.Font = new System.Drawing.Font("微软雅黑", 8F, System.Drawing.FontStyle.Italic);
            this.SysNewVersion.ForeColor = System.Drawing.Color.Red;
            this.SysNewVersion.Image = global::DevelopAssistant.Core.Properties.Resources.security_caution_16px;
            this.SysNewVersion.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.SysNewVersion.Name = "SysNewVersion";
            this.SysNewVersion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.SysNewVersion.Size = new System.Drawing.Size(18, 21);
            this.SysNewVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.SysNewVersion.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.SysNewVersion.Visible = false;
            this.SysNewVersion.Click += new System.EventHandler(this.SysNewVersion_Click);
            // 
            // SysStatusInfo
            // 
            this.SysStatusInfo.AutoToolTip = true;
            this.SysStatusInfo.Name = "SysStatusInfo";
            this.SysStatusInfo.Size = new System.Drawing.Size(32, 21);
            this.SysStatusInfo.Text = "完成";
            this.SysStatusInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SysTodoInfo
            // 
            this.SysTodoInfo.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.SysTodoInfo.ForeColor = System.Drawing.Color.Maroon;
            this.SysTodoInfo.Image = global::DevelopAssistant.Core.Properties.Resources.lamp_24px;
            this.SysTodoInfo.Name = "SysTodoInfo";
            this.SysTodoInfo.RightToLeftAutoMirrorImage = true;
            this.SysTodoInfo.Size = new System.Drawing.Size(74, 21);
            this.SysTodoInfo.Text = "待办提醒";
            this.SysTodoInfo.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.SysTodoInfo.Visible = false;
            this.SysTodoInfo.Click += new System.EventHandler(this.SysTodoInfo_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.关闭ToolStripMenuItem,
            this.关闭其它ToolStripMenuItem,
            this.关闭所有ToolStripMenuItem,
            this.刷新ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 92);
            // 
            // 关闭ToolStripMenuItem
            // 
            this.关闭ToolStripMenuItem.AddIn = null;
            this.关闭ToolStripMenuItem.Index = 0;
            this.关闭ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.关闭ToolStripMenuItem.Level = 0;
            this.关闭ToolStripMenuItem.Name = "关闭ToolStripMenuItem";
            this.关闭ToolStripMenuItem.Position = null;
            this.关闭ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.关闭ToolStripMenuItem.Text = "关闭";
            this.关闭ToolStripMenuItem.Click += new System.EventHandler(this.关闭ToolStripMenuItem_Click);
            // 
            // 关闭其它ToolStripMenuItem
            // 
            this.关闭其它ToolStripMenuItem.AddIn = null;
            this.关闭其它ToolStripMenuItem.Index = 0;
            this.关闭其它ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.关闭其它ToolStripMenuItem.Level = 0;
            this.关闭其它ToolStripMenuItem.Name = "关闭其它ToolStripMenuItem";
            this.关闭其它ToolStripMenuItem.Position = null;
            this.关闭其它ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.关闭其它ToolStripMenuItem.Text = "关闭其它";
            this.关闭其它ToolStripMenuItem.Click += new System.EventHandler(this.关闭其它ToolStripMenuItem_Click);
            // 
            // 关闭所有ToolStripMenuItem
            // 
            this.关闭所有ToolStripMenuItem.AddIn = null;
            this.关闭所有ToolStripMenuItem.Index = 0;
            this.关闭所有ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.关闭所有ToolStripMenuItem.Level = 0;
            this.关闭所有ToolStripMenuItem.Name = "关闭所有ToolStripMenuItem";
            this.关闭所有ToolStripMenuItem.Position = null;
            this.关闭所有ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.关闭所有ToolStripMenuItem.Text = "关闭所有";
            this.关闭所有ToolStripMenuItem.Click += new System.EventHandler(this.关闭所有ToolStripMenuItem_Click);
            // 
            // 刷新ToolStripMenuItem
            // 
            this.刷新ToolStripMenuItem.AddIn = null;
            this.刷新ToolStripMenuItem.Index = 0;
            this.刷新ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.刷新ToolStripMenuItem.Level = 0;
            this.刷新ToolStripMenuItem.Name = "刷新ToolStripMenuItem";
            this.刷新ToolStripMenuItem.Position = null;
            this.刷新ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.刷新ToolStripMenuItem.Text = "刷新";
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.关闭所有ToolStripMenuItem1});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(125, 26);
            // 
            // 关闭所有ToolStripMenuItem1
            // 
            this.关闭所有ToolStripMenuItem1.AddIn = null;
            this.关闭所有ToolStripMenuItem1.Index = 0;
            this.关闭所有ToolStripMenuItem1.ItemType = "ToolStripMenuItem";
            this.关闭所有ToolStripMenuItem1.Level = 0;
            this.关闭所有ToolStripMenuItem1.Name = "关闭所有ToolStripMenuItem1";
            this.关闭所有ToolStripMenuItem1.Position = null;
            this.关闭所有ToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.关闭所有ToolStripMenuItem1.Text = "关闭所有";
            this.关闭所有ToolStripMenuItem1.Click += new System.EventHandler(this.关闭所有ToolStripMenuItem_Click);
            // 
            // dockPanel
            // 
            this.dockPanel.DefaultFloatWindowSize = new System.Drawing.Size(500, 400);
            this.dockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPanel.DockBackColor = System.Drawing.SystemColors.AppWorkspace;
            this.dockPanel.DockBottomPortion = 150D;
            this.dockPanel.DockLeftPortion = 200D;
            this.dockPanel.DockRightPortion = 200D;
            this.dockPanel.DockTopPortion = 150D;
            this.dockPanel.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(0)));
            this.dockPanel.Location = new System.Drawing.Point(6, 89);
            this.dockPanel.Name = "dockPanel";
            this.dockPanel.RightToLeftLayout = true;
            this.dockPanel.Size = new System.Drawing.Size(1084, 579);
            this.dockPanel.TabIndex = 8;
            this.dockPanel.WindowTheme = null;
            // 
            // MainToolBar
            // 
            this.MainToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBarServers,
            this.toolBarButtonTheme,
            this.toolBarButtonToolBox,
            this.toolBarTodo,
            this.toolBarButtonLogger,
            this.toolBarButtonSeparator1,
            this.toolButtonNotepad,
            this.toolBarButtonNewQuery,
            this.toolBarButtonOpen,
            this.toolBarButtonSave,
            this.toolBarButtonFind,
            this.toolBarButtonDocument,
            this.toolStripSeparator2,
            this.toolBarUndo,
            this.toolBarRedo,
            this.toolBarFinder,
            this.toolStripSeparator5,
            this.toolBarAddComment,
            this.toolBarRemoveComment,
            this.toolBarIndent,
            this.toolBarOutIndent,
            this.toolStripSeparator7,
            this.toolBarFormat,
            this.toolBarMinum,
            this.toolStripSeparator6,
            this.toolBarRollback,
            this.toolBarCommit,
            this.toolBarStop,
            this.toolBarExcecute,
            this.toolBarButtonSeparator2,
            this.toolBarButtonRestore,
            this.toolBarButtonTorestore,
            this.toolBarLogout,
            this.toolBarExit});
            this.MainToolBar.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.MainToolBar.Location = new System.Drawing.Point(6, 61);
            this.MainToolBar.Name = "MainToolBar";
            this.MainToolBar.Padding = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.MainToolBar.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.MainToolBar.Size = new System.Drawing.Size(1084, 28);
            this.MainToolBar.Stretch = true;
            this.MainToolBar.TabIndex = 10;
            // 
            // toolBarServers
            // 
            this.toolBarServers.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.连接Sql数据库ToolStripMenuItem,
            this.连接sqlite数据库ToolStripMenuItem,                         
            this.mySql数据库ToolStripMenuItem,
            this.postgresql数据库ToolStripMenuItem,
            this.access数据库ToolStripMenuItem});
            this.toolBarServers.Image = global::DevelopAssistant.Core.Properties.Resources.add_database;
            this.toolBarServers.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarServers.Name = "toolBarServers";
            this.toolBarServers.Size = new System.Drawing.Size(61, 21);
            this.toolBarServers.Text = "连接";
            // 
            // 连接Sql数据库ToolStripMenuItem
            // 
            this.连接Sql数据库ToolStripMenuItem.AddIn = null;
            this.连接Sql数据库ToolStripMenuItem.Index = 0;
            this.连接Sql数据库ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.连接Sql数据库ToolStripMenuItem.Level = 0;
            this.连接Sql数据库ToolStripMenuItem.Name = "连接Sql数据库ToolStripMenuItem";
            this.连接Sql数据库ToolStripMenuItem.Position = null;
            this.连接Sql数据库ToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.连接Sql数据库ToolStripMenuItem.Text = "Sql数据库";
            this.连接Sql数据库ToolStripMenuItem.Click += new System.EventHandler(this.toolBarServers_Click);
            // 
            // 连接sqlite数据库ToolStripMenuItem
            // 
            this.连接sqlite数据库ToolStripMenuItem.AddIn = null;
            this.连接sqlite数据库ToolStripMenuItem.Index = 0;
            this.连接sqlite数据库ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.连接sqlite数据库ToolStripMenuItem.Level = 0;
            this.连接sqlite数据库ToolStripMenuItem.Name = "连接sqlite数据库ToolStripMenuItem";
            this.连接sqlite数据库ToolStripMenuItem.Position = null;
            this.连接sqlite数据库ToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.连接sqlite数据库ToolStripMenuItem.Text = "Sqlite数据库";
            this.连接sqlite数据库ToolStripMenuItem.Click += new System.EventHandler(this.toolBarServers_Click);
            // 
            // postgresql数据库ToolStripMenuItem
            // 
            this.postgresql数据库ToolStripMenuItem.AddIn = null;
            this.postgresql数据库ToolStripMenuItem.Index = 0;
            this.postgresql数据库ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.postgresql数据库ToolStripMenuItem.Level = 0;
            this.postgresql数据库ToolStripMenuItem.Name = "postgresql数据库ToolStripMenuItem";
            this.postgresql数据库ToolStripMenuItem.Position = null;
            this.postgresql数据库ToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.postgresql数据库ToolStripMenuItem.Text = "Postgresql数据库";
            this.postgresql数据库ToolStripMenuItem.Click += new System.EventHandler(this.toolBarServers_Click);
            // 
            // oracle数据库ToolStripMenuItem
            // 
            this.access数据库ToolStripMenuItem.AddIn = null;
            this.access数据库ToolStripMenuItem.Index = 0;
            this.access数据库ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.access数据库ToolStripMenuItem.Level = 0;
            this.access数据库ToolStripMenuItem.Name = "access数据库ToolStripMenuItem";
            this.access数据库ToolStripMenuItem.Position = null;
            this.access数据库ToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.access数据库ToolStripMenuItem.Text = "Access数据库";
            this.access数据库ToolStripMenuItem.Click += new System.EventHandler(this.toolBarServers_Click);
            // 
            // mySql数据库ToolStripMenuItem
            // 
            this.mySql数据库ToolStripMenuItem.AddIn = null;
            this.mySql数据库ToolStripMenuItem.Index = 0;
            this.mySql数据库ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.mySql数据库ToolStripMenuItem.Level = 0;
            this.mySql数据库ToolStripMenuItem.Name = "mySql数据库ToolStripMenuItem";
            this.mySql数据库ToolStripMenuItem.Position = null;
            this.mySql数据库ToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.mySql数据库ToolStripMenuItem.Text = "MySql数据库";
            this.mySql数据库ToolStripMenuItem.Click += new System.EventHandler(this.toolBarServers_Click);
            // 
            // toolBarButtonTheme
            // 
            this.toolBarButtonTheme.Image = global::DevelopAssistant.Core.Properties.Resources.theme;
            this.toolBarButtonTheme.Name = "toolBarButtonTheme";
            this.toolBarButtonTheme.Size = new System.Drawing.Size(52, 21);
            this.toolBarButtonTheme.Text = "主题";
            this.toolBarButtonTheme.Click += new System.EventHandler(this.toolBarButtonTheme_Click);
            // 
            // toolBarButtonToolBox
            // 
            this.toolBarButtonToolBox.Image = global::DevelopAssistant.Core.Properties.Resources.tool;
            this.toolBarButtonToolBox.Name = "toolBarButtonToolBox";
            this.toolBarButtonToolBox.Size = new System.Drawing.Size(52, 21);
            this.toolBarButtonToolBox.Text = "工具";
            this.toolBarButtonToolBox.ToolTipText = "工具箱";
            this.toolBarButtonToolBox.Click += new System.EventHandler(this.toolBarButtonToolBox_Click);
            // 
            // toolBarTodo
            // 
            this.toolBarTodo.Image = global::DevelopAssistant.Core.Properties.Resources.client_account_template;
            this.toolBarTodo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarTodo.Name = "toolBarTodo";
            this.toolBarTodo.Size = new System.Drawing.Size(52, 21);
            this.toolBarTodo.Text = "待办";
            this.toolBarTodo.Click += new System.EventHandler(this.toolBarTodo_Click);
            // 
            // toolBarButtonLogger
            // 
            this.toolBarButtonLogger.Image = global::DevelopAssistant.Core.Properties.Resources.document_editing;
            this.toolBarButtonLogger.Name = "toolBarButtonLogger";
            this.toolBarButtonLogger.Size = new System.Drawing.Size(52, 21);
            this.toolBarButtonLogger.Text = "日志";
            this.toolBarButtonLogger.ToolTipText = "查看日志";
            this.toolBarButtonLogger.Click += new System.EventHandler(this.toolBarButtonLogger_Click);
            // 
            // toolBarButtonSeparator1
            // 
            this.toolBarButtonSeparator1.Name = "toolBarButtonSeparator1";
            this.toolBarButtonSeparator1.Size = new System.Drawing.Size(6, 24);
            // 
            // toolButtonNotepad
            // 
            this.toolButtonNotepad.Image = global::DevelopAssistant.Core.Properties.Resources.edit_Pencil_24px;
            this.toolButtonNotepad.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonNotepad.Name = "toolButtonNotepad";
            this.toolButtonNotepad.Size = new System.Drawing.Size(52, 21);
            this.toolButtonNotepad.Text = "笔记";
            this.toolButtonNotepad.Click += new System.EventHandler(this.toolButtonNotepad_Click);
            // 
            // toolBarButtonNewQuery
            // 
            this.toolBarButtonNewQuery.Image = global::DevelopAssistant.Core.Properties.Resources.databasedocumet;
            this.toolBarButtonNewQuery.Name = "toolBarButtonNewQuery";
            this.toolBarButtonNewQuery.Size = new System.Drawing.Size(52, 21);
            this.toolBarButtonNewQuery.Text = "新建";
            this.toolBarButtonNewQuery.ToolTipText = "新建查询";
            this.toolBarButtonNewQuery.Click += new System.EventHandler(this.toolBarButtonNewWindow_Click);
            // 
            // toolBarButtonOpen
            // 
            this.toolBarButtonOpen.Image = global::DevelopAssistant.Core.Properties.Resources.open_file;
            this.toolBarButtonOpen.Name = "toolBarButtonOpen";
            this.toolBarButtonOpen.Size = new System.Drawing.Size(52, 21);
            this.toolBarButtonOpen.Text = "打开";
            this.toolBarButtonOpen.Visible = false;
            this.toolBarButtonOpen.Click += new System.EventHandler(this.打开文件ToolStripMenuItem_Click);
            // 
            // toolBarButtonSave
            // 
            this.toolBarButtonSave.Image = global::DevelopAssistant.Core.Properties.Resources.save;
            this.toolBarButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarButtonSave.Name = "toolBarButtonSave";
            this.toolBarButtonSave.Size = new System.Drawing.Size(52, 21);
            this.toolBarButtonSave.Text = "保存";
            this.toolBarButtonSave.Visible = false;
            this.toolBarButtonSave.Click += new System.EventHandler(this.toolBarButtonSave_Click);
            // 
            // toolBarButtonFind
            // 
            this.toolBarButtonFind.Image = global::DevelopAssistant.Core.Properties.Resources.query;
            this.toolBarButtonFind.Name = "toolBarButtonFind";
            this.toolBarButtonFind.Size = new System.Drawing.Size(52, 21);
            this.toolBarButtonFind.Text = "查找";
            this.toolBarButtonFind.ToolTipText = "快速查找";
            this.toolBarButtonFind.Visible = false;
            // 
            // toolBarButtonDocument
            // 
            this.toolBarButtonDocument.Image = global::DevelopAssistant.Core.Properties.Resources.abiword;
            this.toolBarButtonDocument.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarButtonDocument.Name = "toolBarButtonDocument";
            this.toolBarButtonDocument.Size = new System.Drawing.Size(76, 21);
            this.toolBarButtonDocument.Text = "生成文档";
            this.toolBarButtonDocument.Visible = false;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 24);
            this.toolStripSeparator2.Visible = false;
            // 
            // toolBarUndo
            // 
            this.toolBarUndo.Image = global::DevelopAssistant.Core.Properties.Resources.undo;
            this.toolBarUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarUndo.Name = "toolBarUndo";
            this.toolBarUndo.Size = new System.Drawing.Size(52, 21);
            this.toolBarUndo.Text = "撤销";
            this.toolBarUndo.Visible = false;
            this.toolBarUndo.Click += new System.EventHandler(this.toolBarUndo_Click);
            // 
            // toolBarRedo
            // 
            this.toolBarRedo.Image = global::DevelopAssistant.Core.Properties.Resources.redo;
            this.toolBarRedo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarRedo.Name = "toolBarRedo";
            this.toolBarRedo.Size = new System.Drawing.Size(52, 21);
            this.toolBarRedo.Text = "重做";
            this.toolBarRedo.ToolTipText = "重做";
            this.toolBarRedo.Visible = false;
            this.toolBarRedo.Click += new System.EventHandler(this.toolBarRedo_Click);
            // 
            // toolBarFinder
            // 
            this.toolBarFinder.Image = global::DevelopAssistant.Core.Properties.Resources.find;
            this.toolBarFinder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarFinder.Name = "toolBarFinder";
            this.toolBarFinder.Size = new System.Drawing.Size(52, 21);
            this.toolBarFinder.Text = "查找";
            this.toolBarFinder.ToolTipText = "查找";
            this.toolBarFinder.Visible = false;
            this.toolBarFinder.Click += new System.EventHandler(this.toolBarFinder_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 24);
            this.toolStripSeparator5.Visible = false;
            // 
            // toolBarAddComment
            // 
            this.toolBarAddComment.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBarAddComment.Image = global::DevelopAssistant.Core.Properties.Resources.comment;
            this.toolBarAddComment.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarAddComment.Name = "toolBarAddComment";
            this.toolBarAddComment.Size = new System.Drawing.Size(23, 21);
            this.toolBarAddComment.Text = "添加注释";
            this.toolBarAddComment.ToolTipText = "添加注释";
            this.toolBarAddComment.Visible = false;
            this.toolBarAddComment.Click += new System.EventHandler(this.toolBarAddComment_Click);
            // 
            // toolBarRemoveComment
            // 
            this.toolBarRemoveComment.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBarRemoveComment.Image = global::DevelopAssistant.Core.Properties.Resources.uncomment;
            this.toolBarRemoveComment.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarRemoveComment.Name = "toolBarRemoveComment";
            this.toolBarRemoveComment.Size = new System.Drawing.Size(23, 21);
            this.toolBarRemoveComment.Text = "取消注释";
            this.toolBarRemoveComment.ToolTipText = "取消注释";
            this.toolBarRemoveComment.Visible = false;
            this.toolBarRemoveComment.Click += new System.EventHandler(this.toolBarRemoveComment_Click);
            // 
            // toolBarIndent
            // 
            this.toolBarIndent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBarIndent.Image = global::DevelopAssistant.Core.Properties.Resources.indent;
            this.toolBarIndent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarIndent.Name = "toolBarIndent";
            this.toolBarIndent.Size = new System.Drawing.Size(23, 21);
            this.toolBarIndent.Text = "向右缩进";
            this.toolBarIndent.Visible = false;
            this.toolBarIndent.Click += new System.EventHandler(this.toolBarIndent_Click);
            // 
            // toolBarOutIndent
            // 
            this.toolBarOutIndent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBarOutIndent.Image = global::DevelopAssistant.Core.Properties.Resources.outdent;
            this.toolBarOutIndent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarOutIndent.Name = "toolBarOutIndent";
            this.toolBarOutIndent.Size = new System.Drawing.Size(23, 21);
            this.toolBarOutIndent.Text = "向左缩进";
            this.toolBarOutIndent.Visible = false;
            this.toolBarOutIndent.Click += new System.EventHandler(this.toolBarOutIndent_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 24);
            this.toolStripSeparator7.Visible = false;
            // 
            // toolBarFormat
            // 
            this.toolBarFormat.Image = global::DevelopAssistant.Core.Properties.Resources.document_editing;
            this.toolBarFormat.Name = "toolBarFormat";
            this.toolBarFormat.Size = new System.Drawing.Size(52, 21);
            this.toolBarFormat.Text = "美化";
            this.toolBarFormat.Visible = false;
            this.toolBarFormat.Click += new System.EventHandler(this.toolBarFormat_Click);
            // 
            // toolBarMinum
            // 
            this.toolBarMinum.Image = global::DevelopAssistant.Core.Properties.Resources.sample;
            this.toolBarMinum.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarMinum.Name = "toolBarMinum";
            this.toolBarMinum.Size = new System.Drawing.Size(52, 21);
            this.toolBarMinum.Text = "压缩";
            this.toolBarMinum.Visible = false;
            this.toolBarMinum.Click += new System.EventHandler(this.toolBarMinum_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 24);
            this.toolStripSeparator6.Visible = false;
            // 
            // toolBarRollback
            // 
            this.toolBarRollback.Image = global::DevelopAssistant.Core.Properties.Resources.rollback;
            this.toolBarRollback.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarRollback.Name = "toolBarRollback";
            this.toolBarRollback.Size = new System.Drawing.Size(52, 21);
            this.toolBarRollback.Text = "回滚";
            this.toolBarRollback.Visible = false;
            this.toolBarRollback.Click += new System.EventHandler(this.toolBarRollback_Click);
            // 
            // toolBarCommit
            // 
            this.toolBarCommit.Image = global::DevelopAssistant.Core.Properties.Resources.commit;
            this.toolBarCommit.Name = "toolBarCommit";
            this.toolBarCommit.Size = new System.Drawing.Size(52, 21);
            this.toolBarCommit.Text = "提交";
            this.toolBarCommit.ToolTipText = "提交事务";
            this.toolBarCommit.Visible = false;
            this.toolBarCommit.Click += new System.EventHandler(this.toolBarCommit_Click);
            // 
            // toolBarStop
            // 
            this.toolBarStop.Enabled = false;
            this.toolBarStop.Image = global::DevelopAssistant.Core.Properties.Resources.control_stop;
            this.toolBarStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarStop.Name = "toolBarStop";
            this.toolBarStop.Size = new System.Drawing.Size(52, 21);
            this.toolBarStop.Text = "停止";
            this.toolBarStop.Visible = false;
            this.toolBarStop.Click += new System.EventHandler(this.toolButtonStop_Click);
            // 
            // toolBarExcecute
            // 
            this.toolBarExcecute.Image = global::DevelopAssistant.Core.Properties.Resources.run;
            this.toolBarExcecute.Name = "toolBarExcecute";
            this.toolBarExcecute.Size = new System.Drawing.Size(52, 21);
            this.toolBarExcecute.Text = "执行";
            this.toolBarExcecute.Visible = false;
            this.toolBarExcecute.Click += new System.EventHandler(this.toolBarExecute_Click);
            // 
            // toolBarButtonSeparator2
            // 
            this.toolBarButtonSeparator2.Name = "toolBarButtonSeparator2";
            this.toolBarButtonSeparator2.Size = new System.Drawing.Size(6, 24);
            // 
            // toolBarButtonRestore
            // 
            this.toolBarButtonRestore.Image = global::DevelopAssistant.Core.Properties.Resources.database_yellow;
            this.toolBarButtonRestore.Name = "toolBarButtonRestore";
            this.toolBarButtonRestore.Size = new System.Drawing.Size(52, 21);
            this.toolBarButtonRestore.Text = "还原";
            this.toolBarButtonRestore.ToolTipText = "数据库还原";
            // 
            // toolBarButtonTorestore
            // 
            this.toolBarButtonTorestore.Image = global::DevelopAssistant.Core.Properties.Resources.database_edit;
            this.toolBarButtonTorestore.Name = "toolBarButtonTorestore";
            this.toolBarButtonTorestore.Size = new System.Drawing.Size(52, 21);
            this.toolBarButtonTorestore.Text = "备份";
            this.toolBarButtonTorestore.ToolTipText = "数据库备份";
            // 
            // toolBarLogout
            // 
            this.toolBarLogout.Image = ((System.Drawing.Image)(resources.GetObject("toolBarLogout.Image")));
            this.toolBarLogout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBarLogout.Name = "toolBarLogout";
            this.toolBarLogout.Size = new System.Drawing.Size(52, 21);
            this.toolBarLogout.Text = "注销";
            this.toolBarLogout.Click += new System.EventHandler(this.toolBarLogout_Click);
            // 
            // toolBarExit
            // 
            this.toolBarExit.Image = global::DevelopAssistant.Core.Properties.Resources.delete;
            this.toolBarExit.Name = "toolBarExit";
            this.toolBarExit.Size = new System.Drawing.Size(52, 21);
            this.toolBarExit.Text = "退出";
            this.toolBarExit.Click += new System.EventHandler(this.toolBarButtonExit_Click);
            // 
            // menuItemEdit
            //                      
            this.menuItemEdit.AddIn = null;
            this.menuItemEdit.Index = 0;
            this.menuItemEdit.ItemType = "ToolStripMenuItem";
            this.menuItemEdit.Level = 0;          
            this.menuItemEdit.Name = "编辑ToolStripMenuItem";
            this.menuItemEdit.Position = null;
            this.menuItemEdit.Size = new System.Drawing.Size(44, 21);
            this.menuItemEdit.Text = "&编辑";
            this.menuItemEdit.ToolTipText = "编辑";
            this.menuItemEdit.Visible = true;
            this.menuItemEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
                menuItemFormat,
                menuItemFind,
                menuItemExecute
            });
            // 
            // MainMenu
            // 
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemMenu,
            this.menuItemFunction,
            this.menuItemView,            
            this.menuItemFile,           
            this.menuItemTools,           
            this.menuItemOptions,
            this.menuItemEdit,
            this.menuItemWindow,
            this.menuItemHelp});
            this.MainMenu.Location = new System.Drawing.Point(6, 34);
            this.MainMenu.MdiWindowListItem = this.menuItemWindow;
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Padding = new System.Windows.Forms.Padding(6, 3, 0, 3);
            this.MainMenu.Size = new System.Drawing.Size(1084, 27);
            this.MainMenu.TabIndex = 11;
            // 
            // menuItemMenu
            // 
            this.menuItemMenu.AddIn = null;
            this.menuItemMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.新建文档ToolStripMenuItem,
            this.打开文件ToolStripMenuItem,
            this.设置主题ToolStripMenuItem,
            this.toolStripSeparator4,
            this.生成文档ToolStripMenuItem,
            this.文本保存ToolStripMenuItem,
            this.toolStripSeparator3,
            this.退出ToolStripMenuItem});
            this.menuItemMenu.Index = 0;
            this.menuItemMenu.ItemType = "ToolStripMenuItem";
            this.menuItemMenu.Level = 0;
            this.menuItemMenu.Name = "menuItemMenu";
            this.menuItemMenu.Position = null;
            this.menuItemMenu.Size = new System.Drawing.Size(44, 21);
            this.menuItemMenu.Text = "&菜单";
            // 
            // 新建文档ToolStripMenuItem
            // 
            this.新建文档ToolStripMenuItem.AddIn = null;
            this.新建文档ToolStripMenuItem.Index = 0;
            this.新建文档ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.新建文档ToolStripMenuItem.Level = 0;
            this.新建文档ToolStripMenuItem.Name = "新建文档ToolStripMenuItem";
            this.新建文档ToolStripMenuItem.Position = null;
            this.新建文档ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.新建文档ToolStripMenuItem.Text = "新建文档";
            this.新建文档ToolStripMenuItem.Click += new System.EventHandler(this.toolBarButtonNewWindow_Click);
            // 
            // 打开文件ToolStripMenuItem
            // 
            this.打开文件ToolStripMenuItem.AddIn = null;
            this.打开文件ToolStripMenuItem.Index = 0;
            this.打开文件ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.打开文件ToolStripMenuItem.Level = 0;
            this.打开文件ToolStripMenuItem.Name = "打开文件ToolStripMenuItem";
            this.打开文件ToolStripMenuItem.Position = null;
            this.打开文件ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.打开文件ToolStripMenuItem.Text = "打开文件";
            this.打开文件ToolStripMenuItem.Click += new System.EventHandler(this.打开文件ToolStripMenuItem_Click);
            // 
            // 设置主题ToolStripMenuItem
            // 
            this.设置主题ToolStripMenuItem.AddIn = null;
            this.设置主题ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MacThemeToolStripMenuItem,
            this.WindowsThemeToolStripMenuItem,
            this.BootstrapThemeToolStripMenuItem});
            this.设置主题ToolStripMenuItem.Index = 0;
            this.设置主题ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.设置主题ToolStripMenuItem.Level = 0;
            this.设置主题ToolStripMenuItem.Name = "设置主题ToolStripMenuItem";
            this.设置主题ToolStripMenuItem.Position = null;
            this.设置主题ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.设置主题ToolStripMenuItem.Text = "设置主题";
            // 
            // MacThemeToolStripMenuItem
            // 
            this.MacThemeToolStripMenuItem.AddIn = null;
            this.MacThemeToolStripMenuItem.Checked = true;
            this.MacThemeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MacThemeToolStripMenuItem.Index = 0;
            this.MacThemeToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.MacThemeToolStripMenuItem.Level = 0;
            this.MacThemeToolStripMenuItem.Name = "MacThemeToolStripMenuItem";
            this.MacThemeToolStripMenuItem.Position = null;
            this.MacThemeToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.MacThemeToolStripMenuItem.Text = "Apple 主题";
            this.MacThemeToolStripMenuItem.Click += new System.EventHandler(this.vs2005ThemeToolStripMenuItem_Click);
            // 
            // WindowsThemeToolStripMenuItem
            // 
            this.WindowsThemeToolStripMenuItem.AddIn = null;
            this.WindowsThemeToolStripMenuItem.Index = 0;
            this.WindowsThemeToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.WindowsThemeToolStripMenuItem.Level = 0;
            this.WindowsThemeToolStripMenuItem.Name = "WindowsThemeToolStripMenuItem";
            this.WindowsThemeToolStripMenuItem.Position = null;
            this.WindowsThemeToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.WindowsThemeToolStripMenuItem.Text = "Windows 主题";
            this.WindowsThemeToolStripMenuItem.Click += new System.EventHandler(this.vs2012ThemeToolStripMenuItem_Click);
            // 
            // BootstrapThemeToolStripMenuItem
            // 
            this.BootstrapThemeToolStripMenuItem.AddIn = null;
            this.BootstrapThemeToolStripMenuItem.Index = 0;
            this.BootstrapThemeToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.BootstrapThemeToolStripMenuItem.Level = 0;
            this.BootstrapThemeToolStripMenuItem.Name = "BootstrapThemeToolStripMenuItem";
            this.BootstrapThemeToolStripMenuItem.Position = null;
            this.BootstrapThemeToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.BootstrapThemeToolStripMenuItem.Text = "Bootstrap 主题";
            this.BootstrapThemeToolStripMenuItem.Click += new System.EventHandler(this.setSchema_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(121, 6);
            // 
            // 生成文档ToolStripMenuItem
            // 
            this.生成文档ToolStripMenuItem.AddIn = null;
            this.生成文档ToolStripMenuItem.Index = 0;
            this.生成文档ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.生成文档ToolStripMenuItem.Level = 0;
            this.生成文档ToolStripMenuItem.Name = "生成文档ToolStripMenuItem";
            this.生成文档ToolStripMenuItem.Position = null;
            this.生成文档ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.生成文档ToolStripMenuItem.Text = "生成文档";
            // 
            // 文本保存ToolStripMenuItem
            // 
            this.文本保存ToolStripMenuItem.AddIn = null;
            this.文本保存ToolStripMenuItem.Index = 0;
            this.文本保存ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.文本保存ToolStripMenuItem.Level = 0;
            this.文本保存ToolStripMenuItem.Name = "文本保存ToolStripMenuItem";
            this.文本保存ToolStripMenuItem.Position = null;
            this.文本保存ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.文本保存ToolStripMenuItem.Text = "文本保存";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(121, 6);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.AddIn = null;
            this.退出ToolStripMenuItem.Index = 0;
            this.退出ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.退出ToolStripMenuItem.Level = 0;
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Position = null;
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.toolBarButtonExit_Click);
            // 
            // menuItemFunction
            // 
            this.menuItemFunction.AddIn = null;
            this.menuItemFunction.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.我的待办ToolStripMenuItem});
            this.menuItemFunction.Index = 0;
            this.menuItemFunction.ItemType = "ToolStripMenuItem";
            this.menuItemFunction.Level = 0;
            this.menuItemFunction.Name = "menuItemFunction";
            this.menuItemFunction.Position = null;
            this.menuItemFunction.Size = new System.Drawing.Size(44, 21);
            this.menuItemFunction.Text = "&功能";
            // 
            // 我的待办ToolStripMenuItem
            // 
            this.我的待办ToolStripMenuItem.AddIn = null;
            this.我的待办ToolStripMenuItem.Image = global::DevelopAssistant.Core.Properties.Resources.client_account_template;
            this.我的待办ToolStripMenuItem.Index = 0;
            this.我的待办ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.我的待办ToolStripMenuItem.Level = 0;
            this.我的待办ToolStripMenuItem.Name = "我的待办ToolStripMenuItem";
            this.我的待办ToolStripMenuItem.Position = null;
            this.我的待办ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.我的待办ToolStripMenuItem.Text = "我的待办";
            this.我的待办ToolStripMenuItem.Click += new System.EventHandler(this.我的待办ToolStripMenuItem_Click);
            // 
            // menuItemView
            // 
            this.menuItemView.AddIn = null;
            this.menuItemView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemMaxSize,
            this.menuItemNormalSize,
            this.menuItemDefaultWindow,
            this.toolStripSeparator1,
            this.menuItemClose,
            this.menuItemCloseAll,
            this.menuItemCloseAllButThisOne});
            this.menuItemView.Index = 0;
            this.menuItemView.ItemType = "ToolStripMenuItem";
            this.menuItemView.Level = 0;
            this.menuItemView.MergeIndex = 1;
            this.menuItemView.Name = "menuItemView";
            this.menuItemView.Position = null;
            this.menuItemView.Size = new System.Drawing.Size(44, 21);
            this.menuItemView.Text = "&视图";
            // 
            // menuItemMaxSize
            // 
            this.menuItemMaxSize.AddIn = null;
            this.menuItemMaxSize.Index = 0;
            this.menuItemMaxSize.ItemType = "ToolStripMenuItem";
            this.menuItemMaxSize.Level = 0;
            this.menuItemMaxSize.Name = "menuItemMaxSize";
            this.menuItemMaxSize.Position = null;
            this.menuItemMaxSize.Size = new System.Drawing.Size(148, 22);
            this.menuItemMaxSize.Text = "最大化";
            this.menuItemMaxSize.Click += new System.EventHandler(this.menuItemMaxSize_Click);
            // 
            // menuItemNormalSize
            // 
            this.menuItemNormalSize.AddIn = null;
            this.menuItemNormalSize.Index = 0;
            this.menuItemNormalSize.ItemType = "ToolStripMenuItem";
            this.menuItemNormalSize.Level = 0;
            this.menuItemNormalSize.Name = "menuItemNormalSize";
            this.menuItemNormalSize.Position = null;
            this.menuItemNormalSize.Size = new System.Drawing.Size(148, 22);
            this.menuItemNormalSize.Text = "还原大小";
            this.menuItemNormalSize.Click += new System.EventHandler(this.menuItemNormalSize_Click);
            // 
            // menuItemDefaultWindow
            // 
            this.menuItemDefaultWindow.AddIn = null;
            this.menuItemDefaultWindow.Index = 0;
            this.menuItemDefaultWindow.ItemType = "ToolStripMenuItem";
            this.menuItemDefaultWindow.Level = 0;
            this.menuItemDefaultWindow.Name = "menuItemDefaultWindow";
            this.menuItemDefaultWindow.Position = null;
            this.menuItemDefaultWindow.Size = new System.Drawing.Size(148, 22);
            this.menuItemDefaultWindow.Text = "重置布局";
            this.menuItemDefaultWindow.Click += new System.EventHandler(this.menuItemDefaultWindow_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(145, 6);
            // 
            // menuItemClose
            // 
            this.menuItemClose.AddIn = null;
            this.menuItemClose.Index = 0;
            this.menuItemClose.ItemType = "ToolStripMenuItem";
            this.menuItemClose.Level = 0;
            this.menuItemClose.Name = "menuItemClose";
            this.menuItemClose.Position = null;
            this.menuItemClose.Size = new System.Drawing.Size(148, 22);
            this.menuItemClose.Text = "&关闭";
            this.menuItemClose.Click += new System.EventHandler(this.关闭ToolStripMenuItem_Click);
            // 
            // menuItemCloseAll
            // 
            this.menuItemCloseAll.AddIn = null;
            this.menuItemCloseAll.Index = 0;
            this.menuItemCloseAll.ItemType = "ToolStripMenuItem";
            this.menuItemCloseAll.Level = 0;
            this.menuItemCloseAll.Name = "menuItemCloseAll";
            this.menuItemCloseAll.Position = null;
            this.menuItemCloseAll.Size = new System.Drawing.Size(148, 22);
            this.menuItemCloseAll.Text = "关闭&所有";
            this.menuItemCloseAll.Click += new System.EventHandler(this.关闭所有ToolStripMenuItem_Click);
            // 
            // menuItemCloseAllButThisOne
            // 
            this.menuItemCloseAllButThisOne.AddIn = null;
            this.menuItemCloseAllButThisOne.Index = 0;
            this.menuItemCloseAllButThisOne.ItemType = "ToolStripMenuItem";
            this.menuItemCloseAllButThisOne.Level = 0;
            this.menuItemCloseAllButThisOne.Name = "menuItemCloseAllButThisOne";
            this.menuItemCloseAllButThisOne.Position = null;
            this.menuItemCloseAllButThisOne.Size = new System.Drawing.Size(148, 22);
            this.menuItemCloseAllButThisOne.Text = "关闭其它所有";
            this.menuItemCloseAllButThisOne.Click += new System.EventHandler(this.关闭其它ToolStripMenuItem_Click);
            // 
            // menuItemFile
            // 
            this.menuItemFile.AddIn = null;
            this.menuItemFile.Index = 0;
            this.menuItemFile.ItemType = "ToolStripMenuItem";
            this.menuItemFile.Level = 0;
            this.menuItemFile.Name = "menuItemFile";
            this.menuItemFile.Position = null;
            this.menuItemFile.Size = new System.Drawing.Size(44, 21);
            this.menuItemFile.Text = "&文件";
            this.menuItemFile.Visible = false;
            // 
            // menuItemTools
            // 
            this.menuItemTools.AddIn = null;
            this.menuItemTools.Index = 0;
            this.menuItemTools.ItemType = "ToolStripMenuItem";
            this.menuItemTools.Level = 0;
            this.menuItemTools.MergeIndex = 2;
            this.menuItemTools.Name = "menuItemTools";
            this.menuItemTools.Position = null;
            this.menuItemTools.Size = new System.Drawing.Size(56, 21);
            this.menuItemTools.Text = "&工具箱";
            // 
            // menuItemOptions
            // 
            this.menuItemOptions.AddIn = null;
            this.menuItemOptions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.编辑器设置ToolStripMenuItem,
            this.时间格式设置ToolStripMenuItem});
            this.menuItemOptions.Index = 0;
            this.menuItemOptions.ItemType = "ToolStripMenuItem";
            this.menuItemOptions.Level = 0;
            this.menuItemOptions.Name = "menuItemOptions";
            this.menuItemOptions.Position = null;
            this.menuItemOptions.Size = new System.Drawing.Size(44, 21);
            this.menuItemOptions.Text = "&选项";
            // 
            // 编辑器设置ToolStripMenuItem
            // 
            this.编辑器设置ToolStripMenuItem.AddIn = null;
            this.编辑器设置ToolStripMenuItem.Index = 0;
            this.编辑器设置ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.编辑器设置ToolStripMenuItem.Level = 0;
            this.编辑器设置ToolStripMenuItem.Name = "编辑器设置ToolStripMenuItem";
            this.编辑器设置ToolStripMenuItem.Position = null;
            this.编辑器设置ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.编辑器设置ToolStripMenuItem.Text = "编辑器设置";
            this.编辑器设置ToolStripMenuItem.Click += new System.EventHandler(this.编辑器设置ToolStripMenuItem_Click);
            // 
            // 时间格式设置ToolStripMenuItem
            // 
            this.时间格式设置ToolStripMenuItem.AddIn = null;
            this.时间格式设置ToolStripMenuItem.Index = 0;
            this.时间格式设置ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.时间格式设置ToolStripMenuItem.Level = 0;
            this.时间格式设置ToolStripMenuItem.Name = "时间格式设置ToolStripMenuItem";
            this.时间格式设置ToolStripMenuItem.Position = null;
            this.时间格式设置ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.时间格式设置ToolStripMenuItem.Text = "时间格式设置";
            this.时间格式设置ToolStripMenuItem.Click += new System.EventHandler(this.时间格式设置ToolStripMenuItem_Click);
            // 
            // menuItemWindow
            // 
            this.menuItemWindow.AddIn = null;
            this.menuItemWindow.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemNewWindow});
            this.menuItemWindow.Index = 0;
            this.menuItemWindow.ItemType = "ToolStripMenuItem";
            this.menuItemWindow.Level = 0;
            this.menuItemWindow.MergeIndex = 2;
            this.menuItemWindow.Name = "menuItemWindow";
            this.menuItemWindow.Position = null;
            this.menuItemWindow.Size = new System.Drawing.Size(44, 21);
            this.menuItemWindow.Text = "&窗口";
            // 
            // menuItemNewWindow
            // 
            this.menuItemNewWindow.AddIn = null;
            this.menuItemNewWindow.Index = 0;
            this.menuItemNewWindow.ItemType = "ToolStripMenuItem";
            this.menuItemNewWindow.Level = 0;
            this.menuItemNewWindow.Name = "menuItemNewWindow";
            this.menuItemNewWindow.Position = null;
            this.menuItemNewWindow.Size = new System.Drawing.Size(112, 22);
            this.menuItemNewWindow.Text = "新窗口";
            this.menuItemNewWindow.Click += new System.EventHandler(this.toolBarButtonNewWindow_Click);
            // 
            // menuItemHelp
            // 
            this.menuItemHelp.AddIn = null;
            this.menuItemHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.帮助ToolStripMenuItem,
            this.关于ToolStripMenuItem});
            this.menuItemHelp.Index = 0;
            this.menuItemHelp.ItemType = "ToolStripMenuItem";
            this.menuItemHelp.Level = 0;
            this.menuItemHelp.MergeIndex = 3;
            this.menuItemHelp.Name = "menuItemHelp";
            this.menuItemHelp.Position = null;
            this.menuItemHelp.Size = new System.Drawing.Size(44, 21);
            this.menuItemHelp.Text = "&帮助";
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.AddIn = null;
            this.帮助ToolStripMenuItem.Index = 0;
            this.帮助ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Level = 0;
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Position = null;
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.帮助ToolStripMenuItem.Text = "&帮助";
            this.帮助ToolStripMenuItem.ToolTipText = "使用帮助";
            this.帮助ToolStripMenuItem.Click += new System.EventHandler(this.帮助ToolStripMenuItem_Click);
            // 
            // 关于ToolStripMenuItem
            // 
            this.关于ToolStripMenuItem.AddIn = null;
            this.关于ToolStripMenuItem.Index = 0;
            this.关于ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.关于ToolStripMenuItem.Level = 0;
            this.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem";
            this.关于ToolStripMenuItem.Position = null;
            this.关于ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.关于ToolStripMenuItem.Text = "&关于";
            this.关于ToolStripMenuItem.ToolTipText = "关于软件";
            this.关于ToolStripMenuItem.Click += new System.EventHandler(this.关于ToolStripMenuItem_Click);
            // 
            // contextMenuStrip3
            // 
            this.contextMenuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.连接服务器ToolStripMenuItem,
            this.断开连接ToolStripMenuItem,
            this.删除连接ToolStripMenuItem,
            this.编辑属性ToolStripMenuItem,
            this.新增表ToolStripMenuItem,
            this.打开前100行ToolStripMenuItem,
            this.打开表ToolStripMenuItem,
            this.设计表ToolStripMenuItem,
            this.编辑数据ToolStripMenuItem,
            this.删除表ToolStripMenuItem,
            this.查看表对象ToolStripMenuItem,
            this.生成脚本ToolStripMenuItem,
            this.生成代码ToolStripMenuItem,
            this.刷新ToolStripMenuItem1});
            this.contextMenuStrip3.Name = "contextMenuStrip3";
            this.contextMenuStrip3.Size = new System.Drawing.Size(166, 312);
            // 
            // 连接服务器ToolStripMenuItem
            // 
            this.连接服务器ToolStripMenuItem.AddIn = null;
            this.连接服务器ToolStripMenuItem.Index = 0;
            this.连接服务器ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.连接服务器ToolStripMenuItem.Level = 0;
            this.连接服务器ToolStripMenuItem.Name = "连接服务器ToolStripMenuItem";
            this.连接服务器ToolStripMenuItem.Position = null;
            this.连接服务器ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.连接服务器ToolStripMenuItem.Text = "连接服务器";
            // 
            // 断开连接ToolStripMenuItem
            // 
            this.断开连接ToolStripMenuItem.AddIn = null;
            this.断开连接ToolStripMenuItem.Index = 0;
            this.断开连接ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.断开连接ToolStripMenuItem.Level = 0;
            this.断开连接ToolStripMenuItem.Name = "断开连接ToolStripMenuItem";
            this.断开连接ToolStripMenuItem.Position = null;
            this.断开连接ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.断开连接ToolStripMenuItem.Text = "断开连接";
            // 
            // 删除连接ToolStripMenuItem
            // 
            this.删除连接ToolStripMenuItem.AddIn = null;
            this.删除连接ToolStripMenuItem.Index = 0;
            this.删除连接ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.删除连接ToolStripMenuItem.Level = 0;
            this.删除连接ToolStripMenuItem.Name = "删除连接ToolStripMenuItem";
            this.删除连接ToolStripMenuItem.Position = null;
            this.删除连接ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.删除连接ToolStripMenuItem.Text = "删除连接";
            // 
            // 编辑属性ToolStripMenuItem
            // 
            this.编辑属性ToolStripMenuItem.AddIn = null;
            this.编辑属性ToolStripMenuItem.Index = 0;
            this.编辑属性ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.编辑属性ToolStripMenuItem.Level = 0;
            this.编辑属性ToolStripMenuItem.Name = "编辑属性ToolStripMenuItem";
            this.编辑属性ToolStripMenuItem.Position = null;
            this.编辑属性ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.编辑属性ToolStripMenuItem.Text = "编辑属性";
            // 
            // 新增表ToolStripMenuItem
            // 
            this.新增表ToolStripMenuItem.AddIn = null;
            this.新增表ToolStripMenuItem.Index = 0;
            this.新增表ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.新增表ToolStripMenuItem.Level = 0;
            this.新增表ToolStripMenuItem.Name = "新增表ToolStripMenuItem";
            this.新增表ToolStripMenuItem.Position = null;
            this.新增表ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.新增表ToolStripMenuItem.Text = "新增表";
            // 
            // 打开前100行ToolStripMenuItem
            // 
            this.打开前100行ToolStripMenuItem.AddIn = null;
            this.打开前100行ToolStripMenuItem.Index = 0;
            this.打开前100行ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.打开前100行ToolStripMenuItem.Level = 0;
            this.打开前100行ToolStripMenuItem.Name = "打开前100行ToolStripMenuItem";
            this.打开前100行ToolStripMenuItem.Position = null;
            this.打开前100行ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.打开前100行ToolStripMenuItem.Text = "打开表(前100行)";
            // 
            // 打开表ToolStripMenuItem
            // 
            this.打开表ToolStripMenuItem.AddIn = null;
            this.打开表ToolStripMenuItem.Index = 0;
            this.打开表ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.打开表ToolStripMenuItem.Level = 0;
            this.打开表ToolStripMenuItem.Name = "打开表ToolStripMenuItem";
            this.打开表ToolStripMenuItem.Position = null;
            this.打开表ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.打开表ToolStripMenuItem.Text = "打开表(所有行)";
            // 
            // 设计表ToolStripMenuItem
            // 
            this.设计表ToolStripMenuItem.AddIn = null;
            this.设计表ToolStripMenuItem.Index = 0;
            this.设计表ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.设计表ToolStripMenuItem.Level = 0;
            this.设计表ToolStripMenuItem.Name = "设计表ToolStripMenuItem";
            this.设计表ToolStripMenuItem.Position = null;
            this.设计表ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.设计表ToolStripMenuItem.Text = "设计表";
            // 
            // 编辑数据ToolStripMenuItem
            // 
            this.编辑数据ToolStripMenuItem.AddIn = null;
            this.编辑数据ToolStripMenuItem.Index = 0;
            this.编辑数据ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.编辑数据ToolStripMenuItem.Level = 0;
            this.编辑数据ToolStripMenuItem.Name = "编辑数据ToolStripMenuItem";
            this.编辑数据ToolStripMenuItem.Position = null;
            this.编辑数据ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.编辑数据ToolStripMenuItem.Text = "编辑数据";
            // 
            // 删除表ToolStripMenuItem
            // 
            this.删除表ToolStripMenuItem.AddIn = null;
            this.删除表ToolStripMenuItem.Index = 0;
            this.删除表ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.删除表ToolStripMenuItem.Level = 0;
            this.删除表ToolStripMenuItem.Name = "删除表ToolStripMenuItem";
            this.删除表ToolStripMenuItem.Position = null;
            this.删除表ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.删除表ToolStripMenuItem.Text = "删除表";
            // 
            // 查看表对象ToolStripMenuItem
            // 
            this.查看表对象ToolStripMenuItem.AddIn = null;
            this.查看表对象ToolStripMenuItem.Index = 0;
            this.查看表对象ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.查看表对象ToolStripMenuItem.Level = 0;
            this.查看表对象ToolStripMenuItem.Name = "查看表对象ToolStripMenuItem";
            this.查看表对象ToolStripMenuItem.Position = null;
            this.查看表对象ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.查看表对象ToolStripMenuItem.Text = "查看表对象";
            // 
            // 生成脚本ToolStripMenuItem
            // 
            this.生成脚本ToolStripMenuItem.AddIn = null;
            this.生成脚本ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createSQLToolStripMenuItem,
            this.insertSQLToolStripMenuItem,
            this.updateSQLToolStripMenuItem,
            this.clearSQLToolStripMenuItem});
            this.生成脚本ToolStripMenuItem.Index = 0;
            this.生成脚本ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.生成脚本ToolStripMenuItem.Level = 0;
            this.生成脚本ToolStripMenuItem.Name = "生成脚本ToolStripMenuItem";
            this.生成脚本ToolStripMenuItem.Position = null;
            this.生成脚本ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.生成脚本ToolStripMenuItem.Text = "生成脚本";
            // 
            // createSQLToolStripMenuItem
            // 
            this.createSQLToolStripMenuItem.AddIn = null;
            this.createSQLToolStripMenuItem.Index = 0;
            this.createSQLToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.createSQLToolStripMenuItem.Level = 0;
            this.createSQLToolStripMenuItem.Name = "createSQLToolStripMenuItem";
            this.createSQLToolStripMenuItem.Position = null;
            this.createSQLToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.createSQLToolStripMenuItem.Text = "Create 脚本";
            // 
            // insertSQLToolStripMenuItem
            // 
            this.insertSQLToolStripMenuItem.AddIn = null;
            this.insertSQLToolStripMenuItem.Index = 0;
            this.insertSQLToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.insertSQLToolStripMenuItem.Level = 0;
            this.insertSQLToolStripMenuItem.Name = "insertSQLToolStripMenuItem";
            this.insertSQLToolStripMenuItem.Position = null;
            this.insertSQLToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.insertSQLToolStripMenuItem.Text = "Insert 脚本";
            // 
            // updateSQLToolStripMenuItem
            // 
            this.updateSQLToolStripMenuItem.AddIn = null;
            this.updateSQLToolStripMenuItem.Index = 0;
            this.updateSQLToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.updateSQLToolStripMenuItem.Level = 0;
            this.updateSQLToolStripMenuItem.Name = "updateSQLToolStripMenuItem";
            this.updateSQLToolStripMenuItem.Position = null;
            this.updateSQLToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.updateSQLToolStripMenuItem.Text = "Update 脚本";
            // 
            // clearSQLToolStripMenuItem
            // 
            this.clearSQLToolStripMenuItem.AddIn = null;
            this.clearSQLToolStripMenuItem.Index = 0;
            this.clearSQLToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.clearSQLToolStripMenuItem.Level = 0;
            this.clearSQLToolStripMenuItem.Name = "clearSQLToolStripMenuItem";
            this.clearSQLToolStripMenuItem.Position = null;
            this.clearSQLToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.clearSQLToolStripMenuItem.Text = "Clear 脚本";
            // 
            // 生成代码ToolStripMenuItem
            // 
            this.生成代码ToolStripMenuItem.AddIn = null;
            this.生成代码ToolStripMenuItem.Index = 0;
            this.生成代码ToolStripMenuItem.ItemType = "ToolStripMenuItem";
            this.生成代码ToolStripMenuItem.Level = 0;
            this.生成代码ToolStripMenuItem.Name = "生成代码ToolStripMenuItem";
            this.生成代码ToolStripMenuItem.Position = null;
            this.生成代码ToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.生成代码ToolStripMenuItem.Text = "生成代码(C#)";
            // 
            // 刷新ToolStripMenuItem1
            // 
            this.刷新ToolStripMenuItem1.AddIn = null;
            this.刷新ToolStripMenuItem1.Index = 0;
            this.刷新ToolStripMenuItem1.ItemType = "ToolStripMenuItem";
            this.刷新ToolStripMenuItem1.Level = 0;
            this.刷新ToolStripMenuItem1.Name = "刷新ToolStripMenuItem1";
            this.刷新ToolStripMenuItem1.Position = null;
            this.刷新ToolStripMenuItem1.Size = new System.Drawing.Size(165, 22);
            this.刷新ToolStripMenuItem1.Text = "刷新";
            // 
            // SystemContextMenu
            // 
            this.SystemContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.toolStripMenuItem2,
            this.toolStripMenuItem1});
            this.SystemContextMenu.Name = "contextMenuStrip4";
            this.SystemContextMenu.Size = new System.Drawing.Size(125, 70);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.AddIn = null;
            this.toolStripMenuItem3.Image = global::DevelopAssistant.Core.Properties.Resources.help_16px_easyicon;
            this.toolStripMenuItem3.Index = 0;
            this.toolStripMenuItem3.ItemType = "ToolStripMenuItem";
            this.toolStripMenuItem3.Level = 0;
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Position = null;
            this.toolStripMenuItem3.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem3.Text = "帮助文档";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.帮助ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.AddIn = null;
            this.toolStripMenuItem2.Image = global::DevelopAssistant.Core.Properties.Resources.information_16px_easyicon;
            this.toolStripMenuItem2.Index = 0;
            this.toolStripMenuItem2.ItemType = "ToolStripMenuItem";
            this.toolStripMenuItem2.Level = 0;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Position = null;
            this.toolStripMenuItem2.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem2.Text = "关于软件";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.关于ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.AddIn = null;
            this.toolStripMenuItem1.Image = global::DevelopAssistant.Core.Properties.Resources.close_16px_easyicon;
            this.toolStripMenuItem1.Index = 0;
            this.toolStripMenuItem1.ItemType = "ToolStripMenuItem";
            this.toolStripMenuItem1.Level = 0;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Position = null;
            this.toolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem1.Text = "退出系统";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolBarButtonExit_Click);
            // 
            // menuItemFind
            // 
            this.menuItemFind.AddIn = null;
            this.menuItemFind.Index = 0;
            this.menuItemFind.ItemType = "ToolStripMenuItem";
            this.menuItemFind.Level = 0;
            this.menuItemFind.Name = "menuItemFind";            
            this.menuItemFind.Size = new System.Drawing.Size(100, 22);
            this.menuItemFind.Text = "&查找替换(Ctrl+F)";            
            this.menuItemFind.ToolTipText = "查找替换(Ctrl+F)";
            this.menuItemFind.ShowShortcutKeys = false;
            this.menuItemFind.ShortcutKeys= ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.menuItemFind.Click += new System.EventHandler(this.toolBarFinder_Click);
            // 
            // menuItemExceute
            // 
            this.menuItemExecute.AddIn = null;
            this.menuItemExecute.Index = 0;
            this.menuItemExecute.ItemType = "ToolStripMenuItem";
            this.menuItemExecute.Level = 0;
            this.menuItemExecute.Name = "menuItemFind";           
            this.menuItemExecute.Size = new System.Drawing.Size(100, 22);
            this.menuItemExecute.Text = "&执行命令(Ctrl+F5)";
            this.menuItemExecute.ToolTipText = "执行命令(Ctrl+F5)";
            this.menuItemExecute.ShowShortcutKeys = false;
            this.menuItemExecute.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F5)));
            this.menuItemExecute.Click += new System.EventHandler(this.toolBarExecute_Click);
            // 
            // menuItemFormat
            // 
            this.menuItemFormat.AddIn = null;
            this.menuItemFormat.Index = 0;
            this.menuItemFormat.ItemType = "ToolStripMenuItem";
            this.menuItemFormat.Level = 0;
            this.menuItemFormat.Name = "menuItemFormat";            
            this.menuItemFormat.Size = new System.Drawing.Size(100, 22);
            this.menuItemFormat.Text = "&格式化(Ctrl+D)";
            this.menuItemFormat.ToolTipText = "格式化(Ctrl+D)";
            this.menuItemFormat.ShowShortcutKeys = false;
            this.menuItemFormat.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.menuItemFormat.Click += new System.EventHandler(this.toolBarFormat_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1096, 700);
            this.CloseBoxSize = new System.Drawing.Size(25, 18);
            this.Controls.Add(this.dockPanel);
            this.Controls.Add(this.MainToolBar);
            this.Controls.Add(this.MainMenu);
            this.Controls.Add(this.statusBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "MainForm";
            //this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "凌云开发助手";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.MainToolBar.ResumeLayout(false);
            this.MainToolBar.PerformLayout();
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.contextMenuStrip3.ResumeLayout(false);
            this.SystemContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private ICSharpCode.WinFormsUI.Docking.DockPanel dockPanel;
        private ICSharpCode.WinFormsUI.Controls.NToolStrip MainToolBar;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarButtonToolBox;
        private ICSharpCode.WinFormsUI.Controls.NToolStripSeparator toolBarButtonSeparator1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarButtonFind;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarButtonNewQuery;
        private ICSharpCode.WinFormsUI.Controls.NToolStripSeparator toolBarButtonSeparator2;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarButtonLogger;
        private ICSharpCode.WinFormsUI.Controls.NMenuStrip MainMenu;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem menuItemMenu;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem menuItemClose;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem menuItemCloseAll;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem menuItemCloseAllButThisOne;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem menuItemView;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem menuItemTools;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem menuItemWindow;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem menuItemNewWindow;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem menuItemHelp;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 帮助ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NStatusBar statusBar;
        private ICSharpCode.WinFormsUI.Controls.NContextMenuStrip contextMenuStrip1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 关闭ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 关闭所有ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 关闭其它ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem menuItemMaxSize;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem menuItemNormalSize;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem menuItemDefaultWindow;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarButtonTorestore;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarButtonRestore;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarExit;
        private ICSharpCode.WinFormsUI.Controls.NContextMenuStrip contextMenuStrip2;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 关闭所有ToolStripMenuItem1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarButtonTheme;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarButtonDocument;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem menuItemFile;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem menuItemFunction;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem menuItemEdit;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem menuItemFind;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem menuItemExecute;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem menuItemFormat;
        private System.Windows.Forms.ToolStripStatusLabel SysStatusInfo;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarLogout;

        #endregion
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem MacThemeToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem WindowsThemeToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripSeparator toolStripSeparator1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 退出ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 刷新ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 新建文档ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 打开文件ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 设置主题ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarButtonOpen;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarExcecute;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarFormat;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarUndo;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarCommit;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarRedo;
        private ICSharpCode.WinFormsUI.Controls.NToolStripDropDownButton toolBarServers;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 连接Sql数据库ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 连接sqlite数据库ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripSeparator toolStripSeparator2;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 文本保存ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripSeparator toolStripSeparator4;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 生成文档ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripSeparator toolStripSeparator3;
        private ICSharpCode.WinFormsUI.Controls.NContextMenuStrip contextMenuStrip3;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 连接服务器ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 生成脚本ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarRollback;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarFinder;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 断开连接ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 打开表ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 设计表ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 编辑数据ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 删除表ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem createSQLToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem insertSQLToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem clearSQLToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 生成代码ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 编辑属性ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 刷新ToolStripMenuItem1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem postgresql数据库ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem access数据库ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem mySql数据库ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarStop;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 删除连接ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem updateSQLToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMainMenuItem menuItemOptions;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 编辑器设置ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem BootstrapThemeToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 关于ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 新增表ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 查看表对象ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarButtonSave;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 打开前100行ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NContextMenuStrip SystemContextMenu;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem toolStripMenuItem1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem toolStripMenuItem2;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripStatusLabel SysNewVersion;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarAddComment;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarRemoveComment;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarIndent;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarOutIndent;
        private ICSharpCode.WinFormsUI.Controls.NToolStripSeparator toolStripSeparator5;
        private ICSharpCode.WinFormsUI.Controls.NToolStripSeparator toolStripSeparator6;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarMinum;
        private ICSharpCode.WinFormsUI.Controls.NToolStripSeparator toolStripSeparator7;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 我的待办ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolBarTodo;
        private ICSharpCode.WinFormsUI.Controls.NToolStripMenuItem 时间格式设置ToolStripMenuItem;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolButtonNotepad;        
        private System.Windows.Forms.ToolStripStatusLabel SysTodoInfo;
    }
}