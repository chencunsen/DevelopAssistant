﻿namespace DevelopAssistant.Core.Contents
{
    partial class HomeContent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomeContent));
            this.nScrollingText1 = new ICSharpCode.WinFormsUI.Controls.NScrollingText();
            this.ScrollingTextBox = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.nPanel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.WorkBench = new ICSharpCode.WinFormsUI.Controls.NWorkbench();
            this.ScrollingTextBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // nScrollingText1
            // 
            this.nScrollingText1.BackColor = System.Drawing.SystemColors.Control;
            this.nScrollingText1.BorderColor = System.Drawing.Color.LightGray;
            this.nScrollingText1.Cursor = System.Windows.Forms.Cursors.Default;
            this.nScrollingText1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nScrollingText1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.nScrollingText1.ForegroundBrush = null;
            this.nScrollingText1.Location = new System.Drawing.Point(2, 14);
            this.nScrollingText1.Name = "nScrollingText1";
            this.nScrollingText1.ScrollDirection = ICSharpCode.WinFormsUI.Controls.ScrollDirection.RightToLeft;
            this.nScrollingText1.ScrollText = "Text";
            this.nScrollingText1.ShowBorder = false;
            this.nScrollingText1.Size = new System.Drawing.Size(631, 16);
            this.nScrollingText1.StopScrollOnMouseOver = false;
            this.nScrollingText1.TabIndex = 0;
            this.nScrollingText1.Text = "欢迎使用开发助手";
            this.nScrollingText1.TextScrollDistance = 1;
            this.nScrollingText1.TextScrollSpeed = 25;
            this.nScrollingText1.VerticleTextPosition = ICSharpCode.WinFormsUI.Controls.VerticleTextPosition.Center;
            this.nScrollingText1.Visible = false;
            this.nScrollingText1.Click += new System.EventHandler(this.nScrollingText1_Click);
            // 
            // ScrollingTextBox
            // 
            this.ScrollingTextBox.Controls.Add(this.pictureBox1);
            this.ScrollingTextBox.Controls.Add(this.nScrollingText1);
            this.ScrollingTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ScrollingTextBox.Location = new System.Drawing.Point(0, 323);
            this.ScrollingTextBox.Name = "ScrollingTextBox";
            this.ScrollingTextBox.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ScrollingTextBox.Size = new System.Drawing.Size(635, 30);
            this.ScrollingTextBox.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackgroundImage = global::DevelopAssistant.Core.Properties.Resources.close_16px_easyicon;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Location = new System.Drawing.Point(618, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(12, 12);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // nPanel1
            // 
            this.nPanel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.nPanel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.All;
            this.nPanel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.nPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nPanel1.Location = new System.Drawing.Point(0, 0);
            this.nPanel1.MarginWidth = 0;
            this.nPanel1.Name = "nPanel1";
            this.nPanel1.Padding = new System.Windows.Forms.Padding(4);
            this.nPanel1.Size = new System.Drawing.Size(635, 353);
            this.nPanel1.TabIndex = 2;
            this.nPanel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // WorkBench
            // 
            this.WorkBench.AlarmIcon = null;
            this.WorkBench.BackColor = System.Drawing.SystemColors.Control;
            this.WorkBench.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.WorkBench.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.WorkBench.BottomBlackColor = System.Drawing.Color.Empty;
            this.WorkBench.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WorkBench.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.WorkBench.Icon = global::DevelopAssistant.Core.Properties.Resources.flag_new;
            this.WorkBench.AlarmIcon = global::DevelopAssistant.Core.Properties.Resources.alarm_clock_16px;
            this.WorkBench.Location = new System.Drawing.Point(0, 0);
            this.WorkBench.MarginWidth = 0;
            this.WorkBench.Name = "WorkBench";
            this.WorkBench.Size = new System.Drawing.Size(635, 323);
            this.WorkBench.TabIndex = 5;
            this.WorkBench.ThemeName = null;
            this.WorkBench.Title = "请您关注！";
            this.WorkBench.TodoOpenState = false;
            this.WorkBench.TopBlackColor = System.Drawing.Color.Empty;
            this.WorkBench.WorkbenchList = null;
            // 
            // HomeContent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 353);
            this.Controls.Add(this.WorkBench);
            this.Controls.Add(this.ScrollingTextBox);
            this.Controls.Add(this.nPanel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "HomeContent";
            this.Text = "欢迎登陆";
            this.Load += new System.EventHandler(this.HomeContent_Load);
            this.ScrollingTextBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NScrollingText nScrollingText1;
        private System.Windows.Forms.Panel ScrollingTextBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private ICSharpCode.WinFormsUI.Controls.NPanel nPanel1;
        private ICSharpCode.WinFormsUI.Controls.NWorkbench WorkBench;
    }
}