﻿namespace DevelopAssistant.Core.Tabs
{
    partial class TodoListManagerTab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TodoListManagerTab));
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.comboBoxImportState = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.textBox1 = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnQuery = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.dataGridView1 = new ICSharpCode.WinFormsUI.Controls.NDataGridView();
            this.ColumnCheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColumnId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSummary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnImportState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTimeliness = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAnthor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new ICSharpCode.WinFormsUI.Controls.NToolStrip();
            this.toolStripDeleteButton = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripButtonRemove = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripUpdateButton = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripAddButton = new ICSharpCode.WinFormsUI.Controls.NToolStripButton();
            this.toolStripCheckBoxButton = new ICSharpCode.WinFormsUI.Controls.NToolStripCheckBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.panel3 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.pager1 = new ICSharpCode.WinFormsUI.Controls.NPager();
            this.bindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.comboBoxDeleteState = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pager1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.comboBoxImportState);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 10);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(772, 35);
            this.panel1.TabIndex = 0;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // comboBoxImportState
            // 
            this.comboBoxImportState.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.comboBoxImportState.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxImportState.DropDownButtonWidth = 20;
            this.comboBoxImportState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxImportState.DropDownWidth = 0;
            this.comboBoxImportState.Font = new System.Drawing.Font("宋体", 10F);
            this.comboBoxImportState.FormattingEnabled = true;
            this.comboBoxImportState.ItemHeight = 18;
            this.comboBoxImportState.ItemIcon = null;
            this.comboBoxImportState.Location = new System.Drawing.Point(334, 1);
            this.comboBoxImportState.Name = "comboBoxImportState";
            this.comboBoxImportState.SelectedIndex = -1;
            this.comboBoxImportState.SelectedItem = null;
            this.comboBoxImportState.Size = new System.Drawing.Size(121, 24);
            this.comboBoxImportState.TabIndex = 3;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.ForeColor = System.Drawing.Color.Gray;
            this.textBox1.FousedColor = System.Drawing.Color.Orange;
            this.textBox1.Icon = null;
            this.textBox1.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.textBox1.IsButtonTextBox = false;
            this.textBox1.IsClearTextBox = false;
            this.textBox1.IsPasswordTextBox = false;
            this.textBox1.Location = new System.Drawing.Point(71, 1);
            this.textBox1.MaxLength = 32767;
            this.textBox1.Multiline = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.PasswordChar = '\0';
            this.textBox1.Placeholder = null;
            this.textBox1.ReadOnly = false;
            this.textBox1.Size = new System.Drawing.Size(125, 24);
            this.textBox1.TabIndex = 1;
            this.textBox1.UseSystemPasswordChar = false;
            this.textBox1.XBackColor = System.Drawing.Color.White;
            this.textBox1.XDisableColor = System.Drawing.Color.Empty;
            this.textBox1.XForeColor = System.Drawing.Color.Gray;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "年份：";
            // 
            // btnQuery
            // 
            this.btnQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuery.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnQuery.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnQuery.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnQuery.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnQuery.FouseColor = System.Drawing.Color.White;
            this.btnQuery.Foused = false;
            this.btnQuery.FouseTextColor = System.Drawing.Color.Black;
            this.btnQuery.Icon = null;
            this.btnQuery.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnQuery.Location = new System.Drawing.Point(695, 8);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Radius = 2;
            this.btnQuery.Size = new System.Drawing.Size(75, 28);
            this.btnQuery.TabIndex = 4;
            this.btnQuery.Text = "查询";
            this.btnQuery.UnableIcon = null;
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(230, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "当前状态：";
            // 
            // panel2
            // 
            this.panel2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.panel2.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(10, 45);
            this.panel2.MarginWidth = 0;
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(1);
            this.panel2.Size = new System.Drawing.Size(772, 377);
            this.panel2.TabIndex = 1;
            this.panel2.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 4, 0, 4);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnCheckBox,
            this.ColumnId,
            this.ColumnItemName,
            this.ColumnTitle,
            this.ColumnSummary,
            this.ColumnImportState,
            this.ColumnDateTime,
            this.ColumnTimeliness,
            this.ColumnAnthor});
            this.dataGridView1.DatetimeFormat = "yyyy-MM-dd HH:mm:ss";
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(1, 30);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView1.RowHeadersWidth = 44;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.ShowRowNumber = false;
            this.dataGridView1.Size = new System.Drawing.Size(770, 346);
            this.dataGridView1.TabIndex = 1;
            // 
            // ColumnCheckBox
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.NullValue = false;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.ColumnCheckBox.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColumnCheckBox.HeaderText = "选择";
            this.ColumnCheckBox.Name = "ColumnCheckBox";
            this.ColumnCheckBox.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColumnCheckBox.Width = 50;
            // 
            // ColumnId
            // 
            this.ColumnId.DataPropertyName = "ID";
            this.ColumnId.HeaderText = "编号";
            this.ColumnId.Name = "ColumnId";
            this.ColumnId.ReadOnly = true;
            // 
            // ColumnItemName
            // 
            this.ColumnItemName.DataPropertyName = "Name";
            this.ColumnItemName.HeaderText = "名称";
            this.ColumnItemName.Name = "ColumnItemName";
            this.ColumnItemName.ReadOnly = true;
            // 
            // ColumnTitle
            // 
            this.ColumnTitle.DataPropertyName = "Title";
            this.ColumnTitle.HeaderText = "标题";
            this.ColumnTitle.Name = "ColumnTitle";
            this.ColumnTitle.ReadOnly = true;
            // 
            // ColumnSummary
            // 
            this.ColumnSummary.DataPropertyName = "Summary";
            this.ColumnSummary.HeaderText = "摘要";
            this.ColumnSummary.Name = "ColumnSummary";
            this.ColumnSummary.ReadOnly = true;
            // 
            // ColumnImportState
            // 
            this.ColumnImportState.DataPropertyName = "TimelinessState";
            this.ColumnImportState.HeaderText = "执行状态";
            this.ColumnImportState.Name = "ColumnImportState";
            this.ColumnImportState.ReadOnly = true;
            // 
            // ColumnDateTime
            // 
            this.ColumnDateTime.DataPropertyName = "DateTime";
            this.ColumnDateTime.HeaderText = "预期完成时间";
            this.ColumnDateTime.Name = "ColumnDateTime";
            this.ColumnDateTime.ReadOnly = true;
            this.ColumnDateTime.Width = 140;
            // 
            // ColumnTimeliness
            // 
            this.ColumnTimeliness.DataPropertyName = "Timeliness";
            this.ColumnTimeliness.HeaderText = "时效";
            this.ColumnTimeliness.Name = "ColumnTimeliness";
            this.ColumnTimeliness.ReadOnly = true;
            // 
            // ColumnAnthor
            // 
            this.ColumnAnthor.DataPropertyName = "ResponsiblePerson";
            this.ColumnAnthor.HeaderText = "责任人";
            this.ColumnAnthor.Name = "ColumnAnthor";
            this.ColumnAnthor.ReadOnly = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Font = new System.Drawing.Font("微软雅黑", 10F);            
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDeleteButton,
            this.toolStripButtonRemove,
            this.toolStripUpdateButton,
            this.toolStripAddButton,
            this.toolStripLabel1,
            this.toolStripCheckBoxButton});
            this.toolStrip1.Location = new System.Drawing.Point(1, 1);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(1);
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(770, 29);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDeleteButton
            // 
            this.toolStripDeleteButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripDeleteButton.Image = global::DevelopAssistant.Core.Properties.Resources.delete;
            this.toolStripDeleteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDeleteButton.Name = "toolStripDeleteButton";
            this.toolStripDeleteButton.Size = new System.Drawing.Size(87, 24);
            this.toolStripDeleteButton.Text = "彻底删除";
            this.toolStripDeleteButton.Click += new System.EventHandler(this.toolStripDeleteButton_Click);
            // 
            // toolStripButtonRemove
            // 
            this.toolStripButtonRemove.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonRemove.Image = global::DevelopAssistant.Core.Properties.Resources.remove;
            this.toolStripButtonRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRemove.Name = "toolStripButtonRemove";
            this.toolStripButtonRemove.Size = new System.Drawing.Size(87, 24);
            this.toolStripButtonRemove.Text = "删除事项";
            this.toolStripButtonRemove.Click += new System.EventHandler(this.toolStripButtonRemove_Click);
            // 
            // toolStripUpdateButton
            // 
            this.toolStripUpdateButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripUpdateButton.Image = global::DevelopAssistant.Core.Properties.Resources.edit_Pencil_24px;
            this.toolStripUpdateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripUpdateButton.Name = "toolStripUpdateButton";
            this.toolStripUpdateButton.Size = new System.Drawing.Size(87, 24);
            this.toolStripUpdateButton.Text = "编辑事项";
            this.toolStripUpdateButton.Click += new System.EventHandler(this.toolStripUpdateButton_Click);
            // 
            // toolStripAddButton
            // 
            this.toolStripAddButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripAddButton.Image = global::DevelopAssistant.Core.Properties.Resources.add;
            this.toolStripAddButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAddButton.Name = "toolStripAddButton";
            this.toolStripAddButton.Size = new System.Drawing.Size(87, 24);
            this.toolStripAddButton.Text = "添加事项";
            this.toolStripAddButton.Click += new System.EventHandler(this.toolStripAddButton_Click);
            // 
            // toolStripCheckBoxButton
            // 
            this.toolStripCheckBoxButton.CheckState = false;
            this.toolStripCheckBoxButton.Name = "toolStripCheckBoxButton";
            this.toolStripCheckBoxButton.Size = new System.Drawing.Size(54, 24);
            this.toolStripCheckBoxButton.Text = "全选";
            this.toolStripCheckBoxButton.CheckStateChanged += new System.EventHandler(this.toolStripCheckBoxButton_CheckStateChanged);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(40, 24);
            this.toolStripLabel1.Text = "列表：";
            // 
            // panel3
            // 
            this.panel3.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel3.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.panel3.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel3.Controls.Add(this.pager1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(10, 422);
            this.panel3.MarginWidth = 0;
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 30);
            this.panel3.TabIndex = 2;
            this.panel3.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // pager1
            // 
            this.pager1.Controls.Add(this.bindingNavigator);
            this.pager1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pager1.Location = new System.Drawing.Point(0, 0);
            this.pager1.Margin = new System.Windows.Forms.Padding(4);
            this.pager1.Name = "pager1";
            this.pager1.PageCount = 0;
            this.pager1.PageCurrent = 1;
            this.pager1.Size = new System.Drawing.Size(772, 30);
            this.pager1.TabIndex = 1;
            this.pager1.TotalRecord = 0;
            this.pager1.EventPaging += new ICSharpCode.WinFormsUI.Controls.NPager.EventPagingHandler(this.pager1_EventPaging);
            // 
            // bindingNavigator
            // 
            this.bindingNavigator.AddNewItem = null;
            this.bindingNavigator.BackColor = System.Drawing.SystemColors.Control;
            this.bindingNavigator.CountItem = null;
            this.bindingNavigator.DeleteItem = null;
            this.bindingNavigator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.bindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator.MoveFirstItem = null;
            this.bindingNavigator.MoveLastItem = null;
            this.bindingNavigator.MoveNextItem = null;
            this.bindingNavigator.MovePreviousItem = null;
            this.bindingNavigator.Name = "bindingNavigator";
            this.bindingNavigator.PositionItem = null;
            this.bindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bindingNavigator.Size = new System.Drawing.Size(772, 30);
            this.bindingNavigator.Stretch = true;
            this.bindingNavigator.TabIndex = 0;
            this.bindingNavigator.Text = " ";
            // 
            // comboBoxDeleteState
            // 
            this.comboBoxDeleteState.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.comboBoxDeleteState.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxDeleteState.DropDownButtonWidth = 20;
            this.comboBoxDeleteState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDeleteState.DropDownWidth = 0;
            this.comboBoxDeleteState.Font = new System.Drawing.Font("宋体", 10F);
            this.comboBoxDeleteState.FormattingEnabled = true;
            this.comboBoxDeleteState.ItemHeight = 18;
            this.comboBoxDeleteState.ItemIcon = null;
            this.comboBoxDeleteState.Location = new System.Drawing.Point(549, 11);
            this.comboBoxDeleteState.Name = "comboBoxDeleteState";
            this.comboBoxDeleteState.SelectedIndex = -1;
            this.comboBoxDeleteState.SelectedItem = null;
            this.comboBoxDeleteState.Size = new System.Drawing.Size(121, 24);
            this.comboBoxDeleteState.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(490, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "状态：";
            // 
            // TodoListManagerTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 452);
            this.Controls.Add(this.comboBoxDeleteState);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnQuery);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TodoListManagerTab";
            this.Padding = new System.Windows.Forms.Padding(10, 10, 10, 0);
            this.Text = "待办事项管理";
            this.Load += new System.EventHandler(this.TodoListManagerTab_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.pager1.ResumeLayout(false);
            this.pager1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private ICSharpCode.WinFormsUI.Controls.NButton btnQuery;
        private ICSharpCode.WinFormsUI.Controls.NComboBox comboBoxImportState;
        private System.Windows.Forms.Label label2;
        private ICSharpCode.WinFormsUI.Controls.NTextBox textBox1;
        private System.Windows.Forms.Label label1;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel2;
        private ICSharpCode.WinFormsUI.Controls.NDataGridView dataGridView1;
        private ICSharpCode.WinFormsUI.Controls.NToolStrip toolStrip1;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripDeleteButton;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripAddButton;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel3;
        private ICSharpCode.WinFormsUI.Controls.NPager pager1;
        private System.Windows.Forms.BindingNavigator bindingNavigator;
        private ICSharpCode.WinFormsUI.Controls.NComboBox comboBoxDeleteState;
        private System.Windows.Forms.Label label3;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripButtonRemove;
        private ICSharpCode.WinFormsUI.Controls.NToolStripCheckBox toolStripCheckBoxButton;
        private ICSharpCode.WinFormsUI.Controls.NToolStripButton toolStripUpdateButton;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnCheckBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSummary;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnImportState;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTimeliness;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAnthor;
    }
}