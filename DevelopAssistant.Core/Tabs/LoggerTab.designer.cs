﻿namespace DevelopAssistant.Core.Tabs
{
    partial class LoggerTab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoggerTab));
            this.LoggerContent = new ICSharpCode.TextEditor.TextEditorControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblNowDate = new System.Windows.Forms.Label();
            this.combDateTimes = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.panel2 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // LoggerContent
            // 
            this.LoggerContent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.LoggerContent.BookMarkEnableToggle = true;
            this.LoggerContent.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.LoggerContent.ComparisonState = false;
            this.LoggerContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LoggerContent.ImageLayout = System.Windows.Forms.ImageLayout.None;
            this.LoggerContent.IsReadOnly = false;
            this.LoggerContent.Location = new System.Drawing.Point(0, 0);
            this.LoggerContent.Name = "LoggerContent";
            this.LoggerContent.ShowGuidelines = false;
            this.LoggerContent.Size = new System.Drawing.Size(751, 508);
            this.LoggerContent.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.LoggerContent);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(751, 508);
            this.panel1.TabIndex = 1;
            // 
            // lblNowDate
            // 
            this.lblNowDate.AutoSize = true;
            this.lblNowDate.Location = new System.Drawing.Point(3, 9);
            this.lblNowDate.Name = "lblNowDate";
            this.lblNowDate.Size = new System.Drawing.Size(53, 18);
            this.lblNowDate.TabIndex = 2;
            this.lblNowDate.Text = "当前:";
            // 
            // combDateTimes
            // 
            this.combDateTimes.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.combDateTimes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.combDateTimes.DropDownButtonWidth = 20;
            this.combDateTimes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combDateTimes.DropDownWidth = 0;
            this.combDateTimes.Font = new System.Drawing.Font("宋体", 9F);
            this.combDateTimes.FormattingEnabled = true;
            this.combDateTimes.ItemHeight = 18;
            this.combDateTimes.ItemIcon = null;
            this.combDateTimes.Location = new System.Drawing.Point(606, 4);
            this.combDateTimes.Name = "combDateTimes";
            this.combDateTimes.SelectedIndex = -1;
            this.combDateTimes.SelectedItem = null;
            this.combDateTimes.Size = new System.Drawing.Size(142, 22);
            this.combDateTimes.TabIndex = 1;
            this.combDateTimes.SelectedIndexChanged += new System.EventHandler(this.combDateTimes_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Bottom;
            this.panel2.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel2.Controls.Add(this.combDateTimes);
            this.panel2.Controls.Add(this.lblNowDate);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.MarginWidth = 0;
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(751, 28);
            this.panel2.TabIndex = 2;
            this.panel2.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // LoggerTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 536);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoggerTab";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "系统日志";
            this.Load += new System.EventHandler(this.LoggerForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.TextEditor.TextEditorControl LoggerContent;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblNowDate;
        private ICSharpCode.WinFormsUI.Controls.NComboBox combDateTimes;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel2;
    }
}