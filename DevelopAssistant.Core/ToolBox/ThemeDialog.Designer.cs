﻿namespace DevelopAssistant.Core.ToolBox
{
    partial class ThemeDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThemeDialog));
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.btnCancel = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.btnApply = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.groupBox1 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.themeMore = new ICSharpCode.WinFormsUI.Controls.NTheme();
            this.themeApple = new ICSharpCode.WinFormsUI.Controls.NTheme();
            this.themeBootstrip = new ICSharpCode.WinFormsUI.Controls.NTheme();
            this.themeWindows = new ICSharpCode.WinFormsUI.Controls.NTheme();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnApply);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(6, 225);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(350, 61);
            this.panel1.TabIndex = 12;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnCancel.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnCancel.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnCancel.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnCancel.FouseColor = System.Drawing.Color.White;
            this.btnCancel.Foused = false;
            this.btnCancel.FouseTextColor = System.Drawing.Color.Black;
            this.btnCancel.Icon = null;
            this.btnCancel.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(251, 13);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Radius = 0;
            this.btnCancel.Size = new System.Drawing.Size(88, 32);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "取消";
            this.btnCancel.UnableIcon = null;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApply.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnApply.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnApply.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnApply.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnApply.FouseColor = System.Drawing.Color.White;
            this.btnApply.Foused = false;
            this.btnApply.FouseTextColor = System.Drawing.Color.Black;
            this.btnApply.Icon = null;
            this.btnApply.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnApply.Location = new System.Drawing.Point(145, 13);
            this.btnApply.Name = "btnApply";
            this.btnApply.Radius = 0;
            this.btnApply.Size = new System.Drawing.Size(88, 32);
            this.btnApply.TabIndex = 0;
            this.btnApply.Text = "确定";
            this.btnApply.UnableIcon = null;
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox1.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox1.Controls.Add(this.themeMore);
            this.groupBox1.Controls.Add(this.themeApple);
            this.groupBox1.Controls.Add(this.themeBootstrip);
            this.groupBox1.Controls.Add(this.themeWindows);
            this.groupBox1.GridLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(189)))), ((int)(((byte)(189)))));
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.MarginWidth = 0;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(343, 185);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Title = "";
            this.groupBox1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // themeMore
            // 
            this.themeMore.BackgroundImage = global::DevelopAssistant.Core.Properties.Resources.more_theme;
            this.themeMore.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.themeMore.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.themeMore.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.All;
            this.themeMore.BottomBlackColor = System.Drawing.Color.Empty;
            this.themeMore.Checked = false;
            this.themeMore.Location = new System.Drawing.Point(179, 95);
            this.themeMore.MarginWidth = 0;
            this.themeMore.Name = "themeMore";
            this.themeMore.Size = new System.Drawing.Size(146, 64);
            this.themeMore.TabIndex = 3;
            this.themeMore.TopBlackColor = System.Drawing.Color.Empty;
            this.themeMore.Click += new System.EventHandler(this.pnlTheme_Click);
            // 
            // themeApple
            // 
            this.themeApple.BackgroundImage = global::DevelopAssistant.Core.Properties.Resources.Apple;
            this.themeApple.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.themeApple.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.themeApple.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.All;
            this.themeApple.BottomBlackColor = System.Drawing.Color.Empty;
            this.themeApple.Checked = false;
            this.themeApple.Location = new System.Drawing.Point(179, 21);
            this.themeApple.MarginWidth = 0;
            this.themeApple.Name = "themeApple";
            this.themeApple.Size = new System.Drawing.Size(146, 64);
            this.themeApple.TabIndex = 2;
            this.themeApple.TopBlackColor = System.Drawing.Color.Empty;
            this.themeApple.Click += new System.EventHandler(this.pnlTheme_Click);
            // 
            // themeBootstrip
            // 
            this.themeBootstrip.BackgroundImage = global::DevelopAssistant.Core.Properties.Resources.bootstrap;
            this.themeBootstrip.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.themeBootstrip.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.themeBootstrip.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.All;
            this.themeBootstrip.BottomBlackColor = System.Drawing.Color.Empty;
            this.themeBootstrip.Checked = false;
            this.themeBootstrip.Location = new System.Drawing.Point(18, 95);
            this.themeBootstrip.MarginWidth = 0;
            this.themeBootstrip.Name = "themeBootstrip";
            this.themeBootstrip.Size = new System.Drawing.Size(146, 64);
            this.themeBootstrip.TabIndex = 1;
            this.themeBootstrip.TopBlackColor = System.Drawing.Color.Empty;
            this.themeBootstrip.Click += new System.EventHandler(this.pnlTheme_Click);
            // 
            // themeWindows
            // 
            this.themeWindows.BackgroundImage = global::DevelopAssistant.Core.Properties.Resources.Windows;
            this.themeWindows.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.themeWindows.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.themeWindows.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.All;
            this.themeWindows.BottomBlackColor = System.Drawing.Color.Empty;
            this.themeWindows.Checked = false;
            this.themeWindows.Location = new System.Drawing.Point(18, 21);
            this.themeWindows.MarginWidth = 0;
            this.themeWindows.Name = "themeWindows";
            this.themeWindows.Size = new System.Drawing.Size(146, 64);
            this.themeWindows.TabIndex = 0;
            this.themeWindows.TopBlackColor = System.Drawing.Color.Empty;
            this.themeWindows.Click += new System.EventHandler(this.pnlTheme_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Location = new System.Drawing.Point(6, 34);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(350, 191);
            this.panel2.TabIndex = 14;
            // 
            // ThemeDialog
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(362, 292);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "ThemeDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "选择主题";
            this.Load += new System.EventHandler(this.ThemeDialog_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private ICSharpCode.WinFormsUI.Controls.NButton btnCancel;
        private ICSharpCode.WinFormsUI.Controls.NButton btnApply;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox1;
        private ICSharpCode.WinFormsUI.Controls.NTheme themeApple;
        private ICSharpCode.WinFormsUI.Controls.NTheme themeBootstrip;
        private ICSharpCode.WinFormsUI.Controls.NTheme themeWindows;
        private ICSharpCode.WinFormsUI.Controls.NTheme themeMore;
        private System.Windows.Forms.Panel panel2;
    }
}