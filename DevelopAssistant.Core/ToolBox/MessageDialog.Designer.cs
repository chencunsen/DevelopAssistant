﻿namespace DevelopAssistant.Core.ToolBox
{
    partial class MessageDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageDialog));
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.btnApplyOk = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.btnCancle = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.AlertPanel = new System.Windows.Forms.Panel();
            this.MessagePanel = new System.Windows.Forms.Panel();
            this.MessageIcon = new System.Windows.Forms.PictureBox();
            this.MessageText = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.AlertPanel.SuspendLayout();
            this.MessagePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MessageIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.btnApplyOk);
            this.panel1.Controls.Add(this.btnCancle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(6, 103);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(331, 56);
            this.panel1.TabIndex = 1;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // btnApplyOk
            // 
            this.btnApplyOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplyOk.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnApplyOk.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnApplyOk.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnApplyOk.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnApplyOk.FouseColor = System.Drawing.Color.White;
            this.btnApplyOk.Foused = false;
            this.btnApplyOk.FouseTextColor = System.Drawing.Color.Black;
            this.btnApplyOk.Icon = null;
            this.btnApplyOk.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnApplyOk.Location = new System.Drawing.Point(147, 7);
            this.btnApplyOk.Name = "btnApplyOk";
            this.btnApplyOk.Radius = 0;
            this.btnApplyOk.Size = new System.Drawing.Size(75, 36);
            this.btnApplyOk.TabIndex = 1;
            this.btnApplyOk.Text = "确定";
            this.btnApplyOk.UnableIcon = null;
            this.btnApplyOk.UseVisualStyleBackColor = true;
            this.btnApplyOk.Click += new System.EventHandler(this.btnApplyOk_Click);
            // 
            // btnCancle
            // 
            this.btnCancle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancle.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnCancle.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnCancle.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnCancle.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnCancle.FouseColor = System.Drawing.Color.White;
            this.btnCancle.Foused = false;
            this.btnCancle.FouseTextColor = System.Drawing.Color.Black;
            this.btnCancle.Icon = null;
            this.btnCancle.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnCancle.Location = new System.Drawing.Point(236, 7);
            this.btnCancle.Name = "btnCancle";
            this.btnCancle.Radius = 0;
            this.btnCancle.Size = new System.Drawing.Size(75, 36);
            this.btnCancle.TabIndex = 0;
            this.btnCancle.Text = "取消";
            this.btnCancle.UnableIcon = null;
            this.btnCancle.UseVisualStyleBackColor = true;
            this.btnCancle.Click += new System.EventHandler(this.btnCancle_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(25, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "提示信息：";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(96, 20);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(184, 24);
            this.textBox1.TabIndex = 3;
            // 
            // AlertPanel
            // 
            this.AlertPanel.BackColor = System.Drawing.Color.Transparent;
            this.AlertPanel.Controls.Add(this.label1);
            this.AlertPanel.Controls.Add(this.textBox1);
            this.AlertPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AlertPanel.Location = new System.Drawing.Point(19, 45);
            this.AlertPanel.Name = "AlertPanel";
            this.AlertPanel.Size = new System.Drawing.Size(304, 48);
            this.AlertPanel.TabIndex = 4;
            // 
            // MessagePanel
            // 
            this.MessagePanel.Controls.Add(this.MessageIcon);
            this.MessagePanel.Controls.Add(this.MessageText);
            this.MessagePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MessagePanel.Location = new System.Drawing.Point(6, 34);
            this.MessagePanel.Name = "MessagePanel";
            this.MessagePanel.Size = new System.Drawing.Size(331, 69);
            this.MessagePanel.TabIndex = 4;
            // 
            // MessageIcon
            // 
            this.MessageIcon.Image = global::DevelopAssistant.Core.Properties.Resources.question_32px;
            this.MessageIcon.Location = new System.Drawing.Point(41, 18);
            this.MessageIcon.Name = "MessageIcon";
            this.MessageIcon.Size = new System.Drawing.Size(32, 32);
            this.MessageIcon.TabIndex = 0;
            this.MessageIcon.TabStop = false;
            // 
            // MessageText
            // 
            this.MessageText.Location = new System.Drawing.Point(85, 18);
            this.MessageText.Name = "MessageText";
            this.MessageText.Size = new System.Drawing.Size(215, 32);
            this.MessageText.TabIndex = 1;
            this.MessageText.Text = "提示信息";
            this.MessageText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MessageDialog
            // 
            this.ClientSize = new System.Drawing.Size(343, 165);
            this.Controls.Add(this.MessagePanel);
            this.Controls.Add(this.AlertPanel);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MessageDialog";
            this.Resizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "提示信息";
            this.Load += new System.EventHandler(this.MessageDialog_Load);
            this.panel1.ResumeLayout(false);
            this.AlertPanel.ResumeLayout(false);
            this.AlertPanel.PerformLayout();
            this.MessagePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MessageIcon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private ICSharpCode.WinFormsUI.Controls.NButton btnApplyOk;
        private ICSharpCode.WinFormsUI.Controls.NButton btnCancle;
        private System.Windows.Forms.Label label1;
        private ICSharpCode.WinFormsUI.Controls.NTextBox textBox1;
        private System.Windows.Forms.Panel AlertPanel;
        private System.Windows.Forms.Panel MessagePanel;
        private System.Windows.Forms.Label MessageText;
        private System.Windows.Forms.PictureBox MessageIcon;
    }
}