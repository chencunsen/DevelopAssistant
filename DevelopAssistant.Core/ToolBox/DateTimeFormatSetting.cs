﻿using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class DateTimeFormatSetting : BaseForm
    {
        public DateTimeFormatSetting()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            this.XTheme = AppSettings.WindowTheme;
            this.panel1.BorderColor = this.XTheme.FormBorderOutterColor;
        }

        private void DateTimeFormatSetting_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new EventArgs());
            this.textBox1.Text = AppSettings.DateTimeFormat;
        }

        private void btnApplyOk_Click(object sender, EventArgs e)
        {
            AppSettings.DateTimeFormat = this.textBox1.Text.Trim();
            this.Close();
        }

        private void btnCancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color linkColor = System.Drawing.Color.Blue;
            Color toolBackColor = SystemColors.Control;
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    linkColor = System.Drawing.Color.Blue;
                    toolBackColor = SystemColors.Control;
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(038, 038, 038);
                    linkColor = Color.FromArgb(051, 153, 153);
                    toolBackColor = Color.FromArgb(045, 045, 048);
                    break;
            }

            this.label1.ForeColor = foreColor;
            this.panel1.BackColor = toolBackColor;
            this.panel2.BackColor = backColor;
            this.groupBox1.BackColor = backColor;
            this.textBox1.XForeColor = foreColor;
            this.textBox1.XBackColor = backColor;
            this.btnApplyOk.ForeColor = foreColor;
            this.btnApplyOk.BackColor = toolBackColor;
            this.btnCancle.ForeColor = foreColor;
            this.btnCancle.BackColor = toolBackColor;

        }
    }
}
