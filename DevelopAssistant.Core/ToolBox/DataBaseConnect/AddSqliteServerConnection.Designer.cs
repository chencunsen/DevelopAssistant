﻿namespace DevelopAssistant.Core.ToolBox
{
    partial class AddSqliteServerConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddSqliteServerConnection));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtPassword = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtUserID = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtConnectionName = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.txtDataFilePath = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.chkEfficient = new System.Windows.Forms.CheckBox();
            this.tester = new System.Windows.Forms.Label();
            this.selector = new System.Windows.Forms.Label();
            this.chkLoginStyle = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(27, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "连接名称：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(27, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "文件路径：";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox1.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.txtPassword);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtUserID);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Enabled = false;
            this.groupBox1.GridLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(189)))), ((int)(((byte)(189)))));
            this.groupBox1.Location = new System.Drawing.Point(15, 94);
            this.groupBox1.MarginWidth = 0;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(296, 116);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Title = "";
            this.groupBox1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(240, 69);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(48, 16);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "加密";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPassword.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtPassword.FousedColor = System.Drawing.Color.Orange;
            this.txtPassword.Icon = null;
            this.txtPassword.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtPassword.IsButtonTextBox = false;
            this.txtPassword.IsClearTextBox = false;
            this.txtPassword.IsPasswordTextBox = false;
            this.txtPassword.Location = new System.Drawing.Point(79, 65);
            this.txtPassword.MaxLength = 32767;
            this.txtPassword.Multiline = false;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Placeholder = null;
            this.txtPassword.ReadOnly = false;
            this.txtPassword.Size = new System.Drawing.Size(155, 24);
            this.txtPassword.TabIndex = 3;
            this.txtPassword.UseSystemPasswordChar = false;
            this.txtPassword.XBackColor = System.Drawing.SystemColors.Window;
            this.txtPassword.XDisableColor = System.Drawing.Color.Empty;
            this.txtPassword.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "密码：";
            // 
            // txtUserID
            // 
            this.txtUserID.BackColor = System.Drawing.SystemColors.Window;
            this.txtUserID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUserID.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtUserID.FousedColor = System.Drawing.Color.Orange;
            this.txtUserID.Icon = null;
            this.txtUserID.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtUserID.IsButtonTextBox = false;
            this.txtUserID.IsClearTextBox = false;
            this.txtUserID.IsPasswordTextBox = false;
            this.txtUserID.Location = new System.Drawing.Point(79, 31);
            this.txtUserID.MaxLength = 32767;
            this.txtUserID.Multiline = false;
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.PasswordChar = '\0';
            this.txtUserID.Placeholder = null;
            this.txtUserID.ReadOnly = false;
            this.txtUserID.Size = new System.Drawing.Size(127, 24);
            this.txtUserID.TabIndex = 1;
            this.txtUserID.UseSystemPasswordChar = false;
            this.txtUserID.XBackColor = System.Drawing.SystemColors.Window;
            this.txtUserID.XDisableColor = System.Drawing.Color.Empty;
            this.txtUserID.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "用户名：";
            // 
            // txtConnectionName
            // 
            this.txtConnectionName.BackColor = System.Drawing.SystemColors.Window;
            this.txtConnectionName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtConnectionName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtConnectionName.FousedColor = System.Drawing.Color.Orange;
            this.txtConnectionName.Icon = null;
            this.txtConnectionName.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtConnectionName.IsButtonTextBox = false;
            this.txtConnectionName.IsClearTextBox = false;
            this.txtConnectionName.IsPasswordTextBox = false;
            this.txtConnectionName.Location = new System.Drawing.Point(95, 18);
            this.txtConnectionName.MaxLength = 32767;
            this.txtConnectionName.Multiline = false;
            this.txtConnectionName.Name = "txtConnectionName";
            this.txtConnectionName.PasswordChar = '\0';
            this.txtConnectionName.Placeholder = null;
            this.txtConnectionName.ReadOnly = false;
            this.txtConnectionName.Size = new System.Drawing.Size(169, 24);
            this.txtConnectionName.TabIndex = 4;
            this.txtConnectionName.UseSystemPasswordChar = false;
            this.txtConnectionName.XBackColor = System.Drawing.SystemColors.Window;
            this.txtConnectionName.XDisableColor = System.Drawing.Color.Empty;
            this.txtConnectionName.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // txtDataFilePath
            // 
            this.txtDataFilePath.BackColor = System.Drawing.SystemColors.Window;
            this.txtDataFilePath.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDataFilePath.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtDataFilePath.FousedColor = System.Drawing.Color.Orange;
            this.txtDataFilePath.Icon = null;
            this.txtDataFilePath.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtDataFilePath.IsButtonTextBox = false;
            this.txtDataFilePath.IsClearTextBox = false;
            this.txtDataFilePath.IsPasswordTextBox = false;
            this.txtDataFilePath.Location = new System.Drawing.Point(95, 54);
            this.txtDataFilePath.MaxLength = 32767;
            this.txtDataFilePath.Multiline = false;
            this.txtDataFilePath.Name = "txtDataFilePath";
            this.txtDataFilePath.PasswordChar = '\0';
            this.txtDataFilePath.Placeholder = null;
            this.txtDataFilePath.ReadOnly = true;
            this.txtDataFilePath.Size = new System.Drawing.Size(169, 24);
            this.txtDataFilePath.TabIndex = 5;
            this.txtDataFilePath.UseSystemPasswordChar = false;
            this.txtDataFilePath.XBackColor = System.Drawing.SystemColors.Window;
            this.txtDataFilePath.XDisableColor = System.Drawing.Color.Empty;
            this.txtDataFilePath.XForeColor = System.Drawing.SystemColors.ControlText;
            this.txtDataFilePath.Click += new System.EventHandler(this.selector_Click);
            // 
            // chkEfficient
            // 
            this.chkEfficient.AutoSize = true;
            this.chkEfficient.BackColor = System.Drawing.Color.Transparent;
            this.chkEfficient.Location = new System.Drawing.Point(100, 225);
            this.chkEfficient.Name = "chkEfficient";
            this.chkEfficient.Size = new System.Drawing.Size(96, 16);
            this.chkEfficient.TabIndex = 6;
            this.chkEfficient.Text = "高效连接模式";
            this.chkEfficient.UseVisualStyleBackColor = false;
            // 
            // tester
            // 
            this.tester.AutoSize = true;
            this.tester.BackColor = System.Drawing.Color.Transparent;
            this.tester.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tester.ForeColor = System.Drawing.Color.Blue;
            this.tester.Location = new System.Drawing.Point(202, 226);
            this.tester.Name = "tester";
            this.tester.Size = new System.Drawing.Size(53, 12);
            this.tester.TabIndex = 7;
            this.tester.Text = "测试连接";
            this.tester.Click += new System.EventHandler(this.tester_Click);
            // 
            // selector
            // 
            this.selector.AutoSize = true;
            this.selector.BackColor = System.Drawing.Color.Transparent;
            this.selector.Cursor = System.Windows.Forms.Cursors.Hand;
            this.selector.ForeColor = System.Drawing.Color.Blue;
            this.selector.Location = new System.Drawing.Point(270, 60);
            this.selector.Name = "selector";
            this.selector.Size = new System.Drawing.Size(47, 12);
            this.selector.TabIndex = 8;
            this.selector.Text = "选择...";
            this.selector.Click += new System.EventHandler(this.selector_Click);
            // 
            // chkLoginStyle
            // 
            this.chkLoginStyle.AutoSize = true;
            this.chkLoginStyle.BackColor = System.Drawing.Color.Transparent;
            this.chkLoginStyle.Checked = true;
            this.chkLoginStyle.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLoginStyle.Location = new System.Drawing.Point(94, 92);
            this.chkLoginStyle.Name = "chkLoginStyle";
            this.chkLoginStyle.Size = new System.Drawing.Size(96, 16);
            this.chkLoginStyle.TabIndex = 9;
            this.chkLoginStyle.Text = "集成身份登陆";
            this.chkLoginStyle.UseVisualStyleBackColor = false;
            this.chkLoginStyle.Click += new System.EventHandler(this.chkLoginStyle_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.chkLoginStyle);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.selector);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.tester);
            this.panel2.Controls.Add(this.chkEfficient);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtDataFilePath);
            this.panel2.Controls.Add(this.txtConnectionName);
            this.panel2.Location = new System.Drawing.Point(6, 34);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(327, 263);
            this.panel2.TabIndex = 10;
            // 
            // AddSqliteServerConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 363);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(1171, 582);
            this.MinimumSize = new System.Drawing.Size(187, 75);
            this.Name = "AddSqliteServerConnection";
            this.Text = "Sqlite 数据库连接";
            this.Load += new System.EventHandler(this.AddSqliteServerConnection_Load);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox1;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtPassword;
        private System.Windows.Forms.Label label5;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtUserID;
        private System.Windows.Forms.Label label4;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtConnectionName;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtDataFilePath;
        private System.Windows.Forms.CheckBox chkEfficient;
        private System.Windows.Forms.Label tester;
        private System.Windows.Forms.Label selector;
        private System.Windows.Forms.CheckBox chkLoginStyle;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Panel panel2;
    }
}