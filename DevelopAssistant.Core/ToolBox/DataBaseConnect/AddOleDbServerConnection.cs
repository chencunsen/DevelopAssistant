﻿using DevelopAssistant.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class AddOleDbServerConnection : AddDataBaseConnection
    {
        OpenFileDialog OpenFile = new OpenFileDialog();

        public AddOleDbServerConnection()
        {
            InitializeComponent();
            InitializeControls();
        }

        public AddOleDbServerConnection(MainForm OwnerForm)
            : this()
        {          
            this.Apply = new EventHandler(Apply_Click); 
        }

        private void InitializeControls()
        {
            OpenFile.Filter = "access或者SQLite|*.accdb;*.mdb";
            SetBtnApplyEnabled(false);
        }

        private void Apply_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtConnectionName.Text))
            {
                MessageBox.Show("数据库连接名称不能为空");
                return;
            }

            string _connectionString = string.Empty;
            _connectionString += "Provider=Microsoft.Ace.OleDb.12.0;Data Source=" + txtDataFilePath.Tag + ";";
            _connectionString += "Persist Security Info=False;";

            //Provider=Microsoft.Ace.OleDb.12.0;Data Source= C:\Users\wxdongtt2007\Desktop\UPS_科华_KR33500.mdb;Persist Security Info=False;

            if (!chkLoginStyle.Checked)
            {
                if (checkBox1.Checked)
                    _connectionString += "Password=" + NORM.Common.EncryptDES.EncryptDESM(txtPassword.Text) + ";";//4HxbajmTkLY=
                else
                    _connectionString += "Password=" + txtPassword.Text + ";";
            }
            //_connectionString += "UseUTF16Encoding=True;";

            ConnectionString = _connectionString;
            ProviderName = "System.Data.Sqlite";
            ConnectionName = txtConnectionName.Text;
            AllObjectLoad = !chkEfficient.Checked;
        }

        private void selector_Click(object sender, EventArgs e)
        {
            if (this.OpenFile.ShowDialog().Equals(DialogResult.OK))
            {
                if (!string.IsNullOrEmpty(this.OpenFile.FileName))
                {
                    txtDataFilePath.Tag = OpenFile.FileName;
                    string file = OpenFile.FileName;
                    file = file.Substring(file.LastIndexOf("\\"));
                    txtDataFilePath.Text = "..." + file;
                }
            }
        }

        private void tester_Click(object sender, EventArgs e)
        {

            string _connectionString = string.Empty;
            _connectionString += "Provider=Microsoft.Ace.OleDb.12.0;Data Source=" + txtDataFilePath.Tag + ";";
            _connectionString += "Persist Security Info=False;";

            //Provider=Microsoft.Ace.OleDb.12.0;Data Source= C:\Users\wxdongtt2007\Desktop\UPS_科华_KR33500.mdb;Persist Security Info=False;
            
            if (!chkLoginStyle.Checked)
            {
                if (checkBox1.Checked)
                    _connectionString += "Password=" + NORM.Common.EncryptDES.EncryptDESM(txtPassword.Text) + ";";//4HxbajmTkLY=
                else
                    _connectionString += "Password=" + txtPassword.Text + ";";
            }
            //_connectionString += "UseUTF16Encoding=True;";

            ConnectionString = _connectionString;
            ProviderName = "System.Data.OleDb";
            DataBaseServer dbserver = new DataBaseServer(ConnectionString, ProviderName);

            using (var db = Utility.GetAdohelper(dbserver))
            {
                string messageText = string.Empty;
                if (db.TestConnect(out messageText))
                    SetBtnApplyEnabled(true);
                else
                    MessageBox.Show(messageText);
            }           
        }

        private void chkLoginStyle_Click(object sender, EventArgs e)
        {
            if (chkLoginStyle.Checked)
            {
                groupBox1.Enabled = false;                
            }
            else
            {
                groupBox1.Enabled = true;                 
            }
            txtUserID.Enabled = groupBox1.Enabled;
            txtPassword.Enabled = groupBox1.Enabled;
        }

        protected override void OnThemeChanged(ThemeEventArgs e)
        {
            base.OnThemeChanged(e);

            string themeName = e.themeName;

            Color linkColor = Color.Blue;
            Color textBackColor = SystemColors.Window;
            Color disableColor = SystemColors.Control;
            switch (themeName)
            {
                case "Default":
                    linkColor = Color.Blue;
                    textBackColor = SystemColors.Window;
                    disableColor = SystemColors.Control;
                    break;
                case "Black":
                    linkColor = Color.FromArgb(051, 153, 153);
                    textBackColor = Color.FromArgb(038, 038, 038);
                    disableColor = Color.FromArgb(045, 045, 048);
                    break;
            }

            panel2.ForeColor = foreColor;
            panel2.BackColor = backColor;
            txtConnectionName.XForeColor = foreColor;
            txtConnectionName.XBackColor = textBackColor;
            txtUserID.XForeColor = foreColor;
            txtUserID.XBackColor = textBackColor;
            txtUserID.XDisableColor = disableColor;
            txtPassword.XForeColor = foreColor;
            txtPassword.XBackColor = textBackColor;
            txtDataFilePath.XDisableColor = disableColor;
            txtDataFilePath.XForeColor = foreColor;
            txtDataFilePath.XBackColor = textBackColor;
            tester.ForeColor = linkColor;
            selector.ForeColor = linkColor;

        }

        private void AddSqliteServerConnection_Load(object sender, EventArgs e)
        {            
            OnThemeChanged(new ThemeEventArgs());
            txtUserID.Enabled = false;
            txtPassword.Enabled = false;
        }
    }
}
