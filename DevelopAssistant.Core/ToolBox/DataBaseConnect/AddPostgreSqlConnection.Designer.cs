﻿namespace DevelopAssistant.Core.ToolBox
{
    partial class AddPostgreSqlConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddPostgreSqlConnection));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.chkEfficient = new System.Windows.Forms.CheckBox();
            this.tester = new System.Windows.Forms.Label();
            this.txtConnectionName = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.combServers = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.txtPort = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.txtDataBaseName = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.txtUserID = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.txtPassword = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "连接名称：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "服务器：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(68, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "端口：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "数据库名称：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(56, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "用户名：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(68, 207);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 6;
            this.label6.Text = "密码：";
            // 
            // chkEfficient
            // 
            this.chkEfficient.AutoSize = true;
            this.chkEfficient.Location = new System.Drawing.Point(120, 236);
            this.chkEfficient.Name = "chkEfficient";
            this.chkEfficient.Size = new System.Drawing.Size(96, 16);
            this.chkEfficient.TabIndex = 7;
            this.chkEfficient.Text = "高效连接模式";
            this.chkEfficient.UseVisualStyleBackColor = true;
            // 
            // tester
            // 
            this.tester.AutoSize = true;
            this.tester.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tester.ForeColor = System.Drawing.Color.Blue;
            this.tester.Location = new System.Drawing.Point(225, 237);
            this.tester.Name = "tester";
            this.tester.Size = new System.Drawing.Size(53, 12);
            this.tester.TabIndex = 8;
            this.tester.Text = "测试连接";
            this.tester.Click += new System.EventHandler(this.tester_Click);
            // 
            // txtConnectionName
            // 
            this.txtConnectionName.BackColor = System.Drawing.SystemColors.Window;
            this.txtConnectionName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtConnectionName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtConnectionName.FousedColor = System.Drawing.Color.Orange;
            this.txtConnectionName.Icon = null;
            this.txtConnectionName.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtConnectionName.IsButtonTextBox = false;
            this.txtConnectionName.IsClearTextBox = false;
            this.txtConnectionName.IsPasswordTextBox = false;
            this.txtConnectionName.Location = new System.Drawing.Point(120, 16);
            this.txtConnectionName.MaxLength = 32767;
            this.txtConnectionName.Multiline = false;
            this.txtConnectionName.Name = "txtConnectionName";
            this.txtConnectionName.PasswordChar = '\0';
            this.txtConnectionName.Placeholder = null;
            this.txtConnectionName.ReadOnly = false;
            this.txtConnectionName.Size = new System.Drawing.Size(146, 24);
            this.txtConnectionName.TabIndex = 9;
            this.txtConnectionName.UseSystemPasswordChar = false;
            this.txtConnectionName.XBackColor = System.Drawing.SystemColors.Window;
            this.txtConnectionName.XDisableColor = System.Drawing.Color.Empty;
            this.txtConnectionName.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // combServers
            // 
            this.combServers.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.combServers.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.combServers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combServers.Font = new System.Drawing.Font("宋体", 9F);
            this.combServers.FormattingEnabled = true;
            this.combServers.ItemIcon = null;
            this.combServers.Location = new System.Drawing.Point(121, 57);
            this.combServers.Name = "combServers";
            this.combServers.Size = new System.Drawing.Size(145, 22);
            this.combServers.TabIndex = 10;
            // 
            // txtPort
            // 
            this.txtPort.BackColor = System.Drawing.SystemColors.Window;
            this.txtPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPort.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtPort.FousedColor = System.Drawing.Color.Orange;
            this.txtPort.Icon = null;
            this.txtPort.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtPort.IsButtonTextBox = false;
            this.txtPort.IsClearTextBox = false;
            this.txtPort.IsPasswordTextBox = false;
            this.txtPort.Location = new System.Drawing.Point(121, 93);
            this.txtPort.MaxLength = 32767;
            this.txtPort.Multiline = false;
            this.txtPort.Name = "txtPort";
            this.txtPort.PasswordChar = '\0';
            this.txtPort.Placeholder = null;
            this.txtPort.ReadOnly = false;
            this.txtPort.Size = new System.Drawing.Size(100, 24);
            this.txtPort.TabIndex = 11;
            this.txtPort.UseSystemPasswordChar = false;
            this.txtPort.XBackColor = System.Drawing.SystemColors.Window;
            this.txtPort.XDisableColor = System.Drawing.Color.Empty;
            this.txtPort.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // txtDataBaseName
            // 
            this.txtDataBaseName.BackColor = System.Drawing.SystemColors.Window;
            this.txtDataBaseName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDataBaseName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtDataBaseName.FousedColor = System.Drawing.Color.Orange;
            this.txtDataBaseName.Icon = null;
            this.txtDataBaseName.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtDataBaseName.IsButtonTextBox = false;
            this.txtDataBaseName.IsClearTextBox = false;
            this.txtDataBaseName.IsPasswordTextBox = false;
            this.txtDataBaseName.Location = new System.Drawing.Point(121, 131);
            this.txtDataBaseName.MaxLength = 32767;
            this.txtDataBaseName.Multiline = false;
            this.txtDataBaseName.Name = "txtDataBaseName";
            this.txtDataBaseName.PasswordChar = '\0';
            this.txtDataBaseName.Placeholder = null;
            this.txtDataBaseName.ReadOnly = false;
            this.txtDataBaseName.Size = new System.Drawing.Size(145, 24);
            this.txtDataBaseName.TabIndex = 12;
            this.txtDataBaseName.UseSystemPasswordChar = false;
            this.txtDataBaseName.XBackColor = System.Drawing.SystemColors.Window;
            this.txtDataBaseName.XDisableColor = System.Drawing.Color.Empty;
            this.txtDataBaseName.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // txtUserID
            // 
            this.txtUserID.BackColor = System.Drawing.SystemColors.Window;
            this.txtUserID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUserID.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtUserID.FousedColor = System.Drawing.Color.Orange;
            this.txtUserID.Icon = null;
            this.txtUserID.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtUserID.IsButtonTextBox = false;
            this.txtUserID.IsClearTextBox = false;
            this.txtUserID.IsPasswordTextBox = false;
            this.txtUserID.Location = new System.Drawing.Point(121, 167);
            this.txtUserID.MaxLength = 32767;
            this.txtUserID.Multiline = false;
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.PasswordChar = '\0';
            this.txtUserID.Placeholder = null;
            this.txtUserID.ReadOnly = false;
            this.txtUserID.Size = new System.Drawing.Size(100, 24);
            this.txtUserID.TabIndex = 13;
            this.txtUserID.UseSystemPasswordChar = false;
            this.txtUserID.XBackColor = System.Drawing.SystemColors.Window;
            this.txtUserID.XDisableColor = System.Drawing.Color.Empty;
            this.txtUserID.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPassword.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtPassword.FousedColor = System.Drawing.Color.Orange;
            this.txtPassword.Icon = null;
            this.txtPassword.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtPassword.IsButtonTextBox = false;
            this.txtPassword.IsClearTextBox = false;
            this.txtPassword.IsPasswordTextBox = false;
            this.txtPassword.Location = new System.Drawing.Point(121, 201);
            this.txtPassword.MaxLength = 32767;
            this.txtPassword.Multiline = false;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Placeholder = null;
            this.txtPassword.ReadOnly = false;
            this.txtPassword.Size = new System.Drawing.Size(145, 24);
            this.txtPassword.TabIndex = 14;
            this.txtPassword.UseSystemPasswordChar = false;
            this.txtPassword.XBackColor = System.Drawing.SystemColors.Window;
            this.txtPassword.XDisableColor = System.Drawing.Color.Empty;
            this.txtPassword.XForeColor = System.Drawing.SystemColors.ControlText;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(227, 171);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 12);
            this.label8.TabIndex = 15;
            this.label8.Text = "如：postgres";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtPassword);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.txtUserID);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.txtDataBaseName);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtPort);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.combServers);
            this.panel2.Controls.Add(this.chkEfficient);
            this.panel2.Controls.Add(this.txtConnectionName);
            this.panel2.Controls.Add(this.tester);
            this.panel2.Location = new System.Drawing.Point(6, 34);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(327, 264);
            this.panel2.TabIndex = 16;
            // 
            // AddPostgreSqlConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 364);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddPostgreSqlConnection";
            this.Text = "PostgreSql数据库连接";
            this.Load += new System.EventHandler(this.AddPostgreSqlConnection_Load);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkEfficient;
        private System.Windows.Forms.Label tester;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtConnectionName;
        private ICSharpCode.WinFormsUI.Controls.NComboBox combServers;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtPort;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtDataBaseName;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtUserID;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtPassword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel2;
    }
}