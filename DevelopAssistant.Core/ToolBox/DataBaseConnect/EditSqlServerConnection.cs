﻿using DevelopAssistant.Core.ToolBar;
using DevelopAssistant.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class EditSqlServerConnection : EditDataBaseConnection
    {
        public EditSqlServerConnection()
        {
            InitializeComponent();
            InitializeControls();
        }

        public EditSqlServerConnection(DataBaseServer dbserver,MenuBar menubar)
            : this()
        {
            this.ConnectionId = dbserver.ID;
            //this.Text = "dbo." + dbserver.DataBaseName;
            this.combServer.Text = dbserver.Server;
            this.combServer.DropDownStyle = ComboBoxStyle.DropDown;
            this.combDataBases.Items.Add(dbserver.DataBaseName);
            this.combDataBases.SelectedIndex = 0;
            this.combDataBases.Text = dbserver.DataBaseName;
            this.combDataBases.Enabled = false;            
            this.txtInstance.Text =dbserver.Instance_Name;
            if (!string.IsNullOrEmpty(dbserver.Instance_Name))
            {
                this.chkInstance.Checked = true;
                this.txtInstance.Enabled = true;
            }
            this.txtUser_ID.Text = dbserver.UserID;
            this.txtPassword.Text = dbserver.Password;
            this.txtPort.Text = string.IsNullOrEmpty(dbserver.Port) ? "默认" : dbserver.Port;
            if (dbserver.DataBaseLoginStyle == DataBaseLoginStyle.Integrated_Security)
            {
                this.chkLoginStyle.Checked = true;
            }
            else
            {
                this.chkLoginStyle.Checked = false;
            }

            string connectionName = string.Empty;

            if (string.IsNullOrEmpty(ConnectionName))
            {
                connectionName = dbserver.DataBaseName +
                    "(" + dbserver.Server + "";
                if (!string.IsNullOrEmpty(dbserver.Port))
                {
                    connectionName += "," + dbserver.Port + "";
                }
                if (!string.IsNullOrEmpty(dbserver.Instance_Name))
                {
                    connectionName += "\\" + dbserver.Instance_Name;
                }
                connectionName += ")";
            }
            else
            {
                connectionName = ConnectionName;
            }

            this.dbserverName = connectionName;

            this.Apply = new EventHandler(Apply_Click); 
        }

        private void InitializeControls()
        {
            SetBtnApplyEnabled(false);
            this.chkLoginStyle .CheckedChanged +=chkLoginStyle_CheckedChanged;
            this.chkInstance.CheckedChanged +=chkInstance_CheckedChanged;
        }

        private void chkLoginStyle_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLoginStyle.Checked)
            {
                gboxLoginPanel.Enabled = false;
            }
            else
            {
                gboxLoginPanel.Enabled = true;
            }
            txtUser_ID.Enabled = gboxLoginPanel.Enabled;
            txtPassword.Enabled = gboxLoginPanel.Enabled;
        }

        private void chkInstance_CheckedChanged(object sender, EventArgs e)
        {
            if (chkInstance.Checked)
            {
                txtInstance.Enabled = true;
                txtInstance.ReadOnly = false;
            }
            else
            {
                txtInstance.Enabled = false;
                txtInstance.ReadOnly = true;
            }
        }

        private void label8_Click(object sender, EventArgs e)
        {
            if (txtPassword.PasswordChar=='*')
            {
                txtPassword.PasswordChar = txtUser_ID.PasswordChar;
                label8.Image = DevelopAssistant.Core.Properties.Resources.closeeye;
                //txtPassword.UseSystemPasswordChar = false;
            }
            else
            {
                txtPassword.PasswordChar = '*';
                label8.Image = DevelopAssistant.Core.Properties.Resources.openeye;
                //txtPassword.UseSystemPasswordChar = true;
            }
        }

        private void tester_Click(object sender, EventArgs e)
        {
            try
            {
                DataBaseServer dbserver = new DataBaseServer();
                dbserver.Server = combServer.SelectedText;
                string connectionString = "";
                connectionString += "Server=" + combServer.Text.ToString();
                if (txtPort.Text != "默认")
                {
                    connectionString += "," + txtPort.Text;
                }
                if (!string.IsNullOrEmpty(txtInstance.Text))
                {
                    connectionString += "\\" + txtInstance.Text;
                }
                connectionString += ";";
                if (!string.IsNullOrEmpty(combDataBases.Text))
                {
                    connectionString += "Initial Catalog=" + combDataBases.Text + ";";
                }
                else
                {
                    connectionString += "Initial Catalog=" + "master;";
                }
                if (!chkLoginStyle.Checked)
                {
                    connectionString += "User ID=" + txtUser_ID.Text + ";";
                    connectionString += "Pwd=" + txtPassword.Text + ";";
                }
                else
                {
                    connectionString += "Integrated Security=SSPI;";
                }

                ProviderName = "System.Data.SQL";
                ConnectionString = connectionString.Clone().ToString();
                AllObjectLoad = chkEfficient.Checked;

                dbserver.ConnectionString = ConnectionString;
                dbserver.ProviderName = ProviderName;

                using (var db = DevelopAssistant.Service.Utility.GetAdohelper(dbserver))
                {
                    string message=string.Empty;
                    if (db.TestConnect(out message))
                    {
                        SetBtnApplyEnabled(true);
                    }
                    else
                    {
                        MessageBox.Show(message);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                SetBtnApplyEnabled(false);
            }
        }

        private void Apply_Click(object sender, EventArgs e)
        {
            if (combDataBases.Items.Count < 1)
            {
                MessageBox.Show("请选择要连接的数据库名");
                return;
            }

            string connectionString = "";
            connectionString += "Server=" + combServer.Text.ToString();
            if (txtPort.Text != "默认")
            {
                connectionString += "," + txtPort.Text;
            }
            if (!string.IsNullOrEmpty(txtInstance.Text))
            {
                connectionString += "\\" + txtInstance.Text;
            }
            connectionString += ";";
            if (!string.IsNullOrEmpty(combDataBases.Text.ToString()))
            {
                connectionString += "Initial Catalog=" + combDataBases.Text.ToString() + ";";
            }
            else
            {
                connectionString += "Initial Catalog=" + "master;";
            }
            if (!chkLoginStyle.Checked)
            {
                connectionString += "User ID=" + txtUser_ID.Text + ";";
                connectionString += "Pwd=" + txtPassword.Text + ";";
            }
            else
            {
                connectionString += "Integrated Security=SSPI;";
            }

            ProviderName = "System.Data.SQL";
            ConnectionString = connectionString.Clone().ToString();
            AllObjectLoad = !chkEfficient.Checked;

        }

        protected override void OnThemeChanged(ThemeEventArgs e)
        {
            base.OnThemeChanged(e);

            string themeName = e.themeName;

            Color linkColor = Color.Blue;
            Color textBackColor = SystemColors.Window;
            Color disableColor = SystemColors.Control;
            switch (themeName)
            {
                case "Default":
                    linkColor = Color.Blue;
                    textBackColor = SystemColors.Window;
                    disableColor = SystemColors.Control;
                    break;
                case "Black":
                    linkColor = Color.FromArgb(051, 153, 153);
                    textBackColor = Color.FromArgb(038, 038, 038);
                    disableColor = Color.FromArgb(045, 045, 048);
                    break;
            }

            panel2.ForeColor = foreColor;
            panel2.BackColor = backColor;
            txtInstance.XForeColor = foreColor;
            txtInstance.XBackColor = textBackColor;
            txtInstance.XDisableColor = disableColor;
            txtPort.XForeColor = foreColor;
            txtPort.XBackColor = textBackColor;
            txtUser_ID.XForeColor = foreColor;
            txtUser_ID.XBackColor = textBackColor;
            txtUser_ID.XDisableColor = disableColor;
            txtPassword.XForeColor = foreColor;
            txtPassword.XBackColor = textBackColor;
            txtPassword.XDisableColor = disableColor;
            combDataBases.ForeColor = foreColor;
            combDataBases.BackColor = textBackColor;
            combServer.ForeColor = foreColor;
            combServer.BackColor = textBackColor;
            tester.ForeColor = linkColor;

        }

        private void EditSqlServerConnection_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new ThemeEventArgs());
            txtInstance.Enabled = false;
            txtUser_ID.Enabled = gboxLoginPanel.Enabled;
            txtPassword.Enabled = gboxLoginPanel.Enabled;
        }
    }
}
