﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class ColumnDesign : ToolBoxBase
    {
        private ColumnDesign()
        {
            InitializeComponent();
        }

        public ColumnDesign(string columnName, string cisNull)
            : this()
        {
            this.lblColumnName.Text = columnName;
        }

        private void btnApplyOk_Click(object sender, EventArgs e)
        {

        }

        private void ColumnDesign_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new EventArgs());
        }

        protected virtual void OnThemeChanged(EventArgs e)
        {

        }
    }
}
