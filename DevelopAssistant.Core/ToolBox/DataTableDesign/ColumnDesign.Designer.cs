﻿namespace DevelopAssistant.Core.ToolBox
{
    partial class ColumnDesign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ColumnDesign));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.lblColumnName = new System.Windows.Forms.Label();
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.btnApplyOk = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.btnCancle = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "字段名称:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "是否允许为空:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "是否自增长:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(134, 52);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(48, 16);
            this.checkBox1.TabIndex = 3;
            this.checkBox1.Text = "允许";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(134, 85);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(36, 16);
            this.checkBox2.TabIndex = 4;
            this.checkBox2.Text = "是";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // lblColumnName
            // 
            this.lblColumnName.AutoSize = true;
            this.lblColumnName.Location = new System.Drawing.Point(109, 19);
            this.lblColumnName.Name = "lblColumnName";
            this.lblColumnName.Size = new System.Drawing.Size(41, 12);
            this.lblColumnName.TabIndex = 5;
            this.lblColumnName.Text = "label4";
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.btnApplyOk);
            this.panel1.Controls.Add(this.btnCancle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(6, 160);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(239, 60);
            this.panel1.TabIndex = 6;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // btnApplyOk
            // 
            this.btnApplyOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplyOk.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnApplyOk.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnApplyOk.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnApplyOk.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnApplyOk.FouseColor = System.Drawing.Color.White;
            this.btnApplyOk.Foused = false;
            this.btnApplyOk.FouseTextColor = System.Drawing.Color.Black;
            this.btnApplyOk.Icon = null;
            this.btnApplyOk.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnApplyOk.Location = new System.Drawing.Point(55, 12);
            this.btnApplyOk.Name = "btnApplyOk";
            this.btnApplyOk.Radius = 0;
            this.btnApplyOk.Size = new System.Drawing.Size(75, 32);
            this.btnApplyOk.TabIndex = 1;
            this.btnApplyOk.Text = "确定";
            this.btnApplyOk.UnableIcon = null;
            this.btnApplyOk.UseVisualStyleBackColor = true;
            this.btnApplyOk.Click += new System.EventHandler(this.btnApplyOk_Click);
            // 
            // btnCancle
            // 
            this.btnCancle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancle.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnCancle.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnCancle.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnCancle.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnCancle.FouseColor = System.Drawing.Color.White;
            this.btnCancle.Foused = false;
            this.btnCancle.FouseTextColor = System.Drawing.Color.Black;
            this.btnCancle.Icon = null;
            this.btnCancle.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnCancle.Location = new System.Drawing.Point(147, 12);
            this.btnCancle.Name = "btnCancle";
            this.btnCancle.Radius = 0;
            this.btnCancle.Size = new System.Drawing.Size(75, 32);
            this.btnCancle.TabIndex = 0;
            this.btnCancle.Text = "取消";
            this.btnCancle.UnableIcon = null;
            this.btnCancle.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.lblColumnName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.checkBox2);
            this.panel2.Controls.Add(this.checkBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(6, 34);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(239, 126);
            this.panel2.TabIndex = 7;
            // 
            // ColumnDesign
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(251, 226);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "ColumnDesign";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "设计列";
            this.Load += new System.EventHandler(this.ColumnDesign_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label lblColumnName;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private ICSharpCode.WinFormsUI.Controls.NButton btnApplyOk;
        private ICSharpCode.WinFormsUI.Controls.NButton btnCancle;
        private System.Windows.Forms.Panel panel2;
    }
}