﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
 
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class ShowCellValue : ToolBoxBase
    {
        public ShowCellValue()
        {
            InitializeComponent();
        }

        public ShowCellValue(object value)
            : this()
        {
            StringBuilder sb = new StringBuilder();
            if(value !=null && DBNull.Value != value)
            {
                Byte[] array = (Byte[])value;
                for (int i = 0; i < array.Length; i++)
                {
                    sb.Append(array[i].ToString("X2"));
                }
                string strValue = "Ox" + sb.ToString();
                this.txtValueShow.Text = strValue;
            }            
        }

    }
}
