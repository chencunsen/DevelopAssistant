﻿using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Core.ToolBox
{
    public partial class SnippetScriptSelectForm : ToolBoxBase
    {
        protected Control.ControlCollection collection = null;

        public SnippetScriptItem selectedItem { get; set; }

        public SnippetScriptSelectForm()
        {
            InitializeComponent();
            InitializeControls();
        }       

        public SnippetScriptSelectForm(Control.ControlCollection viewCollection) : this()
        {
            collection = viewCollection;
        }

        private void InitializeControls()
        {
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.AutoGenerateColumns = false;
        }

        private void SnippetScriptSelectForm_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new EventArgs());

            var index = 0;
            var list = new List<SnippetScriptItem>();
            foreach (var control in collection)
            {
                if (control is NDataTablePanel)
                {
                    index = index + 1;
                    var dataTablePanel = (NDataTablePanel)control;
                    SnippetScriptItem item = new SnippetScriptItem();
                    item.OrderNo = index;                   
                    item.TableName = "视图 " + index + " ";
                    item.TableName2 = dataTablePanel.TableName;
                    item.DataGrid = (NDataGridView)dataTablePanel.Controls[0];
                    item.Mark = item.DataGrid == null ? "" : "共计 " + item.DataGrid.Rows.Count + " 行";
                    list.Add(item);
                }
            }

            this.dataGridView1.DataSource = list;

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count < 1)
                return;

            var dataGridViewRow = dataGridView1.SelectedRows[0];
           
            for (int index = 0; index < dataGridView1.Rows.Count; index++)
            {
                if(index != dataGridViewRow.Index)
                {
                    dataGridView1.Rows[index].Cells[1].Value = false;
                }
                else
                {
                    dataGridView1.Rows[index].Cells[1].Value = true;
                }
            }           
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            var _selectedItem = new SnippetScriptItem();

            this.dataGridView1.EndEdit();

            foreach (DataGridViewRow dataGridViewRow in this.dataGridView1.Rows)
            {
                var value = dataGridViewRow.Cells[1].Value;
                if (value != null && Convert.ToBoolean(value))
                {
                    _selectedItem.IsChecked = true;
                    _selectedItem.Mark = ((SnippetScriptItem)dataGridViewRow.DataBoundItem).Mark;
                    _selectedItem.DataGrid = ((SnippetScriptItem)dataGridViewRow.DataBoundItem).DataGrid;
                    _selectedItem.TableName = ((SnippetScriptItem)dataGridViewRow.DataBoundItem).TableName2;                    
                }
            }

            this.DialogResult = DialogResult.OK;
            this.selectedItem = _selectedItem;

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color toolBackColor = SystemColors.Control;
            Color textBackColor = SystemColors.Window;
            Color toolBorderColor = SystemColors.ControlLight;
            Color toolStripBackColor = SystemColors.ControlLight;
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    textBackColor = SystemColors.Window;
                    toolBackColor = SystemColors.Control;
                    toolBorderColor = SystemColors.ControlLight;                    
                    toolStripBackColor = Color.FromArgb(246, 248, 250);
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(045, 045, 048);
                    textBackColor = Color.FromArgb(038, 038, 038);
                    toolBackColor = Color.FromArgb(038, 038, 038);
                    toolBorderColor = SystemColors.ControlDark;                    
                    toolStripBackColor = Color.FromArgb(062, 062, 062);
                    break;
            }

            panel1.ForeColor = foreColor;
            panel1.BackColor = toolBackColor;        

            this.dataGridView1.SetTheme(themeName);                  
            this.NToolBar.ForeColor = foreColor;
            this.NToolBar.BackColor = toolStripBackColor;
            this.NToolBar.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.NToolBar.BorderColor = toolBorderColor;

            btnApply.ForeColor = foreColor;
            btnApply.BackColor = toolBackColor;
            btnCancel.ForeColor = foreColor;
            btnCancel.BackColor = toolBackColor;            
        }
      
    }

    public class SnippetScriptItem
    {
        public int OrderNo { get; set; }
        public bool IsChecked { get; set; }
        public string TableName { get; set; }
        public string TableName2 { get; set; }
        public NDataGridView DataGrid { get; set; }
        public string Mark { get; set; }
    }
}
