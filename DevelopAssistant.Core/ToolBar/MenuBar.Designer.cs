﻿namespace DevelopAssistant.Core.ToolBar
{
    partial class MenuBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuBar));
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.SystemDataBaseTree = new ICSharpCode.WinFormsUI.Controls.NTreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel2 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.btnfilter = new ICSharpCode.WinFormsUI.Controls.NImageButton();
            this.TagLabel = new System.Windows.Forms.Label();
            this.btn_database_refresh = new ICSharpCode.WinFormsUI.Controls.NImageButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.SystemDataBaseTree);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(284, 555);
            this.panel1.TabIndex = 0;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // SystemDataBaseTree
            // 
            this.SystemDataBaseTree.AllowDrop = true;
            this.SystemDataBaseTree.BackColor = System.Drawing.SystemColors.Window;
            this.SystemDataBaseTree.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SystemDataBaseTree.Cursor = System.Windows.Forms.Cursors.Default;
            this.SystemDataBaseTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SystemDataBaseTree.DragDropMarkColor = System.Drawing.Color.Black;
            this.SystemDataBaseTree.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.SystemDataBaseTree.ImageList = this.imageList1;
            this.SystemDataBaseTree.LineColor = System.Drawing.SystemColors.ControlDark;
            this.SystemDataBaseTree.Location = new System.Drawing.Point(0, 30);
            this.SystemDataBaseTree.Model = null;
            this.SystemDataBaseTree.Name = "SystemDataBaseTree";
            this.SystemDataBaseTree.NBorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.SystemDataBaseTree.SelectedImageIndex = 8;
            this.SystemDataBaseTree.SelectedNode = null;
            this.SystemDataBaseTree.Size = new System.Drawing.Size(284, 525);
            this.SystemDataBaseTree.TabIndex = 1;
            this.SystemDataBaseTree.NodeMouseClick += new ICSharpCode.WinFormsUI.Controls.NTreeNodeMouseClickEventHandler(this.SystemDataBaseTree_NodeMouseClick);
            this.SystemDataBaseTree.NodeMouseDoubleClick += new ICSharpCode.WinFormsUI.Controls.NTreeNodeMouseClickEventHandler(this.SystemDataBaseTree_NodeMouseDoubleClick);
            this.SystemDataBaseTree.BeforeExpand += new ICSharpCode.WinFormsUI.Controls.NTreeViewEventHandler(this.SystemDataBaseTree_BeforeExpand);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "database.png");
            this.imageList1.Images.SetKeyName(1, "table_multiple.png");
            this.imageList1.Images.SetKeyName(2, "view_multiple.png");
            this.imageList1.Images.SetKeyName(3, "table.png");
            this.imageList1.Images.SetKeyName(4, "view.png");
            this.imageList1.Images.SetKeyName(5, "script.png");
            this.imageList1.Images.SetKeyName(6, "script_code.png");
            this.imageList1.Images.SetKeyName(7, "snippets.png");
            this.imageList1.Images.SetKeyName(8, "tick.png");
            this.imageList1.Images.SetKeyName(9, "column.png");
            this.imageList1.Images.SetKeyName(10, "function-blue.png");
            this.imageList1.Images.SetKeyName(11, "page_white_freehand.png");
            this.imageList1.Images.SetKeyName(12, "database_check.png");
            this.imageList1.Images.SetKeyName(13, "database_connect.png");
            this.imageList1.Images.SetKeyName(14, "script_white.png");
            this.imageList1.Images.SetKeyName(15, "pk.gif");
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Bottom;
            this.panel2.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel2.Controls.Add(this.btnfilter);
            this.panel2.Controls.Add(this.TagLabel);
            this.panel2.Controls.Add(this.btn_database_refresh);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.MarginWidth = 0;
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(284, 30);
            this.panel2.TabIndex = 0;
            this.panel2.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // btnfilter
            // 
            this.btnfilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnfilter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnfilter.Image = global::DevelopAssistant.Core.Properties.Resources.filter;
            this.btnfilter.Location = new System.Drawing.Point(232, 4);
            this.btnfilter.Name = "btnfilter";
            this.btnfilter.Size = new System.Drawing.Size(22, 22);
            this.btnfilter.TabIndex = 2;
            this.btnfilter.ToolTipText = "条件过滤器";
            this.btnfilter.Click += new System.EventHandler(this.btnfilter_Click);
            // 
            // TagLabel
            // 
            this.TagLabel.AutoSize = true;
            this.TagLabel.Location = new System.Drawing.Point(4, 8);
            this.TagLabel.Name = "TagLabel";
            this.TagLabel.Size = new System.Drawing.Size(107, 18);
            this.TagLabel.TabIndex = 1;
            this.TagLabel.Text = "数据库列表:";
            // 
            // btn_database_refresh
            // 
            this.btn_database_refresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_database_refresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_database_refresh.Image = global::DevelopAssistant.Core.Properties.Resources.recovery;
            this.btn_database_refresh.Location = new System.Drawing.Point(259, 4);
            this.btn_database_refresh.Name = "btn_database_refresh";
            this.btn_database_refresh.Size = new System.Drawing.Size(22, 22);
            this.btn_database_refresh.TabIndex = 0;
            this.btn_database_refresh.ToolTipText = "刷新";
            this.btn_database_refresh.Click += new System.EventHandler(this.btn_database_refresh_Click);
            // 
            // MenuBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 555);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MenuBar";
            this.Text = "系统菜单";
            this.Load += new System.EventHandler(this.LeftMenuBar_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private ICSharpCode.WinFormsUI.Controls.NTreeView SystemDataBaseTree;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel2;
        private ICSharpCode.WinFormsUI.Controls.NImageButton btn_database_refresh;
        private System.Windows.Forms.Label TagLabel;
        private System.Windows.Forms.ImageList imageList1;
        private ICSharpCode.WinFormsUI.Controls.NImageButton btnfilter;

    }
}