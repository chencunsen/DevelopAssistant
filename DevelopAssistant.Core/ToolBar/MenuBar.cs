﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;

using NORM.DataBase;
using DevelopAssistant.Common;
using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Docking;
using DevelopAssistant.Core.ToolBox;
using System.Runtime.Serialization.Formatters.Binary;
using ICSharpCode.WinFormsUI.Controls;
using ICSharpCode.WinFormsUI.Controls.NodeControls;

namespace DevelopAssistant.Core.ToolBar
{
    public partial class MenuBar : DockContent
    {
        string Keywords = string.Empty;

        MainForm _MAINFORM = null;
        ContextMenuStrip contextmenu = null;
        new ContextMenuStrip ContextMenu = null;
        NTreeNode CurrentDatabaseServer = null;

        delegate void DelegateAddTreeNode(NTreeNode ownerNode, NTreeNode noneNode);

        Dictionary<string, NToolStripMenuItem> m_ContextMenuItems = new Dictionary<string, NToolStripMenuItem>();

        public MenuBar()
        {
            InitializeComponent();
            InitializeSystemDataBaseTree();
        }

        public MenuBar(MainForm mainform, System.Windows.Forms.ContextMenuStrip contextmenu)
            : this()
        {
            _MAINFORM = mainform;          
            this.contextmenu = contextmenu;
            this.SystemDataBaseTree.Nodes.Clear();
        }

        private void InitializeControls()
        {
            TagLabel.Font = NToolStripItemTheme.TextFont;
        }

        private void InitializeSystemDataBaseTree()
        {            
            this.AllowEndUserDocking = false;                        

            NodeIcon nodeIconBox1 = new NodeIcon();
            nodeIconBox1.DataPropertyName = "Icon";
            SystemDataBaseTree.NodeControls.Add(nodeIconBox1);

            NodeTextBox nodeTextBox1 = new NodeTextBox();
            nodeTextBox1.DataPropertyName = "Text";
            SystemDataBaseTree.NodeControls.Add(nodeTextBox1);

            SystemDataBaseTree.SetTheme("Default");

            SystemDataBaseTree.ItemDrag += new ItemDragEventHandler(TreeNode_ItemDrag); 
        }

        private void LoadContextMenuItems()
        {
            ContextMenu = new NContextMenuStrip();
            m_ContextMenuItems = new Dictionary<string, NToolStripMenuItem>();

            try
            {                 
                foreach (ToolStripMenuItem item in contextmenu.Items)
                {
                    NToolStripMenuItem n_item = new NToolStripMenuItem();
                    n_item.Name = item.Name;
                    n_item.Text = item.Text;
                    n_item.Image = item.Image;
                    n_item.AutoSize = true;
                    n_item.Level = ((NToolStripMenuItem)item).Level;
                    n_item.AddIn = ((NToolStripMenuItem)item).AddIn;
                    n_item.ItemType = ((NToolStripMenuItem)item).ItemType;

                    if (item.DropDownItems != null && item.DropDownItems.Count > 0)
                    {
                        foreach (ToolStripMenuItem sonitem in item.DropDownItems)
                        {
                            NToolStripMenuItem nn_item = new NToolStripMenuItem();
                            nn_item.Name = sonitem.Name;
                            nn_item.Text = sonitem.Text;
                            nn_item.Image = sonitem.Image;
                            nn_item.AutoSize = true;
                            nn_item.Level = ((NToolStripMenuItem)sonitem).Level;
                            nn_item.AddIn = ((NToolStripMenuItem)sonitem).AddIn;

                            nn_item.Click += new EventHandler(ContextMenuItem_Click);
                            m_ContextMenuItems.Add(n_item.Name + ">" + nn_item.Name, nn_item);
                            n_item.DropDownItems.Add(nn_item);
                        }
                    }

                    n_item.Click += new EventHandler(ContextMenuItem_Click);
                    m_ContextMenuItems.Add(n_item.Name, n_item);
                    ContextMenu.Items.Add(n_item);
                }

                this.SystemDataBaseTree.ContextMenuStrip = ContextMenu;

            }
            catch (Exception ex)
            {
                DevelopAssistant.Common.NLogger.WriteToLine(ex.Message, "错误", DateTime.Now, ex.Source, ex.StackTrace);
            }

        }
              
        private void LeftMenuBar_Load(object sender, EventArgs e)
        {            
            LoadServers();
            //LoadContextMenuItems();
            //SetTheme(AppSettings.EditorSettings.TSQLEditorTheme);
        }

        private void ContextMenuItem_Click(object sender, EventArgs e)
        {
            string SnippetCode = null;
            NToolStripMenuItem Item = sender as NToolStripMenuItem;
            NTreeNode SelectedTreeNode = SystemDataBaseTree.SelectedNode;

            if (SelectedTreeNode == null || SelectedTreeNode.Tag == null)
                return;          

            switch (Item.Text)
            {
                case "连接服务器":
                    DataBaseServer dataBaseServer = SelectedTreeNode.Tag as DataBaseServer;
                    if (dataBaseServer != null && !dataBaseServer.OnConnecting)
                    {
                        bool bl = false;
                        string message = string.Empty;
                        dataBaseServer.OnConnecting = true;
                        SystemDataBaseTree.Enabled = false;
                        System.Threading.Thread th = new System.Threading.Thread(() =>
                        {
                            ConnectDataBase(dataBaseServer, ref bl, ref message);
                            this.Invoke(new MethodInvoker(delegate ()
                            {
                                SystemDataBaseTree.Enabled = true;
                                if (bl)
                                    _MAINFORM.SetSysStatusInfo("完成");
                                else
                                    _MAINFORM.SetSysStatusInfo(message);
                            }));
                        });
                        th.IsBackground = true;
                        th.Start();

                    }
                    break;
                case "断开连接":
                    DisConnectDataBase();
                    break;
                case "编辑连接":
                case "编辑属性":
                    DataBaseServer dbserver = SelectedTreeNode.Tag as DataBaseServer;
                    EditDataBaseConnection dcnt = new EditDataBaseConnection().EditDataBaseConnectiontor(dbserver, this);                     
                    if (dcnt.ShowDialog(this).Equals(DialogResult.OK))
                    {
                        dbserver.ConnectionString = dcnt.ConnectionString;
                        dbserver.Name = dcnt.dbserverName;
                        SelectedTreeNode.Text = dbserver.Name;
                    }
                    _MAINFORM.BringToFront(); 
                    break;
                case "删除连接":
                    DeleteConnectionDataBase();
                    break;
                case "刷新":
                    RefreshDataBase();
                    break;
            }

            try
            {
                if (Item.ItemType.Equals("AddInToolStripMenuItem"))
                {
                    DevelopAssistant.AddIn.AddInBase addin = Item.AddIn as DevelopAssistant.AddIn.AddInBase;
                    if (addin.GetType().Equals(typeof(DevelopAssistant.AddIn.ExecuteFileAddIn)))
                    {
                        _MAINFORM.ExecuteFile(addin.ExecuteFile);
                    }
                    else
                    {
                        _MAINFORM.ExecuteAddIn(addin, _MAINFORM, this, _MAINFORM.ConnectedDataBaseServer, this.CurrentDatabaseServer, AppSettings.WindowTheme.Name, AppSettings.EditorSettings.TSQLEditorTheme);
                    }
                }
            }
            catch (Exception ex)
            {
                DevelopAssistant.Common.NLogger.WriteToLine(ex.Message, "执行插件错误", DateTime.Now, ex.Source, ex.StackTrace);
            } 

            if (_MAINFORM.ConnectedDataBaseServer == null || !_MAINFORM.ConnectedDataBaseServer.OnConnecting)
                return;
           
            switch (Item.Text)
            {
                case "新增表":
                    DevelopAssistant.Core.DBMS.AddTableDocument tableAdd = new DBMS.AddTableDocument(SelectedTreeNode, _MAINFORM.ConnectedDataBaseServer, this);
                    _MAINFORM.AddFloatWindow(tableAdd, "新增表");
                    break;
                case "新增视图":
                    SnippetCode = Service.CreateOrReplaceObject.CreateNewView(_MAINFORM.ConnectedDataBaseServer);
                    _MAINFORM.SetQueryDocumentText(SnippetCode, AppSettings.NewOpenQueryDocument); 
                    break;
                case "新增函数":
                    FunctionType functionType = new FunctionType();
                    if (functionType.ShowDialog(this).Equals(DialogResult.OK))
                    {
                        SnippetCode = Service.CreateOrReplaceObject.CreateNewFunction(_MAINFORM.ConnectedDataBaseServer, functionType.FuntionTypeName);
                        _MAINFORM.SetQueryDocumentText(SnippetCode, AppSettings.NewOpenQueryDocument); 
                    }
                    break;
                case "新增存储过程":
                    SnippetCode = Service.CreateOrReplaceObject.CreateNewProcedure(_MAINFORM.ConnectedDataBaseServer);
                    _MAINFORM.SetQueryDocumentText(SnippetCode, AppSettings.NewOpenQueryDocument);
                    break;
                case "打开表(前100行)":
                case "打开前100行":
                case "打开视图(前100行)":
                case "打开视图前100行":
                    SnippetCode = Service.CreateOrReplaceObject.CreateTop100Query(SelectedTreeNode.Text,_MAINFORM.ConnectedDataBaseServer);
                    _MAINFORM.SetQueryDocumentText(SnippetCode, AppSettings.NewOpenQueryDocument);
                    break;
                case "打开表(所有行)":
                case "打开表":               
                    SnippetCode = Service.SelectTable.ToSnippetCode(SelectedTreeNode.Text, _MAINFORM.ConnectedDataBaseServer);
                    _MAINFORM.SetQueryDocumentText(SnippetCode, AppSettings.NewOpenQueryDocument);
                    break;
                case "设计表":
                    DevelopAssistant.Core.DBMS.TableDesignnerDocument tableDesignner = new DBMS.TableDesignnerDocument(this._MAINFORM, SelectedTreeNode.Text, _MAINFORM.ConnectedDataBaseServer);
                    _MAINFORM.AddFloatWindow(tableDesignner, "设计" + SelectedTreeNode.Text + "表");
                    break;
                case "生成代码":
                case "生成代码(C#)":
                    ToolBox.CreateCodePicker codePicker = new ToolBox.CreateCodePicker(SelectedTreeNode.Text, SnippetCode);
                    codePicker.ShowDialog(_MAINFORM);
                    break;
                case "查看表对象":
                    DevelopAssistant.Core.DBMS.TableObjectsDocument tableObjects = new DevelopAssistant.Core.DBMS.TableObjectsDocument(SelectedTreeNode.Text, _MAINFORM.ConnectedDataBaseServer);
                    _MAINFORM.AddNewContent(tableObjects, SelectedTreeNode.Text + " 对象");
                    break;
                case "删除表":
                    DevelopAssistant.Core.DBMS.DeleteTableDocument tableDelete = (DevelopAssistant.Core.DBMS.DeleteTableDocument)_MAINFORM.AddSingleContent(new DBMS.DeleteTableDocument(SelectedTreeNode, _MAINFORM.ConnectedDataBaseServer, this), "删除 " + SelectedTreeNode.Text + " 表");
                    tableDelete.RefreshDocument(SelectedTreeNode, _MAINFORM.ConnectedDataBaseServer);                
                    break;
                case "Create 脚本":
                    switch (SelectedTreeNode.Tag + "")
                    {                                           
                        case "table":
                            SnippetCode = Service.CreateTable.ToSnippetCode(SelectedTreeNode.Text, _MAINFORM.ConnectedDataBaseServer);
                            break;
                        case "view":
                            SnippetCode = Service.CreateView.ToSnippetCode(SelectedTreeNode.Text, _MAINFORM.ConnectedDataBaseServer);
                            break;   
                        case "procedure":
                            SnippetCode = Service.CreateProcedure.ToSnippetCode(SelectedTreeNode.Text, _MAINFORM.ConnectedDataBaseServer);
                            break;
                        case "function":
                            SnippetCode = Service.CreateFunction.ToSnippetCode(SelectedTreeNode.Text, _MAINFORM.ConnectedDataBaseServer);
                            break;
                        default:
                            SnippetCode = Service.CreateTable.ToSnippetCode(SelectedTreeNode.Text, _MAINFORM.ConnectedDataBaseServer);
                            break;
                    }
                    _MAINFORM.SetQueryDocumentText(SnippetCode, AppSettings.NewOpenQueryDocument);
                    break;
                case "Insert 脚本":
                    SnippetCode = Service.InsertTable.ToSnippetCode(SelectedTreeNode.Text, _MAINFORM.ConnectedDataBaseServer);
                    _MAINFORM.SetQueryDocumentText(SnippetCode, AppSettings.NewOpenQueryDocument);
                    break;
                case "Clear 脚本":
                    SnippetCode = Service.DeleteTable.ToSnippetCode(SelectedTreeNode.Text, _MAINFORM.ConnectedDataBaseServer);
                    _MAINFORM.SetQueryDocumentText(SnippetCode, AppSettings.NewOpenQueryDocument);
                    break;
                case "Update 脚本":
                    SnippetCode = Service.UpdateTable.ToSnippetCode(SelectedTreeNode.Text, _MAINFORM.ConnectedDataBaseServer);
                    _MAINFORM.SetQueryDocumentText(SnippetCode, AppSettings.NewOpenQueryDocument);
                    break;
                case "编辑数据":
                    //new DevelopAssistant.Core.DBMS.TableDataEditorDocument(SelectedTreeNode.Text, _MAINFORM.ConnectedDataBaseServer).ShowDialog(_MAINFORM);
                    DevelopAssistant.Core.DBMS.TableDataEditorDocument tableDataEditForm = new DevelopAssistant.Core.DBMS.TableDataEditorDocument(SelectedTreeNode.Text, _MAINFORM.ConnectedDataBaseServer);
                    _MAINFORM.AddFloatWindow(tableDataEditForm, "编辑" + SelectedTreeNode.Text + "数据");
                    tableDataEditForm.InitializeData();
                    break;
                case "执行存储过程":
                    ExecuteCommand ProcedureExecute = new ExecuteCommand(SelectedTreeNode.Text, "Procedure", _MAINFORM.ConnectedDataBaseServer);
                    if (ProcedureExecute.ShowDialog(_MAINFORM).Equals(DialogResult.OK))
                        _MAINFORM.SetQueryDocumentText(ProcedureExecute.SqlText, AppSettings.NewOpenQueryDocument);
                    break;
                case "执行函数":
                    ExecuteCommand FunctionExecute = new ExecuteCommand(SelectedTreeNode.Text, "Function", _MAINFORM.ConnectedDataBaseServer);
                    if (FunctionExecute.ShowDialog(_MAINFORM).Equals(DialogResult.OK))
                        _MAINFORM.SetQueryDocumentText(FunctionExecute.SqlText, AppSettings.NewOpenQueryDocument);
                    break;
            }                      

        }

        private void SystemDataBaseTree_NodeMouseClick(object sender, NTreeNodeMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (e.Node.Level >= 0)
                    SystemDataBaseTree.SelectedNode = e.Node;
            }
            //e.Node.ContextMenuStrip = ContextMenu;              
            SetContextMenuItem(e.Node.Level, e.Node.Text, e.Node.Tag+"");
            SelectCurrentDataBaseServer(e.Node);
        }

        private void TreeNode_ItemDrag(object sender, ItemDragEventArgs e)
        {
            NTreeNode[] nodes = (NTreeNode[])e.Item;

            if (nodes == null || nodes.Length <= 0)
                return;

            NTreeNode node = nodes[0] as NTreeNode;
           
            //if (node.Tag != null && (node.Tag.ToString() == "table") || (node.Tag.ToString() == "view") || (node.Tag.ToString() == "column") ||
            // (node.Tag.ToString() == "procedure") || (node.Tag.ToString() == "function"))
            //{
            //    DoDragDrop(node, System.Windows.Forms.DragDropEffects.Copy);
            //}

            switch (node.Level)
            {
                case 0:
                case 2:
                case 3:
                case 4:
                case 5:
                    DoDragDrop(node, System.Windows.Forms.DragDropEffects.Copy);
                    break;
            }
        }

        public bool SetContextMenuItem(int level,string text,string category)
        {
            bool result = false;
            try
            {
                switch (level)
                {
                    case 0:
                        m_ContextMenuItems["连接服务器ToolStripMenuItem"].Visible = true;
                        m_ContextMenuItems["断开连接ToolStripMenuItem"].Visible = true;
                        m_ContextMenuItems["编辑属性ToolStripMenuItem"].Visible = true;
                        m_ContextMenuItems["删除连接ToolStripMenuItem"].Visible = true;
                        m_ContextMenuItems["打开前100行ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["打开表ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["设计表ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["删除表ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["编辑数据ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["生成脚本ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["生成代码ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["新增表ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["查看表对象ToolStripMenuItem"].Visible = false;                       
                        m_ContextMenuItems["刷新ToolStripMenuItem1"].Visible = true;
                        break;
                    case 1:
                        m_ContextMenuItems["连接服务器ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["断开连接ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["编辑属性ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["删除连接ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["打开前100行ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["打开表ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["设计表ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["删除表ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["编辑数据ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["生成脚本ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["生成代码ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["新增表ToolStripMenuItem"].Visible = true;
                        m_ContextMenuItems["查看表对象ToolStripMenuItem"].Visible = false;                        
                        m_ContextMenuItems["刷新ToolStripMenuItem1"].Visible = true;

                        switch (category)
                        {
                            case "Table":                               
                                m_ContextMenuItems["新增表ToolStripMenuItem"].Text = "新增表";
                                break;
                            case "View":
                                m_ContextMenuItems["新增表ToolStripMenuItem"].Text = "新增视图";
                                break;
                            case "Function":
                                m_ContextMenuItems["新增表ToolStripMenuItem"].Text = "新增函数";
                                break;
                            case "Procedure":
                                switch (_MAINFORM.ConnectedDataBaseServer.ProviderName)
                                {
                                    case "System.Data.Sqlite":
                                        m_ContextMenuItems["新增表ToolStripMenuItem"].Visible = false;
                                        break;
                                }
                                m_ContextMenuItems["新增表ToolStripMenuItem"].Text = "新增存储过程";
                                break;
                        }

                        break;
                    case 2:
                        m_ContextMenuItems["连接服务器ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["断开连接ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["编辑属性ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["删除连接ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["新增表ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["打开表ToolStripMenuItem"].Visible = true;

                        switch (category)
                        {
                            case "table":
                                 m_ContextMenuItems["打开前100行ToolStripMenuItem"].Visible = true;
                                 m_ContextMenuItems["打开表ToolStripMenuItem"].Text = "打开表(所有行)";
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>createSQLToolStripMenuItem"].Visible = true;
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>insertSQLToolStripMenuItem"].Visible = true;
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>updateSQLToolStripMenuItem"].Visible = true;
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>clearSQLToolStripMenuItem"].Visible = true;
                                 m_ContextMenuItems["设计表ToolStripMenuItem"].Visible = true;
                                 m_ContextMenuItems["删除表ToolStripMenuItem"].Visible = true;
                                 m_ContextMenuItems["编辑数据ToolStripMenuItem"].Visible = true;
                                 m_ContextMenuItems["查看表对象ToolStripMenuItem"].Visible = true;
                                 m_ContextMenuItems["生成代码ToolStripMenuItem"].Visible = true;
                                break;
                            case "view":
                                 m_ContextMenuItems["打开前100行ToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["打开表ToolStripMenuItem"].Text = "打开视图(前100行)";
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>createSQLToolStripMenuItem"].Visible = true;
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>insertSQLToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>updateSQLToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>clearSQLToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["设计表ToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["删除表ToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["编辑数据ToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["查看表对象ToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["生成代码ToolStripMenuItem"].Visible = true;
                                break;
                            case "procedure":
                                 m_ContextMenuItems["打开前100行ToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["打开表ToolStripMenuItem"].Text = "执行存储过程";
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>createSQLToolStripMenuItem"].Visible = true;
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>insertSQLToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>updateSQLToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>clearSQLToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["设计表ToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["删除表ToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["编辑数据ToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["查看表对象ToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["生成代码ToolStripMenuItem"].Visible = false;
                                break;
                            case "function":
                                 m_ContextMenuItems["打开前100行ToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["打开表ToolStripMenuItem"].Text = "执行函数";
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>createSQLToolStripMenuItem"].Visible = true;
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>insertSQLToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>updateSQLToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["生成脚本ToolStripMenuItem>clearSQLToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["设计表ToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["删除表ToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["编辑数据ToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["查看表对象ToolStripMenuItem"].Visible = false;
                                 m_ContextMenuItems["生成代码ToolStripMenuItem"].Visible = false;
                                break;
                        }
                       
                        m_ContextMenuItems["生成脚本ToolStripMenuItem"].Visible = true;                        
                        m_ContextMenuItems["刷新ToolStripMenuItem1"].Visible = true;
                        break;
                    case 3:                       
                        m_ContextMenuItems["连接服务器ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["断开连接ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["编辑属性ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["删除连接ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["打开前100行ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["打开表ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["设计表ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["删除表ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["编辑数据ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["生成脚本ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["生成代码ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["新增表ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["查看表对象ToolStripMenuItem"].Visible = false;
                        m_ContextMenuItems["刷新ToolStripMenuItem1"].Visible = false;
                        break;
                }

                foreach (var item in m_ContextMenuItems.Values)
                {
                    if (level == -1)
                    {
                        item.Visible = false;
                        continue;
                    }
                    if (item.ItemType.Equals("AddInToolStripMenuItem"))
                    {
                        if (level != item.Level)
                        {
                            item.Visible = false;
                        }
                        else
                        {
                            item.Visible = true;
                        }
                    }
                }

            }
            catch(Exception ex)
            {
                DevelopAssistant.Common.NLogger.WriteToLine(ex.Message, "错误", DateTime.Now, ex.Source, ex.StackTrace);
            }
           
            return result;
        }

        public int GetContextMemuItemIndex(string name)
        {
            int index = -1;
            for (int i = 0; i < ContextMenu.Items.Count; i++)
            {
                if (ContextMenu.Items[i].Name.ToLower().Equals(name.ToLower()))
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        public void LoadServers()
        {
            System.Threading.Thread th = new System.Threading.Thread(() => {
                using (var db = NORM.DataBase.DataBaseFactory.Create("default"))
                {
                    string sqlText = "SELECT * FROM [T_DataBaseServers] ORDER BY [id] ASC "; //[dataBaseType] ASC,
                    DataTable dt = db.QueryDataSet(System.Data.CommandType.Text, sqlText, null).Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        DataBaseServer dbs = new DataBaseServer();
                        dbs.ID = dr["connectionID"] + "";
                        dbs.Name = dr["connectionName"] + "";
                        dbs.ProviderName = dr["dataBaseType"] + "";                       
                        dbs.ConnectionString = dr["connectionString"] + "";
                        dbs.Parse();

                        AddDataBaseConnectionSynch(dbs);
                    }
                }            
            });
            th.IsBackground = true;
            th.Start();
            
        }

        public void AddDataBaseConnectionSynch(DataBaseServer databaseServer)
        {
            bool Sucess = false;
            string Message = String.Empty;

            NTreeNode ServerDataBaseNode = new NTreeNode() { ImageIndex = 0 };
            ServerDataBaseNode.SelectedImageIndex = 12;
            ServerDataBaseNode.Tag = databaseServer;
            ServerDataBaseNode.Text = databaseServer.Name;

            NTreeNode node_Table = new NTreeNode() { Name = "Table", Text = "数据表", Tag = "Table", ImageIndex = 1 };
            NTreeNode node_View = new NTreeNode() { Name = "View", Text = "数据视图", Tag = "View", ImageIndex = 2 };
            NTreeNode node_Function = new NTreeNode() { Name = "Function", Text = "函数", Tag = "Function", ImageIndex = 11 };
            NTreeNode node_Procedure = new NTreeNode() { Name = "Procedure", Text = "存储过程", Tag = "Procedure", ImageIndex = 5 };

            this.Invoke(new MethodInvoker(delegate ()
            {
                SystemDataBaseTree.Nodes.Add(ServerDataBaseNode);
                SystemDataBaseTree.SelectedNode = CurrentDatabaseServer = ServerDataBaseNode;
            }));

            ServerDataBaseNode.Nodes.AddRange(new NTreeNode[] {
                        node_Table,
                        node_View,
                        node_Function,
                        node_Procedure
                    });

            if (databaseServer.OnConnecting)
                ConnectDataBase(databaseServer, ref Sucess, ref Message);

        }

        public void AddDataBaseConnectionAsync(object Paramater, Delegate CallBack)
        {
            bool Sucess = false; 
            string Message = String.Empty;

            DataBaseServer databaseServer = Paramater as DataBaseServer;

            System.Threading.Thread th = new System.Threading.Thread(() =>
            {               
                NTreeNode ServerDataBaseNode = new NTreeNode() { ImageIndex = 0 };
                ServerDataBaseNode.SelectedImageIndex = 12;
                ServerDataBaseNode.Tag = databaseServer;
                ServerDataBaseNode.Text = databaseServer.Name;

                NTreeNode node_Table = new NTreeNode() { Name = "Table", Text = "数据表", Tag = "Table", ImageIndex = 1 };
                NTreeNode node_View = new NTreeNode() { Name = "View", Text = "数据视图", Tag = "View", ImageIndex = 2 };
                NTreeNode node_Function = new NTreeNode() { Name = "Function", Text = "函数", Tag = "Function", ImageIndex = 11 };
                NTreeNode node_Procedure = new NTreeNode() { Name = "Procedure", Text = "存储过程", Tag = "Procedure", ImageIndex = 5 };

                this.Invoke(new MethodInvoker(delegate()
                {                   
                    SystemDataBaseTree.Nodes.Add(ServerDataBaseNode);
                    SystemDataBaseTree.SelectedNode = CurrentDatabaseServer = ServerDataBaseNode;
                }));

                ServerDataBaseNode.Nodes.AddRange(new NTreeNode[] {
                        node_Table,
                        node_View,
                        node_Function,
                        node_Procedure
                    });

                if (databaseServer.OnConnecting)
                    ConnectDataBase(databaseServer, ref Sucess, ref Message);
                if (CallBack != null) CallBack.DynamicInvoke(Message, Message);  

            });

            th.IsBackground = true;
            th.Start();
        }

        public void ConnectDataBase(DataBaseServer databaseServer,ref bool sucess,ref string message,Delegate callback=null)
        {
            this.Invoke(new MethodInvoker(delegate () {
                this._MAINFORM.SetMainFormToolBarEnable(false);
            }));

            if (CurrentDatabaseServer != null && CurrentDatabaseServer.Level == 0)
            {
                NTreeNode node_Table = new NTreeNode() { Name = "Table", Text = "数据表", Tag = "Table", ImageIndex = 1 };
                NTreeNode node_View = new NTreeNode() { Name = "View", Text = "数据视图", Tag = "View", ImageIndex = 2 };
                NTreeNode node_Function = new NTreeNode() { Name = "Function", Text = "函数", Tag = "Function", ImageIndex = 11 };
                NTreeNode node_Procedure = new NTreeNode() { Name = "Procedure", Text = "存储过程", Tag = "Procedure", ImageIndex = 5 };

                this.Invoke(new MethodInvoker(delegate()
                {
                    CurrentDatabaseServer.Nodes.Clear();
                    CurrentDatabaseServer.Nodes.AddRange(new NTreeNode[] { 
                        node_Table,
                        node_View,
                        node_Function,
                        node_Procedure
                    });
                }));

                databaseServer.Tables = new List<Table>();
                databaseServer.Views = new List<DevelopAssistant.Service.View>();
                databaseServer.Procedures = new List<Procedure>();
                databaseServer.Functions = new List<Function>();
                
                try
                {
                    _MAINFORM.ConnectedDataBaseServer = databaseServer;

                    databaseServer.DataBaseServerNode = CurrentDatabaseServer;

                    DelegateAddTreeNode d = new DelegateAddTreeNode(AddTreeNode);

                    var dt = new DataTable();
                    using (var db = Utility.GetAdohelper(databaseServer))
                    {
                        dt = db.GetDataBaseObject();

                        foreach (DataRow dr in dt.Rows)
                        {
                            string table_type = dr["table_type"] + "";
                            string table_name = dr["table_name"] + "";

                            bool matchSuccess = true;
                            if(!string.IsNullOrEmpty(Keywords)&&
                                table_name.IndexOf(Keywords,
                                StringComparison.OrdinalIgnoreCase) < 0)
                            {
                                matchSuccess = false;
                            }

                            if (matchSuccess)
                            {
                                NTreeNode node = new NTreeNode() { Text = table_name };
                                switch (table_type)
                                {
                                    case "table":
                                        DevelopAssistant.Service.Table t = new DevelopAssistant.Service.Table();
                                        t.Name = table_name;
                                        databaseServer.Tables.Add(t);
                                        node.Tag = "table";
                                        node.ImageIndex = 3;
                                        d.Invoke(node_Table, node);
                                        AddDataBaseObjectItem(node, databaseServer, db);
                                        break;
                                    case "view":
                                        DevelopAssistant.Service.View v = new DevelopAssistant.Service.View();
                                        v.Name = table_name;
                                        databaseServer.Views.Add(v);
                                        node.Tag = "view";
                                        node.ImageIndex = 4;
                                        d.Invoke(node_View, node);
                                        AddDataBaseObjectItem(node, databaseServer, db);
                                        break;
                                    case "function":
                                        DevelopAssistant.Service.Function f = new DevelopAssistant.Service.Function();
                                        f.Name = table_name;
                                        f.Parameters = new List<SqlParameter>();
                                        f.Describtion = "返回类型：" + table_type + "\r\n" + "属于：" + databaseServer.DataBaseName;
                                        DataTable pdt = db.GetTableObject(f.Name);
                                        foreach (DataRow row in pdt.Rows)
                                        {
                                            SqlParameter p1 = new SqlParameter();
                                            p1.Name = row["ColumnName"] + "";
                                            p1.DataType = row["TypeName"] + "";
                                            if (!string.IsNullOrEmpty(p1.Name))
                                                f.Parameters.Add(p1);
                                        }
                                        databaseServer.Functions.Add(f);
                                        node.Tag = "function";
                                        node.ImageIndex = 10;
                                        d.Invoke(node_Function, node);
                                        break;
                                    case "procedure":
                                        DevelopAssistant.Service.Procedure p = new DevelopAssistant.Service.Procedure();
                                        p.Name = table_name;
                                        databaseServer.Procedures.Add(p);
                                        node.Tag = "procedure";
                                        node.ImageIndex = 6;
                                        d.Invoke(node_Procedure, node);
                                        break;
                                } 
                            }
                        }

                        AddDataBaseDefaultFuntions(databaseServer);

                    }

                    this.Invoke(new MethodInvoker(delegate()
                    {
                        CurrentDatabaseServer.ImageIndex = 13;
                        SystemDataBaseTree.SelectedNode = CurrentDatabaseServer;
                    }));

                    sucess = true;
                    message = "完成";

                    databaseServer.OnConnecting = true;                   

                }
                catch (Exception ex)
                {
                    this.Invoke(new MethodInvoker(delegate()
                    {
                        SystemDataBaseTree.SelectedNode = CurrentDatabaseServer;
                        CurrentDatabaseServer.ImageIndex = 0;                        
                    }));
                    sucess = false;
                    message = ex.Message; //"建立连接时出现与网络相关的或特定于实例的错误";
                }
                finally
                {
                    this.Invoke(new MethodInvoker(delegate() { node_Table.Expand(); }));

                    if (callback != null)
                        callback.DynamicInvoke();
                }

            }

            this.Invoke(new MethodInvoker(delegate () {
                this._MAINFORM.SetMainFormToolBarEnable(true);
            }));

        }

        public void DisConnectDataBase()//DataBaseServer databaseServer
        {
            NTreeNode SelectedTreeNode = SystemDataBaseTree.SelectedNode;
            if (SelectedTreeNode.Level == 0)
            {
                SelectedTreeNode.Nodes.Clear();
                SelectedTreeNode.ImageIndex = 0;
                ((DataBaseServer)SelectedTreeNode.Tag).OnConnecting = false;
            }               
        }

        public void DeleteConnectionDataBase()
        {
            NTreeNode SelectedTreeNode = SystemDataBaseTree.SelectedNode;
            if (SelectedTreeNode.Level == 0)
            {               
                DataBaseServer dataBaseServer = SelectedTreeNode.Tag as DataBaseServer;
                using (var db = NORM.DataBase.DataBaseFactory.Create("default"))
                {
                    db.BeginTransaction();
                    try
                    {
                        string sqlText = "DELETE FROM [T_DataBaseServers] WHERE [connectionID]='" + dataBaseServer.ID + "'";
                        int val = db.Execute(CommandType.Text, sqlText, null);
                        SelectedTreeNode.Nodes.Clear();
                        SystemDataBaseTree.Nodes.Remove(SelectedTreeNode);
                        db.Commit();
                    }
                    catch (Exception ex)
                    {
                        db.RollBack();
                    }                   
                }
            }          
        }

        public void RefreshDataBase()
        {
            bool _Success = false;
            string _MessageInfo = string.Empty;
            DataBaseServer _DataBaseServer = null;

            NTreeNode SelectedTreeNode = SystemDataBaseTree.SelectedNode;
            if (SelectedTreeNode == null)
                return;

            SystemDataBaseTree.Enabled = false;
            SelectedTreeNode.Nodes.Clear();
            switch (SelectedTreeNode.Level)
            {
                case 0:
                    _DataBaseServer = SelectedTreeNode.Tag as DataBaseServer;
                    _DataBaseServer.OnConnecting = true;
                    ConnectDataBase(_DataBaseServer, ref _Success, ref _MessageInfo, null);
                    break;
                case 1:
                    _DataBaseServer = SelectedTreeNode.Parent.Tag as DataBaseServer;
                    using (var db = Utility.GetAdohelper(_DataBaseServer))
                    {
                        AddDataBaseObject(SelectedTreeNode, _DataBaseServer, db);
                        //SelectedTreeNode.Expand();
                    }
                    break;
                case 2:
                    _DataBaseServer = SelectedTreeNode.Parent.Parent.Tag as DataBaseServer;
                    using (var db = Utility.GetAdohelper(_DataBaseServer))
                    {
                        _DataBaseServer.AllObject = true;
                        if ((SelectedTreeNode.Tag.ToString()) == "table" | (SelectedTreeNode.Tag.ToString()) == "view")
                            AddDataBaseObjectItem(SelectedTreeNode, _DataBaseServer, db);
                        SelectedTreeNode.Expand();
                    }
                    break;
                case 3:
                    _DataBaseServer = SelectedTreeNode.Parent.Parent.Parent.Tag as DataBaseServer;
                    //using (var db = Utility.GetAdohelper(_DataBaseServer))
                    //{
                    //    _DataBaseServer.AllObject = true;
                    //    if ((SelectedTreeNode.Tag.ToString()) == "table" | (SelectedTreeNode.Tag.ToString()) == "view")
                    //        AddDataBaseObjectItem(SelectedTreeNode, _DataBaseServer, db);
                    //    SelectedTreeNode.Expand();
                    //}
                    break;
            }

            _MAINFORM.ConnectedDataBaseServer = _DataBaseServer;
            _MAINFORM.SetSysStatusInfo("完成");
            SystemDataBaseTree.Enabled = true;

        }

        public void RefreshTreeMenu()
        {
            this.LoadContextMenuItems();
        }

        public void AddDataBaseDefaultFuntions(DataBaseServer databaseServer)
        {
            string databaseType = databaseServer.ProviderName;
            switch (databaseType)
            {
                case "System.Data.Sql":
                case "System.Data.SQL":

                    Function sqlfun = new Function();
                    sqlfun.Name = "row_number";
                    sqlfun.Describtion = "获取数据行号";
                    databaseServer.Functions.Add(sqlfun);

                    sqlfun = new Function();
                    sqlfun.Name = "isnull";
                    sqlfun.Describtion = "";
                    databaseServer.Functions.Add(sqlfun);

                    sqlfun = new Function();
                    sqlfun.Name = "substring";                    
                    sqlfun.Describtion = "字符串处理函数";
                    databaseServer.Functions.Add(sqlfun);

                    sqlfun = new Function();
                    sqlfun.Name = "NEWID";
                    sqlfun.Describtion = "生成GUID函数";
                    databaseServer.Functions.Add(sqlfun);

                    break;
                case "System.Data.PostgreSql":

                    Function postgrefun = new Function();
                    postgrefun.Name = "row_number";
                    postgrefun.Describtion = "获取数据行号";
                    databaseServer.Functions.Add(postgrefun);

                    postgrefun = new Function();
                    postgrefun.Name = "cast";
                    postgrefun.Describtion = "数据转换如：cast(index as integer)";
                    databaseServer.Functions.Add(postgrefun);

                    postgrefun = new Function();
                    postgrefun.Name = "coalesce";
                    postgrefun.Describtion = "";
                    databaseServer.Functions.Add(postgrefun);

                    postgrefun = new Function();
                    postgrefun.Name = "concat";
                    postgrefun.Describtion = "字符串连接,例如：concat(字段名,',')";
                    databaseServer.Functions.Add(postgrefun);

                    postgrefun = new Function();
                    postgrefun.Name = "substring";
                    postgrefun.Describtion = "字符串处理函数";
                    databaseServer.Functions.Add(postgrefun);

                    postgrefun = new Function();
                    postgrefun.Name = "uuid_generate_v4";
                    postgrefun.Describtion = "生成UUID";
                    databaseServer.Functions.Add(postgrefun);

                    break;
            }

        }

        public void RefreshTreeMenu(System.Windows.Forms.ContextMenuStrip contextmenu)
        {
            this.contextmenu = contextmenu;
            this.LoadContextMenuItems();
        }

        public void AddDataBaseObject(NTreeNode ownerNode, DataBaseServer dbs, DataBase db)
        {
            DelegateAddTreeNode d = new DelegateAddTreeNode(AddTreeNode);

            DataTable dt = new DataTable();
            string collectionName = "table";
            if (ownerNode.Text == "数据表")
            {
                collectionName = "u";
                dbs.Tables = new List<Table>();
                dt = db.GetDataBaseObject(collectionName);
                foreach (DataRow dr in dt.Rows)
                {
                    string table_type = dr["table_type"] + "";
                    string table_name = dr["table_name"] + "";

                    bool matchSuccess = true;
                    if (!string.IsNullOrEmpty(Keywords) &&
                        table_name.IndexOf(Keywords,
                        StringComparison.OrdinalIgnoreCase) < 0)
                    {
                        matchSuccess = false;
                    }

                    if (matchSuccess)
                    {
                        NTreeNode node = new NTreeNode() { Text = table_name };
                        node.ImageIndex = 3;
                        node.Tag = "table";
                        Table t = new Table();
                        t.Name = table_name;
                        dbs.Tables.Add(t);
                        d.Invoke(ownerNode, node);
                        AddDataBaseObjectItem(node, dbs, db);
                    }
                   
                }
            }
            else if (ownerNode.Text == "数据视图")
            {
                collectionName = "v";
                dbs.Views = new List<DevelopAssistant.Service.View>();
                dt = db.GetDataBaseObject(collectionName);
                foreach (DataRow dr in dt.Rows)
                {
                    string view_type = dr["table_type"] + "";
                    string view_name = dr["table_name"] + "";

                    bool matchSuccess = true;
                    if (!string.IsNullOrEmpty(Keywords) &&
                        view_name.IndexOf(Keywords,
                        StringComparison.OrdinalIgnoreCase) < 0)
                    {
                        matchSuccess = false;
                    }

                    if (matchSuccess)
                    {
                        NTreeNode node = new NTreeNode() { Text = view_name };
                        node.ImageIndex = 4;
                        node.Tag = "view";
                        Service.View v = new Service.View();
                        v.Name = view_name;
                        dbs.Views.Add(v);
                        d.Invoke(ownerNode, node);
                        AddDataBaseObjectItem(node, dbs, db);
                    }
                   
                }
            }
            else if (ownerNode.Text == "函数")
            {
                collectionName = "fn";
                dbs.Functions = new List<Service.Function>();
                dt = db.GetDataBaseObject(collectionName);
                foreach (DataRow dr in dt.Rows)
                {
                    string function_type = dr["table_type"] + "";
                    string function_name = dr["table_name"] + "";

                    bool matchSuccess = true;
                    if (!string.IsNullOrEmpty(Keywords) &&
                        function_name.IndexOf(Keywords,
                        StringComparison.OrdinalIgnoreCase) < 0)
                    {
                        matchSuccess = false;
                    }

                    if (matchSuccess)
                    {
                        NTreeNode node = new NTreeNode() { Text = function_name };
                        node.ImageIndex = 10;
                        node.Tag = "function";
                        Service.Function f = new Service.Function();
                        f.Name = function_name;
                        f.Describtion = "返回类型：" + function_type + "\r\n" + "属于：" + dbs.DataBaseName;
                        DataTable pdt = db.GetTableObject(f.Name);
                        foreach (DataRow row in pdt.Rows)
                        {
                            SqlParameter p1 = new SqlParameter();
                            p1.Name = row["ColumnName"] + "";
                            p1.DataType = row["TypeName"] + "";
                            if (!string.IsNullOrEmpty(p1.Name))
                                f.Parameters.Add(p1);
                        }
                        dbs.Functions.Add(f);
                        d.Invoke(ownerNode, node);
                    }                    
                }

                AddDataBaseDefaultFuntions(dbs);

            }
            else if (ownerNode.Text == "存储过程")
            {
                collectionName = "p";
                dbs.Views = new List<DevelopAssistant.Service.View>();
                dt = db.GetDataBaseObject(collectionName);
                foreach (DataRow dr in dt.Rows)
                {
                    string procedure_type = dr["table_type"] + "";
                    string procedure_name = dr["table_name"] + "";

                    bool matchSuccess = true;
                    if (!string.IsNullOrEmpty(Keywords) &&
                        procedure_name.IndexOf(Keywords,
                        StringComparison.OrdinalIgnoreCase) < 0)
                    {
                        matchSuccess = false;
                    }

                    if (matchSuccess)
                    {
                        NTreeNode node = new NTreeNode() { Text = procedure_name };
                        node.ImageIndex = 6;
                        node.Tag = "procedure";
                        Service.Procedure p = new Service.Procedure();
                        p.Name = procedure_name;
                        dbs.Procedures.Add(p);
                        d.Invoke(ownerNode, node);
                    }

                }
            }           

        }

        public void AddDataBaseObjectItem(NTreeNode ownerNode, DataBaseServer dbs, DataBase db)
        {
            var dateItem = new DataObjectEnumerable();
            var dateTable = db.GetTableObject(ownerNode.Text.Trim());
            dateItem = dbs.Tables.Find(w => w.Name == ownerNode.Text.Trim());
            if (dateItem == null)
                dateItem = dbs.Views.Find(w => w.Name == ownerNode.Text.Trim());
            if (dateItem == null)
                return;

            dateItem.Columns = new List<Column>();

            if ((ownerNode.Tag + "") != "table" && (ownerNode.Tag + "") != "view")
                return;

            DelegateAddTreeNode d = new DelegateAddTreeNode(AddTreeNode);

            foreach (DataRow dr in dateTable.Rows)
            {
                string ColumnName = dr["ColumnName"] + "";
                string TypeName = dr["TypeName"] + "";
                string Length = dr["Length"] + "";
                string CisNull = dr["CisNull"] + "";

                bool matchSuccess = true;
                if (ownerNode.Level != 2 &&
                    !string.IsNullOrEmpty(Keywords) &&
                    ColumnName.IndexOf(Keywords,
                    StringComparison.OrdinalIgnoreCase) < 0)
                {
                    matchSuccess = false;
                }

                if (matchSuccess)
                {
                    bool is_pk = false;
                    if (CisNull.Contains("pk"))
                        is_pk = true;

                    Column c = new Column();
                    c.Name = ColumnName;
                    c.DataType = TypeName;
                    c.Len = Length;
                    c.Description = "数据类型：" + TypeName + "\r\n属于：" + ownerNode.Text;

                    if (dbs.AllObject)
                    {
                        string innerText = ColumnName;
                        string innerLength = Length;
                        if (TypeName != "int" && TypeName != "double"
                            && TypeName != "float" && TypeName != "decimal"
                            && TypeName != "uuid" && TypeName != "text"
                            && TypeName != "integer" && TypeName != "image"
                            && TypeName != "datetime")
                        {
                            innerLength = "(" + Length + ")";
                        }
                        else
                        {
                            innerLength = "";
                        }
                        innerText += "[" + "" + TypeName + "" + innerLength + "]";

                        NTreeNode node = new NTreeNode() { Text = innerText, Tag = ColumnName, ToolTipText = innerText };
                        node.Tag = "column";

                        if (is_pk)
                            node.ImageIndex = 15;
                        else
                            node.ImageIndex = 9;

                        d.Invoke(ownerNode, node);

                    }

                    dateItem.Columns.Add(c);
                }

            }

        }

        public void AddTreeNode(NTreeNode ownerNode, NTreeNode noneNode)
        {
            SystemDataBaseTree.Invoke(new MethodInvoker(delegate()
            {
                _MAINFORM.SetSysStatusInfo("添加" + noneNode.Text);  
                ownerNode.Nodes.Add(noneNode);                   
            }));           
        }

        public void SelectCurrentDataBaseServer(NTreeNode node)
        {
            switch (node.Level)
            {
                case 0: _MAINFORM.ConnectedDataBaseServer = (DataBaseServer)node.Tag; this.CurrentDatabaseServer = node; break;
                case 1: _MAINFORM.ConnectedDataBaseServer = (DataBaseServer)node.Parent.Tag; this.CurrentDatabaseServer = node.Parent; break;
                case 2: _MAINFORM.ConnectedDataBaseServer = (DataBaseServer)node.Parent.Parent.Tag; this.CurrentDatabaseServer = node.Parent.Parent; break;
                case 3: _MAINFORM.ConnectedDataBaseServer = (DataBaseServer)node.Parent.Parent.Parent.Tag; this.CurrentDatabaseServer = node.Parent.Parent.Parent; break;
                case 4: _MAINFORM.ConnectedDataBaseServer = (DataBaseServer)node.Parent.Parent.Parent.Parent.Tag; this.CurrentDatabaseServer = node.Parent.Parent.Parent.Parent; break;
            }
        }

        public void SetSelectedNode(NTreeNode node)
        {          
            this.SystemDataBaseTree.SelectedNode = node;
        }

        public DataBaseServer GetDataBaseServerByConnectionID(string id)
        {
            DataBaseServer _databaseServer = (DataBaseServer)this.CurrentDatabaseServer.Tag;

            foreach (NTreeNode servernode in SystemDataBaseTree.Nodes)
            {
                if (servernode.Level == 0 && ((DataBaseServer)servernode.Tag).ID == id)
                {
                    _databaseServer = (DataBaseServer)servernode.Tag;
                }
            }

            return _databaseServer;
        }

        public void SetTheme(string themeName)
        {
            Color backColor = SystemColors.Window;
            switch (themeName)
            {
                case "Default":
                    TagLabel.ForeColor = SystemColors.WindowText;
                    panel2.BackColor = SystemColors.Control;
                    backColor = SystemColors.Window;
                    break;
                case "Black":
                    TagLabel.ForeColor = SystemColors.Window;
                    panel2.BackColor = Color.FromArgb(045, 045, 048);
                    backColor = Color.FromArgb(045, 045, 048);
                    break;
            }
            this.BackColor = backColor;
            this.SystemDataBaseTree.SetTheme(themeName);
        }

        private void SystemDataBaseTree_BeforeExpand(object sender, NTreeViewEventArgs e)
        {
            var node = e.Node;
            if (node.Level == 0)
            {
                CurrentDatabaseServer = node;
                var dataBaseServer = CurrentDatabaseServer.Tag as DataBaseServer;
                if (!dataBaseServer.OnConnecting)
                {
                    bool bl = false;
                    string message = string.Empty;
                    dataBaseServer.OnConnecting = true;
                    SystemDataBaseTree.Enabled = false;
                    System.Threading.Thread th = new System.Threading.Thread(() =>
                    {
                        ConnectDataBase(dataBaseServer, ref bl, ref message);
                        this.Invoke(new MethodInvoker(delegate() {
                            SystemDataBaseTree.Enabled = true;
                            if (bl)
                                _MAINFORM.SetSysStatusInfo("完成");
                            else
                                _MAINFORM.SetSysStatusInfo(message);   
                        }));                                                    
                    });
                    th.IsBackground = true;
                    th.Start ();                 
                }
            }
        }

        //private void SystemDataBaseTree_MouseDoubleClick(object sender, MouseEventArgs e)
        //{
        //    NTreeNode mouseclick_node = SystemDataBaseTree.GetNodeAt(new Point(e.X, e.Y));
        //    if (mouseclick_node != null && mouseclick_node.Level == 2)
        //    {
        //        if (((mouseclick_node.Tag + "") == "table" || (mouseclick_node.Tag+"") == "view")
        //            && mouseclick_node.Nodes.Count < 1)
        //        {
        //            DataBaseServer dataBaseServer = (DataBaseServer)_MAINFORM.ConnectedDataBaseServer.Clone();
        //            dataBaseServer.AllObject = true;
        //            AddDataBaseObjectItem(mouseclick_node, dataBaseServer, DevelopAssistant.Service.Utility.GetAdohelper(_MAINFORM.ConnectedDataBaseServer));
        //            _MAINFORM.SetSysStatusInfo("完成");
        //            mouseclick_node.Expand(); 
        //        }
        //    }
        //}

        private void SystemDataBaseTree_NodeMouseDoubleClick(object sender, NTreeNodeMouseEventArgs e)
        {
            NTreeNode mouseclick_node = e.Node;
            if (mouseclick_node != null && mouseclick_node.Level == 2)
            {
                if (((mouseclick_node.Tag + "") == "table" || (mouseclick_node.Tag + "") == "view")
                    && mouseclick_node.Nodes.Count < 1)
                {
                    DataBaseServer dataBaseServer = (DataBaseServer)_MAINFORM.ConnectedDataBaseServer.Clone();
                    dataBaseServer.AllObject = true;
                    AddDataBaseObjectItem(mouseclick_node, dataBaseServer, DevelopAssistant.Service.Utility.GetAdohelper(_MAINFORM.ConnectedDataBaseServer));
                    _MAINFORM.SetSysStatusInfo("完成");
                    mouseclick_node.Expand();
                    e.Handled = true; 
                }
            }
        }

        private void btn_database_refresh_Click(object sender, EventArgs e)
        {
            RefreshDataBase();
        }

        private void btnfilter_Click(object sender, EventArgs e)
        {
            TreeViewFilter filter = new TreeViewFilter(_MAINFORM, this.Keywords);
            if (filter.ShowDialog(_MAINFORM).Equals(DialogResult.OK))
            {
                this.Keywords = filter.Keywords;
                RefreshDataBase();
            }

        }

    }
}
