﻿namespace DevelopAssistant.Core.ToolBar
{
    partial class TodoBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TodoBar));
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.TimelineControl1 = new ICSharpCode.WinFormsUI.Controls.NTimeLine.NTimelineControl();
            this.HeadPanel = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.lblRecords = new System.Windows.Forms.Label();
            this.btn_todolist_refresh = new ICSharpCode.WinFormsUI.Controls.NImageButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.HeadPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.TimelineControl1);
            this.panel1.Controls.Add(this.HeadPanel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(221, 459);
            this.panel1.TabIndex = 0;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // TimelineControl1
            // 
            this.TimelineControl1.BackColor = System.Drawing.Color.White;
            this.TimelineControl1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.TimelineControl1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.TimelineControl1.BottomBlackColor = System.Drawing.Color.Empty;
            this.TimelineControl1.DataList = ((System.Collections.Generic.List<ICSharpCode.WinFormsUI.Controls.NTimeLine.MonthItem>)(resources.GetObject("TimelineControl1.DataList")));
            this.TimelineControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TimelineControl1.IsEditModel = true;
            this.TimelineControl1.Location = new System.Drawing.Point(0, 30);
            this.TimelineControl1.MarginWidth = 0;
            this.TimelineControl1.Name = "TimelineControl1";
            this.TimelineControl1.Size = new System.Drawing.Size(221, 429);
            this.TimelineControl1.TabIndex = 1;
            this.TimelineControl1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // HeadPanel
            // 
            this.HeadPanel.BackColor = System.Drawing.SystemColors.Control;
            this.HeadPanel.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.HeadPanel.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Bottom;
            this.HeadPanel.BottomBlackColor = System.Drawing.Color.Empty;
            this.HeadPanel.Controls.Add(this.lblRecords);
            this.HeadPanel.Controls.Add(this.btn_todolist_refresh);
            this.HeadPanel.Controls.Add(this.label1);
            this.HeadPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeadPanel.Location = new System.Drawing.Point(0, 0);
            this.HeadPanel.MarginWidth = 0;
            this.HeadPanel.Name = "HeadPanel";
            this.HeadPanel.Size = new System.Drawing.Size(221, 30);
            this.HeadPanel.TabIndex = 0;
            this.HeadPanel.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // lblRecords
            // 
            this.lblRecords.AutoSize = true;
            this.lblRecords.BackColor = System.Drawing.Color.Transparent;
            this.lblRecords.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRecords.ForeColor = System.Drawing.Color.Blue;
            this.lblRecords.Location = new System.Drawing.Point(85, 9);
            this.lblRecords.Name = "lblRecords";
            this.lblRecords.Size = new System.Drawing.Size(71, 18);
            this.lblRecords.TabIndex = 1;
            this.lblRecords.Text = "共 0 项";
            this.lblRecords.Click += new System.EventHandler(this.lblRecords_Click);
            // 
            // btn_todolist_refresh
            // 
            this.btn_todolist_refresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_todolist_refresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_todolist_refresh.Image = global::DevelopAssistant.Core.Properties.Resources.recovery;
            this.btn_todolist_refresh.Location = new System.Drawing.Point(196, 5);
            this.btn_todolist_refresh.Name = "btn_todolist_refresh";
            this.btn_todolist_refresh.Size = new System.Drawing.Size(22, 22);
            this.btn_todolist_refresh.TabIndex = 1;
            this.btn_todolist_refresh.ToolTipText = "刷新";
            this.btn_todolist_refresh.Click += new System.EventHandler(this.btn_todolist_refresh_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(4, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "事项列表：";
            // 
            // TodoBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(221, 459);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TodoBar";
            this.Text = "待办事项";
            this.Load += new System.EventHandler(this.TodoBar_Load);
            this.panel1.ResumeLayout(false);
            this.HeadPanel.ResumeLayout(false);
            this.HeadPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private ICSharpCode.WinFormsUI.Controls.NPanel HeadPanel;
        private System.Windows.Forms.Label label1;
        private ICSharpCode.WinFormsUI.Controls.NImageButton btn_todolist_refresh;
        private ICSharpCode.WinFormsUI.Controls.NTimeLine.NTimelineControl TimelineControl1;
        private System.Windows.Forms.Label lblRecords;
    }
}