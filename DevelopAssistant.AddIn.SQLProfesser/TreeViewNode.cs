﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.AddIn.SQLProfesser
{
    public class TreeViewNode : System.Windows.Forms.TreeNode
    {
        public int NodeId { get; set; }
        public string Server { get; set; }
        public string UserID { get; set; }
        public string Password { get; set; }
        public string ConfigureDataBase { get; set; }
        public string MeasureDataBase { get; set; }

    }
}
