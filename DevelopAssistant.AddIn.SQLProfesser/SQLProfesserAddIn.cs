﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevelopAssistant.AddIn.SQLProfesser.Properties;
using DevelopAssistant.AddIn; 
using ICSharpCode.WinFormsUI.Forms;
using DevelopAssistant.Service; 

namespace DevelopAssistant.AddIn.SQLProfesser
{
    public class SQLProfesserAddIn : DockContentAddIn 
    {
        public SQLProfesserAddIn()
        {
            this.IdentityID = "f6344f39-d10c-4c75-9863-ec5cbbb2a1ee";
            this.Name = "数据维护专家";
            this.Text = "数据维护专家";
            this.Tooltip = "数据维护专家";            
            this.Icon = Resources.plugin;
        }

        public override void Initialize(string AssemblyName, string Winname)
        {
            //
        }

        public override object Execute(params object[] Parameter)
        {
            MainForm f = new MainForm((BaseForm)Parameter[0], (ICSharpCode.WinFormsUI.Docking.DockPanel)Parameter[1])
            {
                //WindowFloat = new DelegateWindowFloatHandler(FloatParentCenter) 
            };         
            return f;             
        }
    }
}
