﻿using System;

namespace ICSharpCode.WinFormsUI.Docking
{
    using ICSharpCode.WinFormsUI.Theme;
    using System.ComponentModel;

    public partial class DockPanel
    {
        private DockPanelSkin m_dockPanelSkin = VS2003Theme.CreateVisualStudio2003();
        [LocalizedCategory("Category_Docking")]
        [LocalizedDescription("DockPanel_DockPanelSkin")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //[Obsolete("Please use Theme instead.")]
        [Browsable(false)]
        public DockPanelSkin Skin
        {
            get { return m_dockPanelSkin;  }
            set { m_dockPanelSkin = value; }
        }

        private DockPanelThemeBase m_dockPanelTheme = new DefaultTheme();
        [LocalizedCategory("Category_Docking")]
        [LocalizedDescription("DockPanel_DockPanelTheme")]
        public DockPanelThemeBase DockPanelTheme
        {
            get { return m_dockPanelTheme; }
            set
            {
                if (value == null)
                {
                    return;
                }

                if (m_dockPanelTheme.GetType() 
                    == value.GetType())
                {
                    return;
                }

                m_dockPanelTheme = value;
                m_dockPanelTheme.Apply(this);
            }
        }

        private WinFormsUIThemeBase _windowTheme;
        [LocalizedCategory("Category_Docking")]
        [LocalizedDescription("MainForm_WindowTheme")]
        public WinFormsUIThemeBase WindowTheme
        {
            get { return _windowTheme; }
            set { _windowTheme = value; }
        }

    }
}
