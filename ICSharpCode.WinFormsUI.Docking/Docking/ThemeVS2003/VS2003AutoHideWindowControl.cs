﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Docking
{
    public class VS2003AutoHideWindowControl : ICSharpCode.WinFormsUI.Docking.DockPanel.AutoHideWindowControl
    {
        private class VS2003BlueAutoHideWindowSplitterControl : SplitterBase
        {
            public VS2003BlueAutoHideWindowSplitterControl(DockPanel.AutoHideWindowControl autoHideWindow)
            {
                AutoHideWindow = autoHideWindow;
            }

            private DockPanel.AutoHideWindowControl AutoHideWindow { get; set; }

            protected override int SplitterSize
            {
                get { return Measures.SplitterSize; }
            }

            protected override void StartDrag()
            {
                AutoHideWindow.DockPanel.BeginDrag(AutoHideWindow, AutoHideWindow.RectangleToScreen(Bounds));
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                base.OnPaint(e);

                Rectangle rect = ClientRectangle;

                if (rect.Width <= 0 || rect.Height <= 0)
                    return;

                Color backColor = SystemColors.Control;
                switch (DockPanelThemeStyle.ColorStyle.Name)
                {
                    case "Default":
                        backColor = SystemColors.Control;
                        break;
                    case "Black":
                        backColor = Color.FromArgb(045, 045, 048);
                        break;
                }
                e.Graphics.FillRectangle(new SolidBrush(backColor), rect);

                Pen pen = new Pen(SystemColors.ControlDark);
                switch (AutoHideWindow.DockState)
                {
                    case DockState.DockLeftAutoHide:
                        e.Graphics.DrawLine(pen, new Point(0, 0), new Point(0, rect.Height));
                        e.Graphics.DrawLine(pen, new Point(rect.Width - 1, 0), new Point(rect.Width - 1, rect.Height));
                        break;
                    case DockState.DockRightAutoHide:
                        e.Graphics.DrawLine(pen, new Point(0, 0), new Point(0, rect.Height));
                        e.Graphics.DrawLine(pen, new Point(rect.Width - 1, 0), new Point(rect.Width - 1, rect.Height));
                        break;
                    case DockState.DockTopAutoHide:
                        e.Graphics.DrawLine(pen, new Point(0, rect.Height - 1), new Point(1, rect.Height - 1));
                        break;
                    case DockState.DockBottomAutoHide:
                        e.Graphics.DrawLine(pen, new Point(0, 0), new Point(rect.Width, 0));
                        break;
                }
                pen.Dispose();
            }

        }

        public VS2003AutoHideWindowControl(DockPanel dockPanel)
            : base(dockPanel)
        {
            m_splitter = new VS2003BlueAutoHideWindowSplitterControl(this); //new SplitterControl(this);
            Controls.Add(m_splitter);
        }

        protected override Rectangle DisplayingRectangle
        {
            get
            {
                Rectangle rect = ClientRectangle;

                // exclude the border and the splitter
                if (DockState == DockState.DockBottomAutoHide)
                {
                    rect.Y += Measures.SplitterSize;
                    rect.Height -= Measures.SplitterSize;
                }
                else if (DockState == DockState.DockRightAutoHide)
                {
                    rect.X += Measures.SplitterSize;
                    rect.Width -= Measures.SplitterSize;
                }
                else if (DockState == DockState.DockTopAutoHide)
                    rect.Height -= Measures.SplitterSize;
                else if (DockState == DockState.DockLeftAutoHide)
                    rect.Width -= Measures.SplitterSize;

                return rect;
            }
        }

        protected override void OnLayout(LayoutEventArgs levent)
        {
            DockPadding.All = 0;
            if (DockState == DockState.DockLeftAutoHide)
            {
                //DockPadding.Right = 2;
                m_splitter.Dock = DockStyle.Right;
            }
            else if (DockState == DockState.DockRightAutoHide)
            {
                //DockPadding.Left = 2;
                m_splitter.Dock = DockStyle.Left;
            }
            else if (DockState == DockState.DockTopAutoHide)
            {
                //DockPadding.Bottom = 2;
                m_splitter.Dock = DockStyle.Bottom;
            }
            else if (DockState == DockState.DockBottomAutoHide)
            {
                //DockPadding.Top = 2;
                m_splitter.Dock = DockStyle.Top;
            }

            Rectangle rectDisplaying = DisplayingRectangle;
            Rectangle rectHidden = new Rectangle(-rectDisplaying.Width, rectDisplaying.Y, rectDisplaying.Width, rectDisplaying.Height);
            foreach (Control c in Controls)
            {
                DockPane pane = c as DockPane;
                if (pane == null)
                    continue;


                if (pane == ActivePane)
                    pane.Bounds = rectDisplaying;
                else
                    pane.Bounds = rectHidden;
            }

            base.OnLayout(levent);
        }
    }
}
