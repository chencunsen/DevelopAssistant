﻿using System;
using System.Text; 

namespace DevelopAssistant.Common
{
    public class NLogger
    {
        private static object lockedobj = new object();
        private static string _baseDirectory = string.Empty;
        public static string BaseDirectory
        {
            get
            {
                if (string.IsNullOrEmpty(_baseDirectory))
                    _baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    //throw new Exception("请设置日志文件存放的路径");
                return _baseDirectory;
            }
            set
            {
                if (string.IsNullOrEmpty(_baseDirectory))
                    _baseDirectory = value;
            }
        }

        public static bool WriteToLine(string Message, string Tag, DateTime Time, string Source)
        {
            return WriteToLine(Message, Tag, Time, Source, string.Empty);
        }

        public static bool WriteToLine(string Message, string Tag, DateTime Time, string Source, string Trace)
        {
            bool rvl = false;
            lock (lockedobj)
            {
                string logfile = DateTime.Now.ToString("yyyyMMdd");
                string logpathYear = DateTime.Now.Year.ToString("0000");
                string logpathMonth = DateTime.Now.ToString("yyyyMM");

                if (string.IsNullOrEmpty(BaseDirectory))
                    BaseDirectory = AppDomain.CurrentDomain.BaseDirectory;

                try
                {
                    if (!System.IO.Directory.Exists(BaseDirectory + "//Logger"))
                        System.IO.Directory.CreateDirectory(BaseDirectory + "//Logger");

                    if (!System.IO.Directory.Exists(BaseDirectory + "//Logger//" + logpathYear))
                        System.IO.Directory.CreateDirectory(BaseDirectory + "//Logger//" + logpathYear);

                    if (!System.IO.Directory.Exists(BaseDirectory + "//Logger//" + logpathYear + "//" + logpathMonth))
                        System.IO.Directory.CreateDirectory(BaseDirectory + "//Logger//" + logpathYear + "//" + logpathMonth);

                    string txtPath = BaseDirectory + "//Logger//" + logpathYear + "//" + logpathMonth + "//" + logfile + ".txt";
                    if (!System.IO.File.Exists(txtPath))
                    {
                        using (System.IO.FileStream fs = System.IO.File.Create(txtPath))
                        {
                            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(fs, System.Text.Encoding.UTF8))
                            {
                                sw.WriteLine("日志文件创建于：{0} By {1}", DateTime.Now.ToString("yyyy年MM月dd日hh时mm分ss秒"), "开发助手日志记录");
                                sw.WriteLine("-------------------------------------------------------------------");
                                sw.Flush();
                            }
                            fs.Close();
                        }
                        WriteToLine(Message, Tag, Time, Source, Trace);
                    }
                    else
                    {
                        using (System.IO.StreamWriter sw = System.IO.File.AppendText(txtPath))
                        {
                            sw.WriteLine("");
                            sw.WriteLine("" + Tag + "信息: {0}", Message.Trim());
                            sw.WriteLine("" + Tag + "时间: {0}", Time.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                            if (!string.IsNullOrEmpty(Source))
                            {
                                sw.WriteLine("信息来源: {0}", Source.Trim());
                            }
                            if (!string.IsNullOrEmpty(Trace))
                            {
                                sw.WriteLine("" + Tag + "跟踪: {0}", Trace.Trim());
                            }
                            sw.WriteLine("-------------------------------------------------------------------");
                            sw.Flush();
                        }
                    }
                }
                catch (Exception ex)
                {
                    rvl = false;
                }
            }
            return rvl;
        }

        public static bool LogDebug(string message,params string[] args)
        {
            return WriteToLine(string.Format(message, args), "调试", DateTime.Now, "", "");
        }

        public static bool LogError(Exception ex)
        {
            return WriteToLine(ex.Message, "错误", DateTime.Now, ex.Source, ex.StackTrace);
        }
    }
}
