﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
 

namespace DevelopAssistant.Common
{
    public class UnrepeatableList<T> : List<T>
    {
        public new void Add(T item)
        {
            if (!Contains(item))
            {
                base.Add(item);
            }
        }
    }
}
