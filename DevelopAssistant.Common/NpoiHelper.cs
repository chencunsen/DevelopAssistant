﻿using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.OpenXmlFormats.Wordprocessing;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using NPOI.XWPF.UserModel;

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text; 
using System.Web;

namespace DevelopAssistant.Common
{
    public class NpoiHelper
    {
        #region NPIO 操作Excel

        private static string DecimalToFormat(string value)
        {
            if (!string.IsNullOrEmpty(value) && value != "0")
            {
                value = Math.Round(decimal.Parse(value), 2).ToString("0.00");
                if (value.Equals("0.00"))
                {
                    value = "≈0";
                }
            }
            return value;
        }

        private static string PercentageToFormat(string v, string value)
        {
            if (!string.IsNullOrEmpty(v) && v != "0")
            {
                if (!string.IsNullOrEmpty(value))
                {
                    value = Math.Round((decimal.Parse(value) * 100), 2).ToString("0.00");
                    if (value.Equals("0.00"))
                    {
                        value = "≈0";
                    }
                    else
                    {
                        value = value + "%";
                    }
                }
            }
            else
            {
                value = "0%";
            }
            return value;
        }

        private static string GetSheetColumnName(int num)
        {
            Dictionary<int, string> di = new Dictionary<int, string>();
            di.Add(0, "A");
            di.Add(1, "B");
            di.Add(2, "C");
            di.Add(3, "D");
            di.Add(4, "E");
            di.Add(5, "F");
            di.Add(6, "G");
            di.Add(7, "H");
            di.Add(8, "I");
            di.Add(9, "J");
            di.Add(10, "K");
            di.Add(11, "L");
            di.Add(12, "M");
            di.Add(13, "N");
            di.Add(14, "O");
            di.Add(15, "P");
            di.Add(16, "Q");
            di.Add(17, "R");
            di.Add(18, "S");
            di.Add(19, "T");
            di.Add(20, "U");
            di.Add(21, "V");
            di.Add(22, "W");
            di.Add(23, "X");
            di.Add(24, "Y");
            di.Add(25, "Z");

            return di[num];
        }

        private static string AddDataColumn(DataTable dt, string columnname, ref int index,ref bool goon)
        {
            string val = columnname;                   
            if (dt.Columns != null && dt.Columns.Count > 0 && dt.Columns.Contains(columnname))
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.ColumnName.Equals(columnname))
                    {
                        index++;
                    }
                }
            }
            if (index > 0)
            {
                if (columnname.Contains("["))
                    columnname = columnname.Substring(0, columnname.LastIndexOf("["));
                val = columnname+"[" + index+"]";
            }

            #region 递规调用
            if (!dt.Columns.Contains(val))
            {
                goon = false;
                dt.Columns.Add(val, typeof(string));
            }
            while (goon)
            {               
                AddDataColumn(dt, val, ref index, ref goon);
            }
            #endregion

            return val;
        }

        private static Dictionary<string, ICellStyle> CreateSheetCellStyles(HSSFWorkbook Workbook)
        {
            Dictionary<string, ICellStyle> CellStyles = new Dictionary<string, ICellStyle>();

            IFont Font = Workbook.CreateFont();

            ICellStyle cellcstyle = Workbook.CreateCellStyle();
            cellcstyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
            cellcstyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
            cellcstyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            cellcstyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            cellcstyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            cellcstyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            cellcstyle.BottomBorderColor = HSSFColor.Black.Index;
            cellcstyle.LeftBorderColor = HSSFColor.Black.Index;
            cellcstyle.RightBorderColor = HSSFColor.Black.Index;
            cellcstyle.TopBorderColor = HSSFColor.Black.Index;
            cellcstyle.WrapText = true;

            Font = Workbook.CreateFont();            
            Font.FontHeightInPoints = 11;
            cellcstyle.SetFont(Font);

            CellStyles.Add("BodyCell", cellcstyle);

            ICellStyle cellcbstyle = Workbook.CreateCellStyle();
            cellcbstyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
            cellcbstyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
            cellcbstyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            cellcbstyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            cellcbstyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            cellcbstyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            cellcbstyle.BottomBorderColor = HSSFColor.Black.Index;
            cellcbstyle.LeftBorderColor = HSSFColor.Black.Index;
            cellcbstyle.RightBorderColor = HSSFColor.Black.Index;
            cellcbstyle.TopBorderColor = HSSFColor.Black.Index;
            cellcbstyle.WrapText = true;

            Font = Workbook.CreateFont();
            Font.Boldweight = (short)FontBoldWeight.Bold;
            cellcbstyle.SetFont(Font);

            CellStyles.Add("BodyBoldCell", cellcbstyle);

            ICellStyle cellhstyle = Workbook.CreateCellStyle();
            cellhstyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
            cellhstyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
            cellhstyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            cellhstyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            cellhstyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            cellhstyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            cellhstyle.BottomBorderColor = HSSFColor.Black.Index;
            cellhstyle.LeftBorderColor = HSSFColor.Black.Index;
            cellhstyle.RightBorderColor = HSSFColor.Black.Index;
            cellhstyle.TopBorderColor = HSSFColor.Black.Index;

            Font = Workbook.CreateFont();
            //Font.Color = HSSFColor.RED.index;
            Font.Boldweight = (short)FontBoldWeight.Bold;
            Font.FontHeightInPoints = 10;
            cellhstyle.SetFont(Font);

            cellhstyle.FillForegroundColor = HSSFColor.PaleBlue.Index; //GetNPOIColor(Workbook, System.Drawing.Color.Orange); //HSSFColor.YELLOW.index;
            cellhstyle.FillBackgroundColor = HSSFColor.PaleBlue.Index; //GetNPOIColor(Workbook, System.Drawing.Color.YellowGreen);//HSSFColor.YELLOW.index;
            cellhstyle.FillPattern = FillPattern.SolidForeground;

            CellStyles.Add("HeadCell", cellhstyle);

            ICellStyle celltstyle = Workbook.CreateCellStyle();
            celltstyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Right;
            celltstyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;

            CellStyles.Add("DateCell", celltstyle);

            ICellStyle cellttstyle = Workbook.CreateCellStyle();
            cellttstyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
            cellttstyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;

            Font = Workbook.CreateFont();
            Font.Boldweight = (short)FontBoldWeight.Bold;
            Font.FontHeightInPoints = 12;
            cellttstyle.SetFont(Font);
            CellStyles.Add("TitleCell", cellttstyle);

            return CellStyles;
        }

        private static ICellStyle CreateSheetCellStyle(HSSFWorkbook Workbook)
        {
            ICellStyle CellStyle = Workbook.CreateCellStyle();

            CellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
            CellStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
            CellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            CellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            CellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            CellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            CellStyle.BottomBorderColor = HSSFColor.Black.Index;
            CellStyle.LeftBorderColor = HSSFColor.Black.Index;
            CellStyle.RightBorderColor = HSSFColor.Black.Index;
            CellStyle.TopBorderColor = HSSFColor.Black.Index;
            CellStyle.WrapText = true;

            return CellStyle;
        }

        private static ICellStyle CellStyleClone(HSSFWorkbook Workbook, ICellStyle CellStyle)
        {
            ICellStyle _cellstyle = Workbook.CreateCellStyle();
            PropertyInfo[] properties = _cellstyle.GetType().GetProperties();

            foreach (PropertyInfo fi in properties)
            {
                object obj = fi.GetValue(CellStyle);
                if (obj != null && obj.GetType().Equals(fi.PropertyType) && fi.CanWrite)
                {
                    fi.SetValue(_cellstyle, obj, null);
                }
            }

            _cellstyle.SetFont(CellStyle.GetFont(Workbook));

            return _cellstyle;
        }

        private static short GetNPOIColor(HSSFWorkbook Workbook, System.Drawing.Color SystemColor)
        {
            short s = 0;
            HSSFPalette XlPalette = Workbook.GetCustomPalette();
            HSSFColor XlColour = XlPalette.FindColor(SystemColor.R, SystemColor.G, SystemColor.B);
            if (XlColour == null)
            {
                if (NPOI.HSSF.Record.PaletteRecord.STANDARD_PALETTE_SIZE < 255)
                {
                    //if (NPOI.HSSF.Record.PaletteRecord.STANDARD_PALETTE_SIZE < 64)
                    //{
                    //    //NPOI.HSSF.Record.PaletteRecord.STANDARD_PALETTE_SIZE = 64;
                    //    //NPOI.HSSF.Record.PaletteRecord.STANDARD_PALETTE_SIZE = 65;
                    //    XlColour = XlPalette.AddColor(SystemColor.R, SystemColor.G, SystemColor.B);
                    //}
                    //else
                    //{
                    //    XlColour = XlPalette.FindSimilarColor(SystemColor.R, SystemColor.G, SystemColor.B);
                    //}
                    XlColour = XlPalette.AddColor(SystemColor.R, SystemColor.G, SystemColor.B);
                    s = XlColour.Indexed;
                }
            }
            else
            {
                s = XlColour.Indexed;
            }
            return s;
        }
        
        /// <summary>
        /// 支持前7列可合并单元格
        /// </summary>
        /// <param name="TemplatePath"></param>
        /// <param name="Merge"></param>
        /// <param name="DataSource"></param>
        /// <param name="Foot"></param>
        /// <returns></returns>
        private static HSSFWorkbook CreateNPIOExcel(String TemplatePath,bool Merge, DataTable DataSource, DataTable Foot)
        {
            string filetype;

            HSSFWorkbook Workbook = (HSSFWorkbook)TemplateNPIOExcel(TemplatePath, out filetype);
            ISheet Sheet1 = Workbook.GetSheetAt(0);

            #region 初始化
            ////create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "上海合驿物流有限公司";
            Workbook.DocumentSummaryInformation = dsi;

            ////create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "报表导出";
            Workbook.SummaryInformation = si;
            #endregion

            #region 创建样式

            ICellStyle body_cellstyle = CreateSheetCellStyle(Workbook);
            ICellStyle bodybold_cellstyle = CellStyleClone(Workbook, body_cellstyle);  //CellStyles["BodyBoldCell"];//内容加粗样式
            IFont Font = Workbook.CreateFont();
            Font.Boldweight = (short)FontBoldWeight.Bold;
            bodybold_cellstyle.SetFont(Font);
            ICellStyle footbold_cellstyle = CellStyleClone(Workbook, body_cellstyle); //CellStyles["RatioCell"];//表尾百分比样式
            footbold_cellstyle.SetFont(Font);
            footbold_cellstyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("0.00%");

            #endregion

            int rowIndex = 0;
            int colIndex = 0;

            System.Collections.IEnumerator rows = Sheet1.GetRowEnumerator();
            while (rows.MoveNext())
            {
                rowIndex++;
            }           

            IDataFormat datastyle = Workbook.CreateDataFormat();

            int _rowNumber = 1; int _rowTotal = DataSource.Rows.Count;

            DataRow _temprow = null;

            foreach (DataRow dr in DataSource.Rows)
            {
                IRow row = Sheet1.CreateRow(rowIndex);
                colIndex = 0;                

                foreach (DataColumn dc in DataSource.Columns)
                {
                    NPOI.SS.UserModel.ICell cell = row.CreateCell(colIndex);
                    cell.CellStyle = body_cellstyle;

                    string colname = dc.ColumnName;
                    string value = dr[colname] + "";

                    switch (dc.DataType.Name)
                    {
                        case "DateTime":
                            if (!string.IsNullOrEmpty(value))
                            {
                                value = DateTime.Parse(value).ToString("yyyy-MM-dd");
                                cell.SetCellValue(value);
                            }
                            break;
                        case "Int16":
                        case "Int32":
                        case "Int64":
                            if (!string.IsNullOrEmpty(value))
                                cell.SetCellValue(Int32.Parse(value));
                            break;
                        case "Single":
                        case "Double":
                        case "Decimal":
                            if (colname.Contains("比例") || colname.Contains("占比") || colname.Contains("百分比"))
                            {
                                cell.SetCellValue(PercentageToFormat("23", value));
                            }
                            else if (!string.IsNullOrEmpty(value))
                            {
                                cell.SetCellValue(Math.Round(double.Parse(value), 2));//DecimalToFormat(value);
                            }
                            break;
                        case "byte[]":
                        case "Byte[]":
                            if (dr[colname] != null && dr[colname] != DBNull.Value)
                            {
                                StringBuilder sb = new StringBuilder();
                                Byte[] array = (Byte[])dr[colname];
                                int maxlen = array.Length;
                                if (maxlen > 16)
                                {
                                    maxlen = 16;
                                }
                                for (int i = 0; i < maxlen; i++)
                                {
                                    sb.Append(array[i].ToString("X2"));
                                }
                                value = "Ox" + sb.ToString();
                                maxlen = array.Length;
                                if (maxlen > 16)
                                {
                                    value += "...";
                                }
                                cell.SetCellValue(value);
                            }
                            break;
                        default:
                            cell.SetCellValue(value);
                            break;
                    }

                    if (Merge)
                    {
                        if (_temprow != null && dc.DataType.Name.Equals("String") && (_temprow[colname] + "").Equals(value))
                        {
                            switch (colIndex)
                            {
                                case 0:
                                    Sheet1.AddMergedRegion(new CellRangeAddress(rowIndex - 1, rowIndex, colIndex, colIndex));
                                    break;
                                case 1:
                                    if ((_temprow[colIndex - 1] + "").Equals(dr[colIndex - 1] + ""))
                                    {
                                        Sheet1.AddMergedRegion(new CellRangeAddress(rowIndex - 1, rowIndex, colIndex, colIndex));
                                    }
                                    break;
                                case 2:
                                    if ((_temprow[colIndex - 1] + "").Equals(dr[colIndex - 1] + "") 
                                        && (_temprow[colIndex - 2] + "").Equals(dr[colIndex - 2] + ""))
                                    {
                                        Sheet1.AddMergedRegion(new CellRangeAddress(rowIndex - 1, rowIndex, colIndex, colIndex));
                                    }
                                    break;
                                case 3:
                                    if ((_temprow[colIndex - 1] + "").Equals(dr[colIndex - 1] + "")
                                        && (_temprow[colIndex - 2] + "").Equals(dr[colIndex - 2] + "")
                                         && (_temprow[colIndex - 3] + "").Equals(dr[colIndex - 3] + ""))
                                    {
                                        Sheet1.AddMergedRegion(new CellRangeAddress(rowIndex - 1, rowIndex, colIndex, colIndex));
                                    }
                                    break;
                                case 4:
                                    if ((_temprow[colIndex - 1] + "").Equals(dr[colIndex - 1] + "") 
                                        && (_temprow[colIndex - 2] + "").Equals(dr[colIndex - 2] + "")
                                         && (_temprow[colIndex - 3] + "").Equals(dr[colIndex - 3] + "")
                                          && (_temprow[colIndex - 4] + "").Equals(dr[colIndex - 4] + ""))
                                    {
                                        Sheet1.AddMergedRegion(new CellRangeAddress(rowIndex - 1, rowIndex, colIndex, colIndex));
                                    }
                                    break;
                                case 5:
                                    if ((_temprow[colIndex - 1] + "").Equals(dr[colIndex - 1] + "")
                                        && (_temprow[colIndex - 2] + "").Equals(dr[colIndex - 2] + "")
                                         && (_temprow[colIndex - 3] + "").Equals(dr[colIndex - 3] + "")
                                          && (_temprow[colIndex - 4] + "").Equals(dr[colIndex - 4] + "")
                                           && (_temprow[colIndex - 5] + "").Equals(dr[colIndex - 5] + ""))
                                    {
                                        Sheet1.AddMergedRegion(new CellRangeAddress(rowIndex - 1, rowIndex, colIndex, colIndex));
                                    }
                                    break;
                                case 6:
                                    if ((_temprow[colIndex - 1] + "").Equals(dr[colIndex - 1] + "")
                                        && (_temprow[colIndex - 2] + "").Equals(dr[colIndex - 2] + "")
                                         && (_temprow[colIndex - 3] + "").Equals(dr[colIndex - 3] + "")
                                          && (_temprow[colIndex - 4] + "").Equals(dr[colIndex - 4] + "")
                                           && (_temprow[colIndex - 5] + "").Equals(dr[colIndex - 5] + "")
                                            && (_temprow[colIndex - 6] + "").Equals(dr[colIndex - 6] + ""))
                                    {
                                        Sheet1.AddMergedRegion(new CellRangeAddress(rowIndex - 1, rowIndex, colIndex, colIndex));
                                    }
                                    break;
                                case 7:
                                    if ((_temprow[colIndex - 1] + "").Equals(dr[colIndex - 1] + "")
                                        && (_temprow[colIndex - 2] + "").Equals(dr[colIndex - 2] + "")
                                         && (_temprow[colIndex - 3] + "").Equals(dr[colIndex - 3] + "")
                                          && (_temprow[colIndex - 4] + "").Equals(dr[colIndex - 4] + "")
                                           && (_temprow[colIndex - 5] + "").Equals(dr[colIndex - 5] + "")
                                            && (_temprow[colIndex - 6] + "").Equals(dr[colIndex - 6] + "")
                                              && (_temprow[colIndex - 7] + "").Equals(dr[colIndex - 7] + ""))
                                    {
                                        Sheet1.AddMergedRegion(new CellRangeAddress(rowIndex - 1, rowIndex, colIndex, colIndex));
                                    }
                                    break;
                            }
  
                        }
                    }

                    colIndex++;
                }

                if (Merge)
                {
                    _temprow = dr;
                }

                rowIndex++;
                _rowNumber++;

            }

            if (Foot != null)
            {
                foreach (DataRow dr in Foot.Rows)
                {
                    IRow row = Sheet1.CreateRow(rowIndex);
                    colIndex = 0;
                    row.Height = 20 * 20;

                    foreach (DataColumn dc in Foot.Columns)
                    {
                        NPOI.SS.UserModel.ICell cell = row.CreateCell(colIndex);
                        cell.CellStyle = bodybold_cellstyle;

                        string colname = dc.ColumnName;
                        string value = dr[colname] + "";

                        if (!string.IsNullOrEmpty(value))
                        {
                            if (value.StartsWith("fn:"))
                            {
                                //string fun = "sum(" + GetSheetColumnName(colIndex) + "" + (3 + 1) + ":" + GetSheetColumnName(colIndex) + "" + rowIndex + ")"; 自动计算
                                string fun = value.Replace("fn:", "").Replace("{", "").Replace("}", "").Replace("?", "" + rowIndex + "");
                                if (colname.Contains("比例") || colname.Contains("占比") || colname.Contains("百分比"))
                                {
                                    cell.CellStyle = footbold_cellstyle;
                                    cell.SetCellFormula(fun); //利用公式来统计                                   
                                }
                                else
                                {
                                    cell.SetCellFormula(fun); //利用公式来统计
                                }

                            }
                            if (value.StartsWith("vn:"))
                            {
                                value = value.Replace("vn:", "");
                                cell.SetCellValue(value);
                            }
                        }
                        else
                        {
                            cell.SetCellValue("");
                        }

                        colIndex++;
                    }
                }
            }           

            return Workbook;
        }

        private static HSSFWorkbook CreateNPIOExcel(String SheetName, String Title, DataTable DataSource, DataTable Foot)
        {
            HSSFWorkbook Workbook = new HSSFWorkbook();
            ISheet Sheet1 = Workbook.CreateSheet();

            #region 初始化
            ////create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "上海合驿物流有限公司";
            Workbook.DocumentSummaryInformation = dsi;

            ////create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "报表导出";
            Workbook.SummaryInformation = si;
            #endregion

            int rowIndex = 0;
            int colIndex = 0;

            Dictionary<string, ICellStyle> CellStyles = CreateSheetCellStyles(Workbook);

            ICellStyle head_cellstyle = CellStyles["HeadCell"];//表头样式
            ICellStyle body_cellstyle = CellStyles["BodyCell"];//内容样式            
            ICellStyle date_cellstyle = CellStyles["DateCell"];//导出时间样式
            ICellStyle title_cellstyle = CellStyles["TitleCell"];//标题样式
            ICellStyle bodybold_cellstyle = CellStyleClone(Workbook, body_cellstyle);  //CellStyles["BodyBoldCell"];//内容加粗样式
            IFont Font = Workbook.CreateFont();
            Font.Boldweight = (short)FontBoldWeight.Bold;
            bodybold_cellstyle.SetFont(Font);
            ICellStyle footbold_cellstyle = CellStyleClone(Workbook, body_cellstyle); //CellStyles["RatioCell"];//表尾百分比样式
            footbold_cellstyle.SetFont(Font);
            footbold_cellstyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("0.00%");

            IRow trow = Sheet1.CreateRow(rowIndex);
            trow.Height = 18 * 20;
            NPOI.SS.UserModel.ICell tcell = trow.CreateCell(colIndex);
            tcell.SetCellValue(Title);
            tcell.CellStyle = title_cellstyle;
            Sheet1.AddMergedRegion(new CellRangeAddress(rowIndex, rowIndex, colIndex, DataSource.Columns.Count - 1));
            rowIndex++;

            trow = Sheet1.CreateRow(rowIndex);
            trow.Height = 18 * 20;
            tcell = trow.CreateCell(colIndex);
            tcell.SetCellValue("导出时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
            tcell.CellStyle = date_cellstyle;
            Sheet1.AddMergedRegion(new CellRangeAddress(rowIndex, rowIndex, colIndex, DataSource.Columns.Count - 1));
            rowIndex++;

            IRow hrow = Sheet1.CreateRow(rowIndex);
            hrow.Height = 20 * 20;
            foreach (DataColumn dc in DataSource.Columns)
            {
                NPOI.SS.UserModel.ICell hcell = hrow.CreateCell(colIndex, CellType.String);
                hcell.CellStyle = head_cellstyle;
                hcell.SetCellValue(dc.ColumnName);
                Sheet1.SetColumnWidth(colIndex, 20 * 256);
                colIndex++;
            }
            rowIndex++;

            IDataFormat datastyle = Workbook.CreateDataFormat();

            int _rowNumber = 1; int _rowTotal = DataSource.Rows.Count;

            foreach (DataRow dr in DataSource.Rows)
            {
                IRow row = Sheet1.CreateRow(rowIndex);
                row.Height = 18 * 20;
                colIndex = 0;

                foreach (DataColumn dc in DataSource.Columns)
                {
                    NPOI.SS.UserModel.ICell cell = row.CreateCell(colIndex);
                    cell.CellStyle = body_cellstyle;

                    string colname = dc.ColumnName;
                    string value = dr[colname] + "";

                    switch (dc.DataType.Name)
                    {
                        case "DateTime":
                            if (!string.IsNullOrEmpty(value))
                            {
                                value = DateTime.Parse(value).ToString("yyyy-MM-dd");
                                //cell.CellStyle.DataFormat = datastyle.GetFormat("yyyy-MM-dd");
                                cell.SetCellValue(value);
                            }
                            break;
                        case "Int16":
                        case "Int32":
                        case "Int64":
                            if (!string.IsNullOrEmpty(value))
                                cell.SetCellValue(Int32.Parse(value));
                            break;
                        case "Single":
                        case "Double":
                        case "Decimal":
                            if (colname.Contains("比例") || colname.Contains("占比") || colname.Contains("百分比"))
                            {
                                cell.SetCellValue(PercentageToFormat("23", value));
                            }
                            else if (!string.IsNullOrEmpty(value))
                            {
                                cell.SetCellValue(Math.Round(double.Parse(value), 2));//DecimalToFormat(value);
                            }
                            break;
                        case "byte[]":
                        case "Byte[]":
                            if (dr[colname] != null && dr[colname] != DBNull.Value)
                            {
                                StringBuilder sb = new StringBuilder();
                                Byte[] array = (Byte[])dr[colname];
                                int maxlen = array.Length;
                                if (maxlen > 16)
                                {
                                    maxlen = 16;
                                }
                                for (int i = 0; i < maxlen; i++)
                                {
                                    sb.Append(array[i].ToString("X2"));
                                }
                                value = "Ox" + sb.ToString();
                                maxlen = array.Length;
                                if (maxlen > 16)
                                {
                                    value += "...";
                                }
                                cell.SetCellValue(value);
                            }
                            break;
                        default:
                            cell.SetCellValue(value);
                            break;
                    }

                    colIndex++;
                }


                rowIndex++;
                _rowNumber++;

            }

            if (Foot != null)
            {
                foreach (DataRow dr in Foot.Rows)
                {
                    IRow row = Sheet1.CreateRow(rowIndex);
                    colIndex = 0;
                    row.Height = 20 * 20;

                    foreach (DataColumn dc in Foot.Columns)
                    {
                        NPOI.SS.UserModel.ICell cell = row.CreateCell(colIndex);
                        cell.CellStyle = bodybold_cellstyle;

                        string colname = dc.ColumnName;
                        string value = dr[colname] + "";

                        if (!string.IsNullOrEmpty(value))
                        {
                            if (value.StartsWith("fn:"))
                            {
                                //string fun = "sum(" + GetSheetColumnName(colIndex) + "" + (3 + 1) + ":" + GetSheetColumnName(colIndex) + "" + rowIndex + ")"; 自动计算
                                string fun = value.Replace("fn:", "").Replace("{", "").Replace("}", "").Replace("?", "" + rowIndex + "");
                                if (colname.Contains("比例") || colname.Contains("占比") || colname.Contains("百分比"))
                                {
                                    cell.CellStyle = footbold_cellstyle;
                                    cell.SetCellFormula(fun); //利用公式来统计                                   
                                }
                                else
                                {
                                    cell.SetCellFormula(fun); //利用公式来统计
                                }

                            }
                            if (value.StartsWith("vn:"))
                            {
                                value = value.Replace("vn:", "");
                                cell.SetCellValue(value);
                            }
                        }
                        else
                        {
                            cell.SetCellValue("");
                        }

                        colIndex++;
                    }
                }
            }

            CellStyles.Clear();

            return Workbook;
        }

        /// <summary>
        /// 创建默认单元格样式
        /// </summary>
        /// <param name="Workbook"></param>
        /// <returns></returns>
        public static ICellStyle CreateDefaultSheetCellStyle(HSSFWorkbook Workbook)
        {
            return CreateSheetCellStyle(Workbook);
        }

        /// <summary>
        /// 读取Excel数据
        /// </summary>
        /// <param name="ExcelPath"></param>
        /// <param name="SheetName"></param>
        /// <returns></returns>
        public static DataTable GetTable(string ExcelPath, string SheetName)
        {
            DataTable dt = new DataTable();

            string filetype;
            IWorkbook work = TemplateNPIOExcel(ExcelPath, out filetype);
            ISheet sheet = null;

            if (!string.IsNullOrEmpty(SheetName))
            {
                sheet = work.GetSheet(SheetName);
            }
            else
            {
                sheet = work.GetSheetAt(0);
            }

            int rowIndex = 0;
            System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
           
            while (rows.MoveNext())
            {
                IRow row = (IRow)rows.Current;

                if (rowIndex == 0)
                {
                    System.Collections.IEnumerator columns = row.GetEnumerator();
                    while (columns.MoveNext())
                    {
                        NPOI.SS.UserModel.ICell cell = (NPOI.SS.UserModel.ICell)columns.Current;
                        int index = 0;
                        bool goon = true;
                        AddDataColumn(dt, cell.StringCellValue, ref index, ref goon);                         
                    }
                }
                else
                {
                    DataRow dr = dt.NewRow();
                    int columnIndex = 0;
                    foreach (DataColumn dc in dt.Columns)
                    {
                        NPOI.SS.UserModel.ICell cell = row.GetCell(columnIndex);
                        if (cell != null)
                        {
                            switch (cell.CellType)
                            {                               
                                case CellType.Numeric: dr[dc.ColumnName] = cell.NumericCellValue; break;
                                case CellType.Boolean: dr[dc.ColumnName] = cell.BooleanCellValue; break;
                                case CellType.Unknown: dr[dc.ColumnName] = cell.StringCellValue; break;
                                case CellType.String: 
                                default :
                                    dr[dc.ColumnName] = cell.StringCellValue; break;
                            }

                            if (cell.CellType.Equals(CellType.Numeric) && DateUtil.IsCellDateFormatted(cell))
                            {
                                dr[dc.ColumnName] = cell.DateCellValue;
                            }
                        }
                        columnIndex++;
                    }

                    dt.Rows.Add(dr);
                }

                rowIndex++;
            }

            return dt;
        }

        /// <summary>
        /// 复制表结构
        /// </summary>
        /// <param name="DataSorce"></param>
        /// <returns></returns>
        public static DataTable TableClone(DataTable DataSorce)
        {
            DataTable dt = new DataTable();
            foreach (DataColumn dc in DataSorce.Columns)
            {
                DataColumn ndc = new DataColumn(dc.ColumnName, typeof(string));
                dt.Columns.Add(ndc);
            }
            return dt;
        }

        /// <summary>
        /// 读取模板
        /// </summary>
        /// <param name="TemplatePath"></param>
        /// <param name="filetype"></param>
        /// <returns></returns>
        public static IWorkbook TemplateNPIOExcel(String TemplatePath, out string filetype)
        {
            IWorkbook Workbook = null; //HSSFWorkbook
            if (!System.IO.File.Exists(TemplatePath))
            {
                throw new Exception("模板不存在");
            }
            filetype = TemplatePath.Substring(TemplatePath.LastIndexOf(".")).ToLower();
            using (FileStream file = new FileStream(TemplatePath, FileMode.Open, FileAccess.Read))
            {
                switch (filetype)
                {
                    case ".xls":
                        Workbook = new HSSFWorkbook(file);
                        break;
                    case ".xlsx":
                        Workbook = new XSSFWorkbook(file);
                        break;
                    default :
                        Workbook = new HSSFWorkbook(file);
                        break;
                }
                file.Close();
            }
            return Workbook;
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="Context"></param>
        /// <param name="Workbook"></param>
        /// <param name="FileName"></param>
        public static void ExcelOutToPrint(HttpContextBase Context, HSSFWorkbook Workbook, String FileName)
        {
            Context.Response.Charset = "UTF-8";
            Context.Response.ContentType = "application/vnd.ms-excel";
            Context.Response.ContentEncoding = Encoding.UTF8;
            //判断浏览器
            string UserAgent = Context.Request.ServerVariables["http_user_agent"].ToLower();
            FileName = DateTime.Now.ToString("yyyy-MM-dd") + FileName;
            if (UserAgent.IndexOf("firefox") == -1)
            {//非火狐浏览器
                FileName = HttpUtility.UrlEncode(FileName, Encoding.UTF8);
            }
            Context.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", FileName + ".xls"));
            Context.Response.Clear();

            System.IO.MemoryStream file = new System.IO.MemoryStream();
            Workbook.Write(file);
            file.WriteTo(Context.Response.OutputStream);
            Context.Response.End();
        }

        /// <summary>
        /// 自动填充数据并导出
        /// </summary>
        /// <param name="Context"></param>
        /// <param name="DataSource"></param>
        /// <param name="SheetName"></param>
        /// <param name="Title"></param>
        /// <param name="FileName"></param>
        /// <param name="Foot"></param>
        public static void ExcelOutToPrint(HttpContextBase Context, DataTable DataSource, String SheetName, string Title, String FileName, DataTable Foot)
        {
            HSSFWorkbook wb = CreateNPIOExcel(SheetName, Title, DataSource, Foot);
            Context.Response.Charset = "UTF-8";
            Context.Response.ContentType = "application/vnd.ms-excel";
            Context.Response.ContentEncoding = Encoding.UTF8;
            //判断浏览器
            string UserAgent = Context.Request.ServerVariables["http_user_agent"].ToLower();
            FileName = DateTime.Now.ToString("yyyy-MM-dd") + FileName;
            if (UserAgent.IndexOf("firefox") == -1)
            {//非火狐浏览器
                FileName = HttpUtility.UrlEncode(FileName, Encoding.UTF8);
            }
            Context.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", FileName + ".xls"));
            Context.Response.Clear();

            System.IO.MemoryStream file = new System.IO.MemoryStream();
            wb.Write(file);
            file.WriteTo(Context.Response.OutputStream);
            Context.Response.End();
        }
        
        /// <summary>
        /// 读取模板并自动填充数据 并导出
        /// </summary>
        /// <param name="Context"></param>
        /// <param name="TemplatePath"></param>
        /// <param name="DataSource"></param>
        /// <param name="FileName"></param>
        /// <param name="Foot"></param>
        public static void ExcelOutToPrint(HttpContextBase Context, String TemplatePath, bool Merge, DataTable DataSource, String FileName, DataTable Foot)
        {
            HSSFWorkbook wb = CreateNPIOExcel(TemplatePath, Merge, DataSource, Foot);
            Context.Response.Charset = "UTF-8";
            Context.Response.ContentType = "application/vnd.ms-excel";
            Context.Response.ContentEncoding = Encoding.UTF8;
            //判断浏览器
            string UserAgent = Context.Request.ServerVariables["http_user_agent"].ToLower();
            FileName = DateTime.Now.ToString("yyyy-MM-dd") + FileName;
            if (UserAgent.IndexOf("firefox") == -1)
            {//非火狐浏览器
                FileName = HttpUtility.UrlEncode(FileName, Encoding.UTF8);
            }
            Context.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", FileName + ".xls"));
            Context.Response.Clear();

            System.IO.MemoryStream file = new System.IO.MemoryStream();
            wb.Write(file);
            file.WriteTo(Context.Response.OutputStream);
            Context.Response.End();
        }

        public static void ExcelOutToFile(string filePath, DataTable DataSource, string SheetName, string Title,DataTable Foot)
        {            
            HSSFWorkbook wb = CreateNPIOExcel(SheetName, Title, DataSource, Foot);
            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                wb.Write(fs);
            }           
        }

        #endregion

        #region NPOI 操作Word

        private static XWPFDocument CreateNPIOWord(String Text, String TemplatePath, bool Merge, DataTable DataSource, DataTable Foot)
        {
            XWPFDocument doc = null;
            if (!string.IsNullOrEmpty(TemplatePath))
            {
                if (System.IO.File.Exists(TemplatePath))
                {
                    using (FileStream stream = File.OpenRead(TemplatePath))
                    {                        
                        doc = new XWPFDocument(stream);
                        //doc = new XWPFDocument(NPOI.OpenXml4Net.OPC.OPCPackage.OpenOrCreate(TemplatePath));
                    }

                    if (doc.Paragraphs!=null)
                    {
                        foreach (var para in doc.Paragraphs)
                        {
                            if (para.ParagraphText.Equals("{文档标题}"))
                            {
                                var runs = para.Runs;
                                foreach (var run in runs)
                                {
                                    run.SetText(Text);
                                }
                            }
                            if (para.ParagraphText.Equals("{导出时间}"))
                            {
                                var runs = para.Runs;
                                foreach (var run in runs)
                                {
                                    run.SetText(DateTime.Now.ToString());
                                }
                            }
                        }
                    }

                    if (doc.Tables != null)
                    {
                        foreach (var table in doc.Tables)
                        {
                            foreach (var row in table.Rows)
                            {
                                foreach (var col in row.GetTableICells())
                                {
                                    NPOI.XWPF.UserModel.XWPFTableCell cell = (NPOI.XWPF.UserModel.XWPFTableCell)col;
                                    string text = cell.Paragraphs[0].ParagraphText;
                                    //cell.Paragraphs[0].Runs[0].SetText("demo");
                                }
                            }
                        }
                    }

                }
                else
                {
                    doc = new XWPFDocument();
                    CT_P m_p = doc.Document.body.AddNewP();
                    m_p.AddNewPPr().AddNewJc().val = ST_Jc.center;//段落水平居中
                    XWPFParagraph paragraph = new XWPFParagraph(m_p, doc); //创建XWPFParagraph                    
                    paragraph.CreateRun().SetText(Text);                    
                    paragraph.Runs[0].GetCTR().AddNewRPr().AddNewRFonts().ascii = "黑体";
                    paragraph.Runs[0].GetCTR().AddNewRPr().AddNewRFonts().eastAsia = "黑体";
                    paragraph.Runs[0].GetCTR().AddNewRPr().AddNewSz().val = (ulong)44;//2号字体
                    paragraph.Runs[0].GetCTR().AddNewRPr().AddNewSzCs().val = (ulong)44;                  
                    paragraph.Runs[0].GetCTR().AddNewRPr().AddNewB().val = true;//加粗
                    //paragraph.Runs[0].GetCTR().AddNewRPr().AddNewColor().val = "red";//字体颜色                   
                }
            }
            else
            {
                doc = new XWPFDocument();               
                CT_P m_p = doc.Document.body.AddNewP();
                m_p.AddNewPPr().AddNewJc().val = ST_Jc.center;//段落水平居中
                XWPFParagraph paragraph = new XWPFParagraph(m_p, doc); //创建XWPFParagraph                
                paragraph.CreateRun().SetText(Text);              
                paragraph.Runs[0].GetCTR().AddNewRPr().AddNewRFonts().ascii = "黑体";
                paragraph.Runs[0].GetCTR().AddNewRPr().AddNewRFonts().eastAsia = "黑体";
                paragraph.Runs[0].GetCTR().AddNewRPr().AddNewSz().val = (ulong)44;//2号字体
                paragraph.Runs[0].GetCTR().AddNewRPr().AddNewSzCs().val = (ulong)44;
                paragraph.Runs[0].GetCTR().AddNewRPr().AddNewB().val = true;//加粗 
                //paragraph.Runs[0].GetCTR().AddNewRPr().AddNewColor().val = "red";//字体颜色            
            }            

            return doc;
        }

        public static void WordOutToFile(string Title, string TemplatePath, string SavePath, bool Merge, DataTable DataSource, DataTable Foot)
        {
            using (System.IO.FileStream fs = new FileStream(SavePath, FileMode.OpenOrCreate, FileAccess.Write))
            {
                var doc = CreateNPIOWord(Title, TemplatePath, Merge, DataSource, Foot);
                doc.Write(fs);
            }           
        }

        public static void WordOutToFile(string Title, string TemplatePath, string SavePath, bool Merge, DataSet DataSet)
        {
            using (System.IO.FileStream fs = new FileStream(SavePath, FileMode.OpenOrCreate, FileAccess.Write))
            {
                int tableWidth = 8522;

                var doc = CreateNPIOWord(Title, TemplatePath, Merge, null, null);

                doc.Document.body.sectPr = new CT_SectPr();
                CT_SectPr m_SectPr = doc.Document.body.sectPr;

                //1‘=1440twip=25.4mm=72pt(磅point)=96px(像素pixel)
                //1px(像素pixel)=0.75pt(磅point)
                //A4:W=11906 twip=8.269''=210mm,h=16838twip=11.693''=297mm
                //A5:W=8390 twip=5.827''=148mm,h=11906 twip=8.269''=210mm
                //A6:W=5953 twip=4.134''=105mm,h=8390twip=5.827''=1148mm               

                //页面设置A4横向
                //m_SectPr.pgSz.w = (ulong)16838;
                //m_SectPr.pgSz.h = (ulong)11906;

                //m_p.AddNewPPr().AddNewSpacing().line = "400";//行距固定20磅
                //m_p.AddNewPPr().AddNewSpacing().lineRule = ST_LineSpacingRule.exact;

                foreach (var DataTable in DataSet.Tables)
                {
                    System.Data.DataTable dataTable=(System.Data.DataTable) DataTable;
                    int cellWidth = dataTable.Columns.Count == 0 ? tableWidth : tableWidth / dataTable.Columns.Count;

                    CT_P m_p = doc.Document.body.AddNewP();
                    m_p.AddNewPPr().AddNewJc().val = ST_Jc.left;//段落水平居中
                    XWPFParagraph paragraph = new XWPFParagraph(m_p, doc); //创建XWPFParagraph                    
                    paragraph.CreateRun().SetText(""+dataTable.TableName+" :");    

                    #region 创建表

                    CT_Tbl m_CTTbl = doc.Document.body.AddNewTbl();
                    m_CTTbl.AddNewTblPr().jc = new CT_Jc();
                    m_CTTbl.AddNewTblPr().jc.val = ST_Jc.center;//表在页面水平居中

                    m_CTTbl.AddNewTblPr().AddNewTblW().w = tableWidth.ToString();//表宽度
                    m_CTTbl.AddNewTblPr().AddNewTblW().type = ST_TblWidth.dxa;

                    //CT_TblWidth m_CELLwi = new CT_TblWidth() { w = "108", typeSpecified = false };
                    //m_CTTbl.tblPr.tblCellMar = new CT_TblCellMar() { left = m_CELLwi, right = m_CELLwi };

                    var Itable = new XWPFTable(m_CTTbl, doc, 1, dataTable.Columns.Count);//创建1行1列表
                    //doc.CreateTable(1, dataTable.Columns.Count);   

                    Itable.SetCellMargins(0, 108, 0, 108);
                    Itable.StyleID = "4";

                    #endregion

                    #region 设置表头

                    //for (int i = 0; i < m_CTTbl.tblGrid.gridCol.Count; i++)
                    //{
                    //    m_CTTbl.tblGrid.gridCol[i].w = (ulong)cellWidth;//单元格宽
                    //    m_CTTbl.tblGrid.gridCol[i].wSpecified = true;
                    //}

                    Itable.GetRow(0).GetCTRow().AddNewTrPr().AddNewTrHeight().val = (ulong)426; 
                    for (int i = 0; i < dataTable.Columns.Count; i++)
                    {
                        DataColumn dc = dataTable.Columns[i];
                        NPOI.XWPF.UserModel.XWPFTableCell cell = Itable.GetRow(0).GetCell(i);                      

                        //兼容WORD
                        CT_TcPr m_Pr = cell.GetCTTc().AddNewTcPr();
                        m_Pr.tcW = new CT_TblWidth();
                        m_Pr.tcW.w = cellWidth.ToString();//单元格宽
                        m_Pr.tcW.type = ST_TblWidth.dxa;

                        //兼容WPS
                        m_CTTbl.tblGrid.gridCol[i].w = (ulong)cellWidth;//单元格宽
                        m_CTTbl.tblGrid.gridCol[i].wSpecified = true;

                        //NPOI.OpenXmlFormats.Wordprocessing.CT_P m_p = cell.GetCTTc().GetPList()[0];
                        //m_p.AddNewPPr().AddNewSpacing().line = "360";
                        //XWPFParagraph Paragraph = new XWPFParagraph(m_p, doc);
                        //Paragraph.CreateRun().SetText("标题"); 
                                               
                        cell.Paragraphs[0].CreateRun().SetText(dc.ColumnName);
                        //cell.Paragraphs[0].Runs[0].GetCTR().rPr.AddNewColor(); 
                        cell.SetColor("#99CCFF");
                        cell.Paragraphs[0].Runs[0].IsBold = true;
                        cell.SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);

                    }

                    #endregion

                    #region 设置表体内容数据

                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {                        
                        Itable.CreateRow().GetCTRow().AddNewTrPr().AddNewTrHeight().val = (ulong)400; 
                        for (int j = 0; j < dataTable.Columns.Count; j++)
                        {
                            NPOI.XWPF.UserModel.XWPFTableCell cell = Itable.GetRow(i + 1).GetCell(j);
                            cell.SetText(dataTable.Rows[i][j] + "");
                            cell.SetVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                        }
                    }

                    #endregion

                    m_p = doc.Document.body.AddNewP();
                    m_p.AddNewPPr().AddNewJc().val = ST_Jc.center;//段落水平居中

                }

                #region 添加页码

                XWPFParagraph m_xp = doc.CreateParagraph();
                m_xp.CreateRun().AddBreak();    //分页
                //创建页脚
                CT_Ftr m_ftr = new CT_Ftr();
                m_ftr.Items = new System.Collections.ArrayList();

                CT_SdtBlock m_Sdt = new CT_SdtBlock();
                CT_SdtPr m_SdtPr = m_Sdt.AddNewSdtPr();

                CT_SdtDocPart m_SdtDocPartObj = m_SdtPr.AddNewDocPartObj();
                m_SdtDocPartObj.AddNewDocPartGallery().val = "PageNumbers (Bottom of Page)";
                m_SdtDocPartObj.docPartUnique = new CT_OnOff();

                CT_SdtContentBlock m_SdtContent = m_Sdt.AddNewSdtContent();
                CT_P m_SdtContentP = m_SdtContent.AddNewP();
                CT_PPr m_SdtContentPpPr = m_SdtContentP.AddNewPPr();
                m_SdtContentPpPr.AddNewJc().val = ST_Jc.center;
                m_SdtContentP.Items = new System.Collections.ArrayList();

                CT_SimpleField m_fldSimple = new CT_SimpleField();
                m_fldSimple.instr = " PAGE   \\*MERGEFORMAT ";
                m_SdtContentP.Items.Add(m_fldSimple);
                m_ftr.Items.Add(m_Sdt);

                //创建页脚关系（footern.xml）
                XWPFRelation Frelation = XWPFRelation.FOOTER;
                XWPFFooter m_f = (XWPFFooter)doc.CreateRelationship(Frelation, XWPFFactory.GetInstance(), doc.FooterList.Count + 1);

                //设置页脚
                m_f.SetHeaderFooter(m_ftr);
                CT_HdrFtrRef m_HdrFtr = m_SectPr.AddNewFooterReference();
                m_HdrFtr.type = ST_HdrFtr.@default;
                m_HdrFtr.id = m_f.GetPackageRelationship().Id;

                #endregion

                doc.Write(fs);
            } 
        }

        #endregion
    }
}
