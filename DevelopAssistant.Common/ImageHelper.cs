﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms; 

namespace DevelopAssistant.Common
{
    public class ImageHelper
    {
        public static Image TransparentImage(Image srcImage, float opacity)
        {
            float[][] nArray ={ new float[] {1, 0, 0, 0, 0},  
                        new float[] {0, 1, 0, 0, 0},  
                        new float[] {0, 0, 1, 0, 0},  
                        new float[] {0, 0, 0, opacity, 0},  
                        new float[] {0, 0, 0, 0, 1}};
            ColorMatrix matrix = new ColorMatrix(nArray);//定义包含 RGBA 空间坐标的 5 x 5 矩阵。
            ImageAttributes attributes = new ImageAttributes();//对象包含有关在呈现时如何操作位图和图元文件颜色的信息
            attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            Bitmap resultImage = new Bitmap(srcImage.Width, srcImage.Height);
            Graphics g = Graphics.FromImage(resultImage);
            g.DrawImage(srcImage, new Rectangle(0, 0, srcImage.Width, srcImage.Height), 0, 0, srcImage.Width, srcImage.Height, GraphicsUnit.Pixel, attributes);

            return resultImage;
        }

        public static Image TransparentImage(Image srcImage)
        {
            Bitmap bitmap = new Bitmap(srcImage);
            bitmap.MakeTransparent();           
            return (Image)bitmap;
        }

    }
}
