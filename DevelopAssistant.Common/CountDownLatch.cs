﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DevelopAssistant.Common
{
    public class CountDownLatch
    {
        private object lockobj = new object();
        private int counts;

        public int Counts()
        {
            return this.counts;
        }

        public CountDownLatch(int counts)
        {
            this.counts = counts;
        }

        public void Await()
        {
            lock (lockobj)
            {
                while (counts > 0)
                {
                    Monitor.Wait(lockobj);
                }
            }
        }

        public void CountDown()
        {
            lock (lockobj)
            {
                counts--;
                Monitor.PulseAll(lockobj);
            }
        }
    }
}
