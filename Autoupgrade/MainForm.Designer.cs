﻿namespace Autoupgrade
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ToolBar = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.btnExpand = new System.Windows.Forms.Panel();
            this.BtnCancel = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.BtnApply = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.ProgressBar = new ICSharpCode.WinFormsUI.Controls.NProgressBar();
            this.lblTitle = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblResult = new ICSharpCode.WinFormsUI.Controls.NStateLabel();
            this.lblResultLabel = new System.Windows.Forms.Label();
            this.StateList = new ICSharpCode.WinFormsUI.Controls.NDownList();
            this.lbldetailLabel = new System.Windows.Forms.Label();
            this.lblState = new System.Windows.Forms.Label();
            this.lblStateLabel = new System.Windows.Forms.Label();
            this.lblInfo = new ICSharpCode.WinFormsUI.Controls.NStateLabel();
            this.lblInfoLabel = new System.Windows.Forms.Label();
            this.SystemContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolBar.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SystemContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.ToolBar.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.ToolBar.BottomBlackColor = System.Drawing.SystemColors.Control;
            this.ToolBar.Controls.Add(this.btnExpand);
            this.ToolBar.Controls.Add(this.BtnCancel);
            this.ToolBar.Controls.Add(this.BtnApply);
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ToolBar.Location = new System.Drawing.Point(6, 424);
            this.ToolBar.MarginWidth = 1;
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.Size = new System.Drawing.Size(388, 56);
            this.ToolBar.TabIndex = 0;
            this.ToolBar.Text = "nPanel1";
            this.ToolBar.TopBlackColor = System.Drawing.SystemColors.Control;
            // 
            // btnExpand
            // 
            this.btnExpand.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnExpand.BackgroundImage")));
            this.btnExpand.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnExpand.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExpand.Location = new System.Drawing.Point(25, 11);
            this.btnExpand.Name = "btnExpand";
            this.btnExpand.Size = new System.Drawing.Size(24, 24);
            this.btnExpand.TabIndex = 2;
            this.btnExpand.Click += new System.EventHandler(this.btnExpand_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnCancel.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.BtnCancel.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BtnCancel.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.BtnCancel.FouseBorderColor = System.Drawing.Color.Orange;
            this.BtnCancel.FouseColor = System.Drawing.Color.White;
            this.BtnCancel.Foused = false;
            this.BtnCancel.FouseTextColor = System.Drawing.Color.Black;
            this.BtnCancel.Icon = null;
            this.BtnCancel.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.BtnCancel.Location = new System.Drawing.Point(290, 7);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Radius = 4;
            this.BtnCancel.Size = new System.Drawing.Size(74, 36);
            this.BtnCancel.TabIndex = 1;
            this.BtnCancel.Text = "取消";
            this.BtnCancel.UnableIcon = null;
            this.BtnCancel.UseVisualStyleBackColor = false;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnApply
            // 
            this.BtnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnApply.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.BtnApply.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BtnApply.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.BtnApply.FouseBorderColor = System.Drawing.Color.Orange;
            this.BtnApply.FouseColor = System.Drawing.Color.White;
            this.BtnApply.Foused = false;
            this.BtnApply.FouseTextColor = System.Drawing.Color.Black;
            this.BtnApply.Icon = null;
            this.BtnApply.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.BtnApply.Location = new System.Drawing.Point(202, 7);
            this.BtnApply.Name = "BtnApply";
            this.BtnApply.Radius = 4;
            this.BtnApply.Size = new System.Drawing.Size(74, 36);
            this.BtnApply.TabIndex = 0;
            this.BtnApply.Text = "开始";
            this.BtnApply.UnableIcon = null;
            this.BtnApply.UseVisualStyleBackColor = false;
            this.BtnApply.Click += new System.EventHandler(this.BtnApply_Click);
            // 
            // ProgressBar
            // 
            this.ProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ProgressBar.FaceColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(176)))), ((int)(((byte)(227)))));
            this.ProgressBar.LinearBrush = false;
            this.ProgressBar.Location = new System.Drawing.Point(17, 36);
            this.ProgressBar.Maximum = 100;
            this.ProgressBar.Minimum = 0;
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.ShowValue = false;
            this.ProgressBar.Size = new System.Drawing.Size(350, 24);
            this.ProgressBar.TabIndex = 1;
            this.ProgressBar.Text = "nProgressBar1";
            this.ProgressBar.Value = 0;
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Location = new System.Drawing.Point(17, 5);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(351, 26);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "最新版本 1.0.0.1 等待更新";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.lblResult);
            this.panel1.Controls.Add(this.lblResultLabel);
            this.panel1.Controls.Add(this.StateList);
            this.panel1.Controls.Add(this.lbldetailLabel);
            this.panel1.Controls.Add(this.lblState);
            this.panel1.Controls.Add(this.lblStateLabel);
            this.panel1.Controls.Add(this.lblInfo);
            this.panel1.Controls.Add(this.lblInfoLabel);
            this.panel1.Controls.Add(this.ProgressBar);
            this.panel1.Controls.Add(this.lblTitle);
            this.panel1.Location = new System.Drawing.Point(9, 36);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(382, 386);
            this.panel1.TabIndex = 3;
            // 
            // lblResult
            // 
            this.lblResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(81, 352);
            this.lblResult.Name = "lblResult";
            this.lblResult.ShowBorder = false;
            this.lblResult.Size = new System.Drawing.Size(286, 20);
            this.lblResult.TabIndex = 10;
            // 
            // lblResultLabel
            // 
            this.lblResultLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblResultLabel.AutoSize = true;
            this.lblResultLabel.Location = new System.Drawing.Point(17, 356);
            this.lblResultLabel.Name = "lblResultLabel";
            this.lblResultLabel.Size = new System.Drawing.Size(65, 12);
            this.lblResultLabel.TabIndex = 9;
            this.lblResultLabel.Text = "统计信息：";
            // 
            // StateList
            // 
            this.StateList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.StateList.BackColor = System.Drawing.SystemColors.Control;
            this.StateList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.StateList.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.StateList.FormattingEnabled = false;
            this.StateList.ItemHeight = 24;
            this.StateList.Location = new System.Drawing.Point(17, 126);
            this.StateList.Name = "StateList";
            this.StateList.SelectedIndex = -1;
            this.StateList.SelectedItem = null;
            this.StateList.Size = new System.Drawing.Size(350, 216);
            this.StateList.TabIndex = 8;
            this.StateList.Text = "nDownList1";
            // 
            // lbldetailLabel
            // 
            this.lbldetailLabel.AutoSize = true;
            this.lbldetailLabel.Location = new System.Drawing.Point(17, 100);
            this.lbldetailLabel.Name = "lbldetailLabel";
            this.lbldetailLabel.Size = new System.Drawing.Size(65, 12);
            this.lbldetailLabel.TabIndex = 7;
            this.lbldetailLabel.Text = "详细信息：";
            // 
            // lblState
            // 
            this.lblState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(329, 74);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(35, 12);
            this.lblState.TabIndex = 6;
            this.lblState.Text = "100 %";
            // 
            // lblStateLabel
            // 
            this.lblStateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStateLabel.AutoSize = true;
            this.lblStateLabel.Location = new System.Drawing.Point(281, 74);
            this.lblStateLabel.Name = "lblStateLabel";
            this.lblStateLabel.Size = new System.Drawing.Size(41, 12);
            this.lblStateLabel.TabIndex = 5;
            this.lblStateLabel.Text = "状态：";
            // 
            // lblInfo
            // 
            this.lblInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInfo.AutoSize = true;
            this.lblInfo.Location = new System.Drawing.Point(56, 70);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.ShowBorder = false;
            this.lblInfo.Size = new System.Drawing.Size(207, 20);
            this.lblInfo.TabIndex = 4;
            // 
            // lblInfoLabel
            // 
            this.lblInfoLabel.AutoSize = true;
            this.lblInfoLabel.Location = new System.Drawing.Point(17, 74);
            this.lblInfoLabel.Name = "lblInfoLabel";
            this.lblInfoLabel.Size = new System.Drawing.Size(41, 12);
            this.lblInfoLabel.TabIndex = 3;
            this.lblInfoLabel.Text = "信息：";
            // 
            // SystemContextMenu
            // 
            this.SystemContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemSettings,
            this.toolStripMenuItemAbout});
            this.SystemContextMenu.Name = "SystemMenu";
            this.SystemContextMenu.Size = new System.Drawing.Size(153, 70);
            // 
            // toolStripMenuItemSettings
            // 
            this.toolStripMenuItemSettings.Image = global::Autoupgrade.Properties.Resources.settings_16px;
            this.toolStripMenuItemSettings.Name = "toolStripMenuItemSettings";
            this.toolStripMenuItemSettings.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItemSettings.Text = "设置";
            this.toolStripMenuItemSettings.Click += new System.EventHandler(this.toolStripMenuItemSettings_Click);
            // 
            // toolStripMenuItemAbout
            // 
            this.toolStripMenuItemAbout.Image = global::Autoupgrade.Properties.Resources.information_16px;
            this.toolStripMenuItemAbout.Name = "toolStripMenuItemAbout";
            this.toolStripMenuItemAbout.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItemAbout.Text = "关于";
            this.toolStripMenuItemAbout.Click += new System.EventHandler(this.toolStripMenuItemAbout_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 486);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.ShadowWidth = 8;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "程序升级";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ToolBar.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.SystemContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NPanel ToolBar;
        private ICSharpCode.WinFormsUI.Controls.NButton BtnApply;
        private ICSharpCode.WinFormsUI.Controls.NButton BtnCancel;
        private ICSharpCode.WinFormsUI.Controls.NProgressBar ProgressBar;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Label lblStateLabel;
        private ICSharpCode.WinFormsUI.Controls.NStateLabel lblInfo;
        private System.Windows.Forms.Label lblInfoLabel;
        private System.Windows.Forms.Panel btnExpand;
        private ICSharpCode.WinFormsUI.Controls.NDownList StateList;
        private System.Windows.Forms.Label lbldetailLabel;
        private ICSharpCode.WinFormsUI.Controls.NStateLabel lblResult;
        private System.Windows.Forms.Label lblResultLabel;
        private System.Windows.Forms.ContextMenuStrip SystemContextMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAbout;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSettings;

    }
}

