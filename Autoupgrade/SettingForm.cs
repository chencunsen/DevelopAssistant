﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ICSharpCode.WinFormsUI.Forms;
using System.Xml;

namespace Autoupgrade
{
    public partial class SettingForm : BaseForm
    {
        string url = string.Empty;
        string startInfo = string.Empty;
        string productName = string.Empty;

        OpenFileDialog openfiledialog = new OpenFileDialog();

        public SettingForm()
        {
            InitializeComponent();
        }

        public SettingForm(string Text, ICSharpCode.WinFormsUI.Theme.WinFormsUIThemeBase XTheme)
            : this()
        {
            this.XTheme = XTheme;
            InitializeControls();
        }

        private void InitializeControls()
        {
            openfiledialog.DefaultExt = "*.exe";
            openfiledialog.Filter = "*.exe|*.exe";
            openfiledialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

            this.Text = Text;           
            this.ToolBar.BorderColor = XTheme.FormBorderOutterColor;
            this.Resizable = false;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {

            try
            {
                
                this.startInfo = this.txtPath.Text.Trim();
                this.url = this.txtUrl.Text.Trim();
                this.productName = this.txtName.Text.Trim();

                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(AppDomain.CurrentDomain.BaseDirectory + "AssistantConfig.config");
                xmlDocument.SelectSingleNode("//Version");

                var element = xmlDocument.SelectSingleNode("//productname");
                element.InnerText = this.productName;

                element = xmlDocument.SelectSingleNode("//url");
                element.InnerText = this.url;

                element = xmlDocument.SelectSingleNode("//startinfo");
                element.InnerText = this.startInfo;

                element = xmlDocument.SelectSingleNode("//path");
                element.InnerText = "\\Versions\\" + this.productName + "\\Updates\\";

                xmlDocument.Save(AppDomain.CurrentDomain.BaseDirectory + "AssistantConfig.config");

                this.DialogResult = DialogResult.OK;
                this.Close();

            }
            catch (Exception ex)
            {

            }           
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SettingForm_Load(object sender, EventArgs e)
        {
            InitializeSettings();
        }

        private void InitializeSettings()
        {
            try
            {                
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(AppDomain.CurrentDomain.BaseDirectory + "AssistantConfig.config");
                xmlDocument.SelectSingleNode("//Version");              
                this.productName = xmlDocument.SelectSingleNode("//productname").InnerText;
                this.url = xmlDocument.SelectSingleNode("//url").InnerText;
                //this.handler = xmlDocument.SelectSingleNode("//handler").InnerText;
                //this.path = xmlDocument.SelectSingleNode("//path").InnerText;
                this.startInfo = xmlDocument.SelectSingleNode("//startinfo").InnerText;

                this.txtUrl.Text = url;
                this.txtName.Text = productName;
                this.txtPath.Text = this.startInfo;

            }
            catch (Exception ex)
            {                 
                 
            }
        }

        private void txtPath_Click(object sender, EventArgs e)
        {
            if (openfiledialog.ShowDialog(this).Equals(DialogResult.OK))
            {
                if (!string.IsNullOrEmpty(openfiledialog.FileName))
                {
                    System.IO.FileInfo fi = new System.IO.FileInfo(openfiledialog.FileName);
                    this.txtPath.Text = fi.Name;
                }
            }
        }

    }
}
