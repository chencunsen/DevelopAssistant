﻿using DevelopAssistant.AddIn;
using DevelopAssistant.Common;
using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Docking;
using ICSharpCode.WinFormsUI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DevelopAssistant.AddIn.PickIcon
{
    public partial class MainForm : BaseForm
    {
        System.Windows.Forms.Form _mainform;
        System.Windows.Forms.OpenFileDialog _openfiledialog;
        System.Windows.Forms.SaveFileDialog _savefiledialog;
        System.Windows.Forms.FolderBrowserDialog _folderbrowserdialog;

        public MainForm()
        {
            InitializeComponent();
        }

        public MainForm(Form HostWindow, AddInBase Owner)
            : this()
        {
            this._mainform = HostWindow;
            this.XTheme = ((BaseForm)HostWindow).XTheme;
            this.NToolBar.BorderColor = XTheme.FormBorderOutterColor;
            this._openfiledialog = new OpenFileDialog();
            this._savefiledialog = new SaveFileDialog();
            this._folderbrowserdialog = new FolderBrowserDialog();
            this._openfiledialog.Filter = "*.exe|*.exe|*.dll|*.dll";
            this._folderbrowserdialog.Description = "请选择保存位置";
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new EventArgs());
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            List<Icon> icons = new List<Icon>();
            if (!string.IsNullOrEmpty(this.textBox1.Text))
                icons = ExtractIconHelper.ExtractIcon(this.textBox1.Text);

            if (icons.Count < 1)
            {
                MessageBox.Show("文件中没有图标资源");
                return;
            }

           
            if (_folderbrowserdialog.ShowDialog().Equals(DialogResult.OK))
            {
                int index = 0;
                string selectedPath = _folderbrowserdialog.SelectedPath;
              
                foreach (Icon icon in icons)
                {
                    index++;
                    Bitmap bitmap = Image.FromHbitmap(icon.ToBitmap().GetHbitmap());                    
                    using (System.IO.FileStream fs = new System.IO.FileStream(selectedPath + "\\newIcon" + index + ".png", System.IO.FileMode.OpenOrCreate))
                    {
                        Image image = ImageHelper.TransparentImage(bitmap);
                        image.Save(fs, System.Drawing.Imaging.ImageFormat.Png);
                        image.Dispose();
                        fs.Close();
                    }                  
                    bitmap.Dispose();
                }

                MessageBox.Show("操作完成");

            }
        }

        private void PickIcon_Click(object sender, EventArgs e)
        {
            if (this._openfiledialog.ShowDialog(this).Equals(DialogResult.OK))
            {
                if (!string.IsNullOrEmpty(this._openfiledialog.FileName))
                    this.textBox1.Text = this._openfiledialog.FileName;
            }
        }

        private void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color toolBackColor = SystemColors.Control;
            Color textColor = SystemColors.Window;
            Color linkColor = System.Drawing.Color.Blue;
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    linkColor = System.Drawing.Color.Blue;
                    toolBackColor = SystemColors.Control;
                    textColor = SystemColors.Window;
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(045, 045, 048);
                    linkColor = Color.FromArgb(051, 153, 153);
                    toolBackColor = Color.FromArgb(038, 038, 038);
                    textColor = Color.FromArgb(038, 038, 038);
                    break;
            }

            panel2.ForeColor = foreColor;
            panel2.BackColor = backColor;
            NToolBar.ForeColor = foreColor;
            NToolBar.BackColor = toolBackColor;
            btnApply.ForeColor = foreColor;
            btnApply.BackColor = toolBackColor;            
            textBox1.XForeColor = foreColor;
            textBox1.XBackColor = textColor;
            PickIcon.ForeColor = linkColor;
        }
    }
}
