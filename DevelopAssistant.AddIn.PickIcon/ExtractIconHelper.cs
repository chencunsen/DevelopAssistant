﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.AddIn.PickIcon
{
    public class ExtractIconHelper
    {
        [System.Runtime.InteropServices.DllImport("shell32.dll")]
        private static extern int ExtractIconEx(string lpszFile, int niconIndex, IntPtr[] phiconLarge, IntPtr[] phiconSmall, int nIcons);

        public static List<System.Drawing.Icon> ExtractIcon(string appPath)
        {
            List<System.Drawing.Icon> list = new List<System.Drawing.Icon>();

            IntPtr[] largeIcons, smallIcons;  //存放大/小图标的指针数组  

            //第一步：获取程序中的图标数  
            int IconCount = ExtractIconEx(appPath, -1, null, null, 0);

            //第二步：创建存放大/小图标的空间  

            largeIcons = new IntPtr[IconCount];

            smallIcons = new IntPtr[IconCount];

            //第三步：抽取所有的大小图标保存到largeIcons和smallIcons中  

            ExtractIconEx(appPath, 0, largeIcons, smallIcons, IconCount);

            //第四步：把图标添加到List  

            for (int i = 0; i < IconCount; i++)
            {
                list.Add(System.Drawing.Icon.FromHandle(largeIcons[i])); //图标添加进imageList中  
            }  

            return list;
        }
    }
}
