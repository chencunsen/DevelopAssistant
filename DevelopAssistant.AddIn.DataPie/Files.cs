﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DevelopAssistant.Service;

namespace DevelopAssistant.AddIn.DataPie
{
    [ToolboxItem(false)]
    public partial class Files : UserControl
    {
        public Files()
        {
            InitializeComponent();
        }

        public void BindInfo(DataBaseServer dataBaseServer)
        {
            this.textBox1.Text = dataBaseServer.DataBaseName;
            this.textBox2.Text = dataBaseServer.UserID;

            DataBaseHelper db = new DataBaseHelper(dataBaseServer.ConnectionString,dataBaseServer.ProviderName);
            this.dataGridView1.DataSource = db.GetDataBaseFiles();
            db.Dispose();
        }
    }
}
