﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.AddIn.DataPie
{
    public class DataBaseHelper : IDisposable
    {
        NORM.DataBase.DataBase db = null;

        string DataBaseType;

        public DataBaseHelper(string ConnectionString, string DataBaseType)
            : base()
        {            
            NORM.DataBase.DataBaseTypes _DataBaseType = NORM.DataBase.DataBaseTypes.SqlDataBase;
            switch (DataBaseType)
            {
                case "System.Data.Sql":
                case "System.Data.SQL":
                    _DataBaseType = NORM.DataBase.DataBaseTypes.SqlDataBase;
                    break;
                case "System.Data.Sqlite":
                    _DataBaseType = NORM.DataBase.DataBaseTypes.SqliteDataBase;
                    break;
                case "System.Data.PostgreSql":
                    _DataBaseType = NORM.DataBase.DataBaseTypes.PostgreSqlDataBase;
                    break;
                case "System.Data.MySql":
                    _DataBaseType = NORM.DataBase.DataBaseTypes.MySqlDataBase;
                    break;
            }
            db = NORM.DataBase.DataBaseFactory.Create(ConnectionString, _DataBaseType);
            this.DataBaseType = DataBaseType;
        }

        public DataTable GetDataBaseFiles()
        {
            DevelopAssistant.Service.DataBaseServer server = new Service.DataBaseServer(db.ConnectionString, DataBaseType);

            DataTable dataTable = new DataTable();

            dataTable.Columns.Add(new DataColumn("逻辑名称", typeof(string)));
            dataTable.Columns.Add(new DataColumn("文件类型", typeof(string)));
            dataTable.Columns.Add(new DataColumn("文件组", typeof(string)));
            dataTable.Columns.Add(new DataColumn("初始大小", typeof(string)));
            dataTable.Columns.Add(new DataColumn("自动增长", typeof(string)));
            dataTable.Columns.Add(new DataColumn("最大限制", typeof(string)));
            dataTable.Columns.Add(new DataColumn("文件名", typeof(string)));
            dataTable.Columns.Add(new DataColumn("路径", typeof(string)));

            try
            {
                switch (DataBaseType)
                {
                    case "System.Data.Sql":
                    case "System.Data.SQL":

                        string strSql = "select [Groupid],[Growth],[Status],[Perf], ";
                        strSql += "convert(float,size) * (8192.0/1024.0)/1024 as [Size], ";
                        strSql += "convert(float,maxsize) * (8192.0/1024.0)/1024 as [Maxsize] , ";
                        strSql += "name as [Logical_Name] ,filename as [Physical_Name] ";
                        strSql += "from  [dbo].[sysfiles] ";
                        var dt = db.QueryDataSet(CommandType.Text, strSql, null).Tables[0];

                        foreach (DataRow dr in dt.Rows)
                        {
                            string groupid = dr["Groupid"] + "";
                            string path = dr["Physical_Name"] + "";
                            string maxsize = dr["Maxsize"] + "";
                            string filename = path.Substring(path.LastIndexOf("\\")).TrimStart('\\');
                            string growth = string.Format("增量为{0},增长最大限制为{1} M", dr["Growth"], dr["Maxsize"]);
                            DataRow row = dataTable.NewRow();
                            row["逻辑名称"] = dr["Logical_Name"] + "";
                            row["文件类型"] = groupid == "1" ? "行数据" : "日志";
                            row["文件组"] = groupid == "1" ? "PRIMARY" : "不适用";
                            row["初始大小"] = dr["Size"] + "";
                            row["自动增长"] = growth;
                            row["最大限制"] = decimal.Parse(maxsize) < 0 ? "无限制" : maxsize;
                            row["文件名"] = filename;
                            row["路径"] = path;
                            dataTable.Rows.Add(row);
                        }

                        break;

                    case "System.Data.Sqlite":

                        DataRow nrow = dataTable.NewRow();
                        nrow["逻辑名称"] = server.DataBaseName;
                        nrow["文件类型"] = "数据库文件";
                        nrow["文件组"] = "PRIMARY";
                        nrow["最大限制"] = "无限制";                     
                        nrow["路径"] = server.DataFilePath;

                        string NPath = server.DataFilePath;
                        string NFilename = NPath.Substring(NPath.LastIndexOf("\\")).TrimStart('\\');

                        nrow["文件名"] = NFilename;

                        double filesize = 0;

                        if (System.IO.File.Exists(server.Server))
                        {
                            System.IO.FileInfo fileInfo = new System.IO.FileInfo(server.Server);
                            filesize = fileInfo.Length;
                        }

                        filesize = filesize / 1024 / 1024;
                        nrow["初始大小"] = filesize.ToString("N");

                        dataTable.Rows.Add(nrow);

                        break;
                }
            }
            catch (Exception ex)
            {
                //
            }          

            return dataTable;

        }

        public DataTable GetDataBaseObjects()
        {
            return db.GetDataBaseObject();
        }

        public DataTable GetColumnsTable(string name)
        {
            return db.GetTableObject(name);
        }

        public string GetVesion()
        {
            string version = db.GetVersionInfo();
            string[] versions = System.Text.RegularExpressions.Regex.Split(version, "\n");

            //switch (DataBaseType)
            //{
            //    case "System.Data.Sql":
            //    case "System.Data.SQL":
            //        if (versions.Length > 2)
            //        {
            //            version = versions[0] + versions[versions.Length - 2];
            //        }
            //        break;
            //}

            version = versions[0];

            return version.Replace("\r\n", "").Replace("\n", "");
        }

        public void Dispose()
        {
            db.Dispose();
        }

        
    }
}
