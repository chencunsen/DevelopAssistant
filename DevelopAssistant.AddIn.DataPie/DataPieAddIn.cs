﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevelopAssistant.AddIn;
using DevelopAssistant.AddIn.DataPie.Properties;
using ICSharpCode.WinFormsUI.Forms;
using DevelopAssistant.Service;

namespace DevelopAssistant.AddIn.DataPie
{
    public class DataPieAddIn : DockContentAddIn 
    {
        public DataPieAddIn()
        {           
            this.IdentityID = "a991f422-defc-4f6d-8445-592ab3c13cd9";
            this.Name = "提取图标V2.0";
            this.Text = "提取应用图标";
            this.Tooltip = "从EXE,DLL中提取ICON应用图标";            
            this.Icon = Resources.abiword;
        }

        public override void Initialize(string AssemblyName, string Winname)
        {
            //
        }

        public override object Execute(params object[] Parameter)
        {
            MainForm f = new MainForm((BaseForm)Parameter[0], (DataBaseServer)Parameter[2])
            { 
               // WindowFloat = new DelegateWindowFloatHandler(FloatParentCenter) 
            };         
            return f;             
        }
    }
}
