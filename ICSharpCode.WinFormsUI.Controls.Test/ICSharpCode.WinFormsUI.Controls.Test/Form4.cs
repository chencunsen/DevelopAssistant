﻿using ICSharpCode.WinFormsUI.Controls.Chart;
using ICSharpCode.WinFormsUI.Controls.Chart3D;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls.Test
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
            InitializeNChart3DControl();
        }

        public void InitializeNChart3DControl()
        {
            nChart3DControl1.ViewPort.Coordinate.Range = new SizeF(120,160);
            nChart3DControl1.ViewPort.Coordinate.ScaleIsVisable = true;

            nChart3DControl1.ShowCursorValue = true;
            nChart3DControl1.RotateStyle = RotateStyle.XY;

            nChart3DControl1.ShapeClick += new ChartEventHandler(Shape_Click);
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            nChart3DControl1.BeginUpdate();

            Point3D[] data = null;

            SmoothCurve3D line = null;
            ColumnBar3D column = null;

            // 2016

            column = nChart3DControl1.GraphicShapes.AddColumnBar3D("2016-01", new Shape3D());
            column.LineColor = Color.Yellow;
            column.FillColor = Color.Orange;
            column.Location = new Point3D(0, 53, 24);

            column = nChart3DControl1.GraphicShapes.AddColumnBar3D("2016-02", new Shape3D());
            column.LineColor = Color.Yellow;
            column.FillColor = Color.Orange;
            column.Location = new Point3D(20, 43, 24);

            column = nChart3DControl1.GraphicShapes.AddColumnBar3D("2016-03", new Shape3D());
            column.LineColor = Color.Yellow;
            column.FillColor = Color.Orange;
            column.Location = new Point3D(40, 73, 24);

            column = nChart3DControl1.GraphicShapes.AddColumnBar3D("2016-04", new Shape3D());
            column.LineColor = Color.Yellow;
            column.FillColor = Color.Orange;
            column.Location = new Point3D(60, 23, 24);

            column = nChart3DControl1.GraphicShapes.AddColumnBar3D("2016-05", new Shape3D());
            column.LineColor = Color.Yellow;
            column.FillColor = Color.Orange;
            column.Location = new Point3D(80, 4, 24);

            column = nChart3DControl1.GraphicShapes.AddColumnBar3D("2016-06", new Shape3D());
            column.LineColor = Color.Yellow;
            column.FillColor = Color.Orange;
            column.Location = new Point3D(100, 97, 24);

            // 2017

            column = nChart3DControl1.GraphicShapes.AddColumnBar3D("2017-01", new Shape3D());
            column.LineColor = Color.Yellow;
            column.FillColor = Color.Orange;
            column.Location = new Point3D(0, 43, 1);


            column = nChart3DControl1.GraphicShapes.AddColumnBar3D("2017-02", new Shape3D());
            column.LineColor = Color.Yellow;
            column.FillColor = Color.Orange;
            column.Location = new Point3D(20, 97, 1);


            column = nChart3DControl1.GraphicShapes.AddColumnBar3D("2017-03", new Shape3D());
            column.LineColor = Color.Yellow;
            column.FillColor = Color.Orange;
            column.Location = new Point3D(40, 16, 1);


            column = nChart3DControl1.GraphicShapes.AddColumnBar3D("2017-04", new Shape3D());
            column.LineColor = Color.Yellow;
            column.FillColor = Color.Orange;
            column.Location = new Point3D(60, 76, 1);


            column = nChart3DControl1.GraphicShapes.AddColumnBar3D("2017-05", new Shape3D());
            column.LineColor = Color.Yellow;
            column.FillColor = Color.Orange;
            column.Location = new Point3D(80, 46, 1);


            column = nChart3DControl1.GraphicShapes.AddColumnBar3D("2017-06", new Shape3D());
            column.LineColor = Color.Yellow;
            column.FillColor = Color.Orange;
            column.Location = new Point3D(100, 86, 1);

            line = nChart3DControl1.GraphicShapes.AddSmoothCurve3D("lines", new Shape3D());
            data = new Point3D[]{
                new Point3D(10,156,10),
                new Point3D(30,136,10),
                new Point3D(50,176,10),
                new Point3D(70,106,10),
                new Point3D(90,126,10), 
                new Point3D(110,186,10)
            };
            line.LineColor = Color.Green;
            line.DashStyleSize = 8;
            line.DashStyleColor = Color.Red;
            line.DashStyleIsVisable = true;
            line.LineWidth = 4.0F;
            line.Points = data;
           

            nChart3DControl1.EndUpdate();

        }

        private void Shape_Click(object sender, ChartEventArgs e)
        {
            MessageBox.Show(e.Shape.Name);
        }

    }
}
