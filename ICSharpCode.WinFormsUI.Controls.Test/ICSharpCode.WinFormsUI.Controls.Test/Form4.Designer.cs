﻿namespace ICSharpCode.WinFormsUI.Controls.Test
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nChart3DControl1 = new ICSharpCode.WinFormsUI.Controls.Chart3D.NChart3DControl();
            this.SuspendLayout();
            // 
            // nChart3DControl1
            // 
            this.nChart3DControl1.CursorValueFormat = "({0},{1},{2})";
            this.nChart3DControl1.Location = new System.Drawing.Point(1, 2);
            this.nChart3DControl1.Name = "nChart3DControl1";
            this.nChart3DControl1.RotateStyle = ICSharpCode.WinFormsUI.Controls.Chart3D.RotateStyle.X;
            this.nChart3DControl1.ShowCursorValue = false;
            this.nChart3DControl1.Size = new System.Drawing.Size(458, 439);
            this.nChart3DControl1.TabIndex = 0;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 443);
            this.Controls.Add(this.nChart3DControl1);
            this.Name = "Form4";
            this.Text = "Form4";
            this.Load += new System.EventHandler(this.Form4_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Chart3D.NChart3DControl nChart3DControl1;
    }
}