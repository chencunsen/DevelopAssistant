﻿namespace ICSharpCode.WinFormsUI.Controls.Test
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.nChart3DControl1 = new ICSharpCode.WinFormsUI.Controls.Chart3D.NChart3DControl();
            this.SuspendLayout();
            // 
            // nChart3DControl1
            // 
            this.nChart3DControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nChart3DControl1.BackColor = System.Drawing.Color.White;
            this.nChart3DControl1.Location = new System.Drawing.Point(10, 7);
            this.nChart3DControl1.Name = "nChart3DControl1";
            this.nChart3DControl1.RotateStyle = ICSharpCode.WinFormsUI.Controls.Chart3D.RotateStyle.X;
            this.nChart3DControl1.Size = new System.Drawing.Size(617, 463);
            this.nChart3DControl1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 478);
            this.Controls.Add(this.nChart3DControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Chart3D.NChart3DControl nChart3DControl1;

    }
}

