﻿using System;
using System.Drawing; 
using ICSharpCode.WinFormsUI.Core;
using ICSharpCode.WinFormsUI.NGraphics;
using ICSharpCode.WinFormsUI.Controls;

namespace ICSharpCode.WinFormsUI.Theme
{
    public class WinFormsUIThemeNew : WinFormsUIThemeBase
    {
        public WinFormsUIThemeNew()
            : base()
        {
            // about theme
            ThemeName = "A New Look Theme";

            UseDefaultTopRoundingFormRegion = false;
            BorderWidth = 2;
            CaptionHeight = 36;
            IconSize = new Size(24, 24);
            ControlBoxOffset = new Point(6, 9);
            ControlBoxSpace = 2;
            MaxBoxSize = MinBoxSize = CloseBoxSize = new Size(32, 18);
            SideResizeWidth = 4;

            CaptionBackColorBottom = Color.LightSlateGray;
            CaptionBackColorTop = ColorHelper.GetLighterColor(CaptionBackColorBottom, 40);
            
            RoundedStyle = RoundStyle.None;

            FormBorderOutterColor = Color.Black;
            FormBorderInnerColor = Color.FromArgb(200, Color.White);
            SetClientInset = false;
        }
    }

    public class ThemeDevExpress : WinFormsUIThemeBase
    {
        public ThemeDevExpress()
            : base()
        {
            ThemeName = "DevExpress Default";
            Name = "Dev";

            BorderWidth = 2;
            CaptionHeight = 30;
            IconSize = new Size(16, 16);
            ControlBoxOffset = new Point(8, 8);
            ControlBoxSpace = 2;            
            SideResizeWidth = 4;
            UseDefaultTopRoundingFormRegion = false;

            CloseBoxSize = MaxBoxSize = MinBoxSize = new Size(24, 17);

            CaptionBackColorBottom = SystemColors.Control;
            CaptionBackColorTop = Color.FromArgb(214, 219, 233);
            
            RoundedStyle = RoundStyle.None;

            FormBorderOutterColor = Color.FromArgb(0, 144, 198);
            FormBorderInnerColor = Color.White;
            SetClientInset = false;

            CaptionTextCenter = true;
            CaptionTextColor = Color.FromArgb(102, 102, 102);
            FormBackColor = SystemColors.Control;
        }
    }

    public class ThemeVS2012 : WinFormsUIThemeBase
    {
        public ThemeVS2012()
            : base()
        {
            ThemeName = "A VS2012 Look";
            Name = "VS2012";

            BorderWidth = 1;
            CaptionHeight = 32;
            IconLeftMargin = 10;
            IconSize = new Size(20, 20);
            CloseBoxSize = MaxBoxSize = MinBoxSize = new Size(34, 27);
            ControlBoxOffset = new Point(1, 1);
            ControlBoxSpace = 1;
            SideResizeWidth = 6;
            UseDefaultTopRoundingFormRegion = false;

            CaptionBackColorBottom = CaptionBackColorTop = Color.FromArgb(214, 219, 233);

            RoundedStyle = RoundStyle.None;

            FormBorderOutterColor = Color.FromArgb(19, 124, 197);
            FormBorderInnerColor = Color.FromArgb(19, 124, 197);
            SetClientInset = false;
            
            CaptionTextColor = Color.FromArgb(0, 0, 0);
            //FormBackColor = Color.FromArgb(42, 58, 86);             
            FormBackColor = SystemColors.Control;

            CloseBoxColor = ButtonColorTable.GetColorTableVs2013Theme();
            MaxBoxColor = MinBoxColor = CloseBoxColor;            
        }
    }

    public class ThemeMac : ThemeVS2012
    {
        public ThemeMac()
            : base()
        {
            ThemeName = "A mac look theme by user";
            Name = "Mac";

            IconSize = new Size(16, 16);
            FormBorderOutterColor = SystemColors.ControlDarkDark; //Color.FromArgb(101, 101, 101);
            FormBorderInnerColor = SystemColors.ControlDark;
            BorderWidth = 1;
            CaptionHeight = 30;
            CaptionBackColorTop = Color.FromArgb(230, 230, 230);
            CaptionBackColorBottom = Color.FromArgb(205, 205, 205);
            FormBackColor = SystemColors.Control; //SystemColors.Control; //Color.FromArgb(240, 240, 240);
            CaptionTextCenter = true;
            RoundedStyle = RoundStyle.Top;
            Radius = 4; //4

            //shadow
            SideResizeWidth = 0;
            ShowShadow = true;
            ShadowWidth = 4; //2;
            ShadowColor = SystemColors.Control; //Color.FromArgb(207, 207, 207); //Color.Black;
            ShadowAValueDark = 255;
            ShadowAValueLight = 10;
            UseShadowToResize = true;//允许拖放大小

            CloseBoxSize = MaxBoxSize = MinBoxSize = new Size(16, 16);
            ControlBoxOffset = new Point(12, 8);
            ControlBoxSpace = 6;

            CloseBoxBackImageNormal = Properties.Resources.CloseNormal;
            CloseBoxBackImageHover = Properties.Resources.CloseHover;
            CloseBoxBackImagePressed = Properties.Resources.CloseDown;

            MaxBoxBackImageNormal = Properties.Resources.MaxNormal;
            MaxBoxBackImageHover = Properties.Resources.MaxHover;
            MaxBoxBackImagePressed = Properties.Resources.MaxDown;

            ResBoxBackImageNormal = Properties.Resources.ResNormal;
            ResBoxBackImageHover = Properties.Resources.ResHover;
            ResBoxBackImagePressed = Properties.Resources.ResDown;

            MinBoxBackImageNormal = Properties.Resources.MinNormal;
            MinBoxBackImageHover = Properties.Resources.MinHover;
            MinBoxBackImagePressed = Properties.Resources.MinDown;

        }
    }

    public class ThemeShadow : ThemeVS2012
    {
        public ThemeShadow()
            : base()
        {
            ThemeName = "Shadow form demo";
            Name = "Shadow";

            IconSize = new Size(20, 20);
            IconLeftMargin = 8;
            BorderWidth = 2;
            CaptionHeight = 32;
            FormBorderOutterColor = Color.FromArgb(123, 79, 202);
            FormBorderInnerColor = Color.FromArgb(124,123, 79, 202); //Color.FromArgb(120, Color.White);
            CaptionBackColorTop = Color.FromArgb(123, 79, 202);
            CaptionBackColorBottom = Color.FromArgb(123, 79, 202);
            //FormBackColor = Color.FromArgb(242, 242, 242);  
            FormBackColor = SystemColors.Control;
            CaptionTextColor = Color.White;

            // shadow
            SideResizeWidth = 0;
            ShowShadow = true;
            ShadowWidth = 6;
            ShadowColor = Color.Black;
            ShadowAValueDark = 50;
            ShadowAValueLight = 0;
            UseShadowToResize = true;//允许拖放大小

            CloseBoxSize = MaxBoxSize = MinBoxSize = new Size(30, 23);
            ControlBoxOffset = new Point(2, 2);
            ControlBoxSpace = 2;

            ButtonColorTable closeTable = new ButtonColorTable();
            closeTable.ForeColorNormal = closeTable.ForeColorHover
                = closeTable.ForeColorPressed = Color.FromArgb(249, 240, 223);
            closeTable.BackColorHover = Color.FromArgb(217, 71, 71);
            closeTable.BackColorPressed = Color.FromArgb(188, 52, 52);
            CloseBoxColor = closeTable;

            ButtonColorTable minTable = new ButtonColorTable();
            minTable.ForeColorNormal = minTable.ForeColorHover
                = minTable.ForeColorPressed = Color.FromArgb(249, 240, 223);
            minTable.BackColorHover = Color.FromArgb(67, 139, 221);
            minTable.BackColorPressed = Color.FromArgb(50, 125, 210);
            MinBoxColor = MaxBoxColor = minTable;
        }
    }

}
