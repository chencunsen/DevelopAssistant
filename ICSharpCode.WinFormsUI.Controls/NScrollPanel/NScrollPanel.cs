﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NScrollPanel : UserControl
    {
        internal Panel outerPanel;
        internal Panel innerPanel;
        internal NVScrollBar vScrollBar1;

        public ControlCollection DataTablePanels
        {
            get
            {
                return innerPanel.Controls;
            }
        }

        private NScrollBarRenderer scrollBarRenderer = null;

        private NScrollPanelControlCollection _controls = null;

        public new NScrollPanelControlCollection Controls
        {
            get
            {
                return _controls;
            }
            private set
            {
                _controls = value;
            }
        }

        public NScrollPanel()
        {
            InitializeComponent();
            InitializeControls();
        }

        public void InitializeComponent()
        {
            this.outerPanel = new System.Windows.Forms.Panel();
            this.innerPanel = new System.Windows.Forms.Panel();
            this.vScrollBar1 = new NVScrollBar();
            this.outerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // outerPanel
            // 
            this.outerPanel.AutoScroll = false;            
            this.outerPanel.Controls.Add(this.innerPanel);
            this.outerPanel.Location = new System.Drawing.Point(0, 0);
            this.outerPanel.Name = "outerPanel";
            this.outerPanel.Size = new System.Drawing.Size(200, 165);
            this.outerPanel.TabIndex = 6;
            // 
            // innerPanel
            // 
            this.innerPanel.AutoScroll = true;
            this.innerPanel.Location = new System.Drawing.Point(0, 0);
            this.innerPanel.Name = "innerPanel";
            this.innerPanel.Size = new System.Drawing.Size(196, 162);
            this.innerPanel.TabIndex = 0;
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.LargeChange = 15;
            this.vScrollBar1.Location = new System.Drawing.Point(205, 6);
            this.vScrollBar1.Maximum = 250;
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(17, 162);
            this.vScrollBar1.TabIndex = 5;
            this.vScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.VScrollBar_Scroll);
            // 
            // NScrollPanel
            // 
            base.Controls.Add(this.outerPanel);
            base.Controls.Add(this.vScrollBar1);
            this.Name = "NScrollPanel";
            this.AutoScroll = false;
            this.Size = new System.Drawing.Size(224, 172);            
            this.outerPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        public void InitializeControls()
        {
            this.Controls = new NScrollPanelControlCollection(this);

            this.vScrollBar1.Minimum = 0;
            this.vScrollBar1.Maximum = this.innerPanel.DisplayRectangle.Height;
            this.vScrollBar1.LargeChange = vScrollBar1.Maximum / vScrollBar1.Height + this.innerPanel.Height;
            this.vScrollBar1.SmallChange = 15;
            this.vScrollBar1.Value = Math.Abs(this.innerPanel.AutoScrollPosition.Y);
            UpdataScrollBar();
        }

        public void UpdataScrollBar()
        {
            innerPanel.Size = new Size(this.Width, this.Height);
            innerPanel.Location = new Point(0, 0);

            var controlsWidth = outerPanel.Size.Width;
            if (!innerPanel.VerticalScroll.Visible)
            {
                vScrollBar1.Visible = false;
                controlsWidth = this.Width;
            }
            else
            {               
                vScrollBar1.Visible = true;
                controlsWidth = this.Width - ScrollBarInformation.VerticalScrollBarWidth;
            }

            vScrollBar1.Size = new Size(ScrollBarInformation.VerticalScrollBarWidth, this.Height);
            vScrollBar1.Location = new Point(this.Width - vScrollBar1.Width, 0);

            outerPanel.Size = new Size(controlsWidth, this.Height);
            outerPanel.Location = new Point(0, 0);

            this.vScrollBar1.Maximum = this.innerPanel.DisplayRectangle.Height;
            this.vScrollBar1.LargeChange = vScrollBar1.Maximum / vScrollBar1.Height + this.innerPanel.Height;

        }

        private void VScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            innerPanel.AutoScrollPosition = new Point(0, vScrollBar1.Value);
        }

        private void UIComponentLayer()
        {
            int offset = 0;
            var needUpdateScrollBar = false;
            foreach (Control control in innerPanel.Controls)
            {
                if (control is NDataTablePanel)
                {
                    var dataTablePanel = (NDataTablePanel)control;
                    if (dataTablePanel.Precent <= 3)
                    {
                        var width = outerPanel.Width;
                        var height = outerPanel.Height / dataTablePanel.Precent - 3;
                        control.SetBounds(0, offset, width, height);                        
                    }
                    needUpdateScrollBar = true;
                }
                offset += control.Height + 6;
            }
            if (needUpdateScrollBar)
                UpdataScrollBar();
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);           
            UpdataScrollBar();
            UIComponentLayer();
        }

        public void SetTheme(string themeName)
        {            
            switch (themeName)
            {
                case "Default":
                    vScrollBar1.BorderColor = SystemColors.ControlLight;
                    //hScrollBar.BorderColor = SystemColors.ControlLight;
                    vScrollBar1.ScrollBarRenderer = new WhiteScrollBarRenderer();
                    //hScrollBar.ScrollBarRenderer = new WhiteScrollBarRenderer();
                    BackColor = SystemColors.Window;
                    //foreColor = SystemColors.WindowText;                   
                    break;
                case "Black":
                    vScrollBar1.BorderColor = Color.FromArgb(93, 140, 201);
                    //hScrollBar.BorderColor = Color.FromArgb(93, 140, 201);
                    vScrollBar1.ScrollBarRenderer = new BlackScrollBarRenderer();
                    //hScrollBar.ScrollBarRenderer = new BlackScrollBarRenderer();
                    BackColor = Color.FromArgb(045, 045, 048);
                    //foreColor = Color.FromArgb(200, 200, 200);                   
                    break;
            }
            scrollBarRenderer = vScrollBar1.ScrollBarRenderer;            
        }
    }

    public class NDataTablePanel : Panel
    {
        public int Precent { get; set; }  
        
        public string TableName { get; set; } 
    }
}
