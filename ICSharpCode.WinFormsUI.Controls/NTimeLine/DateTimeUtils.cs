﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class DateTimeUtils
    {
        /// <summary>
        /// 判断是否在本月
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        private static bool IsCurrentMonth(DateTime datetime)
        {
            DateTime nowDateTime = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));
            DateTime startMonth = nowDateTime.AddDays(1 - nowDateTime.Day);  //本月月初
            DateTime endMonth = startMonth.AddMonths(1).AddDays(-1);  //本月月末

            if (datetime >= startMonth && datetime <= endMonth)
            {
                return true;
            }

            return false;
        }

        public static string ToDateLabel(DateTime dateTime)
        {
            string result = dateTime.Month.ToString() + "月";
            if (IsCurrentMonth(dateTime))
            {
                result = "本月";
            }
            return result;
        }
    }
}
