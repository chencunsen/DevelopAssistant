﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NListItemArray : IEnumerator
    {
        private int position;

        private object[] items = null;

        public object Current
        {
            get
            {
                return (object)this.items[this.position];
            }
        }

        public NListItemArray(object[] list)
        {
            this.items = list;
        }

        public bool MoveNext()
        {
            ++this.position;
            return this.position < this.items.Length;
        }

        public void Reset()
        {
            this.position = -1;
        }

    }

    public class NListItemCollection : IEnumerable
    {
        //NListBox listBox = null;
        List<object> listArray = new List<object>();

        public int Count
        {
            get
            {
                return listArray.Count;
            }
        }

        public object this[int index]
        {
            get
            {
                return listArray[index];
            }
        }

        public NListItemCollection(NListBox owner)
        {
            //this.listBox = owner;
        }

        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)new NListItemArray(listArray.ToArray());
        }

        public void Clear()
        {
            this.listArray.Clear();
            //this.listBox.SetScrollBarInformation();
        }

        public void Add(object item)
        {
            this.listArray.Add(item);
        }

        public void AddRange(IEnumerable<object> items)
        {
            this.listArray.AddRange(items);
        }

        public void Remove(object item)
        {
            this.listArray.Remove(item);
        }

        public void RemoveAt(int index)
        {
            this.listArray.RemoveAt(index);
        }

    }
}
