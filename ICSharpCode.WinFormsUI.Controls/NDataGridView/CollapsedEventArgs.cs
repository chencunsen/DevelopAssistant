﻿// Type: ICSharpCode.WinFormsUI.Controls.CollapsedEventArgs
// Assembly: ExpandableGridView, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 01AF7F03-14C8-403C-9988-D5C2B7E87D00
// Assembly location: C:\Users\lenovo\Desktop\TreeGridViewTest\TreeGridViewTest\lib\ExpandableGridView.dll

namespace ICSharpCode.WinFormsUI.Controls
{
    public class CollapsedEventArgs : TreeGridNodeEventBase
    {
        public CollapsedEventArgs(TreeGridNode node)
            : base(node)
        {
        }
    }
}
