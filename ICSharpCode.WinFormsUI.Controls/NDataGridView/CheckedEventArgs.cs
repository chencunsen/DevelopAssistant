﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class CheckedEventArgs : TreeGridNodeEventBase
    {
        public CheckedEventArgs(TreeGridNode node)
            : base(node)
        {
        }
    }
}
