﻿// Type: ICSharpCode.WinFormsUI.Controls.TreeGridNode
// Assembly: ExpandableGridView, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 01AF7F03-14C8-403C-9988-D5C2B7E87D00
// Assembly location: C:\Users\lenovo\Desktop\TreeGridViewTest\TreeGridViewTest\lib\ExpandableGridView.dll

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Design;
using System.Globalization;
using System.Text;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    [ToolboxItem(false)]
    [DesignTimeVisible(false)]
    public class TreeGridNode : DataGridViewRow
    {
        private Random rndSeed = new Random();
        public int UniqueValue = -1;
        private bool childCellsCreated = false;
        private ISite site = (ISite)null;
        private EventHandler disposed = (EventHandler)null;
        internal NTreeGridView _grid;
        internal TreeGridNode _parent;
        internal TreeGridNodeCollection _owner;
        internal bool IsExpanded;
        internal bool IsRoot;
        internal bool _isSited;
        internal bool _isCheckbox;
        internal bool _isFirstSibling;
        internal bool _isLastSibling;
        internal Image _image;
        internal int _imageIndex;
        private TreeGridCell _treeCell;
        private TreeGridNodeCollection childrenNodes;
        private int _index;
        private int _level;
        private object _id;

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]        
        [Browsable(false)]
        public object Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Description("Represents the index of this row in the Grid. Advanced usage.")]
        [Browsable(false)]
        public int RowIndex
        {
            get
            {
                return base.Index;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new int Index
        {
            get
            {
                if (this._index == -1)
                    this._index = this._owner.IndexOf(this);
                return this._index;
            }
            internal set
            {
                this._index = value;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public ImageList ImageList
        {
            get
            {
                if (this._grid != null)
                    return this._grid.ImageList;
                else
                    return (ImageList)null;
            }
        }

        [TypeConverter(typeof(ImageIndexConverter))]
        [Category("Appearance")]
        [Editor("System.Windows.Forms.Design.ImageIndexEditor", typeof(UITypeEditor))]
        [DefaultValue(-1)]
        [Description("...")]
        public int ImageIndex
        {
            get
            {
                return this._imageIndex;
            }
            set
            {
                this._imageIndex = value;
                if (this._imageIndex != -1)
                    this._image = (Image)null;
                if (!this._isSited)
                    return;
                this._treeCell.UpdateStyle();
                if (this.Displayed)
                    this._grid.InvalidateRow(this.RowIndex);
            }
        }

        public Image Image
        {
            get
            {
                if (this._image != null || this._imageIndex == -1)
                    return this._image;
                if (this.ImageList != null && this._imageIndex < this.ImageList.Images.Count)
                    return this.ImageList.Images[this._imageIndex];
                else
                    return (Image)null;
            }
            set
            {
                this._image = value;
                if (this._image != null)
                    this._imageIndex = -1;
                if (!this._isSited)
                    return;
                this._treeCell.UpdateStyle();
                if (this.Displayed)
                    this._grid.InvalidateRow(this.RowIndex);
            }
        }

        public bool Checkbox
        {
            get
            {                
                return _isCheckbox;
            }
            set
            {
                _isCheckbox = value;
                if (!this._isSited)
                    return;
                this._treeCell.UpdateStyle();
                if (this.Displayed)
                    this._grid.InvalidateRow(this.RowIndex);
            }
        }

        [Editor(typeof(CollectionEditor), typeof(UITypeEditor))]
        [Description("The collection of root nodes in the treelist.")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Category("Data")]
        public TreeGridNodeCollection Nodes
        {
            get
            {
                if (this.childrenNodes == null)
                    this.childrenNodes = new TreeGridNodeCollection(this);
                return this.childrenNodes;
            }
            set
            {
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public new DataGridViewCellCollection Cells
        {
            get
            {
                if (!this.childCellsCreated && this.DataGridView == null)
                {
                    if (this._grid == null)
                        return (DataGridViewCellCollection)null;
                    this.CreateCells((DataGridView)this._grid);
                    this.childCellsCreated = true;
                }
                return base.Cells;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Level
        {
            get
            {
                if (this._level == -1)
                {
                    int num = 0;
                    for (TreeGridNode parent = this.Parent; parent != null; parent = parent.Parent)
                        ++num;
                    this._level = num;
                }
                return this._level;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public TreeGridNode Parent
        {
            get
            {
                return this._parent;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual bool HasChildren
        {
            get
            {
                return this.childrenNodes != null && this.Nodes.Count != 0;
            }
        }

        [Browsable(false)]
        public bool IsSited
        {
            get
            {
                return this._isSited;
            }
        }

        [Browsable(false)]
        public bool IsFirstSibling
        {
            get
            {
                return this.Index == 0;
            }
        }

        [Browsable(false)]
        public bool IsLastSibling
        {
            get
            {
                TreeGridNode parent = this.Parent;
                if (parent != null && parent.HasChildren)
                    return this.Index == parent.Nodes.Count - 1;
                else
                    return true;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public ISite Site
        {
            get
            {
                return this.site;
            }
            set
            {
                this.site = value;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        [Browsable(false)]
        public event EventHandler Disposed
        {
            add
            {
                this.disposed += value;
            }
            remove
            {
                this.disposed -= value;
            }
        }

        internal TreeGridNode(NTreeGridView owner)
            : this()
        {
            this._grid = owner;
            this.IsExpanded = true;
        }

        public TreeGridNode()
        {
            this._index = -1;
            this._level = -1;
            this.IsExpanded = false;
            this.UniqueValue = this.rndSeed.Next();
            this._isSited = false;
            this._isCheckbox = true;
            this._isFirstSibling = false;
            this._isLastSibling = false;
            this._imageIndex = -1;
        }

        public override object Clone()
        {
            TreeGridNode treeGridNode = (TreeGridNode)base.Clone();
            treeGridNode.UniqueValue = -1;
            treeGridNode._level = this._level;
            treeGridNode._grid = this._grid;
            treeGridNode._parent = this.Parent;
            treeGridNode._imageIndex = this._imageIndex;
            if (treeGridNode._imageIndex == -1)
                treeGridNode.Image = this.Image;
            treeGridNode.IsExpanded = this.IsExpanded;
            return (object)treeGridNode;
        }

        protected internal virtual void UnSited()
        {
            foreach (DataGridViewCell dataGridViewCell in (BaseCollection)this.Cells)
            {
                TreeGridCell treeGridCell = dataGridViewCell as TreeGridCell;
                if (treeGridCell != null)
                    treeGridCell.UnSited();
            }
            this._isSited = false;
        }

        protected internal virtual void Sited()
        {
            this._isSited = true;
            this.childCellsCreated = true;
            Debug.Assert(this._grid != null);
            foreach (DataGridViewCell dataGridViewCell in (BaseCollection)this.Cells)
            {
                TreeGridCell treeGridCell = dataGridViewCell as TreeGridCell;
                if (treeGridCell != null)
                    treeGridCell.Sited();
            }
        }

        private bool ShouldSerializeImageIndex()
        {
            return this._imageIndex != -1 && this._image == null;
        }

        private bool ShouldSerializeImage()
        {
            return this._imageIndex == -1 && this._image != null;
        }

        protected override DataGridViewCellCollection CreateCellsInstance()
        {
            DataGridViewCellCollection cellsInstance = base.CreateCellsInstance();
            cellsInstance.CollectionChanged += new CollectionChangeEventHandler(this.cells_CollectionChanged);
            return cellsInstance;
        }

        private void cells_CollectionChanged(object sender, CollectionChangeEventArgs e)
        {
            if (this._treeCell != null || e.Action != CollectionChangeAction.Add && e.Action != CollectionChangeAction.Refresh)
                return;
            TreeGridCell treeGridCell = (TreeGridCell)null;
            if (e.Element == null)
            {
                foreach (DataGridViewCell dataGridViewCell in (BaseCollection)base.Cells)
                {
                    if (dataGridViewCell.GetType().IsAssignableFrom(typeof(TreeGridCell)))
                    {
                        treeGridCell = (TreeGridCell)dataGridViewCell;
                        break;
                    }
                }
            }
            else
                treeGridCell = e.Element as TreeGridCell;
            if (treeGridCell != null)
                this._treeCell = treeGridCell;
        }

        public virtual bool Collapse()
        {
            return this._grid.CollapseNode(this);
        }

        public virtual bool Expand()
        {
            if (this._grid != null)
                return this._grid.ExpandNode(this);
            this.IsExpanded = true;
            return true;
        }

        protected internal virtual bool InsertChildNode(int index, TreeGridNode node)
        {
            node._parent = this;
            node._grid = this._grid;
            if (this._grid != null)
                this.UpdateChildNodes(node);
            if ((this._isSited || this.IsRoot) && this.IsExpanded)
                this._grid.SiteNode(node);
            return true;
        }

        protected internal virtual bool InsertChildNodes(int index, params TreeGridNode[] nodes)
        {
            foreach (TreeGridNode node in nodes)
                this.InsertChildNode(index, node);
            return true;
        }

        protected internal virtual bool AddChildNode(TreeGridNode node)
        {
            node._parent = this;
            node._grid = this._grid;
            if (this._grid != null)
                this.UpdateChildNodes(node);
            if ((this._isSited || this.IsRoot) && this.IsExpanded && !node._isSited)
                this._grid.SiteNode(node);
            return true;
        }

        protected internal virtual bool AddChildNodes(params TreeGridNode[] nodes)
        {
            foreach (TreeGridNode node in nodes)
                this.AddChildNode(node);
            return true;
        }

        protected internal virtual bool RemoveChildNode(TreeGridNode node)
        {
            if ((this.IsRoot || this._isSited) && this.IsExpanded)
                this._grid.UnSiteNode(node);
            node._grid = (NTreeGridView)null;
            node._parent = (TreeGridNode)null;
            return true;
        }

        protected internal virtual bool ClearNodes()
        {
            if (this.HasChildren)
            {
                for (int index = this.Nodes.Count - 1; index >= 0; --index)
                    this.Nodes.RemoveAt(index);
            }
            return true;
        }

        private void UpdateChildNodes(TreeGridNode node)
        {
            if (!node.HasChildren)
                return;
            foreach (TreeGridNode node1 in node.Nodes)
            {
                node1._grid = node._grid;
                this.UpdateChildNodes(node1);
            }
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder(36);
            stringBuilder.Append("TreeGridNode { Index=");
            stringBuilder.Append(this.RowIndex.ToString((IFormatProvider)CultureInfo.CurrentCulture));
            stringBuilder.Append(" }");
            return ((object)stringBuilder).ToString();
        }
    }
}
