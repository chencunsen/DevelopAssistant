﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NWorkbench : NPanel
    {
        public string Title { get; set; }
        public Bitmap Icon { get; set; }

        public Bitmap AlarmIcon { get; set; }
        
        private int drawPosY = 0;
        private int lastSelectedItemIndex = -1;
        private Padding padding = new Padding(0, 0, 0, 0);

        private List<NWorkbenchItem> _workbenchList;
        public List<NWorkbenchItem> WorkbenchList
        {
            get { return _workbenchList; }
            set
            {
                _workbenchList = value;
                this.Invalidate();
            }
        }

        public event EventHandler<NWorkbenchEventArgs> ItemClick;

        private Rectangle HeaderRectangle = new Rectangle();

        private bool _todoOpenState;
        public bool TodoOpenState
        {
            get { return _todoOpenState; }
            set
            {
                _todoOpenState = value;
                this.Invalidate();
            }
        }

        private string _themeName;
        public string ThemeName
        {
            get { return _themeName; }
            set
            {
                _themeName = value;
                this.Invalidate();
            }
        }

        public NWorkbench()
        {
            this.Font = NToolStripItemTheme.TextFont;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer |
                    ControlStyles.ResizeRedraw |
                    ControlStyles.AllPaintingInWmPaint, true);
        }

        public void ReDraw()
        {
            this.Invalidate();
        }

        public void SetTheme(string themeName)
        {
            this._themeName = themeName;
            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            DrawHeader(g);
            DrawBody(g);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (_workbenchList == null)
                return;

            Cursor newCursor = Cursors.Default;
            int selectedItemIndex = -1;
            for (int i = 0; i < _workbenchList.Count; i++)
            {
                var item = _workbenchList[i];
                if (item.Bound.Contains(e.Location))
                {
                    selectedItemIndex = i;
                    item.MouseInsideState = true;
                    newCursor = Cursors.Hand;
                }
                else
                {
                    item.MouseInsideState = false;
                }
            }
            if (this.Cursor != newCursor)
            {
                this.Cursor = newCursor;
            }

            if(selectedItemIndex!= lastSelectedItemIndex)
            {
                this.Invalidate();
            }

            lastSelectedItemIndex = selectedItemIndex;

        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            if (_workbenchList == null)
                return;

            for (int i = 0; i < _workbenchList.Count; i++)
            {
                var item = _workbenchList[i];
                if (item.Bound.Contains(e.Location))
                {
                    if (ItemClick != null)
                        ItemClick(this,new NWorkbenchEventArgs(item));
                }
            }
        }

        void DrawHeader(Graphics g)
        {
            Color headForeColor = Color.Black;
            Color headBlackgroundColor = Color.FromArgb(240, 240, 240);

            switch (_themeName)
            {
                case "Default":
                    headForeColor = Color.Black;
                    headBlackgroundColor = Color.FromArgb(240, 240, 240);
                    break;
                case "Black":
                    headForeColor = Color.FromArgb(220, 220, 220);
                    headBlackgroundColor = Color.FromArgb(045, 045, 048);                    
                    break;
            }            

            HeaderRectangle = new Rectangle(padding.Left, padding.Top, this.Width - padding.Left - padding.Right, 29);
            g.FillRectangle(new SolidBrush(headBlackgroundColor), HeaderRectangle);

            StringFormat titleTextAlignment = new StringFormat() { LineAlignment = StringAlignment.Center };
            Rectangle titleTextRectangle = new Rectangle(15, 3, this.Width - 30, 26);
            g.DrawString(Title, this.Font, new SolidBrush(headForeColor), titleTextRectangle, titleTextAlignment);

            if (AlarmIcon!=null && _todoOpenState)
            {
                g.DrawImage(AlarmIcon, new Rectangle(Width - AlarmIcon.Width - 16, (30 - AlarmIcon.Height) / 2, AlarmIcon.Width, AlarmIcon.Height));
            }

            g.DrawLine(new Pen(SystemColors.ControlDark), new Point(0, 29), new Point(this.Width, 29));

            drawPosY = HeaderRectangle.Height;
        }

        void DrawBody(Graphics g)
        {
            if (_workbenchList == null)
                return;

            int itemHeight = 33;

            drawPosY = drawPosY + 2 + padding.Top;

            StringFormat stringFormat = new StringFormat();
            stringFormat.LineAlignment = StringAlignment.Center;

            StringFormat numberStringFormat = new StringFormat();
            numberStringFormat.Alignment = StringAlignment.Center;
            numberStringFormat.LineAlignment = StringAlignment.Center;

            Font numberFont = new Font("黑体", 12.0f);

            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            var textBrush = new SolidBrush(Color.Black);
            var textFont = new Font("宋体", 10.0f);
            var mouseInBrush = new SolidBrush(Color.FromArgb(248, 248, 248));
            var splitLine = SystemColors.ControlLight;

            switch (_themeName) {
                case "Default":
                    textBrush = new SolidBrush(Color.Black);
                    splitLine = SystemColors.ControlLight;
                    mouseInBrush = new SolidBrush(Color.FromArgb(248, 248, 248));
                    break;
                case "Black":
                    textBrush = new SolidBrush(Color.FromArgb(220, 220, 220));
                    splitLine = Color.FromArgb(45, 45, 45);
                    mouseInBrush = new SolidBrush(Color.FromArgb(60, 60, 60));
                    break;
            }        

            for (int index = 0; index < _workbenchList.Count; index++)
            {
                var item = _workbenchList[index];                

                Rectangle itemOutRectangle = new Rectangle(padding.Left, drawPosY, this.Width - padding.Left - padding.Right, itemHeight);

                Rectangle itemRectangle = new Rectangle(10 + padding.Left, drawPosY, this.Width - 20 - padding.Left - padding.Right, itemHeight);

                if (item.MouseInsideState)
                {
                    using (var roundedRectanglePath = CommonHelper.CreateRoundedRectanglePath(itemOutRectangle, 2))
                    {
                        g.FillPath(mouseInBrush, roundedRectanglePath);
                    }
                    textFont = new Font("宋体", 10.0f, FontStyle.Underline);
                    textBrush = new SolidBrush(Color.FromArgb(0, 153, 153));
                }

                Rectangle iconRectangle = new Rectangle(10 + padding.Left, drawPosY + (itemHeight - 24) / 2, 24, 24);
                g.FillEllipse(new SolidBrush(Color.FromArgb(51, 153, 153)), iconRectangle);
                g.DrawString((index + 1).ToString(), numberFont, Brushes.White, iconRectangle, numberStringFormat);

                Rectangle TextRectangle = new Rectangle(10 + 24 + 6 + padding.Left, drawPosY, this.Width - 20 - 140 - 24 - 12 - padding.Left, itemHeight);
                //g.FillRectangle(Brushes.Yellow, TextRectangle);
                g.DrawString(item.Title, textFont, textBrush, TextRectangle, stringFormat);

                if (item.MouseInsideState)
                {
                    textFont = new Font("宋体", 10.0f);
                }

                Rectangle DatetimeRectangle = new Rectangle(this.Width - 10 - 140 - padding.Left, drawPosY, 140, itemHeight);
                //g.FillRectangle(Brushes.Red, DatetimeRectangle);
                g.DrawString(item.DateTime.ToString("yyyy-MM-dd HH:mm:ss"), textFont, textBrush, DatetimeRectangle, stringFormat);

                if (!item.MouseInsideState)
                    g.DrawLine(new Pen(splitLine, 1.0f), new Point(0 + padding.Left, drawPosY + itemHeight), new Point(this.Width - 0 - padding.Left - padding.Right, drawPosY + itemHeight));

                drawPosY = drawPosY + itemHeight;

                item.Bound = itemOutRectangle;

            }

            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;

        }

    }
}
