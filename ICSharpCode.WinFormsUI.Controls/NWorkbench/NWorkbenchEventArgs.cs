﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NWorkbenchEventArgs : EventArgs
    {
        public NWorkbenchItem Item { get; set; }

        public NWorkbenchEventArgs(NWorkbenchItem Item)
        {
            this.Item = Item;
        }
    }
}
