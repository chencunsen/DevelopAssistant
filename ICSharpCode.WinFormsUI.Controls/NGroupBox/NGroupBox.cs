﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NGroupBox : NPanel
    {
        private string _title = "";
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                this.Invalidate();
            }
        }

        private Color _gridLineColor = Color.FromArgb(189, 189, 189);
        public Color GridLineColor
        {
            get { return _gridLineColor; }
            set
            {
                _gridLineColor = value;
                this.Invalidate();
            }
        }        

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            g.SmoothingMode = SmoothingMode.HighQuality;

            Rectangle rect = new Rectangle(1, 4, Width - 2, Height - 8);
            using (GraphicsPath graphicsPath = CommonHelper.CreateRoundedRectanglePath(rect, 2))
            {
                g.DrawPath(new Pen(_gridLineColor), graphicsPath);
            }

            g.SmoothingMode = SmoothingMode.Default;

            if (!string.IsNullOrEmpty(this.Title))
            {
                SizeF size = g.MeasureString(this.Title, this.Font);
                RectangleF textRect = new RectangleF(16, 0, size.Width, size.Height);
                g.FillRectangle(new SolidBrush(this.BackColor), textRect);
                g.DrawString(this.Title, this.Font, new SolidBrush(ForeColor), new Point(16, 0));
            }


        }
    }
}
