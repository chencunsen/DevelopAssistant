﻿using ICSharpCode.WinFormsUI.Controls.NodeControls;
using System;
using System.Collections.ObjectModel;
using System.Drawing;

namespace ICSharpCode.WinFormsUI.Controls
{
    #region Inner Classes

    internal struct NodeControlInfo
    {
        public static readonly NodeControlInfo Empty = new NodeControlInfo();

        private NodeControl _control;
        public NodeControl Control
        {
            get { return _control; }
        }

        private Rectangle _bounds;
        public Rectangle Bounds
        {
            get { return _bounds; }
        }

        public NodeControlInfo(NodeControl control, Rectangle bounds)
        {
            _control = control;
            _bounds = bounds;
        }
    }

    internal class ExpandedNode
    {
        private object _tag;
        public object Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        private ExpandedNodeCollection _children = new ExpandedNodeCollection();
        public ExpandedNodeCollection Children
        {
            get { return _children; }
            set { _children = value; }
        }
    }

    internal class ExpandedNodeCollection : Collection<ExpandedNode>
    {

    }

    #endregion
}
