﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NTreeNodeCollection : Collection<NTreeNode>
    {
        protected NTreeNode node;
        protected NTreeView tree;

        List<string> itemRecords = new List<string>();

        public NTreeNodeCollection(NTreeNode node)
        {
            this.node = node;
            this.tree = node.Tree;
        }

        public NTreeNodeCollection(NTreeView tree, NTreeNode node)
        {
            this.tree = tree;
            this.node = node;
        }

        internal void SetTree(NTreeView tree)
        {
            this.tree = tree;
        }

        internal bool ItemInserted(NTreeNode node)
        {
            bool result = false;
            foreach(string identity in itemRecords)
            {
                if (identity == node.Identity)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }         

        protected override void InsertItem(int index, NTreeNode item)
        {
            int Identity = this.Count;
            item.Identity = node.Identity + "_" + Identity.ToString();

            if (ItemInserted(item))
                return;

            this.itemRecords.Add(item.Identity);

            base.InsertItem(index, item);

            if (tree != null && tree.Model == null)
            {
                item.Tree = tree;
                item.Parent = node;
                tree.OnNTreeNodeCollectionInsertItem(index, item);
            }
            
        }

        protected override void RemoveItem(int index)
        {
            if (!ItemInserted(this[index]))
                return;

            this.itemRecords.Remove(this[index].Identity);

            base.RemoveItem(index);

            if (tree.Model == null)
            {
                tree.OnNTreeNodeCollectionRemoveItem(index);
            }
        }

        protected override void SetItem(int index, NTreeNode item)
        {
            base.SetItem(index, item);

            if (tree.Model == null)
            {
                tree.OnNTreeNodeCollectionSetItem(index, item);
            }
        }

        protected override void ClearItems()
        {
            this.itemRecords = new List<string>();

            base.ClearItems();

            if (tree.Model == null)
            {
                tree.OnNTreeNodeCollectionClearItems();
            }
        }

        public void AddRange(NTreeNode[] nodes)
        {
            if (nodes != null)
            {
                for(int i = 0; i < nodes.Length; i++)
                {
                    this.Add(nodes[i]);
                }
            }
        }

    }

    public class ReadOnlyNTreeNodeCollection : ReadOnlyCollection<NTreeNode>
    {
        public ReadOnlyNTreeNodeCollection(IList<NTreeNode> Nodes)
            : base(Nodes)
        {

        }
    }
}
