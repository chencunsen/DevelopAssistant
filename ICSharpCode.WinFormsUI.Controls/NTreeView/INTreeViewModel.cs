using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace ICSharpCode.WinFormsUI.Controls
{
	public interface INTreeViewModel
	{
		IEnumerable GetChildren(NTreeViewPath NTreeViewPath);
		bool IsLeaf(NTreeViewPath NTreeViewPath);

		event EventHandler<NTreeViewModelEventArgs> NodesChanged; 
		event EventHandler<NTreeViewModelEventArgs> NodesInserted;
		event EventHandler<NTreeViewModelEventArgs> NodesRemoved; 
		event EventHandler<NTreeViewPathEventArgs> StructureChanged;
	}
}
