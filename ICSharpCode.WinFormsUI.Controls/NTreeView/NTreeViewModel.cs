using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace ICSharpCode.WinFormsUI.Controls
{
	public class NTreeViewModel : INTreeViewModel
	{
		private Node _root;
		public Node Root
		{
			get { return _root; }
		}

		public NodeCollection Nodes
		{
			get { return _root.Nodes; }
		}

		public NTreeViewModel()
		{
			_root = new Node();
			_root.Model = this;
		}

		public NTreeViewPath GetPath(Node node)
		{
			if (node == _root)
				return NTreeViewPath.Empty;
			else
			{
				Stack<object> stack = new Stack<object>();
				while (node != _root)
				{
					stack.Push(node);
					node = node.Parent;
				}
				return new NTreeViewPath(stack.ToArray());
			}
		}

		public Node FindNode(NTreeViewPath path)
		{
			if (path.IsEmpty())
				return _root;
			else
				return FindNode(_root, path, 0);
		}

		private Node FindNode(Node root, NTreeViewPath path, int level)
		{
			foreach (Node node in root.Nodes)
				if (node == path.FullPath[level])
				{
					if (level == path.FullPath.Length - 1)
						return node;
					else
						return FindNode(node, path, level + 1);
				}
			return null;
		}

		#region INTreeViewModel Members

		public virtual System.Collections.IEnumerable GetChildren(NTreeViewPath NTreeViewPath)
		{
			Node node = FindNode(NTreeViewPath);
			if (node != null)
				foreach (Node n in node.Nodes)
					yield return n;
			else
				yield break;
		}

		public virtual bool IsLeaf(NTreeViewPath NTreeViewPath)
		{
			Node node = FindNode(NTreeViewPath);
			if (node != null)
				return node.IsLeaf;
			else
				throw new ArgumentException("NTreeViewPath");
		}

		public event EventHandler<NTreeViewModelEventArgs> NodesChanged;
		internal void OnNodesChanged(NTreeViewModelEventArgs args)
		{
			if (NodesChanged != null)
				NodesChanged(this, args);
		}

		public event EventHandler<NTreeViewPathEventArgs> StructureChanged;
		public void OnStructureChanged(NTreeViewPathEventArgs args)
		{
			if (StructureChanged != null)
				StructureChanged(this, args);
		}

		public event EventHandler<NTreeViewModelEventArgs> NodesInserted;
		internal void OnNodeInserted(Node parent, int index, Node node)
		{
			if (NodesInserted != null)
			{
				NTreeViewModelEventArgs args = new NTreeViewModelEventArgs(GetPath(parent), new int[] { index }, new object[] { node });
				NodesInserted(this, args);
			}
		}

		public event EventHandler<NTreeViewModelEventArgs> NodesRemoved;
		internal void OnNodeRemoved(Node parent, int index, Node node)
		{
			if (NodesRemoved != null)
			{
				NTreeViewModelEventArgs args = new NTreeViewModelEventArgs(GetPath(parent), new int[] { index }, new object[] { node });
				NodesRemoved(this, args);
			}
		}

		#endregion
	}
}
