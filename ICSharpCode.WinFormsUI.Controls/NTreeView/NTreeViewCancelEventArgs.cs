using System;
using System.Collections.Generic;
using System.Text;

namespace ICSharpCode.WinFormsUI.Controls
{
	public class NTreeViewCancelEventArgs : NTreeViewEventArgs
	{
		private bool _cancel;

		public bool Cancel
		{
			get { return _cancel; }
			set { _cancel = value; }
		}

		public NTreeViewCancelEventArgs(NTreeNode node)
			: base(node)
		{
		}

	}
}
