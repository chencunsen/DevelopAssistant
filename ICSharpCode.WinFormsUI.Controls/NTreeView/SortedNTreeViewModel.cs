﻿using System;
using System.Collections;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class SortedNTreeViewModel : NTreeViewModel
    {
        private INTreeViewModel _innerModel;
        public INTreeViewModel InnerModel
        {
            get { return _innerModel; }
        }

        private IComparer _comparer;
        public IComparer Comparer
        {
            get { return _comparer; }
            set
            {
                _comparer = value;
                OnStructureChanged(new NTreeViewPathEventArgs(NTreeViewPath.Empty));
            }
        }

        public SortedNTreeViewModel(INTreeViewModel innerModel)
        {
            _innerModel = innerModel;
            _innerModel.NodesChanged += new EventHandler<NTreeViewModelEventArgs>(_innerModel_NodesChanged);
            _innerModel.NodesInserted += new EventHandler<NTreeViewModelEventArgs>(_innerModel_NodesInserted);
            _innerModel.NodesRemoved += new EventHandler<NTreeViewModelEventArgs>(_innerModel_NodesRemoved);
            _innerModel.StructureChanged += new EventHandler<NTreeViewPathEventArgs>(_innerModel_StructureChanged);
        }

        void _innerModel_StructureChanged(object sender, NTreeViewPathEventArgs e)
        {
            OnStructureChanged(e);
        }

        void _innerModel_NodesRemoved(object sender, NTreeViewModelEventArgs e)
        {
            OnStructureChanged(new NTreeViewPathEventArgs(e.Path));
        }

        void _innerModel_NodesInserted(object sender, NTreeViewModelEventArgs e)
        {
            OnStructureChanged(new NTreeViewPathEventArgs(e.Path));
        }

        void _innerModel_NodesChanged(object sender, NTreeViewModelEventArgs e)
        {
            OnStructureChanged(new NTreeViewPathEventArgs(e.Path));
        }

        public override IEnumerable GetChildren(NTreeViewPath treePath)
        {
            if (Comparer != null)
            {
                ArrayList list = new ArrayList();
                IEnumerable res = InnerModel.GetChildren(treePath);
                if (res != null)
                {
                    foreach (object obj in res)
                        list.Add(obj);
                    list.Sort(Comparer);
                    return list;
                }
                else
                    return null;
            }
            else
                return InnerModel.GetChildren(treePath);
        }

        public override bool IsLeaf(NTreeViewPath treePath)
        {
            return InnerModel.IsLeaf(treePath);
        }
    }
}

