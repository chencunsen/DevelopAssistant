﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public partial class NTreeNode
    {
        public int Id { get; set; }
        public int Pid { get; set; }
        public string Note { get; set; }       
        public int Childs { get; set; }

        public NTreeView TreeView
        {
            get
            {
                return this.Tree;
            }
        }
    }
}
