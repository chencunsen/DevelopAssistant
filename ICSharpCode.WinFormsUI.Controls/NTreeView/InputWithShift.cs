using System;
using System.Collections.Generic;
using System.Text;

namespace ICSharpCode.WinFormsUI.Controls
{
	internal class InputWithShift: NormalInputState
	{
		public InputWithShift(NTreeView tree): base(tree)
		{
		}

		protected override void FocusRow(NTreeNode node)
		{
			Tree.SuspendSelectionEvent = true;
			try
			{
				if (Tree.SelectionMode == NTreeViewSelectionMode.Single || Tree.SelectionStart == null)
					base.FocusRow(node);
				else if (CanSelect(node))
				{
					SelectAllFromStart(node);
					Tree.CurrentNode = node;
					Tree.ScrollTo(node);
				}
			}
			finally
			{
				Tree.SuspendSelectionEvent = false;
			}
		}

		protected override void DoMouseOperation(NTreeNodeMouseEventArgs args)
		{
			if (Tree.SelectionMode == NTreeViewSelectionMode.Single || Tree.SelectionStart == null)
			{
				base.DoMouseOperation(args);
			}
			else if (CanSelect(args.Node))
			{
				Tree.SuspendSelectionEvent = true;
				try
				{
					SelectAllFromStart(args.Node);
				}
				finally
				{
					Tree.SuspendSelectionEvent = false;
				}
			}
		}

		protected override void MouseDownAtEmptySpace(NTreeNodeMouseEventArgs args)
		{
		}

		private void SelectAllFromStart(NTreeNode node)
		{
			Tree.ClearSelection();
			int a = node.Row;
			int b = Tree.SelectionStart.Row;
			for (int i = Math.Min(a, b); i <= Math.Max(a, b); i++)
			{
				if (Tree.SelectionMode == NTreeViewSelectionMode.Multi || Tree.RowMap[i].Parent == node.Parent)
					Tree.RowMap[i].IsSelected = true;
			}
		}
	}
}
