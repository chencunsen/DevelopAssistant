﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Windows.Forms.VisualStyles;

namespace ICSharpCode.WinFormsUI.Controls
{
    [ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)]
    public partial class NToolStripCheckBox : ToolStripItem
    {
        private bool _checkState = false;

        public bool CheckState
        {
            get
            {
                return _checkState;
            }
            set
            {
                _checkState = value;
                this.Invalidate();
            }
        }

        public event EventHandler CheckStateChanged;

        public NToolStripCheckBox()
        {
            InitializeComponent();
        }

        public override Size GetPreferredSize(Size constrainingSize)
        {
            base.GetPreferredSize(constrainingSize);
            Size preferredSize = base.GetPreferredSize(constrainingSize);
            preferredSize.Width += 13;
            return preferredSize;
        }

        protected void DoCheckStateChanged()
        {
            if (this.CheckStateChanged != null)
                this.CheckStateChanged(this,new EventArgs());
        }

        protected override void OnClick(EventArgs e)
        {
            CheckState = !CheckState;
            DoCheckStateChanged();
            base.OnClick(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Brush textBru = new SolidBrush(NToolStripItemTheme.TextColor);
            Rectangle bound = new Rectangle(0, 0, Width, Height);

            if (this.Selected)
            {
                textBru = new SolidBrush(NToolStripItemTheme.HighlightForeColor);
                e.Graphics.FillRectangle(new SolidBrush(NToolStripItemTheme.HighlightBackColor), bound);
            }

            Point pLocation = new Point(e.ClipRectangle.X + 2, e.ClipRectangle.Height / 2 - (13 / 2));
            Size txtSize = TextRenderer.MeasureText(this.Text, this.Font);
            Rectangle rectText = new Rectangle(pLocation.X + 15, (e.ClipRectangle.Height - txtSize.Height) / 2, txtSize.Width, txtSize.Height);
            CheckBoxState chkState = _checkState ? CheckBoxState.CheckedNormal : CheckBoxState.UncheckedNormal;
            CheckBoxRenderer.DrawCheckBox(e.Graphics, pLocation, rectText, "", this.Font, TextFormatFlags.Left | TextFormatFlags.VerticalCenter, false, chkState);
            e.Graphics.DrawString(this.Text, this.Font, textBru, new PointF(pLocation.X + 15, (e.ClipRectangle.Height - txtSize.Height) / 2));
        }
    }

    public partial class NToolStripCheckBox
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
        }

        #endregion
    }

}
