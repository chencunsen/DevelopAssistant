﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NToolStrip : System.Windows.Forms.ToolStrip
    {
        internal string theme = "Default";       

        public NToolStrip()
        {            
            this.Stretch = true;
            this.Font = NToolStripItemTheme.TextFont;
            this.ImageScalingSize = new Size(18, 18);
            this.GripStyle = ToolStripGripStyle.Visible;
            this.RenderMode = ToolStripRenderMode.Professional;           
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            g.FillRectangle(new SolidBrush(this.BackColor), this.ClientRectangle);

            if ((int)this.LayoutStyle == 1)
            {
                DrawDotsStrip(g, new Rectangle(0, 2, 6, this.Height - 4), NToolStripItemTheme.GripDotsColor);
            }
            else if ((int)this.LayoutStyle == 2)
            {
                DrawDotsStrip(g, new Rectangle(0, 2, this.Width - 4, 6), NToolStripItemTheme.GripDotsColor);
            }

            foreach (ToolStripItem item in this.Items)
            {
                NToolStripPaintUtils.DrawItem(g, item, ContainerType.NToolStrip, (int)this.LayoutStyle);
            }
        }

        public void DrawDotsStrip(Graphics g, Rectangle rectStrip, Color colorDots)
        {
            if (rectStrip.Width <= 0 || rectStrip.Height <= 0)
                return;

            var penDots = new Pen(colorDots, 1);
            penDots.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            penDots.DashPattern = new float[] { 1, 3 };
            int positionX = rectStrip.Width / 2;
            int positionY = rectStrip.Height / 2;

            if ((int)this.LayoutStyle == 1)
            {
                g.DrawLine(penDots, positionX, rectStrip.Top + 2, positionX, rectStrip.Height);
                g.DrawLine(penDots, positionX - 2, rectStrip.Top, positionX - 2, rectStrip.Height);
                g.DrawLine(penDots, positionX + 2, rectStrip.Top, positionX + 2, rectStrip.Height);
            }
            else if ((int)this.LayoutStyle == 2)
            {
                g.DrawLine(penDots, rectStrip.Left + 2 + 2, positionY, Width - 4, positionY);
                g.DrawLine(penDots, rectStrip.Left + 2, positionY - 2, Width - 4, positionY - 2);
                g.DrawLine(penDots, rectStrip.Left + 2, positionY + 2, Width - 4, positionY + 2);
            }

        }

        public void SetTheme(string themeName)
        {
            theme = themeName;

            switch (themeName)
            {
                case "Default":
                    NToolStripItemTheme.HighlightForeColor = SystemColors.ControlText;
                    NToolStripItemTheme.HighlightBackColor = Color.FromArgb(196, 225, 255);
                    NToolStripItemTheme.TextColor = SystemColors.ControlText;
                    NToolStripItemTheme.DisableColor = SystemColors.GrayText;
                    NToolStripItemTheme.BackColor = SystemColors.Window;
                    NToolStripItemTheme.BaseColor = SystemColors.Control;
                    NToolStripItemTheme.PressedColor = Color.FromArgb(246, 248, 250);
                    NToolStripItemTheme.StripSeparatorColor = Color.FromArgb(214, 219, 233);
                    break;
                case "Black":
                    NToolStripItemTheme.HighlightForeColor = Color.White;
                    NToolStripItemTheme.HighlightBackColor = Color.FromArgb(080, 080, 080);
                    NToolStripItemTheme.TextColor = Color.FromArgb(240, 240, 240);
                    NToolStripItemTheme.DisableColor = Color.FromArgb(224, 224, 224);
                    NToolStripItemTheme.BackColor = Color.FromArgb(030, 030, 030);
                    NToolStripItemTheme.BaseColor = Color.FromArgb(030, 030, 030);
                    NToolStripItemTheme.PressedColor = Color.FromArgb(045, 045, 048);
                    NToolStripItemTheme.StripSeparatorColor = Color.FromArgb(098, 098, 098);
                    break;
            }

            NToolStripItemTheme.Name = themeName;
        }

    }  

}
