﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace ICSharpCode.WinFormsUI.Controls
{
    public partial class NMenuStrip : System.Windows .Forms.MenuStrip
    {
        internal string theme = "Default";

        public NMenuStrip()
        {            
            this.Font = NToolStripItemTheme.TextFont;
            this.ImageScalingSize = new Size(20, 20);
            InitializeComponent();                  
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            foreach (ToolStripItem item in this.Items)
            {
                NToolStripPaintUtils.DrawItem(g, item, ContainerType.NToolStrip);
            }
        }

        public void SetTheme(string themeName)
        {
            theme = themeName;
        }

    }
}
