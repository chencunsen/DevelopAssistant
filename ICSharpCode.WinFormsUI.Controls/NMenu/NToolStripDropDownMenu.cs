﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    public class NToolStripDropDownMenu: System.Windows.Forms.ToolStripDropDownMenu
    {
        public NToolStripDropDownMenu()
        {
            this.Font = NToolStripItemTheme.TextFont;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            g.FillRectangle(new SolidBrush(NToolStripItemTheme.BackColor), this.ClientRectangle);
            g.FillRectangle(new SolidBrush(NToolStripItemTheme.PressedColor), new Rectangle(0, 0, 28, this.Height));

            foreach (ToolStripItem item in this.Items)
            {                
                NToolStripPaintUtils.DrawItem(g, item, ContainerType.DropDownMenu);
            }

            if (this.OwnerItem != null)
            {
                //ToolStripDropDownDirection
                if (this.OwnerItem is NToolStripMenuItem)
                    g.DrawLine(new Pen(SystemColors.ControlDarkDark), new Point(0, 0), new Point(Width, 0));
                else
                    g.DrawLine(new Pen(SystemColors.ControlDarkDark), new Point(this.OwnerItem.Width - 1, 0), new Point(Width, 0));
            }
            g.DrawRectangle(new Pen(SystemColors.ControlDarkDark), new Rectangle(0, -1, Width - 1, Height));
        }
        
    }   
}
