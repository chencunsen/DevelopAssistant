﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevelopAssistant.AddIn;

namespace DevelopAssistant.AddIn.DatabaseDocument
{
    using DevelopAssistant.AddIn.DatabaseDocument.Properties;
    using ICSharpCode.WinFormsUI.Docking;

    public class DatabaseDocumentAddIn : DockContentAddIn 
    {
        public DatabaseDocumentAddIn()
        {
            this.IdentityID = "72436a2e-f0cf-4b68-a744-6938141be8db";
            this.Name = "数据库文档";
            this.Text = "数据库文档";
            this.Tooltip = "数据库文档";            
            this.Icon = Resources.plus_shield;
        }

        public override void Initialize(string AssemblyName, string Winname)
        {
            //
        }

        public override object Execute(params object[] Parameter)
        {
            MainForm f = new MainForm((Form)Parameter[0], this, (DevelopAssistant.Service.DataBaseServer)Parameter[2])
            {
                WindowFloat = new DelegateWindowFloatHandler(FloatParentCenter) 
            };         
            return f;             
        }

        private void FloatParentCenter(Form form)
        {

        }
    }
}
