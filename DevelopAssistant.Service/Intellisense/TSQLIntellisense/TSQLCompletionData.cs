﻿using ICSharpCode.TextEditor;
using ICSharpCode.TextEditor.Gui.CompletionWindow;
using System;
using System.Collections.Generic;
using System.Drawing; 
using System.Text; 

namespace DevelopAssistant.Service.TSQLIntellisense
{
    public class TSQLCompletionData : ICompletionData
    {        
        string text;
		string description;
		int imageIndex;
        string databaseType;
		
		public int ImageIndex {
			get {
				return imageIndex;
			}
		}
		
		public string Text {
			get {
				return text;
			}
			set {
				text = value;
			}
		}
		
		public string Description {
			get {
				return description;
			}
		}
		
		double priority;
		
		public double Priority {
			get {
				return priority;
			}
			set {
				priority = value;
			}
		}

        private SqlPromptType sqlPromptType = SqlPromptType.Keyword;
        public SqlPromptType SqlPromptType
        {
            get { return sqlPromptType; }
            set { sqlPromptType = value; }
        }
		
		public bool InsertAction(TextArea textArea, char ch)
		{
            try
            {
                string uncompletedText = textArea.GetWordBeforeCaret();
                Caret caret = textArea.Caret;

                int startOffset = caret.Offset + 1;
                int endOffset = startOffset;

                if (!string.IsNullOrEmpty(uncompletedText))
                {
                    startOffset -= uncompletedText.Length + 1;
                    endOffset--;
                    if (endOffset - startOffset > 0)
                        textArea.Document.Remove(startOffset, endOffset - startOffset);
                    textArea.Caret.Position = textArea.Document.OffsetToPosition(Math.Min(startOffset, textArea.Document.TextLength));
                }

                if (!Utility.IsDataBaseKeywords(text))
                {
                    switch (databaseType)
                    {
                        case "System.Data.Sql":
                        case "System.Data.SQL": text = "[" + text + "]"; break;
                        case "System.Data.Sqlite": text = "\"" + text + "\""; break;
                        case "System.Data.PostgreSql": text = "[" + text + "]"; break;
                    }
                }                

                textArea.InsertString(text);

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }			 

		}
		
		public TSQLCompletionData(string text, int imageIndex)
		{
			this.text        = text;
			this.imageIndex  = imageIndex;
            this.databaseType = "System.Data.SQL";
		}

        public TSQLCompletionData(string text, string description, int imageIndex)
		{
			this.text        = text;
			this.description = description;
			this.imageIndex  = imageIndex;
            this.databaseType = "System.Data.SQL";
		}

        public TSQLCompletionData(string text, string description, int imageIndex, string databasetype) :
            this(text, description, imageIndex)
        {
            this.databaseType = databasetype;
        }
		
		public static int Compare(ICompletionData a, ICompletionData b)
		{
			if (a == null)
				throw new ArgumentNullException("a");
			if (b == null)
				throw new ArgumentNullException("b");
			return string.Compare(a.Text, b.Text, StringComparison.InvariantCultureIgnoreCase);
		}    

    }
}
