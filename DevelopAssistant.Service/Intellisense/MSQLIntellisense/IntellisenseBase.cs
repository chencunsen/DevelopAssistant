﻿using System;
using System.Collections.Generic; 
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using ICSharpCode.TextEditor;

namespace DevelopAssistant.Service.MSQLIntellisense
{
    #region IntellisenseBase
    public abstract class IntellisenseBase
    {
        protected Control intellisenseBox;

        protected void ScrollBar_ValueChanged(object sender, EventArgs e)
        {
            if (this.intellisenseBox == null || !this.intellisenseBox.Visible)
                return;
            this.intellisenseBox.Hide();
        }

        protected static bool IsChracterOrNumberKey(int keyValue)
        {
            if ((keyValue < 48 || keyValue > 57) && (keyValue < 65 || keyValue > 90) && (keyValue < 96 || keyValue > 105))
                return keyValue == 189;
            else
                return true;
        }
    }
    #endregion
}
