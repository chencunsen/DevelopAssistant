﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using ICSharpCode.WinFormsUI.Controls;

namespace DevelopAssistant.Service.MSQLIntellisense
{
    public class IntellisenseBox : NListBox
    {
        protected int MaxListLength = 8;

        private ToolTip tooltip = new ToolTip();

        protected Color backThemeFontColor = Color.FromArgb(241, 241, 241);

        private string lastTooltipText = string.Empty;

        private ImageList imageList;

        public ImageList ImageList
        {
            get
            {
                return this.imageList;
            }
            set
            {
                this.imageList = value;
                if (this.imageList == null)
                    return;
                this.ItemHeight = this.imageList.ImageSize.Height + 2;
                this.Height = this.ItemHeight * 10 + 2;
            }
        }

        public int MaxListItemsLength
        {
            get { return MaxListLength; }
        }

        public IntellisenseBox()
        {
            InitializeControls();
        }

        public void InitializeControls()
        {
            this.Font = new Font("Arial", 9.0f);
            this.DrawMode = DrawMode.OwnerDrawFixed;
            this.BorderStyle = NBorderStyle.All;
            //this.VScrollBar.ScrollBarRenderer = new WhiteScrollBarRenderer();
            //this.HScrollBar.ScrollBarRenderer = new WhiteScrollBarRenderer();
            if (AppSettings.EditorSettings != null && !string.IsNullOrEmpty(AppSettings.EditorSettings.TSQLEditorTheme))
            {
                this.SetTheme(AppSettings.EditorSettings.TSQLEditorTheme);
            }
        }

        protected override void OnDrawItem(NDrawItemEventArgs e)
        {
            e.DrawBackground();
            e.DrawFocusRectangle();

            Color foreColor = e.ForeColor;             
            Rectangle bounds = e.Bounds;
            Size imageSize = new Size(16, 16); //this.imageList.ImageSize;
            try
            {
                IntellisenseBoxItem intellisenseBoxItem = (IntellisenseBoxItem)this.Items[e.Index];
                intellisenseBoxItem.Bounds = bounds;
                if (intellisenseBoxItem.ImageIndex != -1)
                {
                    //this.imageList.Draw(e.Graphics, bounds.Left, bounds.Top, intellisenseBoxItem.ImageIndex);
                    Rectangle imageBound = new Rectangle(bounds.Left, bounds.Top + (bounds.Height - imageSize.Height) / 2, imageSize.Width, imageSize.Height);
                    e.Graphics.DrawImage(this.imageList.Images[intellisenseBoxItem.ImageIndex], imageBound);
                }
                e.Graphics.DrawString(intellisenseBoxItem.Text, e.Font, (Brush)new SolidBrush(foreColor), (float)(bounds.Left + imageSize.Width), (float)bounds.Top + 1);
            }
            catch(Exception ex)
            {
                if (e.Index != -1)
                    e.Graphics.DrawString(this.Items[e.Index].ToString(), e.Font, (Brush)new SolidBrush(foreColor), (float)bounds.Left, (float)bounds.Top + 1);
                else
                    e.Graphics.DrawString(this.Text, e.Font, (Brush)new SolidBrush(foreColor), (float)bounds.Left, (float)bounds.Top + 1);
            }            
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            string _toolTip = FindItemAt(e.Location) + "";
            if (!string.IsNullOrEmpty(_toolTip))
            {
                if(_toolTip != lastTooltipText)
                {
                    tooltip.SetToolTip(this, _toolTip);
                }
                lastTooltipText = _toolTip;
            }
        }

        public virtual bool DoWithPromptBox(KeyEventArgs e)
        {
            return true;
        }

    }
}
