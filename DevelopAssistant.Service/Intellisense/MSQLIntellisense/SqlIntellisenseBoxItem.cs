﻿using DevelopAssistant.Service;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevelopAssistant.Service.MSQLIntellisense
{
    public class SqlIntellisenseBoxItem : IntellisenseBoxItem
    {
        private SqlPromptType sqlPromptType;

        public SqlPromptType SqlPromptType
        {
            get
            {
                return this.sqlPromptType;
            }
            set
            {
                this.sqlPromptType = value;
                this.ImageIndex = SqlIntellisenseBox.Instance.GetImageIndex(this.sqlPromptType);
            }
        }

        public SqlIntellisenseBoxItem()
        {
        }

        public SqlIntellisenseBoxItem(string text, string tooltipText, int index)
            : base(text, tooltipText, index)
        {
        }
    }
}
