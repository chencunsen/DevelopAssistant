﻿using System;

namespace DevelopAssistant.Service.MSQLIntellisense
{
    public class IntellisenseBoxItem
    {
        public string Text { get; set; }

        public int ImageIndex { get; set; }

        public string ImageKey { get; set; }

        public string TooltipText { get; set; }

        public object Tag { get; set; }

        public System.Drawing.Rectangle Bounds { get; set; }         

        public IntellisenseBoxItem(string text, string tooltipText, int imageIndex)
        {
            this.Text = text;
            this.TooltipText = tooltipText;
            this.ImageIndex = imageIndex;
        }

        public IntellisenseBoxItem(string text, string tooltipText, string imageKey)
        {
            this.Text = text;
            this.TooltipText = tooltipText;
            this.ImageKey = imageKey;
        }

        public IntellisenseBoxItem(string text, int imageIndex)
            : this(text, string.Empty, imageIndex)
        {
        }

        public IntellisenseBoxItem(string text, string tooltipText)
            : this(text, tooltipText, -1)
        {
        }

        public IntellisenseBoxItem(string text)
            : this(text, string.Empty, -1)
        {
        }

        public IntellisenseBoxItem()
            : this(string.Empty)
        {
        }

        public override string ToString()
        {
            return this.Text;
        }
    }
}
