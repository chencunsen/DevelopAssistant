﻿using ICSharpCode.WinFormsUI.Theme;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Service
{
    public enum QueryDataGridStyle
    {
        Horizontal,
        Vertical
    }

    public class TSQLEditorSettings
    {
        private bool _enableDatabaseTran = true;
        public bool EnableDataBaseTran
        {
            get { return _enableDatabaseTran; }
            set { _enableDatabaseTran = value; }
        }

        private bool _enableIntellisense = false;
        public bool EnableIntellisense
        {
            set
            {
                _enableIntellisense = value;
            }
            get { return _enableIntellisense; }
        }

        private int _foldMarkStyle = 0;
        public int FoldMarkStyle {
            get { return _foldMarkStyle; }
            set { _foldMarkStyle = value; }
        }

        private System.Drawing.Font _editorFont = new System.Drawing.Font("Arial", 10.0f, System.Drawing.FontStyle.Regular);
        public System.Drawing.Font EditorFont
        {
            get { return _editorFont; }
            set { _editorFont = value; }
        }

        private System.Drawing.Font _promptFont = new System.Drawing.Font("Arial", 10.0f, System.Drawing.FontStyle.Regular);
        public System.Drawing.Font PromptFont
        {
            get { return _promptFont; }
            set { _promptFont = value; }
        }

        private System.Drawing.Color _promptForeColor = System.Drawing.Color.Black;
        public System.Drawing.Color PromptForeColor
        {
            get { return _promptForeColor; }
            set { _promptForeColor = value; }
        }

        private System.Drawing.Color _promptBackColor = System.Drawing.Color.Black;
        public System.Drawing.Color PromptBackColor
        {
            get { return _promptBackColor; }
            set { _promptBackColor = value; }
        }

        private string _tsqlEditortheme = "Default";
        public string TSQLEditorTheme
        {
            get { return _tsqlEditortheme; }
            set { _tsqlEditortheme = value; }
        }

        private System.Drawing.Image _tsqlEditorBlackImage;
        public System.Drawing.Image TSQLEditorBlackImage
        {
            get { return _tsqlEditorBlackImage; }
            set { _tsqlEditorBlackImage = value; }
        }

        private bool _autoSupplementary = true;
        /// <summary>
        /// 自动补充数据库字段符号
        /// </summary>
        public bool AutoSupplementary
        {
            get { return _autoSupplementary; }
            set { _autoSupplementary = value; }
        }

        private bool _keywordsCase = true;
        /// <summary>
        /// 关键词大写
        /// </summary>
        public bool KeywordsCase
        {
            get { return _keywordsCase; }
            set { _keywordsCase = value; }
        }
        
        private bool _spaceIntellisense = false;
        /// <summary>
        /// 
        /// </summary>
        public bool SpaceIntellisense
        {
            get { return _spaceIntellisense; }
            set { _spaceIntellisense = value; }
        }

        private QueryDataGridStyle _queryStyle = QueryDataGridStyle.Horizontal;
        /// <summary>
        /// 查询结果排版布局
        /// </summary>
        public QueryDataGridStyle QueryStyle
        {
            get { return _queryStyle; }
            set { _queryStyle = value; }
        }

    }

    public class AppSettings
    {
        public static int TodoLimit = 15;

        public static bool TodoShowName { get; set; }

        public static bool RequestVersion { get; set; }

        public static string DateTimeFormat { get; set; }

        public static System.Drawing.Font WindowFont { get; set; }

        public static WinFormsUIThemeBase WindowTheme { get; set; }

        public static TSQLEditorSettings EditorSettings { get; set; }

        public static bool NewOpenQueryDocument { get; set; }

        public static bool FoldingStrategyEnable { get; set; }        

        public static object ScheduleTasks { get; set; }

        public static System.Drawing.Font ToolStripMenuFont { get; set; }        

    }    
}
