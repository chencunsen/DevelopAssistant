﻿using DevelopAssistant.Service;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevelopAssistant.Service
{
    [Serializable]
    public class ProgramScript : DataObject
    {
        public string Script { get; set; }

        public List<SqlParameter> Parameters { get; set; }

        public string ReturnType { get; set; }

        public ProgramScript()
        {
            this.ReturnType = "int";
        }
    }
}
