﻿using System;
using System.Collections.Generic;
using System.Text;
 
using DevelopAssistant.Service;
using DevelopAssistant.Service.MSQLIntellisense;


namespace DevelopAssistant.Service
{
    public class SqlPromptDataCache
    {
        private static readonly Dictionary<DataBaseServer, SqlPromptData> cache = new Dictionary<DataBaseServer, SqlPromptData>();

        internal static SqlPromptData promptData;

        internal static SqlPromptData GetData(DataBaseServer db)
        {
            if (db == null)
                return null;
            if (!SqlPromptDataCache.cache.ContainsKey(db))
                SqlPromptDataCache.AddData(db);
            promptData = SqlPromptDataCache.cache[db];
            return promptData;      
        }

        internal static void AddData(DataBaseServer db)
        {
            SqlPromptData data = new SqlPromptData(db);           
            if (db.Keywords != null)
            {
                List<SqlIntellisenseBoxItem> list1 = new List<SqlIntellisenseBoxItem>();
                foreach (Keyword word in db.Keywords)
                {
                    List<SqlIntellisenseBoxItem> list2 = list1;
                    SqlIntellisenseBoxItem intellisenseBoxItem1 = new SqlIntellisenseBoxItem();
                    intellisenseBoxItem1.Text = word.Text;                    
                    intellisenseBoxItem1.TooltipText = word.Description;
                    intellisenseBoxItem1.SqlPromptType = SqlPromptType.Keyword;
                    SqlIntellisenseBoxItem intellisenseBoxItem2 = intellisenseBoxItem1;
                    list2.Add(intellisenseBoxItem2);
                }
                data.AddDbObject(SqlPromptType.Keyword, (IList<SqlIntellisenseBoxItem>)list1);
            }
            if (db.Tables != null && db.Tables.Count > 0)
            {
                List<SqlIntellisenseBoxItem> list = new List<SqlIntellisenseBoxItem>(db.Tables.Count);
                foreach (Table table in db.Tables)
                {
                    SqlIntellisenseBoxItem intellisenseBoxItem1 = new SqlIntellisenseBoxItem();
                    intellisenseBoxItem1.Text = table.Name;
                    intellisenseBoxItem1.SqlPromptType = SqlPromptType.Table;
                    intellisenseBoxItem1.TooltipText = table.Description;
                    //intellisenseBoxItem1.Tag = (object)table;
                    SqlIntellisenseBoxItem intellisenseBoxItem2 = intellisenseBoxItem1;
                    list.Add(intellisenseBoxItem2);
                    SqlPromptDataCache.AddColumnData(table, data);
                }
                data.AddDbObject(SqlPromptType.Table, (IList<SqlIntellisenseBoxItem>)list);
            }
            if (db.Views != null && db.Views.Count > 0)
            {
                List<SqlIntellisenseBoxItem> list1 = new List<SqlIntellisenseBoxItem>(db.Views.Count);
                foreach (View view in db.Views)
                {
                    List<SqlIntellisenseBoxItem> list2 = list1;
                    SqlIntellisenseBoxItem intellisenseBoxItem1 = new SqlIntellisenseBoxItem();
                    intellisenseBoxItem1.Text = view.Name;
                    intellisenseBoxItem1.SqlPromptType = SqlPromptType.View;
                    intellisenseBoxItem1.Tag = (object)view;
                    SqlIntellisenseBoxItem intellisenseBoxItem2 = intellisenseBoxItem1;
                    list2.Add(intellisenseBoxItem2);
                    SqlPromptDataCache.AddColumnData(view, data);
                }
                data.AddDbObject(SqlPromptType.View, (IList<SqlIntellisenseBoxItem>)list1);
            }
            if (db.Procedures != null && db.Procedures.Count > 0)
            {
                List<SqlIntellisenseBoxItem> list1 = new List<SqlIntellisenseBoxItem>(db.Procedures.Count);
                foreach (Procedure programScript in db.Procedures)
                {
                    List<SqlIntellisenseBoxItem> list2 = list1;
                    SqlIntellisenseBoxItem intellisenseBoxItem1 = new SqlIntellisenseBoxItem();
                    intellisenseBoxItem1.Text = programScript.Name;
                    intellisenseBoxItem1.SqlPromptType = SqlPromptType.Procedure;
                    intellisenseBoxItem1.Tag = (object)programScript;
                    SqlIntellisenseBoxItem intellisenseBoxItem2 = intellisenseBoxItem1;
                    list2.Add(intellisenseBoxItem2);
                }
                data.AddDbObject(SqlPromptType.Procedure, (IList<SqlIntellisenseBoxItem>)list1);
            }
            List<SqlIntellisenseBoxItem> list3 = new List<SqlIntellisenseBoxItem>(db.Functions.Count);
            foreach (Function programScript in db.Functions)
            {
                List<SqlIntellisenseBoxItem> list1 = list3;
                SqlIntellisenseBoxItem intellisenseBoxItem1 = new SqlIntellisenseBoxItem();
                intellisenseBoxItem1.Text = programScript.Name;
                intellisenseBoxItem1.TooltipText = programScript.Describtion;
                intellisenseBoxItem1.SqlPromptType = SqlPromptType.UserFunction;
                intellisenseBoxItem1.Tag = (object)programScript;
                SqlIntellisenseBoxItem intellisenseBoxItem2 = intellisenseBoxItem1;
                list1.Add(intellisenseBoxItem2);
            }
            data.AddDbObject(SqlPromptType.UserFunction, (IList<SqlIntellisenseBoxItem>)list3);
            data.AddDbObject(SqlPromptType.Column, (IList<SqlIntellisenseBoxItem>)data.GetColumns());
            promptData = data;
            if (!SqlPromptDataCache.cache.ContainsKey(db))
                SqlPromptDataCache.cache.Add(db, data);
        }

        private static void AddColumnData(Table table, SqlPromptData data)
        {
            List<SqlIntellisenseBoxItem> list = new List<SqlIntellisenseBoxItem>();
            foreach (Column column in table.Columns)
            {
                SqlIntellisenseBoxItem intellisenseBoxItem1 = new SqlIntellisenseBoxItem();
                intellisenseBoxItem1.Text = column.Name;
                intellisenseBoxItem1.SqlPromptType = SqlPromptType.Column;
                intellisenseBoxItem1.Tag = (object)column;
                intellisenseBoxItem1.TooltipText = column.Description;
                SqlIntellisenseBoxItem intellisenseBoxItem2 = intellisenseBoxItem1;
                list.Add(intellisenseBoxItem2);
            }
            data.AddColumns(table.Name.ToLower(), (IList<SqlIntellisenseBoxItem>)list);
        }

        private static void AddColumnData(View view, SqlPromptData data)
        {
            List<SqlIntellisenseBoxItem> list = new List<SqlIntellisenseBoxItem>();
            foreach (Column column in view.Columns)
            {
                SqlIntellisenseBoxItem intellisenseBoxItem1 = new SqlIntellisenseBoxItem();
                intellisenseBoxItem1.Text = column.Name;
                intellisenseBoxItem1.SqlPromptType = SqlPromptType.Column;
                intellisenseBoxItem1.Tag = (object)column;
                intellisenseBoxItem1.TooltipText = column.Description;
                SqlIntellisenseBoxItem intellisenseBoxItem2 = intellisenseBoxItem1;
                list.Add(intellisenseBoxItem2);
            }
            data.AddColumns(view.Name.ToLower(), (IList<SqlIntellisenseBoxItem>)list);
        }

        private static void AddParameterData(Procedure procedure, SqlPromptData data)
        {
            List<SqlIntellisenseBoxItem> list = new List<SqlIntellisenseBoxItem>();
            foreach (SqlParameter parameter in procedure.Parameters)
            {
                SqlIntellisenseBoxItem intellisenseBoxItem1 = new SqlIntellisenseBoxItem();
                intellisenseBoxItem1.Text = parameter.Name;
                intellisenseBoxItem1.SqlPromptType = SqlPromptType.Procedure;
                intellisenseBoxItem1.Tag = (object)parameter;
                intellisenseBoxItem1.TooltipText = "类型：" + parameter.Direction.ToString() + " " + parameter.DataType + "\r\n" + "属于：" + procedure.Name + "存储过程";
                SqlIntellisenseBoxItem intellisenseBoxItem2 = intellisenseBoxItem1;
                list.Add(intellisenseBoxItem2);
            }
            data.AddColumns(procedure.Name.ToLower(), (IList<SqlIntellisenseBoxItem>)list);
        }

    }
}
