﻿using DevelopAssistant.Service.TSQLIntellisense;
using NORM.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Service
{
    public class Utility
    {
        private static TSQLCompletionData[] GetKeywords()
        {
            List<TSQLCompletionData> array = new List<TSQLCompletionData>();
            array.Add(new TSQLCompletionData("SET", "修改表如：UPDATE table Set name1=''", 1));
            array.Add(new TSQLCompletionData("SELECT", "查询", 1));
            array.Add(new TSQLCompletionData("CREATE", "创建", 1));
            array.Add(new TSQLCompletionData("CREATE TABLE", "创建表", 1));
            array.Add(new TSQLCompletionData("CREATE VIEW", "创建表视图", 1));

            return array.ToArray();
        }

        public static TSQLCompletionData[] GetCompletionDataItems(SqlPromptType type)
        {
            TSQLCompletionData[] list = null;
            switch (type)
            {
                case SqlPromptType.Keyword:
                    list = GetKeywords();
                    break;
            }
            return list;
        }

        public static DataBase GetAdohelper(DataBaseServer databaseServer)
        {
            DataBase db = null;
            switch (databaseServer.ProviderName)
            {
                case "System.Data.SQL":
                    db = DataBaseFactory.Create(databaseServer.ConnectionString, DataBaseTypes.SqlDataBase);
                    break;               
                case "System.Data.Sqlite":
                    db = DataBaseFactory.Create(databaseServer.ConnectionString, DataBaseTypes.SqliteDataBase);
                    break;
                case "System.Data.PostgreSql":
                    db = DataBaseFactory.Create(databaseServer.ConnectionString, DataBaseTypes.PostgreSqlDataBase);
                    break;
                case "System.Data.MySql":
                    db = DataBaseFactory.Create(databaseServer.ConnectionString, DataBaseTypes.MySqlDataBase);
                    break;
                case "System.Data.OleDb":
                    db = DataBaseFactory.Create(databaseServer.ConnectionString, DataBaseTypes.AccessDataBase);
                    break;
            }
            return db;
        }

        public static DataBase GetAdohelper(string ConnectionString,string ProviderName)
        {
            DataBase db = null;
            switch (ProviderName)
            {
                case "System.Data.SQL":
                    db = DataBaseFactory.Create(ConnectionString, DataBaseTypes.SqlDataBase);
                    break;
                case "System.Data.Sqlite":
                    db = DataBaseFactory.Create(ConnectionString, DataBaseTypes.SqliteDataBase);
                    break;
                case "System.Data.PostgreSql":
                    db = DataBaseFactory.Create(ConnectionString, DataBaseTypes.PostgreSqlDataBase);
                    break;
                case "System.Data.MySql":
                    db = DataBaseFactory.Create(ConnectionString, DataBaseTypes.MySqlDataBase);
                    break;
                case "System.Data.OleDb":
                    db = DataBaseFactory.Create(ConnectionString, DataBaseTypes.MySqlDataBase);
                    break;
            }
            return db;
        }

        public static bool IsDataBaseKeywords(string input)
        {
            bool rvl = false;
            IEnumerable<TSQLCompletionData> find_list = GetCompletionDataItems(SqlPromptType.Keyword).Where(w => w.Text.Equals(input));
            if (find_list != null && find_list.Count() > 0)
            {
                rvl = true;
            }
            return rvl;
        }

        public static TSQLCompletionData[] GetItems(string exp, SqlPromptType type)
        {
            IEnumerable<TSQLCompletionData> list = GetCompletionDataItems(SqlPromptType.Keyword).Where(w => w.Text.StartsWith(exp,StringComparison.OrdinalIgnoreCase));
            var relt = list.ToArray();
            return list.ToArray();
        } 

    }
}
