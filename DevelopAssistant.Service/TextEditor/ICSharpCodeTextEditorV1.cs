﻿using DevelopAssistant.Service.MSQLIntellisense;
using ICSharpCode.TextEditor;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.Service
{
    public class ICSharpCodeTextEditor : TextEditorControl
    {

        SqlIntellisense Intellisense = new SqlIntellisense();

        public string SelectedText
        {
            get
            {
                string _selectedText = string.Empty;
                _selectedText= this.ActiveTextAreaControl.TextArea.SelectionManager.SelectedText;

                if (!string.IsNullOrEmpty(_selectedText))
                {
                    _selectedText = _selectedText.Replace("[", "").Replace("]", "").Replace("\"", "").Replace("`", "");
                }

                return _selectedText;

            }
        }

        public string TextBeforeCaret
        {
            get
            {
                string _textBeforeCaret = string.Empty;

                if (!string.IsNullOrEmpty(this.Text))
                    _textBeforeCaret= this.Text.Substring(0, this.ActiveTextAreaControl.TextArea.Caret.Offset);
                else
                    _textBeforeCaret= string.Empty;

                if (!string.IsNullOrEmpty(_textBeforeCaret))
                {
                    _textBeforeCaret = _textBeforeCaret.Replace("[", "").Replace("]", "").Replace("\"", "").Replace("`", "");
                }

                return _textBeforeCaret;
            }
        }

        public string TextAfterCaret
        {
            get
            {
                string _textAfterCaret = string.Empty;  
                int offset = this.ActiveTextAreaControl.TextArea.Caret.Offset;
                if (this.Text.Length != offset + 1)
                    _textAfterCaret= this.Text.Substring(offset);
                else
                    _textAfterCaret= string.Empty;

                if (!string.IsNullOrEmpty(_textAfterCaret))
                {
                    _textAfterCaret = _textAfterCaret.Replace("[", "").Replace("]", "").Replace("\"", "").Replace("`", "");
                }

                return _textAfterCaret;
            }
        }

        public string DText
        {
            get
            {
                string _dtext = string.Empty;
                _dtext = this.Text;

                if (!string.IsNullOrEmpty(_dtext))
                {
                    _dtext = _dtext.Replace("[", "").Replace("]", "").Replace("\"", "").Replace("`", "");
                }

                return _dtext;
            }
        }

        public Caret Caret
        {
            get
            {
                return this.ActiveTextAreaControl.TextArea.Caret;
            }
        }

        public TextArea TextArea
        {
            get
            {
                return this.ActiveTextAreaControl.TextArea;
            }
        }

        private bool modified;
        public bool Modified
        {
            get
            {
                return this.modified;
            }
            set
            {
                this.modified = value;
            }
        }
        
        Form _ownerForm = null;
        /// <summary>
        /// 承载该控件的Form窗体
        /// </summary>
        public virtual Form OwnerForm
        {
            get
            {
                return _ownerForm;
            }
            set
            {
                _ownerForm = value;
            }
        }

        DevelopAssistant.Service.DataBaseServer _dbserver = null;
        public virtual DevelopAssistant.Service.DataBaseServer DataBaseServer
        {
            get { return _dbserver; }
            set
            {
                _dbserver = value;
                if (_dbserver != null)
                {
                    this.Intellisense.ProviderName = _dbserver.ProviderName;
                    SqlPromptDataCache.AddData(_dbserver);
                }                   
            }
        }

        public ICSharpCodeTextEditor()
        {
            this.InitializeComponent();            

            this.ShowVRuler = false;
            this.ShowTabs = false;
            this.ShowSpaces = false;
            this.ShowEOLMarkers = false;
            this.ShowInvalidLines = false;

            Intellisense.OwnerForm = _ownerForm;
            Intellisense.Editor = this;
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.textAreaPanel.Size = new Size(476, 360);
            this.Name = "ICSharpCodeTextEditor";
            this.Size = new Size(476, 360);
            this.ResumeLayout(false);
        }

        public void ApplyTSQLEditorPromptTooltipStyle(TSQLEditorSettings EditorSettings)
        {
            this.Font = EditorSettings.EditorFont;
            //Intellisense.SetPromptBoxTheme(EditorSettings.TSQLEditorTheme);
            Intellisense.SetPromptTooltipStyle(EditorSettings.PromptForeColor, EditorSettings.PromptBackColor, EditorSettings.PromptFont);
        }

        public void ApplyTSQLEditorPromptBoxTheme(TSQLEditorSettings EditorSettings)
        {
            this.Font = EditorSettings.EditorFont;
            Intellisense.SetPromptBoxTheme(EditorSettings.TSQLEditorTheme);
            //Intellisense.SetPromptTooltipStyle(EditorSettings.PromptForeColor, EditorSettings.PromptBackColor, EditorSettings.PromptFont);
        }

    }
}
