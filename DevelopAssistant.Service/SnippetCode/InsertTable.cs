﻿using NORM.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Service
{
    public class InsertTable : SnippetBase
    {
        public static string ToSnippetCode(string TableName, DataBaseServer DatabaseServer)
        {
            StringPlus sp = new StringPlus();

            if (AppSettings.EditorSettings.KeywordsCase)
            {
                sp.Append("INSERT INTO ");
            }
            else
            {
                sp.Append("insert into ");
            }
            sp.Append(getObject(TableName, DatabaseServer.ProviderName) + "").Append(Environment.NewLine);
            sp.Append("(").Append(Environment.NewLine);
            sp.Append(" ");
            using (var db = Utility.GetAdohelper(DatabaseServer))
            {
                DataTable dt = db.GetTableObject(TableName);

                StringPlus fstr = new StringPlus();
                StringPlus vstr = new StringPlus();

                foreach (DataRow dr in dt.Rows)
                {
                    string column_name = dr["ColumnName"] + "";
                    string column_type = dr["TypeName"] + "";
                    string column_length = dr["Length"] + "";
                    string column_isnull = dr["CisNull"] + "";

                    if (!column_isnull.Contains("identity"))
                    {
                        fstr.Append("" + getObject(column_name, DatabaseServer.ProviderName) + ",");
                        vstr.Append("'" + column_name + "',");
                    }

                }

                sp.Append(fstr.Value.TrimEnd(',')).Append(Environment.NewLine).Append(")").Append(Environment.NewLine);
                if (AppSettings.EditorSettings.KeywordsCase)
                {
                    sp.Append("VALUES").Append(Environment.NewLine);
                }
                else
                {
                    sp.Append("values").Append(Environment.NewLine);
                }
                sp.Append("(").Append(Environment.NewLine);
                sp.Append(" ");
                sp.Append(vstr.Value.TrimEnd(',')).Append(Environment.NewLine);

            }
            sp.Append(")").Append(Environment.NewLine);
            if (AppSettings.EditorSettings.KeywordsCase)
            {
                sp.Append("GO");
            }
            else
            {
                sp.Append("go");
            }

            return sp.Value;
        }
    }
}
