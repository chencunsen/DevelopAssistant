﻿using NORM.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Service
{
    public class CreateFunction
    {
        public static string ToSnippetCode(string FunctionName, DataBaseServer DatabaseServer)
        {
            return CreateOrReplaceObject.ToSnippetCode(FunctionName, DatabaseServer);
        }

        public static string CreateNewFunction(DataBaseServer DatabaseServer, string Type)
        {
            return CreateOrReplaceObject.CreateNewFunction(DatabaseServer, Type);
        }
    }
}
