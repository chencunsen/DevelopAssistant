﻿using NORM.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Service
{
    public class CreateProcedure
    {
        public static string ToSnippetCode(string ProcedureName, DataBaseServer DatabaseServer)
        {
            return CreateOrReplaceObject.ToSnippetCode(ProcedureName, DatabaseServer);
        }

        public static string CreateNewProcedure(DataBaseServer DatabaseServer)
        {
            return CreateOrReplaceObject.CreateNewProcedure(DatabaseServer);
        }
    }
}
