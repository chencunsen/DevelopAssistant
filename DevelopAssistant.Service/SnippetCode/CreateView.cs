﻿using NORM.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.Service
{
    public class CreateView
    {
        public static string ToSnippetCode(string ViewName, DataBaseServer DatabaseServer)
        {
            return CreateOrReplaceObject.ToSnippetCode(ViewName, DatabaseServer);
        }

        public static string CreateNewView(DataBaseServer DatabaseServer)
        {
            return CreateOrReplaceObject.CreateNewView(DatabaseServer);
        }

    }
}
