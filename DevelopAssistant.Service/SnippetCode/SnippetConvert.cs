﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 

namespace DevelopAssistant.Service.SnippetCode
{
    public class SnippetConvert
    {
        /// <summary>
        /// 加密转换 
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string EncodeSnippetContent(string content)
        {
            if (string.IsNullOrEmpty(content)) 
                return content;

            try
            {
                var result = new System.Text.StringBuilder();
                var temp = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(content));
                var text = System.Text.Encoding.Default.GetString(Convert.FromBase64String(temp));

                for (int i = 0; i < temp.Length; i++)
                {
                    var value = ((int)(temp[i])).ToString("000");
                    result.Append(value);
                }

                return result.ToString();
            }
            catch (Exception ex)
            {
                return content;
            }
        }

        /// <summary>
        /// 解密转换
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string DecodeSnippetContent(string content)
        {
            if (string.IsNullOrEmpty(content))
                return content;

            try
            {
                var temp = new System.Text.StringBuilder();

                for (int i = 0; i < content.Length; i += 3)
                {
                    string value = content.Substring(i, 3);
                    temp.Append((char)(Convert.ToInt32(value)));
                }

                var text = Convert.FromBase64String(temp.ToString());
                var result = System.Text.Encoding.Default.GetString(text);
                return result.ToString();
            }
            catch (Exception ex)
            {
                return content;
            }

        }
    }
}
