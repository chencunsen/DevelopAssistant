﻿using NORM.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DevelopAssistant.Service
{
    public class SelectTable : SnippetBase
    {
        public static string ToSnippetCode(string TableName, DataBaseServer DatabaseServer)
        {
            StringPlus sp = new StringPlus();

            if (AppSettings.EditorSettings.KeywordsCase)
            {
                sp.Append("SELECT");
            }
            else
            {
                sp.Append("select");
            }

            using (var db = Utility.GetAdohelper(DatabaseServer))
            {
                DataTable dt = db.GetTableObject(TableName);

                int ColumnIndex = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (ColumnIndex > 0)
                        sp.Append("\r\n    ," + getObject(dr["ColumnName"], DatabaseServer.ProviderName) + "");
                    else
                        sp.Append(" " + getObject(dr["ColumnName"], DatabaseServer.ProviderName) + "");
                    ColumnIndex++;
                }
            }

            if (AppSettings.EditorSettings.KeywordsCase)
            {
                sp.Append("\r\nFROM ");
            }
            else
            {
                sp.Append("\r\nfrom ");
            }
            sp.Append(getObject(TableName, DatabaseServer.ProviderName) + "");

            return sp.Value;
        }
                
    }
}
