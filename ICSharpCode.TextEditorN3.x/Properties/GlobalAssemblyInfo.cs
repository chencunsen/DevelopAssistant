﻿using System.Reflection;

[assembly: System.Runtime.InteropServices.ComVisible(false)]
[assembly: AssemblyCompany("ICSharpCode")]
[assembly: AssemblyProduct("SharpDevelop")]
[assembly: AssemblyCopyright("2000-2015 AlphaSierraPapa")]
[assembly: AssemblyVersion(RevisionClass.FullVersion)]

internal static class RevisionClass
{
	public const string Major = "3";
	public const string Minor = "1";
	public const string Build = "2";
	public const string Revision = "8437";
	
	public const string MainVersion = Major + "." + Minor;
	public const string FullVersion = Major + "." + Minor + "." + Build + "." + Revision;
}
