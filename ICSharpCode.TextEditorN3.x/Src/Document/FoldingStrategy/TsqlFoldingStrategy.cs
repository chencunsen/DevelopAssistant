﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ICSharpCode.TextEditor.Document
{
    public class TsqlFoldingStrategy : IFoldingStrategy
    {
        /// <summary>
        /// Generates the foldings for our document.
        /// </summary>
        /// <param name="document">The current document.</param>
        /// <param name="fileName">The filename of the document.</param>
        /// <param name="parseInformation">Extra parse information, not used in this sample.</param>
        /// <returns>A list of FoldMarkers.</returns>
        public List<FoldMarker> GenerateFoldMarkers(IDocument document)
        {
            List<FoldMarker> list = new List<FoldMarker>();

            Stack<int> startLines = new Stack<int>();

            // Create foldmarkers for the whole document, enumerate through every line.
            for (int i = 0; i < document.TotalNumberOfLines; i++)
            {
                var seg = document.GetLineSegment(i);
                int offs, end = document.TextLength;
                char c;
                for (offs = seg.Offset; offs < end && ((c = document.GetCharAt(offs)) == ' ' || c == '\t'); offs++)
                { }
                if (offs == end)
                    break;
                int spaceCount = offs - seg.Offset;

                int start = 0;
                string text = "";

                text = document.GetText(offs, seg.Length - spaceCount);

                if (!string.IsNullOrEmpty(text))
                {

                    if (text.ToLower().StartsWith("begin"))
                        startLines.Push(i);
                    if (text.ToLower().StartsWith("end") && startLines.Count > 0)
                    {
                        // Add a new FoldMarker to the list.
                        start = startLines.Pop();
                        list.Add(new FoldMarker(document, start,
                            document.GetLineSegment(start).Length,
                            i, spaceCount + "#endregion".Length));
                    }


                    //text = document.GetText(offs, seg.Length - spaceCount);
                    if (text.StartsWith("("))
                        startLines.Push(i);
                    if (text.StartsWith(")") && startLines.Count > 0)
                    {
                        // Add a new FoldMarker to the list.
                        start = startLines.Pop();
                        list.Add(new FoldMarker(document, start,
                            document.GetLineSegment(start).Length,
                            i, spaceCount + "#endregion".Length));
                    }

                }


            }

            return list;
        }

        /// <summary>
        /// Generates the foldings for our document.
        /// </summary>
        /// <param name="document">The current document.</param>
        /// <param name="fileName">The filename of the document.</param>
        /// <param name="parseInformation">Extra parse information, not used in this sample.</param>
        /// <returns>A list of FoldMarkers.</returns>
        public List<FoldMarker> GenerateFoldMarkers(IDocument document, string fileName, object parseInformation)
        {
            List<FoldMarker> list = new List<FoldMarker>();

            Stack<int> startLines = new Stack<int>();

            // Create foldmarkers for the whole document, enumerate through every line.
            for (int i = 0; i < document.TotalNumberOfLines; i++)
            {
                var seg = document.GetLineSegment(i);
                int offs, end = document.TextLength;
                char c;
                for (offs = seg.Offset; offs < end && ((c = document.GetCharAt(offs)) == ' ' || c == '\t'); offs++)
                { }
                if (offs == end)
                    break;
                int spaceCount = offs - seg.Offset;

                int start = 0;
                string text = "";

                text = document.GetText(offs, seg.Length - spaceCount);
                if (text.StartsWith("begin"))
                    startLines.Push(i);
                if (text.StartsWith("end") && startLines.Count > 0)
                {
                    // Add a new FoldMarker to the list.
                    start = startLines.Pop();
                    list.Add(new FoldMarker(document, start,
                        document.GetLineSegment(start).Length,
                        i, spaceCount + "#endregion".Length));
                }


                text = document.GetText(offs, seg.Length - spaceCount);
                if (text.StartsWith("("))
                    startLines.Push(i);
                if (text.StartsWith(")") && startLines.Count > 0)
                {
                    // Add a new FoldMarker to the list.
                    start = startLines.Pop();
                    list.Add(new FoldMarker(document, start,
                        document.GetLineSegment(start).Length,
                        i, spaceCount + "#endregion".Length));
                }

                
            }

            return list;
        }
    }
}
