﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ICSharpCode.WinFormsUI.Controls
{
    [ToolboxItem(false)]
    public partial class TextBoxBase : UserControl  
    {
        protected string text = "";
        protected string version = "1.0.0";
        protected bool editModel = false;
        protected DocumentType documentType = DocumentType.Text;

        public override string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }

        public virtual DocumentType DocumentType
        {
            get { return documentType; }
            set
            {
                documentType = value;               
            }
        }

        public virtual bool EditModel
        {
            get { return editModel; }
            set { editModel = value; }
        }

        public virtual void OnPropertyChanged()
        {

        }

        public void ShowAbout()
        {
            var control = this.Parent;
            var baseType = control.GetType().BaseType;

            if (baseType.Name == "TextBoxBase")
            {
                control = control.Parent;
                baseType = control.GetType().BaseType;
            }
            if (baseType.Name == "Control")
            {
                control = control.Parent;
                baseType = control.GetType().BaseType;
            }
            if (baseType.Name == "BaseForm")
            {
                AboutForm about = new AboutForm();
                about.ShowDialog(control);
            }
            else if (baseType.Name == "DockContent")
            {
                MessageBox.Show(this, "当前版本号：" + version + " ", "关于", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        public TextBoxBase()
        {
            InitializeComponent();
        }

        public virtual void ReleaseDispose()
        {

        }

    }
}
