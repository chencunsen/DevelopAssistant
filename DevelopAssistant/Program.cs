using DevelopAssistant.Core;
using ICSharpCode.WinFormsUI.Controls.NTimeLine;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

namespace DevelopAssistant
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Application.Run(new DevelopAssistant.Core.ToolBox.SnippetPad());

            string Version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            DevelopAssistant.Common.NLogger.BaseDirectory = string.Format("{0}\\{1}", AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\'), Version);

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(AppDomain.CurrentDomain.BaseDirectory + "AssistantConfig.config");
                xmlDocument.SelectSingleNode("//Version");
                string localVersion = xmlDocument.SelectSingleNode("//value").InnerText;
                Version = localVersion;
            }
            catch (Exception ex)
            {
                DevelopAssistant.Common.NLogger.WriteToLine(ex.Message, "错误", DateTime.Now, ex.Source, ex.StackTrace);
            }            
            
            StartForm startForm = new StartForm();
            MainForm mainform = new MainForm(startForm);
            if (startForm.ShowDialog().Equals(DialogResult.OK))
            {
                mainform.ProductName = "开发助手";
                mainform.ProductVersion = Version;
                mainform.Text = "开发助手 V" + Version;
                mainform.LoadTodoList((IList<MonthItem>)startForm.TodoList, startForm.TodoLimitCount, startForm.TodoTotalCount);
                Application.Run(mainform);
            }
            else
            {
                Application.Exit();
            }


        }
    }
}