﻿using System; 
using System.Drawing; 

namespace ICSharpCode.WinFormsUI.Controls.Chart3D
{
    public class Convert
    {
        public static Vertex ToVertex(Point3D pt3d)
        {
            return pt3d.ToVertex();
        }

        public static Vertex ToVertex(Coordinate3D coordinate, Point3D pt3d)
        {
            Vertex result = null;
            SizeF size = new SizeF(coordinate.Range.Width - 0, coordinate.Range.Height - 0);
            double X = coordinate.Xaxis.Min < 0 ? -size.Width + (coordinate.Xaxis.Distance == 0 ? 0 : size.Width + pt3d.X / coordinate.Xaxis.Distance * size.Width) : -size.Width + (coordinate.Xaxis.Distance == 0 ? 0 : 2 * pt3d.X / coordinate.Xaxis.Distance * size.Width);
            double Y = coordinate.Yaxis.Min < 0 ? -size.Height + (coordinate.Yaxis.Distance == 0 ? 0 : size.Height / 2 + size.Height + pt3d.Y / coordinate.Yaxis.Distance * size.Height / 2) : -size.Height + (coordinate.Yaxis.Distance == 0 ? size.Height : size.Height + pt3d.Y / coordinate.Yaxis.Distance * size.Height);
            double Z = coordinate.Zaxis.Min < 0 ? -size.Width + (coordinate.Zaxis.Distance == 0 ? 0 : pt3d.Z >= 0 ? size.Width + pt3d.Z / coordinate.Zaxis.Distance * size.Width : size.Width + pt3d.Z / coordinate.Zaxis.Distance * size.Width) : -size.Width + (coordinate.Zaxis.Distance == 0 ? 0 : 2 * pt3d.Z / coordinate.Zaxis.Distance * size.Width);
            result = new Vertex((float)X, (float)Z, (float)Y);
            return result;
        }

        public static Vertex[] ToVertexs(Point3D[] array)
        {
            Vertex[] result = new Vertex[array.Length];
            for (int i = 0; i < array.Length; i++)
                result[i] = array[i].ToVertex();
            return result;
        }

        public static Vertex[] ToVertexs(Coordinate3D coordinate, Point3D[] array)
        {
            Vertex[] result = new Vertex[array.Length];
            SizeF size = new SizeF(coordinate.Range.Width - 0, coordinate.Range.Height - 0);
            for (int i = 0; i < array.Length; i++)
            {
                double X = coordinate.Xaxis.Min < 0 ? -size.Width + (coordinate.Xaxis.Distance == 0 ? 0 : size.Width + array[i].X / coordinate.Xaxis.Distance * size.Width) : -size.Width + (coordinate.Xaxis.Distance == 0 ? 0 : 2 * array[i].X / coordinate.Xaxis.Distance * size.Width);
                double Y = coordinate.Yaxis.Min < 0 ? -size.Height + (coordinate.Yaxis.Distance == 0 ? 0 : size.Height / 2 + size.Height + array[i].Y / coordinate.Yaxis.Distance * size.Height / 2) : -size.Height + (coordinate.Yaxis.Distance == 0 ? size.Height : size.Height + array[i].Y / coordinate.Yaxis.Distance * size.Height);
                double Z = coordinate.Zaxis.Min < 0 ? -size.Width + (coordinate.Zaxis.Distance == 0 ? 0 : array[i].Z >= 0 ? size.Width + array[i].Z / coordinate.Zaxis.Distance * size.Width : size.Width + array[i].Z / coordinate.Zaxis.Distance * size.Width) : -size.Width + (coordinate.Zaxis.Distance == 0 ? 0 : 2 * array[i].Z / coordinate.Zaxis.Distance * size.Width);
                result[i] = new Vertex((float)X, (float)Z, (float)Y);
            }
            return result;
        }
    }
}
