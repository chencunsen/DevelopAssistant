﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSharpCode.WinFormsUI.Controls.Chart3D
{
    public class Point3D
    {
        private float _x;
        public float X
        {
            get { return _x; }
            set { _x = value; }
        }

        private float _y;
        public float Y
        {
            get { return _y; }
            set { _y = value; }
        }

        private float _z;
        public float Z
        {
            get { return _z; }
            set { _z = value; }
        }

        /// <summary>
        /// Z,X,Y
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Z"></param>
        public Point3D(float X, float Y, float Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }

        public Vertex ToVertex()
        {
            return new Vertex(X, Z, Y);
        }

        internal string ToShowValues()
        {
            return "(" + X.ToString("N2") + "," + Y.ToString("N2") + "," + Z.ToString("N2") + ")";
        }

    }   

}
