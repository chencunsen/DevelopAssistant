﻿namespace DevelopAssistant.AddIn.JavaProject
{
    partial class ProjectCodeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectCodeForm));
            this.nTabControl1 = new ICSharpCode.WinFormsUI.Controls.NTabControl();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // nTabControl1
            // 
            this.nTabControl1.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(79)))), ((int)(((byte)(125)))));
            this.nTabControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.nTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nTabControl1.ImageList = this.imageList1;
            this.nTabControl1.Location = new System.Drawing.Point(4, 4);
            this.nTabControl1.Name = "nTabControl1";
            this.nTabControl1.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.nTabControl1.SelectedIndex = 0;
            this.nTabControl1.ShowBorder = true;
            this.nTabControl1.ShowClose = false;
            this.nTabControl1.ShowWaitMessage = false;
            this.nTabControl1.Size = new System.Drawing.Size(554, 452);
            this.nTabControl1.TabIndex = 0;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "00786.png");
            this.imageList1.Images.SetKeyName(1, "vsicon.png");
            this.imageList1.Images.SetKeyName(2, "window.png");
            this.imageList1.Images.SetKeyName(3, "00654.png");
            this.imageList1.Images.SetKeyName(4, "document_editing.png");
            this.imageList1.Images.SetKeyName(5, "00541.png");
            this.imageList1.Images.SetKeyName(6, "showrulelinesHS.png");
            // 
            // ProjectCodeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 460);
            this.Controls.Add(this.nTabControl1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProjectCodeForm";
            this.Padding = new System.Windows.Forms.Padding(2, 0, 2, 2);
            this.Text = "SpringBootProjectCodeForm";
            this.Load += new System.EventHandler(this.ProjectCodeForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NTabControl nTabControl1;
        private System.Windows.Forms.ImageList imageList1;
    }
}