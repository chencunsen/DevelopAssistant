﻿package ${PakageName}.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import ${PakageName}.entity.${EntityName}Entity;
import ${PakageName}.exception.BizException;

/**
 * ${EntityName}服务接口
 * @author Administrator
 *
 */
public interface I${EntityName}Service extends IService<${EntityName}Entity> {
    
	/**
	 * 分页获取数据
	 * @param params
	 * @return
	 */
	IPage<Map<String, Object>> query${EntityName}PageList(Map<String,Object> params) throws BizException;
	
	/**
	 * 获取数据列表
	 * @param params
	 * @return
	 */
	List<Map<String,Object>> query${EntityName}ArrayList(Map<String,Object> params) throws BizException;
	
	/**
	 * 添加${EntityName}
	 * @param entity 实体对象	  
	 * @return
	 * @throws BizException
	 */
	String add${EntityName}(${EntityName}Entity entity) throws BizException;
	
}
