﻿using DevelopAssistant.AddIn.JavaProject.Properties;
using ICSharpCode.WinFormsUI.Controls;
using ICSharpCode.WinFormsUI.Docking;
using System;
using System.Windows.Forms;

namespace DevelopAssistant.AddIn.JavaProject
{
    public class JavaProjectAddIn : WindowAddIn // DockContentAddIn
    {
        public JavaProjectAddIn()
        {
            this.IdentityID = "263dd53f-88c3-444b-b5db-4ed69b2681aa";
            this.Name = "生成代码(Java)";
            this.Text = "生成代码(Java)";
            this.Tooltip = "生成代码(Java)";
            this.Icon = Resources.plus_shield;
        }

        public override void Initialize(string AssemblyName, string Winname)
        {
            //
        }

        public override object Execute(params object[] Parameter)
        {
            NTreeNode node = ((NTreeNode)Parameter[3]).TreeView.SelectedNode;            
            MainForm f = new MainForm((DevelopAssistant.Core.MainForm)Parameter[0], this, node,
                (DevelopAssistant.Service.DataBaseServer)Parameter[2],
                Parameter[4].ToString(), Parameter[5].ToString())
            {
                //WindowFloat = new DelegateWindowFloatHandler(FloatParentCenter)
            };
            return f;
        }

        private void FloatParentCenter(Form form)
        {

        }
    }
}
