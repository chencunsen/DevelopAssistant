﻿namespace DevelopAssistant.AddIn.JavaProject
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNameSpace = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.groupBox3 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.checkModel = new System.Windows.Forms.CheckBox();
            this.checkMapper = new System.Windows.Forms.CheckBox();
            this.checkDAO = new System.Windows.Forms.CheckBox();
            this.checkIService = new System.Windows.Forms.CheckBox();
            this.checkServiceImpl = new System.Windows.Forms.CheckBox();
            this.checkController = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbox_identity = new System.Windows.Forms.CheckBox();
            this.txtModelpath = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.combTypes = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPKey = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.progressBar1 = new ICSharpCode.WinFormsUI.Controls.NProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.listView1 = new ICSharpCode.WinFormsUI.Controls.NListView();
            this.groupBox1 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.txtTableName = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtOutMsg = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.panel1 = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.lblStateMsg = new System.Windows.Forms.Label();
            this.btnApplyOk = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.btnCancle = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtNameSpace);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.cbox_identity);
            this.panel2.Controls.Add(this.txtModelpath);
            this.panel2.Controls.Add(this.combTypes);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtPKey);
            this.panel2.Location = new System.Drawing.Point(11, 334);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(655, 125);
            this.panel2.TabIndex = 38;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(3, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 12);
            this.label7.TabIndex = 21;
            this.label7.Text = "顶级命名空间：";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(361, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 22;
            this.label3.Text = "主键：";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(9, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 33;
            this.label2.Text = "生成的类型：";
            // 
            // txtNameSpace
            // 
            this.txtNameSpace.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtNameSpace.BackColor = System.Drawing.Color.White;
            this.txtNameSpace.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNameSpace.Enabled = true;
            this.txtNameSpace.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtNameSpace.FousedColor = System.Drawing.Color.Orange;
            this.txtNameSpace.Icon = null;
            this.txtNameSpace.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtNameSpace.IsButtonTextBox = false;
            this.txtNameSpace.IsClearTextBox = false;
            this.txtNameSpace.IsPasswordTextBox = false;
            this.txtNameSpace.Location = new System.Drawing.Point(98, 6);
            this.txtNameSpace.MaxLength = 32767;
            this.txtNameSpace.Multiline = false;
            this.txtNameSpace.Name = "txtNameSpace";
            this.txtNameSpace.PasswordChar = '\0';
            this.txtNameSpace.Placeholder = null;
            this.txtNameSpace.ReadOnly = false;
            this.txtNameSpace.Size = new System.Drawing.Size(217, 24);
            this.txtNameSpace.TabIndex = 23;
            this.txtNameSpace.UseSystemPasswordChar = false;
            this.txtNameSpace.XBackColor = System.Drawing.Color.White;
            this.txtNameSpace.XForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox3.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox3.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox3.Controls.Add(this.checkModel);
            this.groupBox3.Controls.Add(this.checkMapper);
            this.groupBox3.Controls.Add(this.checkDAO);
            this.groupBox3.Controls.Add(this.checkIService);
            this.groupBox3.Controls.Add(this.checkServiceImpl);
            this.groupBox3.Controls.Add(this.checkController);
            this.groupBox3.Location = new System.Drawing.Point(98, 79);
            this.groupBox3.MarginWidth = 0;
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(543, 39);
            this.groupBox3.TabIndex = 32;
            this.groupBox3.TabStop = false;
            this.groupBox3.Title = "";
            this.groupBox3.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // checkModel
            // 
            this.checkModel.AutoSize = true;
            this.checkModel.Location = new System.Drawing.Point(6, 13);
            this.checkModel.Name = "checkModel";
            this.checkModel.Size = new System.Drawing.Size(60, 16);
            this.checkModel.TabIndex = 4;
            this.checkModel.Text = "Entity";
            this.checkModel.UseVisualStyleBackColor = true;
            // 
            // checkMapper
            // 
            this.checkMapper.AutoSize = true;
            this.checkMapper.Location = new System.Drawing.Point(87, 13);
            this.checkMapper.Name = "checkMapper";
            this.checkMapper.Size = new System.Drawing.Size(60, 16);
            this.checkMapper.TabIndex = 5;
            this.checkMapper.Text = "Mapper";
            this.checkMapper.UseVisualStyleBackColor = true;
            // 
            // checkDAO
            // 
            this.checkDAO.AutoSize = true;
            this.checkDAO.Location = new System.Drawing.Point(164, 13);
            this.checkDAO.Name = "checkDAO";
            this.checkDAO.Size = new System.Drawing.Size(42, 16);
            this.checkDAO.TabIndex = 8;
            this.checkDAO.Text = "DAO";
            this.checkDAO.UseVisualStyleBackColor = true;
            // 
            // checkIService
            // 
            this.checkIService.AutoSize = true;
            this.checkIService.Location = new System.Drawing.Point(231, 13);
            this.checkIService.Name = "checkIService";
            this.checkIService.Size = new System.Drawing.Size(72, 16);
            this.checkIService.TabIndex = 6;
            this.checkIService.Text = "IService";
            this.checkIService.UseVisualStyleBackColor = true;
            // 
            // checkServiceImpl
            // 
            this.checkServiceImpl.AutoSize = true;
            this.checkServiceImpl.Location = new System.Drawing.Point(321, 13);
            this.checkServiceImpl.Name = "checkServiceImpl";
            this.checkServiceImpl.Size = new System.Drawing.Size(90, 16);
            this.checkServiceImpl.TabIndex = 7;
            this.checkServiceImpl.Text = "ServiceImpl";
            this.checkServiceImpl.UseVisualStyleBackColor = true;
            // 
            // checkController
            // 
            this.checkController.AutoSize = true;
            this.checkController.Location = new System.Drawing.Point(433, 13);
            this.checkController.Name = "checkController";
            this.checkController.Size = new System.Drawing.Size(84, 16);
            this.checkController.TabIndex = 9;
            this.checkController.Text = "Controller";
            this.checkController.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(3, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 24;
            this.label4.Text = "二级命名空间：";
            // 
            // cbox_identity
            // 
            this.cbox_identity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbox_identity.AutoSize = true;
            this.cbox_identity.Location = new System.Drawing.Point(558, 10);
            this.cbox_identity.Name = "cbox_identity";
            this.cbox_identity.Size = new System.Drawing.Size(84, 16);
            this.cbox_identity.TabIndex = 31;
            this.cbox_identity.Text = "标识为自增";
            this.cbox_identity.UseVisualStyleBackColor = true;
            // 
            // txtModelpath
            // 
            this.txtModelpath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtModelpath.BackColor = System.Drawing.Color.White;
            this.txtModelpath.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtModelpath.Enabled = true;
            this.txtModelpath.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtModelpath.FousedColor = System.Drawing.Color.Orange;
            this.txtModelpath.Icon = null;
            this.txtModelpath.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtModelpath.IsButtonTextBox = false;
            this.txtModelpath.IsClearTextBox = false;
            this.txtModelpath.IsPasswordTextBox = false;
            this.txtModelpath.Location = new System.Drawing.Point(98, 45);
            this.txtModelpath.MaxLength = 32767;
            this.txtModelpath.Multiline = false;
            this.txtModelpath.Name = "txtModelpath";
            this.txtModelpath.PasswordChar = '\0';
            this.txtModelpath.Placeholder = null;
            this.txtModelpath.ReadOnly = false;
            this.txtModelpath.Size = new System.Drawing.Size(217, 24);
            this.txtModelpath.TabIndex = 25;
            this.txtModelpath.UseSystemPasswordChar = false;
            this.txtModelpath.XBackColor = System.Drawing.Color.White;
            this.txtModelpath.XForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // combTypes
            // 
            this.combTypes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.combTypes.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.combTypes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.combTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combTypes.Font = new System.Drawing.Font("宋体", 9F);
            this.combTypes.FormattingEnabled = true;
            this.combTypes.ItemIcon = null;
            this.combTypes.Location = new System.Drawing.Point(420, 45);
            this.combTypes.Name = "combTypes";
            this.combTypes.Size = new System.Drawing.Size(196, 22);
            this.combTypes.TabIndex = 26;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(361, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 27;
            this.label5.Text = "类型：";
            // 
            // txtPKey
            // 
            this.txtPKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtPKey.BackColor = System.Drawing.Color.White;
            this.txtPKey.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPKey.Enabled = true;
            this.txtPKey.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtPKey.FousedColor = System.Drawing.Color.Orange;
            this.txtPKey.Icon = null;
            this.txtPKey.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtPKey.IsButtonTextBox = false;
            this.txtPKey.IsClearTextBox = false;
            this.txtPKey.IsPasswordTextBox = false;
            this.txtPKey.Location = new System.Drawing.Point(420, 6);
            this.txtPKey.MaxLength = 32767;
            this.txtPKey.Multiline = false;
            this.txtPKey.Name = "txtPKey";
            this.txtPKey.PasswordChar = '\0';
            this.txtPKey.Placeholder = null;
            this.txtPKey.ReadOnly = false;
            this.txtPKey.Size = new System.Drawing.Size(121, 24);
            this.txtPKey.TabIndex = 28;
            this.txtPKey.UseSystemPasswordChar = false;
            this.txtPKey.XBackColor = System.Drawing.Color.White;
            this.txtPKey.XForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.progressBar1.BaseColor = System.Drawing.Color.Empty;
            this.progressBar1.FaceColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(176)))), ((int)(((byte)(227)))));
            this.progressBar1.LinearBrush = false;
            this.progressBar1.Location = new System.Drawing.Point(63, 17);
            this.progressBar1.Maximum = 100;
            this.progressBar1.Minimum = 0;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.ShowValue = false;
            this.progressBar1.Size = new System.Drawing.Size(222, 13);
            this.progressBar1.TabIndex = 30;
            this.progressBar1.Value = 0;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(4, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 29;
            this.label6.Text = "进度：";
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.BackColor = System.Drawing.SystemColors.Window;
            this.listView1.ColumnHeaderHeight = 30;
            this.listView1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.listView1.GridLines = true;
            this.listView1.ItemHeight = 28;
            this.listView1.LabelEdit = true;
            this.listView1.LargeImageList = null;
            this.listView1.Location = new System.Drawing.Point(11, 66);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(655, 262);
            this.listView1.StateImageList = this.imageList1;
            this.listView1.TabIndex = 37;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox1.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox1.Controls.Add(this.checkBox2);
            this.groupBox1.Controls.Add(this.txtTableName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(11, 5);
            this.groupBox1.MarginWidth = 0;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(655, 54);
            this.groupBox1.TabIndex = 36;
            this.groupBox1.TabStop = false;
            this.groupBox1.Title = "";
            this.groupBox1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // checkBox2
            // 
            this.checkBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.Location = new System.Drawing.Point(306, 22);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(96, 16);
            this.checkBox2.TabIndex = 32;
            this.checkBox2.Text = "下划线转驼峰";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // txtTableName
            // 
            this.txtTableName.BackColor = System.Drawing.Color.White;
            this.txtTableName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTableName.Enabled = true;
            this.txtTableName.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtTableName.FousedColor = System.Drawing.Color.Orange;
            this.txtTableName.Icon = null;
            this.txtTableName.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtTableName.IsButtonTextBox = false;
            this.txtTableName.IsClearTextBox = false;
            this.txtTableName.IsPasswordTextBox = false;
            this.txtTableName.Location = new System.Drawing.Point(72, 17);
            this.txtTableName.MaxLength = 32767;
            this.txtTableName.Multiline = false;
            this.txtTableName.Name = "txtTableName";
            this.txtTableName.PasswordChar = '\0';
            this.txtTableName.Placeholder = null;
            this.txtTableName.ReadOnly = false;
            this.txtTableName.Size = new System.Drawing.Size(224, 24);
            this.txtTableName.TabIndex = 6;
            this.txtTableName.UseSystemPasswordChar = false;
            this.txtTableName.XBackColor = System.Drawing.Color.White;
            this.txtTableName.XForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "表名：";
            // 
            // txtOutMsg
            // 
            this.txtOutMsg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutMsg.BackColor = System.Drawing.Color.White;
            this.txtOutMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOutMsg.Enabled = true;
            this.txtOutMsg.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtOutMsg.FousedColor = System.Drawing.Color.Orange;
            this.txtOutMsg.Icon = null;
            this.txtOutMsg.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtOutMsg.IsButtonTextBox = false;
            this.txtOutMsg.IsClearTextBox = false;
            this.txtOutMsg.IsPasswordTextBox = false;
            this.txtOutMsg.Location = new System.Drawing.Point(11, 465);
            this.txtOutMsg.MaxLength = 32767;
            this.txtOutMsg.Multiline = true;
            this.txtOutMsg.Name = "txtOutMsg";
            this.txtOutMsg.PasswordChar = '\0';
            this.txtOutMsg.Placeholder = null;
            this.txtOutMsg.ReadOnly = false;
            this.txtOutMsg.Size = new System.Drawing.Size(655, 72);
            this.txtOutMsg.TabIndex = 40;
            this.txtOutMsg.UseSystemPasswordChar = false;
            this.txtOutMsg.XBackColor = System.Drawing.Color.White;
            this.txtOutMsg.XForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.panel1.BottomBlackColor = System.Drawing.Color.Empty;
            this.panel1.Controls.Add(this.lblStateMsg);
            this.panel1.Controls.Add(this.btnApplyOk);
            this.panel1.Controls.Add(this.btnCancle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(6, 582);
            this.panel1.MarginWidth = 0;
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(678, 60);
            this.panel1.TabIndex = 39;
            this.panel1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // lblStateMsg
            // 
            this.lblStateMsg.AutoSize = true;
            this.lblStateMsg.BackColor = System.Drawing.Color.Transparent;
            this.lblStateMsg.Location = new System.Drawing.Point(21, 26);
            this.lblStateMsg.Name = "lblStateMsg";
            this.lblStateMsg.Size = new System.Drawing.Size(53, 12);
            this.lblStateMsg.TabIndex = 2;
            this.lblStateMsg.Text = "生成成功";
            // 
            // btnApplyOk
            // 
            this.btnApplyOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplyOk.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnApplyOk.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnApplyOk.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnApplyOk.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnApplyOk.FouseColor = System.Drawing.Color.White;
            this.btnApplyOk.Foused = false;
            this.btnApplyOk.FouseTextColor = System.Drawing.Color.Black;
            this.btnApplyOk.Icon = null;
            this.btnApplyOk.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnApplyOk.Location = new System.Drawing.Point(494, 16);
            this.btnApplyOk.Name = "btnApplyOk";
            this.btnApplyOk.Radius = 0;
            this.btnApplyOk.Size = new System.Drawing.Size(75, 32);
            this.btnApplyOk.TabIndex = 1;
            this.btnApplyOk.Text = "生成";
            this.btnApplyOk.UnableIcon = null;
            this.btnApplyOk.UseVisualStyleBackColor = true;
            this.btnApplyOk.Click += new System.EventHandler(this.btnApplyOk_Click);
            // 
            // btnCancle
            // 
            this.btnCancle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancle.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnCancle.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnCancle.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnCancle.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnCancle.FouseColor = System.Drawing.Color.White;
            this.btnCancle.Foused = false;
            this.btnCancle.FouseTextColor = System.Drawing.Color.Black;
            this.btnCancle.Icon = null;
            this.btnCancle.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnCancle.Location = new System.Drawing.Point(585, 16);
            this.btnCancle.Name = "btnCancle";
            this.btnCancle.Radius = 0;
            this.btnCancle.Size = new System.Drawing.Size(75, 32);
            this.btnCancle.TabIndex = 0;
            this.btnCancle.Text = "取消";
            this.btnCancle.UnableIcon = null;
            this.btnCancle.UseVisualStyleBackColor = true;
            this.btnCancle.Click += new System.EventHandler(this.btnCancle_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.progressBar1);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Location = new System.Drawing.Point(16, 590);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(305, 44);
            this.panel3.TabIndex = 41;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "PKey.png");
            this.imageList1.Images.SetKeyName(1, "text_24px.png");
            this.imageList1.Images.SetKeyName(2, "table.png");
            this.imageList1.Images.SetKeyName(3, "pk.gif");
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.groupBox1);
            this.panel4.Controls.Add(this.listView1);
            this.panel4.Controls.Add(this.txtOutMsg);
            this.panel4.Controls.Add(this.panel2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(6, 34);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(678, 548);
            this.panel4.TabIndex = 42;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(690, 648);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "MainForm";
            this.Resizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtNameSpace;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkServiceImpl;
        private System.Windows.Forms.CheckBox checkIService;
        private System.Windows.Forms.CheckBox checkMapper;
        private System.Windows.Forms.CheckBox checkModel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox cbox_identity;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtModelpath;
        private ICSharpCode.WinFormsUI.Controls.NProgressBar progressBar1;
        private ICSharpCode.WinFormsUI.Controls.NComboBox combTypes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtPKey;
        private ICSharpCode.WinFormsUI.Controls.NListView listView1;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox1;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtTableName;
        private System.Windows.Forms.Label label1;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtOutMsg;
        private ICSharpCode.WinFormsUI.Controls.NPanel panel1;
        private System.Windows.Forms.Label lblStateMsg;
        private ICSharpCode.WinFormsUI.Controls.NButton btnApplyOk;
        private ICSharpCode.WinFormsUI.Controls.NButton btnCancle;
        private System.Windows.Forms.CheckBox checkDAO;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.CheckBox checkController;
        private System.Windows.Forms.Panel panel4;
    }
}