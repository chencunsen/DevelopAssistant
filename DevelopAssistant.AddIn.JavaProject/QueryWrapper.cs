﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopAssistant.AddIn.JavaProject
{
    public class QueryWrapper
    {
        private List<QueryEntity> conditions = new List<QueryEntity>();
        private List<QueryEntity> orderbies = new List<QueryEntity>();

        public QueryWrapper(string Column,string Value)
        {
            conditions.Add(new QueryEntity(Column, Value));
        }

        public QueryWrapper()
        {

        }

        public void EQ(string Column,string Value)
        {
            conditions.Add(new QueryEntity(Column, Value));
        }

        public void OrderBy(string Column, string Value)
        {
            orderbies.Add(new QueryEntity(Column, Value));
        }

        public List<QueryEntity> ToList()
        {
            return conditions;
        }

        public List<QueryEntity> Orderbies()
        {
            return orderbies;
        }
    }
}
