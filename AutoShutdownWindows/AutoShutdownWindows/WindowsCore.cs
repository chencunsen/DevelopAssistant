﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoShutdownWindows
{
    public class WindowsCore
    {
        public static void Shutdown(bool cancel, uint interval)
        {
            Process proc = new Process();
            proc.StartInfo.FileName = "cmd.exe"; // 启动命令行程序
            proc.StartInfo.UseShellExecute = false; // 不使用Shell来执行,用程序来执行
            proc.StartInfo.RedirectStandardError = true; // 重定向标准输入输出
            proc.StartInfo.RedirectStandardInput = true;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.CreateNoWindow = true; // 执行时不创建新窗口
            proc.Start();

            string commandLine;
            if (cancel)
                commandLine = @"shutdown /a";
            else
                commandLine = @"shutdown /f /s /t " + (interval / 1000).ToString();

            proc.StandardInput.WriteLine(commandLine);
        }
    }
}
