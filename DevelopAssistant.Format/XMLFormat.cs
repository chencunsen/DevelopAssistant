﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace DevelopAssistant.Format
{
    public class XMLFormatHelper
    {
        /// <summary>
        /// Returns a pretty print version of the given xml.
        /// </summary>
        /// <param name="xml">Xml string to pretty print.</param>
        /// <returns>A pretty print version of the specified xml.  If the
        /// string is not well formed xml the original string is returned.
        /// </returns>
        static string IndentedFormat(string xml)
        {
            string indentedText = String.Empty;

            try
            {
                XmlTextReader reader = new XmlTextReader(new StringReader(xml));
                reader.WhitespaceHandling = WhitespaceHandling.None;

                StringWriter indentedXmlWriter = new StringWriter();
                XmlTextWriter writer = CreateXmlTextWriter(indentedXmlWriter, false, 4);
                writer.WriteNode(reader, false);
                writer.Flush();

                indentedText = indentedXmlWriter.ToString();
            }
            catch (Exception)
            {
                indentedText = xml;
            }

            return indentedText;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textWriter"></param>
        /// <param name="convertTabsToSpaces"></param>
        /// <param name="indentationSize"></param>
        /// <returns></returns>
        static XmlTextWriter CreateXmlTextWriter(System.IO.TextWriter textWriter, bool convertTabsToSpaces, int indentationSize)
        {
            XmlTextWriter writer = new XmlTextWriter(textWriter);
            if (convertTabsToSpaces)
            {
                writer.Indentation = indentationSize;
                writer.IndentChar = ' ';
            }
            else
            {
                writer.Indentation = 1;
                writer.IndentChar = '\t';
            }
            writer.Formatting = System.Xml.Formatting.Indented;
            return writer;
        }

        /// <summary>
        /// Returns a formatted xml string using a simple formatting algorithm.
        /// </summary>
        static string SimpleFormat(string xml)
        {
            return xml.Replace("><", ">\r\n<");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strxml"></param>
        /// <returns></returns>
        public static string FormatToString(string strxml)
        {
            try
            {
                strxml = IndentedFormat(SimpleFormat(IndentedFormat(strxml)));
            }
            catch (Exception ex)
            {
                //
            }
            return strxml;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strxml"></param>
        /// <returns></returns>
        public static string CompressToString(string strxml)
        {
            string result = string.Empty;

            Regex regex = new Regex(@"//.*\n");
            Match match = regex.Match(strxml);
            while (match.Success)
            {
                string matchstr = match.Groups[0].Value;
                strxml = strxml.Replace(matchstr, matchstr.Replace("//", "/*").Replace("\r\n", "*/\r\n"));
                match = match.NextMatch();
            }

            strxml = strxml.Replace('\r', ' ');
            strxml = strxml.Replace('\n', ' ');
            strxml = strxml.Replace('\t', ' ');

            strxml = Regex.Replace(strxml, " {1,}", " ", RegexOptions.IgnoreCase)
                .Replace("('\n", "('").Replace("\n')", "')")
                .Replace("(\"\n", "(\"").Replace("\n\")", "\")")
                .Replace("\"\n", "\"").Replace("\n\"", "\"");

            strxml = strxml.Replace('\n', ' ');
            //string[] lines_array = strjs.Split('\n');  

            result = strxml;

            return result;
        }
    }
}
