﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TSqlFormatter;
using TSqlFormatter.Formatters;

namespace DevelopAssistant.Format
{
    public class TsqlFormatHelper
    {
        public static string FormatToTSQL(string strsql, bool caseWrite)
        {
            string result = string.Empty;

            if (string.IsNullOrEmpty(strsql))
                return strsql;

            try
            {
                //bool errorsEncountered = false;
                //result = SqlFormattingManager.DefaultFormat(strsql, ref errorsEncountered);

                bool errorsEncountered = false;
                SqlFormattingManager formaterManager = new SqlFormattingManager();
                TSqlStandardFormatter formater = (TSqlStandardFormatter)formaterManager.Formatter;
                formater.Options.UppercaseKeywords = caseWrite;
                result = formaterManager.Format(strsql, ref errorsEncountered);

            }
            catch (Exception ex)
            {
                result = strsql;
            }

            return result;
        }

        public static string FormatToString(string strsql,bool caseWrite)
        {
            return FormatToTSQL(strsql, caseWrite);
        }

        public static string CompressToString(string strsql)
        {
            string result = string.Empty;

            strsql = strsql.Replace("//", "");
            strsql = strsql.Replace('\r', ' ');
            strsql = strsql.Replace('\n', ' ');
            //strsql = Regex.Replace(strsql, " +", " ", RegexOptions.None);     

            char[] char_array = strsql.ToCharArray();
            List<char> char_list = new List<char>();

            int space_count = 0;
            for (int i = 0, len = char_array.Length; i < len; i++)
            {
                char chr = char_array[i];
                if (chr == ' ')
                {
                    space_count++;
                    if (space_count == 1)
                    {
                        char_list.Add(chr);
                    }
                }
                if (chr != ' ')
                {
                    space_count = 0;
                    char_list.Add(chr);
                }

            }

            result =string.Concat(char_list.ToArray());

            return result;
        }

    }
}
