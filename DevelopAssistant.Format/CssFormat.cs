﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DevelopAssistant.Format
{
    /// <summary>
    /// 功能描述：css 格式化 
    /// 版本号：v 1.0.0
    /// 创建作者：王晓东
    /// 更新时间：2016-02-17
    /// </summary>
    public class CssFormatHelper
    {
        public static string FormatToCss(string strcss)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(strcss))
                    return strcss;

                int temp_index = 0; //临时记数

                strcss = Regex.Replace(strcss, " {1,}", "\n", RegexOptions.IgnoreCase)
                    .Replace('\r', ' ').Replace('\n', ' ').Replace('\t', ' ');
                
                               
                char[] chr_array = strcss.ToCharArray();
                List<char> chr_list = new List<char>();

                #region 处理换行，去除空格

                for (int i = 0, len = chr_array.Length; i < len; i++)
                {
                    char chr = chr_array[i];                    
                    if (chr != ' ')
                    {
                        if (chr == '{')
                        {                           
                            chr_list.Add(chr);
                            chr_list.Add('\n');
                        }
                        if (chr == '}')
                        {
                            int j = i;
                            char prechr=' ';
                            while (j > 0)
                            {
                                j = j - 1;
                                char ch = chr_array[j];
                                if (ch != ' ')
                                {
                                    j = 0;
                                    prechr = ch;
                                    break;
                                }
                            }
                            if (prechr != ';')
                            {
                                chr_list.Add('\n');
                            }
                            chr_list.Add(chr);
                            chr_list.Add('\n');
                        }
                        if (chr == ';')
                        {
                            chr_list.Add(chr);
                            chr_list.Add('\n');
                        }

                        if (chr == '*')
                        {
                            if (chr_array[i - 1] != '/')
                            {
                                chr_list.Add('\n');
                                chr_list.Add(chr);
                            }
                            else
                            {                                
                                chr_list.Add(chr);
                            }                           
                        }

                        if (chr == '/')
                        {
                            if (i > 1 && chr_array[i - 1] == '*')
                            {
                                chr_list.Add(chr);
                                chr_list.Add('\n');
                            }
                            else
                            {
                                chr_list.Add(chr);
                            }                          
                        }

                        if (chr != '{' && chr != '}' && chr != ';'
                            && chr != '*' && chr != '/')
                        {
                            chr_list.Add(chr);
                        }
                    }
                    else if (i > 0 && i < len - 1 && chr_array[i - 1] != ' ' && chr_array[i + 1] != ' ')
                    {
                        chr_list.Add(chr);
                    }

                }

                #endregion
              
                string temp_str = string.Concat(chr_list.ToArray());

                #region 处理缩进，格式化样式

                string[] lines_array = temp_str.Split('\n');
                foreach (string line in lines_array)
                {
                    if (line != "")
                    {
                        string temp_line = line.ToString().Trim();

                        if (temp_line == "{" || temp_line.EndsWith("{"))
                        {
                            for (int i = 0; i < temp_index; i++)
                            {
                                temp_line = "       " + temp_line;
                            }
                            temp_index = temp_index + 1;
                        }                        

                        if (!temp_line.Contains("{") && temp_line != "}")
                        {
                            for (int i = 0; i < temp_index; i++)
                            {
                                temp_line = "       " + temp_line;
                            }
                        }

                        if (temp_line == "}")
                        {
                            temp_index = temp_index - 1;
                            for (int i = 0; i < temp_index; i++)
                            {
                                temp_line = "       " + temp_line;
                            }
                        }

                        result = result + temp_line + "\r\n";
                    }

                }

                #endregion

                temp_str = null;
                chr_list.Clear();
                chr_list = null;

            }
            catch (Exception ex)
            {
                result = strcss;
            }         
            return result;
        }

        public static string FormatToString(string strcss)
        {
            return FormatToCss(strcss);
        }

        public static string CompressToString(string strcss)
        {
            string result = string.Empty;

            Regex regex = new Regex(@"//.*\n");
            Match match = regex.Match(strcss);
            while (match.Success)
            {
                string matchstr = match.Groups[0].Value;
                strcss = strcss.Replace(matchstr, matchstr.Replace("//", "/*").Replace("\r\n", "*/\r\n"));
                match = match.NextMatch();
            }

            strcss = strcss.Replace('\r', ' ');
            strcss = strcss.Replace('\n', ' ');
            strcss = strcss.Replace('\t', ' ');

            strcss = Regex.Replace(strcss, " {1,}", "\n", RegexOptions.IgnoreCase)
                .Replace("('\n", "('").Replace("\n')", "')")
                .Replace("(\"\n", "(\"").Replace("\n\")", "\")")
                .Replace("\"\n", "\"").Replace("\n\"", "\"");

            strcss = strcss.Replace('\n', ' ');
            //string[] lines_array = strjs.Split('\n');  

            result = strcss;

            return result;
        }
    }
}
