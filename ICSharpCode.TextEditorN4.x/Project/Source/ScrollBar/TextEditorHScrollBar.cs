﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICSharpCode.TextEditor
{
    public class TextEditorHScrollBar : TextEditorScrollBar
    {
        public TextEditorHScrollBar()
        {
            this.orientation = ScrollBarOrientation.Horizontal;
            this.scrollOrientation = System.Windows.Forms.ScrollOrientation.HorizontalScroll;
        }
    }
}
