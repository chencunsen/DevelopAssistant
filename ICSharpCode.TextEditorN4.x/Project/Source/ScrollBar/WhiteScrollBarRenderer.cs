﻿namespace ICSharpCode.TextEditor
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;

    public class WhiteScrollBarRenderer : TextEditorScrollBarRenderer
    {
        public WhiteScrollBarRenderer()
        {
            drawThumbGrip = false;

            // hot state
            thumbColors[0, 0] = SystemColors.ControlDark; //Color.FromArgb(192, 192, 192); // border color
            thumbColors[0, 1] = Color.FromArgb(192, 192, 192); // left/top start color
            thumbColors[0, 2] = Color.FromArgb(192, 192, 192); // left/top end color
            thumbColors[0, 3] = Color.FromArgb(192, 192, 192); // right/bottom line color
            thumbColors[0, 4] = Color.FromArgb(192, 192, 192); // right/bottom start color
            thumbColors[0, 5] = Color.FromArgb(192, 192, 192); // right/bottom end color
            thumbColors[0, 6] = Color.FromArgb(192, 192, 192); // right/bottom middle color
            thumbColors[0, 7] = Color.FromArgb(192, 192, 192); // left/top line color

            // over state
            thumbColors[1, 0] = SystemColors.ControlDark;
            thumbColors[1, 1] = Color.FromArgb(168, 168, 168);
            thumbColors[1, 2] = Color.FromArgb(168, 168, 168);
            thumbColors[1, 3] = Color.FromArgb(168, 168, 168);
            thumbColors[1, 4] = Color.FromArgb(168, 168, 168);
            thumbColors[1, 5] = Color.FromArgb(168, 168, 168);
            thumbColors[1, 6] = Color.FromArgb(168, 168, 168);
            thumbColors[1, 7] = Color.FromArgb(168, 168, 168);

            // pressed state
            thumbColors[2, 0] = SystemColors.ControlDark;
            thumbColors[2, 1] = Color.FromArgb(168, 168, 168);
            thumbColors[2, 2] = Color.FromArgb(168, 168, 168);
            thumbColors[2, 3] = Color.FromArgb(168, 168, 168);
            thumbColors[2, 4] = Color.FromArgb(168, 168, 168);
            thumbColors[2, 5] = Color.FromArgb(168, 168, 168);
            thumbColors[2, 6] = Color.FromArgb(168, 168, 168);
            thumbColors[2, 7] = Color.FromArgb(168, 168, 168);

            /* picture of colors and indices
             *(0,0)
             * -----------------------------------------------
             * |                                             |
             * | |-----------------------------------------| |
             * | |                  (2)                    | |
             * | | |-------------------------------------| | |
             * | | |                (0)                  | | |
             * | | |                                     | | |
             * | | |                                     | | |
             * | |3|                (1)                  |3| |
             * | |6|                (4)                  |6| |
             * | | |                                     | | |
             * | | |                (5)                  | | |
             * | | |-------------------------------------| | |
             * | |                  (12)                   | |
             * | |-----------------------------------------| |
             * |                                             |
             * ----------------------------------------------- (15,17)
             */

            // hot state
            arrowColors[0, 0] = Color.FromArgb(192, 192, 192);
            arrowColors[0, 1] = Color.FromArgb(192, 192, 192);
            arrowColors[0, 2] = Color.FromArgb(192, 192, 192);
            arrowColors[0, 3] = Color.FromArgb(192, 192, 192);
            arrowColors[0, 4] = Color.FromArgb(192, 192, 192);
            arrowColors[0, 5] = Color.FromArgb(192, 192, 192);
            arrowColors[0, 6] = Color.FromArgb(192, 192, 192);
            arrowColors[0, 7] = Color.FromArgb(192, 192, 192);

            // over state
            arrowColors[1, 0] = Color.FromArgb(192, 192, 192);
            arrowColors[1, 1] = Color.FromArgb(192, 192, 192);
            arrowColors[1, 2] = Color.FromArgb(192, 192, 192);
            arrowColors[1, 3] = Color.FromArgb(192, 192, 192);
            arrowColors[1, 4] = Color.FromArgb(192, 192, 192);
            arrowColors[1, 5] = Color.FromArgb(192, 192, 192);
            arrowColors[1, 6] = Color.FromArgb(192, 192, 192);
            arrowColors[1, 7] = Color.FromArgb(192, 192, 192);

            // pressed state
            arrowColors[2, 0] = Color.FromArgb(168, 168, 168);
            arrowColors[2, 1] = Color.FromArgb(168, 168, 168);
            arrowColors[2, 2] = Color.FromArgb(168, 168, 168);
            arrowColors[2, 3] = Color.FromArgb(168, 168, 168);
            arrowColors[2, 4] = Color.FromArgb(168, 168, 168);
            arrowColors[2, 5] = Color.FromArgb(168, 168, 168);
            arrowColors[2, 6] = Color.FromArgb(168, 168, 168);
            arrowColors[2, 7] = Color.FromArgb(168, 168, 168);

            // background colors
            backgroundColors[0] = Color.FromArgb(241, 241, 241);
            backgroundColors[1] = Color.FromArgb(241, 241, 241);
            backgroundColors[2] = Color.FromArgb(241, 241, 241);
            backgroundColors[3] = Color.FromArgb(241, 241, 241);
            backgroundColors[4] = Color.FromArgb(241, 241, 241);

            // track colors
            trackColors[0] = Color.FromArgb(192, 192, 192);
            trackColors[1] = Color.FromArgb(192, 192, 192);

            // arrow border colors
            arrowBorderColors[0] = Color.FromArgb(220, 220, 220);
            arrowBorderColors[1] = Color.FromArgb(220, 220, 220);
            arrowBorderColors[2] = Color.FromArgb(220, 220, 220);
            arrowBorderColors[3] = Color.FromArgb(220, 220, 220);

        }

        public override void DrawBorder(Graphics g, bool enable, Color color, Rectangle rect)
        {
            Color borderColor = SystemColors.ControlLight;
            base.DrawBorder(g, enable, borderColor, rect);
        }

        public override void DrawThumb(Graphics g, Rectangle rect, ScrollBarState state, ScrollBarOrientation orientation)
        {
            base.DrawThumb(g, rect, state, ScrollBarOrientation.None);

            if (orientation == ScrollBarOrientation.Vertical)
            {
                DrawThumbVertical(g, new Rectangle(rect.X + 1, rect.Y + 1, rect.Width - 3, rect.Height - 2), state);
            }
            else
            {
                DrawThumbHorizontal(g, new Rectangle(rect.X + 1, rect.Y + 1, rect.Width - 2, rect.Height - 3), state);
            }

        }
    }    
}
