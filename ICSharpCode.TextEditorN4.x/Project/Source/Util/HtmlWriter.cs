﻿using ICSharpCode.TextEditor.Document;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace ICSharpCode.TextEditor
{
    public class HtmlWriter
    {
        public static string GenerateHtml(TextArea textArea)
        {
            return BuildDocument(textArea);
        }

        private static string BuildDocument(TextArea textArea)
        {
            int fontSize = (int)textArea.Font.Size;
            int fontHeight = textArea.TextView.FontHeight;
            string blackGroundColor = textArea.Document.ThemeName;

            if (blackGroundColor == "Black")
            {
                blackGroundColor = "#1E1E1E";
            }
            else
            {
                blackGroundColor = "#ffffff";
            }

            StringBuilder htmlStringBulder = new StringBuilder();
            htmlStringBulder.Append("<div style=\"padding:10px; font-size:" + fontSize + "px; background:" + blackGroundColor + ";line-height:" + fontHeight + "px;\">");
            htmlStringBulder.Append(BuildHtmlHeader(textArea).ToString());
            htmlStringBulder.Append(BuildHtmlBody(textArea).ToString());
            htmlStringBulder.Append("</div>");

            string outStringText = htmlStringBulder.ToString();
            System.Console.WriteLine("htmlwrite:" + outStringText);
            return outStringText;
        }

        private static string BuildHtmlHeader(TextArea textArea)
        {
            StringBuilder head = new StringBuilder();
            head.Append("<style type=\"text/css\">");
            head.Append(".winc-Paragraph{padding:0px;}");
            head.Append("</style>");
            return head.ToString();
        }

        private static string BuildHtmlBody(TextArea textArea)
        {
            StringBuilder body = new StringBuilder();            

            foreach (ISelection selection in textArea.SelectionManager.SelectionCollection)
            {
                if (!textArea.SelectionManager.IsMutilSelect)
                {
                    if (!selection.IsCurrent)
                        continue;
                }

                //int selectionOffset = textArea.Document.PositionToOffset(selection.StartPosition);
                //int selectionEndOffset = textArea.Document.PositionToOffset(selection.EndPosition);
                for (int i = selection.StartPosition.Y; i <= selection.EndPosition.Y; ++i)
                {
                    LineSegment line = textArea.Document.GetLineSegment(i);                   
                    if (line.Words == null)
                    {
                        continue;
                    }

                    int offset = line.Offset;
                    body.Append("<div class=\"winc-Paragraph\">");

                    foreach (TextWord word in line.Words)
                    {
                        switch (word.Type)
                        {
                            case TextWordType.Space:
                                if (selection.ContainsOffset(offset))
                                {
                                    body.Append("&nbsp;");
                                }                                
                                break;

                            case TextWordType.Tab:
                                if (selection.ContainsOffset(offset))
                                {
                                    body.Append("&nbsp;&nbsp;");
                                }                              
                                break;

                            case TextWordType.Word:
                                Color c = word.Color;

                                StringBuilder style = new StringBuilder();
                                string colorName = "#" + c.R.ToString("X2") + "" + c.G.ToString("X2") + "" + c.B.ToString("X2");

                                style.Append("color:" + colorName + ";");

                                if (word.Italic)
                                {
                                    style.Append("font-style:italic;");
                                }

                                if (word.Bold)
                                {
                                    style.Append("font-weight:bold;");
                                }

                                string printWord = word.Word;
                                printWord = printWord.Replace("&", "&amp;");
                                printWord = printWord.Replace("\"", "&quot;");
                                printWord = printWord.Replace("<", "&lt;");
                                printWord = printWord.Replace(">", "&gt;");                              

                                string wordStyle = style.ToString();

                                body.Append("<span style=\"" + wordStyle + "\">" + printWord + "</span>");


                                break;
                        }
                    }
                    body.Append("</div>");
                }
            }

            return body.ToString();
        }
    }
}
