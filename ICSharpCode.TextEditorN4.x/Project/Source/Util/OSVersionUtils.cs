﻿using System;
using System.Management;

namespace ICSharpCode.TextEditor
{
    public class OSVersionUtils
    {
        public static OSVersionEntity GetOperatingSystemVersion()
        {
            OSVersionEntity result = OSVersionEntity.Unknow;            
            var osVersion = System.Environment.OSVersion.Version;
            try
            {
                result = (OSVersionEntity)Enum.Parse(typeof(OSVersionEntity), Convert.ToString(osVersion.Major + "" + osVersion.Minor));
            }
            catch
            {
                result = OSVersionEntity.Unknow;
            }
            return result;
        }
    }

    public enum OSVersionEntity
    {
        /// <summary>
        /// unknow
        /// </summary>
        Unknow = 00,
        /// <summary>
        /// Windows2000 5.0
        /// </summary>
        Windows2000 = 50,
        /// <summary>
        /// WindowsXP 5.1
        /// </summary>
        WindowsXP = 51,
        /// <summary>
        /// Windows2003 5.2
        /// </summary>
        Windows2003 = 52,
        /// <summary>
        /// WindowsVista Or Windows2008 6.0
        /// </summary>
        WindowsVista = 60,
        /// <summary>
        /// Windows7 Or Windows2008R2 6.1 
        /// </summary>
        Windows7 = 61,
        /// <summary>
        /// Windows8 Or Windows81 6.2
        /// </summary>
        Windows8 = 62,
        /// <summary>
        /// 6.3
        /// </summary>
        Windows2012 = 63,
        /// <summary>
        /// 10.0
        /// </summary>
        Windows10 = 100
    }

}
