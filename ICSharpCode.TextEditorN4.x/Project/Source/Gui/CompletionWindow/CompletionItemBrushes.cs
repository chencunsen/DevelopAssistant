﻿using System;
using System.Drawing;

namespace ICSharpCode.TextEditor
{
    public class CompletionItemBrushes
    {
        public static Brush Highlight { get; set; }

        public static Brush Window { get; set; }

        public static Brush HighlightText { get; set; }

        public static Brush WindowText { get; set; }
    }
}
