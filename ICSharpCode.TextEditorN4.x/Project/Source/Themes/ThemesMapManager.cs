﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace ICSharpCode.TextEditor
{
    public class ThemesMapManager
    {
        private static object syncRoot = new object();

        private static IThemeColorMatcher matcher = null;

        private static Dictionary<string, Dictionary<string, Color>> themesMap = null;

        internal static string selectedTheme { get; set; } 

        internal static Dictionary<string, Dictionary<string, Color>> ThemesMap
        {
            get
            {                
                return themesMap;
            }
        }

        internal static Color getThemeMapColor(string theme, string name)
        {
            Color color = matcher.Color;
            Dictionary<string, Color> map = ThemesMapManager.ThemesMap[theme] as Dictionary<string, Color>;
            if (map.TryGetValue(name, out color))
            {
                return color;
            }
            return Color.White;
        }

        internal static void GenerateThemeMap(string theme)
        {
            lock (syncRoot)
            {
                selectedTheme = theme;

                if (theme == "Default")
                    return;

                themesMap = new Dictionary<string, Dictionary<string, Color>>();

                Dictionary<string, Color> map = new Dictionary<string, Color>();
                switch (theme)
                {
                    case "Black":
                        matcher = new BlackThemeMatcher(map);
                        break;

                    default:

                        break;
                }

                themesMap.Add(theme, map);
            }
        }

        internal static Color MatchThemeColor(Color color, string name)
        {
            return matcher.Resolve(color, name);
        }

    }
}
