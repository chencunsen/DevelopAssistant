﻿using DevelopAssistant.Service;
using ICSharpCode.WinFormsUI.Controls;
using ICSharpCode.WinFormsUI.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.AddIn.Screencolor
{
    public partial class MainForm : ICSharpCode.WinFormsUI.Forms.BaseForm
    {
        protected bool h;
        protected int x1;
        protected int y1;
        protected int x2;
        protected int y2;

        protected int ItemHeight = 0;

        private List<Color> _recordColorList;
        public List<Color> RecordColor
        {
            set { _recordColorList = value; }
        }

        private Color _selectcolor;
        public Color SelectColor
        {
            get { return _selectcolor; }
            set
            {
                _selectcolor = value;
                SelectColorChanged(this,new EventArgs());
            }
        }

        public Form HostForm;

        private void InitializeControls()
        {
            groupBox3.Title = "预览";
            groupBox2.Title = "记录";
            combColor.DropDownStyle = ComboBoxStyle.DropDown;
        }

        public MainForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        public MainForm(Form HostWindow, WindowAddIn Addin)
            : this()
        {
            this.HostForm = HostWindow;
            this.ItemHeight = ColorListBox.Height - 10;
            this.XTheme = ((BaseForm)HostWindow).XTheme;
            this._recordColorList = new List<Color>();
            this.NToolBar.BorderColor = this.XTheme.FormBorderOutterColor;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (h) HostForm.Location = new Point(x1, y1);  
        }

        private void BtnColorPicker_Click(object sender, EventArgs e)
        {
            using (ColorPicker colorPicker = new ColorPicker())
            {
                x2 = this.Location.X;
                y2 = this.Location.Y;
                this.Location = new Point(-this.Width - 20, -this.Height - 20);

                colorPicker.ShowDialog(this);
                this.lblMessage.Text = "完成";
                this.SelectColor = colorPicker.SelectedColor;
                this.Location = new Point(x2, y2);
                BindRecordList();
            }          

        }

        private void BtnHideAssistant_Click(object sender, EventArgs e)
        {
            x1 = HostForm.Location.X;
            y1 = HostForm.Location.Y;
            HostForm.Location = new Point(-HostForm.Width - 20, -HostForm.Height - 20);
            h = true;
        }

        private string getColorName(Color color)
        {
            return string.Format("#{0,2:X2}{1,2:X2}{2,2:X2}", color.R.ToString("X"), color.G.ToString("X"), color.B.ToString("X")).Replace(" ", "0");
        }

        private string getColorRGB(Color color)
        {
            return string.Format("{0},{1},{2},{3}", color.A.ToString("000"), color.R.ToString("000"), color.G.ToString("000"), color.B.ToString("000"));
        }

        private void SelectColorChanged(object sender, EventArgs e)
        {
            this.txtARGB.Text = getColorRGB(_selectcolor);
            this.combColor.Text = getColorName(_selectcolor);           
            this.ColorShowPanel.BackColor = _selectcolor;
        }

        private void BindRecordList()
        {
            int max = 4;

            if (!this._recordColorList.Contains(this._selectcolor))
                this._recordColorList.Add(this._selectcolor);

            if (this._recordColorList.Count > max)
            {
                for (int i = 0; i < max; i++)
                {
                    this._recordColorList[i] = this._recordColorList[i + 1];
                }
                this._recordColorList.RemoveAt(this._recordColorList.Count - 1);
            }
             
            ColorListBox.Items.Clear();
            for (int i = _recordColorList.Count - 1; i >= 0; i--)
            {
                ColorListBox.Items.Add(_recordColorList[i]);
            }    

        }

        private void ColorListBox_DrawItem(object sender, NDrawItemEventArgs e)
        {
            if (e.Index >= 0)
            {
                Rectangle rect = new Rectangle(e.Index * 60 + 6, 2, 56, 56);
                e.Graphics.FillRectangle(new SolidBrush(((Color)ColorListBox.Items[e.Index])), rect);
                e.Graphics.DrawRectangle(new Pen(SystemColors.ControlDark, 1.0f), rect);
            }           
        }

        private void ColorListBox_MouseUp(object sender, MouseEventArgs e)
        {
            Color color = Color.Empty;
            for (int i = 0; i < ColorListBox.Items.Count; i++)
            {
                Rectangle rect = new Rectangle(i * 60 + 6, 2, 56, 56);
                rect = RectangleToScreen(rect);
                Point pt = new Point(e.X, e.Y);
                pt = PointToScreen(pt);
                if (rect.Contains(pt))
                {
                    color = (Color)ColorListBox.Items[i];
                    break;
                }
            }
            this.SelectColor = color;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            OnThemeChanged(new EventArgs());
        }

        public void OnThemeChanged(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color linkColor = System.Drawing.Color.Blue;
            Color toolBackColor = SystemColors.Control;
            Color textColor = SystemColors.Window;
            Color gridLineColor = SystemColors.ControlLight;
            string themeName = AppSettings.EditorSettings.TSQLEditorTheme;
            switch (themeName)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    linkColor = System.Drawing.Color.Blue;
                    toolBackColor = SystemColors.Control;
                    textColor = SystemColors.Window;
                    gridLineColor = SystemColors.ControlLight;
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(045, 045, 048);
                    linkColor = Color.FromArgb(051, 153, 153);
                    toolBackColor = Color.FromArgb(038, 038, 038);
                    textColor = Color.FromArgb(045, 045, 048);
                    gridLineColor = SystemColors.ControlDark;
                    break;
            }

            panel1.ForeColor = foreColor;
            panel1.BackColor = backColor;
            NToolBar.BackColor = toolBackColor;
            groupBox1.ForeColor = foreColor;
            groupBox1.BackColor = backColor;
            groupBox1.GridLineColor = gridLineColor;
            groupBox2.ForeColor = foreColor;
            groupBox2.BackColor = backColor;
            groupBox2.GridLineColor = gridLineColor;
            groupBox3.ForeColor = foreColor;
            groupBox3.BackColor = backColor;
            groupBox3.GridLineColor = gridLineColor;
            btnPickColor.ForeColor = foreColor;
            btnPickColor.BackColor = toolBackColor;
            btnHidenAssistant.ForeColor = foreColor;
            btnHidenAssistant.BackColor = toolBackColor;
            combColor.ForeColor = foreColor;
            combColor.BackColor = textColor;
            txtARGB.XForeColor = foreColor;
            txtARGB.XBackColor = textColor;
            ColorListBox.ForeColor = foreColor;
            ColorListBox.BackColor = backColor;

        }
        
    }
}
