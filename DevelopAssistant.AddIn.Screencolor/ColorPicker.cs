﻿using ICSharpCode.WinFormsUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.AddIn.Screencolor
{
    public partial class ColorPicker : Form
    {
        #region 全局变量

        Bitmap desk;
        Bitmap bmp;
        Graphics g;
        bool isDown = false;
        Point downPoint;
        private Panel pnlColorWindow;
        private TrackBar tckZoomSize;
        private PictureBox picZoom;
        private Label lblYValue;
        private Label lblYName;
        private Label lblXValue;
        private Label lblXName;
        private Label lblShowColor;
        private TextBox txtColorValue;
        private Label lblTitle;
        Rectangle rect;
        Pen pen;
        Bitmap bmpZoom;
        Graphics zoom;
        int zoomSize = 0;
        Color selectedColor = Color.White;
        string colorCode = "";

        #endregion

        public Color SelectedColor
        {
            get { return selectedColor; }
            set { selectedColor = value; }
        }

        public string SelectedColorCode
        {
            get { return colorCode; }
        }

        public ColorPicker()
        {
            InitializeComponent();
            InitializeControls();
        }

        private Image GetDestopImage()
        {
            Rectangle rect = Screen.PrimaryScreen.Bounds; //Screen.GetBounds(this);

            double width = rect.Width * PrimaryScreenHelper.ScaleX;
            double height = rect.Height * PrimaryScreenHelper.ScaleY;

            Bitmap bmp = new Bitmap(rect.Width, rect.Height);

            Bitmap temp = new Bitmap((int)width, (int)height, PixelFormat.Format32bppArgb);

            using (Graphics g = Graphics.FromImage(temp))
            {
                g.CopyFromScreen(0, 0, 0, 0, new System.Drawing.Size((int)width, (int)height));// , CopyPixelOperation.SourceCopy 对屏幕指定区域进行图像复制
                g.Dispose();
            }

            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.DrawImage(temp, new Rectangle(0, 0, bmp.Width, bmp.Height),
                    new Rectangle(0, 0, temp.Width, temp.Height), GraphicsUnit.Pixel);
            }

            return bmp;
        }

        private void InitializeControls()
        {
            int x = Screen.PrimaryScreen.WorkingArea.Width - this.Width;
            int y = Screen.PrimaryScreen.WorkingArea.Height - this.Height;
            //this.pnlColorWindow.Location = new System.Drawing.Point(Screen.PrimaryScreen.WorkingArea.Width - 200, Screen.PrimaryScreen.WorkingArea.Height - 100);// new System.Drawing.Point(3, 3);
            this.pnlColorWindow.Location = new Point(x, y);

            this.MouseUp += new MouseEventHandler(formBox_MouseUp);
            this.MouseMove += new MouseEventHandler(formBox_MouseMove);
            this.Cursor = Cursors.Cross;

            this.TopMost = true;
            this.DoubleBuffered = true;
            this.FormBorderStyle = FormBorderStyle.None;

            rect = Screen.PrimaryScreen.Bounds;
            this.Location = new Point(0, 0);
            this.Size = new Size(rect.Width, rect.Height);

            this.Load += new EventHandler(ColorPicker_Load);

            bmpZoom = new Bitmap(picZoom.Width, picZoom.Height);
            zoom = Graphics.FromImage(bmpZoom);
            picZoom.Image = bmpZoom;

            pen = new Pen(Color.Black, 1);
            pen.DashCap = DashCap.Round;
            pen.DashStyle = DashStyle.Dash;

            zoomSize = tckZoomSize.Value;
        }

        void formBox_MouseMove(object sender, MouseEventArgs e)
        {
            //var scaleX = PrimaryScreenHelper.ScaleX;
            //var scaleY = PrimaryScreenHelper.ScaleY;

            Point mousePoint = Control.MousePosition;
            lblXValue.Text = mousePoint.X.ToString();
            lblYValue.Text = mousePoint.Y.ToString();

            selectedColor = bmp.GetPixel(mousePoint.X + bmp.Width / 4, mousePoint.Y + bmp.Height / 4);
            lblShowColor.BackColor = selectedColor;
            colorCode = string.Format("#{0,2:X2}{1,2:X2}{2,2:X2}", selectedColor.R.ToString("X"), selectedColor.G.ToString("X"), selectedColor.B.ToString("X")).Replace(" ", "0");
            txtColorValue.Text = colorCode;

            int moveX = Control.MousePosition.X - bmpZoom.Width / (2 * zoomSize);
            int moveY = Control.MousePosition.Y - bmpZoom.Height / (2 * zoomSize);
            int moveW = bmpZoom.Width / zoomSize;
            int moveH = bmpZoom.Height / zoomSize;

            //moveX = moveX / (int)scaleX + bmp.Width  / 4;
            //moveY = moveY / (int)scaleY + bmp.Height  / 4;

            moveX = moveX + bmp.Width / 4;
            moveY = moveY + bmp.Height / 4;

            zoom.InterpolationMode = InterpolationMode.NearestNeighbor;
            zoom.DrawImage(bmp,
                new Rectangle(0, 0, bmpZoom.Width, bmpZoom.Height),
                new Rectangle(moveX, moveY, moveW, moveH),
                GraphicsUnit.Pixel);

            zoom.DrawLine(pen, picZoom.Width / 2 - 1, 0, picZoom.Width / 2 - 1, picZoom.Height);
            zoom.DrawLine(pen, 0, picZoom.Height / 2 - 1, picZoom.Width, picZoom.Height / 2 - 1);

            picZoom.Refresh();
        }

        void formBox_MouseUp(object sender, MouseEventArgs e)
        {
            pen.Dispose();
            desk.Dispose();
            bmp.Dispose();
            bmpZoom.Dispose();
            zoom.Dispose();
            g.Dispose();

            Close();
        }

        void ColorPicker_Load(object sender, EventArgs e)
        {
            Bitmap desktopImage = (Bitmap)GetDestopImage();

            bmp = new Bitmap(rect.Width * 2, rect.Height * 2);
            g = Graphics.FromImage(bmp);
            g.Clear(Color.White);
            g.DrawImage(desktopImage, new Rectangle(desktopImage.Width / 2, desktopImage.Height / 2, (int)(desktopImage.Width), (int)(desktopImage.Height)),
                new Rectangle(0, 0, desktopImage.Width, desktopImage.Height), GraphicsUnit.Pixel);
            //g.CopyFromScreen(0, 0, rect.Width / 2, rect.Height / 2, rect.Size);
            g.Dispose();

            //bmp.Save("d:\\bmp.png",ImageFormat.Png);

            desk = new Bitmap(rect.Width, rect.Height);
            g = Graphics.FromImage(desk);            
            g.DrawImage(desktopImage, new Rectangle(0,0,desk.Width,desk.Height), 
                new Rectangle(0, 0, desktopImage.Width, desktopImage.Height), GraphicsUnit.Pixel);
            //g.CopyFromScreen(0, 0, 0, 0, rect.Size);
            g.Dispose();

            //desk.Save("d:\\desk.png", ImageFormat.Png);

            this.BackgroundImage = desk;

            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);//解决闪烁             
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);//解决闪烁 
        }

        private void pnlColorWindow_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDown = true;
                downPoint = e.Location;
            }
        }

        private void pnlColorWindow_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDown)
            {
                pnlColorWindow.Left = Control.MousePosition.X - downPoint.X;
                pnlColorWindow.Top = Control.MousePosition.Y - downPoint.Y;
            }
        }

        private void pnlColorWindow_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDown = false;
            }
        }

        private void tckZoomSize_ValueChanged(object sender, EventArgs e)
        {
            zoomSize = tckZoomSize.Value;
        }
    }
}
