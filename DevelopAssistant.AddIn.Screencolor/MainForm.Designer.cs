﻿namespace DevelopAssistant.AddIn.Screencolor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox1 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.ColorShowPanel = new System.Windows.Forms.Panel();
            this.txtARGB = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.combColor = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new ICSharpCode.WinFormsUI.Controls.NGroupBox();
            this.ColorListBox = new ICSharpCode.WinFormsUI.Controls.NListBox();
            this.NToolBar = new ICSharpCode.WinFormsUI.Controls.NPanel();
            this.btnPickColor = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.btnHidenAssistant = new ICSharpCode.WinFormsUI.Controls.NButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.NToolBar.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox1.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox1.Controls.Add(this.lblMessage);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.txtARGB);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.combColor);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.GridLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(189)))), ((int)(((byte)(189)))));
            this.groupBox1.Location = new System.Drawing.Point(4, 4);
            this.groupBox1.MarginWidth = 0;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 224);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Title = "";
            this.groupBox1.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(52, 96);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(35, 14);
            this.lblMessage.TabIndex = 9;
            this.lblMessage.Text = "就绪";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 14);
            this.label3.TabIndex = 8;
            this.label3.Text = "信息:";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox3.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox3.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox3.Controls.Add(this.ColorShowPanel);
            this.groupBox3.GridLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(189)))), ((int)(((byte)(189)))));
            this.groupBox3.Location = new System.Drawing.Point(183, 10);
            this.groupBox3.MarginWidth = 0;
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(108, 116);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Title = "";
            this.groupBox3.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // ColorShowPanel
            // 
            this.ColorShowPanel.Location = new System.Drawing.Point(11, 19);
            this.ColorShowPanel.Name = "ColorShowPanel";
            this.ColorShowPanel.Size = new System.Drawing.Size(86, 86);
            this.ColorShowPanel.TabIndex = 0;
            // 
            // txtARGB
            // 
            this.txtARGB.BackColor = System.Drawing.Color.White;
            this.txtARGB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtARGB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtARGB.FousedColor = System.Drawing.Color.Orange;
            this.txtARGB.Icon = null;
            this.txtARGB.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.txtARGB.IsButtonTextBox = false;
            this.txtARGB.IsClearTextBox = false;
            this.txtARGB.IsPasswordTextBox = false;
            this.txtARGB.Location = new System.Drawing.Point(52, 53);
            this.txtARGB.MaxLength = 32767;
            this.txtARGB.Multiline = false;
            this.txtARGB.Name = "txtARGB";
            this.txtARGB.PasswordChar = '\0';
            this.txtARGB.Placeholder = null;
            this.txtARGB.ReadOnly = false;
            this.txtARGB.Size = new System.Drawing.Size(125, 24);
            this.txtARGB.TabIndex = 6;
            this.txtARGB.UseSystemPasswordChar = false;
            this.txtARGB.XBackColor = System.Drawing.Color.White;
            this.txtARGB.XDisableColor = System.Drawing.Color.Empty;
            this.txtARGB.XForeColor = System.Drawing.SystemColors.WindowText;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 14);
            this.label2.TabIndex = 5;
            this.label2.Text = "ARGB:";
            // 
            // combColor
            // 
            this.combColor.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.combColor.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.combColor.DropDownButtonWidth = 20;
            this.combColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combColor.DropDownWidth = 0;
            this.combColor.Font = new System.Drawing.Font("宋体", 9F);
            this.combColor.FormattingEnabled = true;
            this.combColor.ItemHeight = 18;
            this.combColor.ItemIcon = null;
            this.combColor.Location = new System.Drawing.Point(52, 22);
            this.combColor.Name = "combColor";
            this.combColor.SelectedIndex = -1;
            this.combColor.SelectedItem = null;
            this.combColor.Size = new System.Drawing.Size(125, 22);
            this.combColor.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 14);
            this.label1.TabIndex = 3;
            this.label1.Text = "名称:";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox2.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.groupBox2.BottomBlackColor = System.Drawing.Color.Empty;
            this.groupBox2.Controls.Add(this.ColorListBox);
            this.groupBox2.GridLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(189)))), ((int)(((byte)(189)))));
            this.groupBox2.Location = new System.Drawing.Point(8, 128);
            this.groupBox2.MarginWidth = 0;
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(284, 88);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Title = "";
            this.groupBox2.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // ColorListBox
            // 
            this.ColorListBox.BackColor = System.Drawing.SystemColors.Control;
            this.ColorListBox.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.ColorListBox.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.None;
            this.ColorListBox.BottomBlackColor = System.Drawing.Color.Empty;
            this.ColorListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ColorListBox.IsHoverStyle = false;
            this.ColorListBox.ItemHeight = 56;
            this.ColorListBox.Location = new System.Drawing.Point(6, 17);
            this.ColorListBox.MarginWidth = 0;
            this.ColorListBox.MultiColumn = true;
            this.ColorListBox.Name = "ColorListBox";
            this.ColorListBox.SelectedIndex = 0;
            this.ColorListBox.SelectedItem = null;
            this.ColorListBox.Size = new System.Drawing.Size(249, 64);
            this.ColorListBox.TabIndex = 0;
            this.ColorListBox.TextRender = ICSharpCode.WinFormsUI.Controls.TextRenderModel.Default;
            this.ColorListBox.TopBlackColor = System.Drawing.Color.Empty;
            this.ColorListBox.DrawItem += new ICSharpCode.WinFormsUI.Controls.NDrawItemEventHandler(this.ColorListBox_DrawItem);
            this.ColorListBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ColorListBox_MouseUp);
            // 
            // NToolBar
            // 
            this.NToolBar.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.NToolBar.BorderStyle = ICSharpCode.WinFormsUI.Controls.NBorderStyle.Top;
            this.NToolBar.BottomBlackColor = System.Drawing.Color.Empty;
            this.NToolBar.Controls.Add(this.btnPickColor);
            this.NToolBar.Controls.Add(this.btnHidenAssistant);
            this.NToolBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.NToolBar.Location = new System.Drawing.Point(6, 266);
            this.NToolBar.MarginWidth = 0;
            this.NToolBar.Name = "NToolBar";
            this.NToolBar.Size = new System.Drawing.Size(308, 58);
            this.NToolBar.TabIndex = 12;
            this.NToolBar.TopBlackColor = System.Drawing.Color.Empty;
            // 
            // btnPickColor
            // 
            this.btnPickColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPickColor.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnPickColor.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnPickColor.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnPickColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPickColor.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnPickColor.FouseColor = System.Drawing.Color.White;
            this.btnPickColor.Foused = false;
            this.btnPickColor.FouseTextColor = System.Drawing.Color.Black;
            this.btnPickColor.Icon = null;
            this.btnPickColor.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnPickColor.Location = new System.Drawing.Point(205, 7);
            this.btnPickColor.Name = "btnPickColor";
            this.btnPickColor.Radius = 0;
            this.btnPickColor.Size = new System.Drawing.Size(96, 34);
            this.btnPickColor.TabIndex = 4;
            this.btnPickColor.Text = "开始取色";
            this.btnPickColor.UnableIcon = null;
            this.btnPickColor.UseVisualStyleBackColor = true;
            this.btnPickColor.Click += new System.EventHandler(this.BtnColorPicker_Click);
            // 
            // btnHidenAssistant
            // 
            this.btnHidenAssistant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHidenAssistant.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.btnHidenAssistant.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.btnHidenAssistant.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.btnHidenAssistant.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHidenAssistant.FouseBorderColor = System.Drawing.Color.Orange;
            this.btnHidenAssistant.FouseColor = System.Drawing.Color.White;
            this.btnHidenAssistant.Foused = false;
            this.btnHidenAssistant.FouseTextColor = System.Drawing.Color.Black;
            this.btnHidenAssistant.Icon = null;
            this.btnHidenAssistant.IconLayoutType = ICSharpCode.WinFormsUI.Controls.IconLayoutType.MiddleLeft;
            this.btnHidenAssistant.Location = new System.Drawing.Point(100, 7);
            this.btnHidenAssistant.Name = "btnHidenAssistant";
            this.btnHidenAssistant.Radius = 0;
            this.btnHidenAssistant.Size = new System.Drawing.Size(96, 34);
            this.btnHidenAssistant.TabIndex = 3;
            this.btnHidenAssistant.Text = "隐藏开发助手";
            this.btnHidenAssistant.UnableIcon = null;
            this.btnHidenAssistant.UseVisualStyleBackColor = true;
            this.btnHidenAssistant.Click += new System.EventHandler(this.BtnHideAssistant_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(6, 34);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(4);
            this.panel1.Size = new System.Drawing.Size(308, 232);
            this.panel1.TabIndex = 13;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 330);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.NToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Resizable = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "屏幕取色器";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.NToolBar.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox1;
        private ICSharpCode.WinFormsUI.Controls.NComboBox combColor;
        private System.Windows.Forms.Label label1;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox2;
        private ICSharpCode.WinFormsUI.Controls.NListBox ColorListBox;
        private ICSharpCode.WinFormsUI.Controls.NTextBox txtARGB;
        private System.Windows.Forms.Label label2;
        private ICSharpCode.WinFormsUI.Controls.NGroupBox groupBox3;
        private System.Windows.Forms.Panel ColorShowPanel;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label label3;
        private ICSharpCode.WinFormsUI.Controls.NPanel NToolBar;
        private ICSharpCode.WinFormsUI.Controls.NButton btnPickColor;
        private ICSharpCode.WinFormsUI.Controls.NButton btnHidenAssistant;
        private System.Windows.Forms.Panel panel1;
    }
}