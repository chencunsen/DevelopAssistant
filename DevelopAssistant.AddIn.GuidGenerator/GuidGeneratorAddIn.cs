﻿
using DevelopAssistant.AddIn.GuidGenerator.Properties;
using System; 

namespace DevelopAssistant.AddIn.GuidGenerator
{
    public class GuidGeneratorAddIn : WindowAddIn
    {
        public GuidGeneratorAddIn()
        {
            this.IdentityID = "4fb146c2-e41e-4b66-bbfb-f3232fa08b2c";
            this.Name = "Guid生成器";
            this.Text = "Guid";
            this.Tooltip = "Guid生成器";
            this.Icon = Resources.coffee_cup_24px;
        }

        public override void Initialize(string AssemblyName, string Winname)
        {
            //
        }

        public override object Execute(params object[] Parameter)
        {
            MainForm f = new MainForm(Parameter[2].ToString(), Parameter[3].ToString())
            {
                // WindowFloat = new DelegateWindowFloatHandler(FloatParentCenter) 
            };
            return f;
        }

    }
}
