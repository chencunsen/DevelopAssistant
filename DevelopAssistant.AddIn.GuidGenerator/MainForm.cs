﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.AddIn.GuidGenerator
{
    public partial class MainForm : ICSharpCode.WinFormsUI.Forms.BaseForm
    {
        private string editTheme = "";
        private string themeName = string.Empty;

        public MainForm(string theme, string editTheme)
        {
            this.themeName = theme;
            this.editTheme = editTheme;
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            ICSharpCode.WinFormsUI.Theme.WinFormsUIThemeBase theme =
                new ICSharpCode.WinFormsUI.Theme.WinFormsUIThemeBase();
            switch (themeName)
            {
                case "Mac":
                    theme = new ICSharpCode.WinFormsUI.Theme.ThemeMac();
                    break;
                case "VS2012":
                    theme = new ICSharpCode.WinFormsUI.Theme.ThemeVS2012();
                    break;
                case "Shadow":
                    theme = new ICSharpCode.WinFormsUI.Theme.ThemeShadow();
                    break;
            }
            this.XTheme = theme;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            OnThemeChange(new EventArgs());
            lblReCreate_Click(this, new EventArgs());
        }

        private void lblReCreate_Click(object sender, EventArgs e)
        {
            string strGuid = Guid.NewGuid().ToString();           
            if (checkBox1.Checked)
            {
                strGuid = strGuid.ToUpper();
            }
            else
            {
                strGuid = strGuid.ToLower();
            }
            nTextBox1.Text = strGuid;
        }

        private void OnThemeChange(EventArgs e)
        {
            Color foreColor = SystemColors.WindowText;
            Color backColor = SystemColors.Control;
            Color linkColor = System.Drawing.Color.Blue;
            Color textBackColor = SystemColors.Window;
            switch (editTheme)
            {
                case "Default":
                    foreColor = SystemColors.WindowText;
                    backColor = SystemColors.Control;
                    linkColor = System.Drawing.Color.Blue;
                    textBackColor = SystemColors.Window;
                    break;
                case "Black":
                    foreColor = Color.FromArgb(240, 240, 240);
                    backColor = Color.FromArgb(045, 045, 048);
                    linkColor = Color.FromArgb(051, 153, 153);
                    textBackColor = Color.FromArgb(038, 038, 038);
                    break;
            }
            lblReCreate.ForeColor = linkColor;
            nTextBox1.XForeColor = foreColor;
            nTextBox1.XBackColor = textBackColor;
            this.ForeColor = foreColor;
            this.BackColor = backColor;
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            string strGuid = nTextBox1.Text;
            if (checkBox1.Checked)
            {
                strGuid = strGuid.ToUpper();
            }
            else
            {
                strGuid = strGuid.ToLower();
            }
            nTextBox1.Text = strGuid;
        }
    }
}
