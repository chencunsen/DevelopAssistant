﻿namespace DevelopAssistant.AddIn.GuidGenerator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.lblReCreate = new System.Windows.Forms.Label();
            this.nTextBox1 = new ICSharpCode.WinFormsUI.Controls.NTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.lblReCreate);
            this.panel1.Controls.Add(this.nTextBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(6, 34);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(297, 159);
            this.panel1.TabIndex = 1;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(103, 15);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(72, 16);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "开启大写";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // lblReCreate
            // 
            this.lblReCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblReCreate.AutoSize = true;
            this.lblReCreate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblReCreate.ForeColor = System.Drawing.Color.Blue;
            this.lblReCreate.Location = new System.Drawing.Point(228, 17);
            this.lblReCreate.Name = "lblReCreate";
            this.lblReCreate.Size = new System.Drawing.Size(53, 12);
            this.lblReCreate.TabIndex = 3;
            this.lblReCreate.Text = "重新生成";
            this.lblReCreate.Click += new System.EventHandler(this.lblReCreate_Click);
            // 
            // nTextBox1
            // 
            this.nTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nTextBox1.BackColor = System.Drawing.Color.White;
            this.nTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nTextBox1.Enabled = true;
            this.nTextBox1.ForeColor = System.Drawing.Color.Gray;
            this.nTextBox1.FousedColor = System.Drawing.Color.Orange;
            this.nTextBox1.Icon = null;
            this.nTextBox1.IconLayout = ICSharpCode.WinFormsUI.Controls.IconLayout.Left;
            this.nTextBox1.IsButtonTextBox = false;
            this.nTextBox1.IsClearTextBox = false;
            this.nTextBox1.IsPasswordTextBox = false;
            this.nTextBox1.Location = new System.Drawing.Point(13, 39);
            this.nTextBox1.MaxLength = 32767;
            this.nTextBox1.Multiline = true;
            this.nTextBox1.Name = "nTextBox1";
            this.nTextBox1.PasswordChar = '\0';
            this.nTextBox1.Placeholder = null;
            this.nTextBox1.ReadOnly = false;
            this.nTextBox1.Size = new System.Drawing.Size(270, 98);
            this.nTextBox1.TabIndex = 2;
            this.nTextBox1.UseSystemPasswordChar = false;
            this.nTextBox1.XBackColor = System.Drawing.Color.White;
            this.nTextBox1.XForeColor = System.Drawing.Color.Gray;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "生成Guid代码：";
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(309, 199);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Resizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Guid生成器";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblReCreate;
        private ICSharpCode.WinFormsUI.Controls.NTextBox nTextBox1;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}