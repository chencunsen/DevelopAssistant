﻿using ICSharpCode.WinFormsUI.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.GuiTest
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            LoadDataGrid();
        }

        private void LoadDataGrid()
        {
            this.panel1.AutoScroll = true;

            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID",typeof(string)));
            dataTable.Columns.Add(new DataColumn("Name", typeof(string)));
            dataTable.Columns.Add(new DataColumn("Size", typeof(string)));

            for (int i = 0; i < 12; i++)
            {
                NDataGridView dg = new NDataGridView();
                dg.Location = new Point(0,(i*100));
                dg.Size = new Size(this.panel1.Width, 100);                
                dg.DataSource = dataTable;
                this.panel1.Controls.Add(dg);
            }
        }
    }
}
