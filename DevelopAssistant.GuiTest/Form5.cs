﻿using ICSharpCode.WinFormsUI.Controls;
using ICSharpCode.WinFormsUI.Controls.NodeControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.GuiTest
{
    public partial class Form5 : Form
    {
        private NTreeViewModel _model;

        public Form5()
        {
            InitializeComponent();
            _model = new NTreeViewModel();
            treeViewAdv1.Model = _model;
        }

        private void LoadTreeViewNodes()
        {
            NodeCheckBox nodeCheckBox = new NodeCheckBox();
            nodeCheckBox.DataPropertyName = "IsChecked";
            treeViewAdv1.NodeControls.Add(nodeCheckBox);

            NodeStateIcon nodeIconBox1 = new NodeStateIcon();
            nodeIconBox1.DataPropertyName = "Icon";
            treeViewAdv1.NodeControls.Add(nodeIconBox1);

            NodeTextBox nodeTextBox1 = new NodeTextBox();
            nodeTextBox1.DataPropertyName = "Text";
            treeViewAdv1.NodeControls.Add(nodeTextBox1);

            Node parent = new Node("root" + _model.Nodes.Count.ToString());
            _model.Nodes.Add(parent);

            for (int i = 0; i < 1; i++)
            {
                Node node = new Node("child" + parent.Nodes.Count.ToString());
                parent.Nodes.Add(node);

                for (int j = 0; j < 5; j++)
                {
                    Node _node = new Node("child_child" + parent.Nodes.Count.ToString());
                    _node.IsChecked = true;
                    node.Nodes.Add(_node);

                    for (int k = 0; k < 5; k++)
                    {
                        Node _node_node = new Node("child_child_child" + parent.Nodes.Count.ToString());
                        _node.Nodes.Add(_node_node);
                    }

                }

            }

            parent = new Node("root" + _model.Nodes.Count.ToString());

            for (int i = 0; i < 2; i++)
            {
                Node node = new Node("child" + parent.Nodes.Count.ToString());
                parent.Nodes.Add(node);
            }

            _model.Nodes.Add(parent);

            treeViewAdv1.SetTheme("Black");
        }

        private void LoadTreeView()
        {
            nTreeView1.Font = new Font("黑体", 12.0f);

            NodeCheckBox nodeCheckBox = new NodeCheckBox();
            nodeCheckBox.DataPropertyName = "IsChecked";
            nTreeView1.NodeControls.Add(nodeCheckBox);

            NodeStateIcon nodeIconBox1 = new NodeStateIcon();
            nodeIconBox1.DataPropertyName = "Icon";
            nTreeView1.NodeControls.Add(nodeIconBox1);

            NodeTextBox nodeTextBox1 = new NodeTextBox();
            nodeTextBox1.DataPropertyName = "Text";
            nTreeView1.NodeControls.Add(nodeTextBox1);


            NTreeNode node = new NTreeNode();
            node.ImageIndex = 0;
            node.SelectedImageIndex = 0;
            node.Text = "111";
            nTreeView1.Nodes.Add(node);

            NTreeNode childNode = new NTreeNode();
            childNode.ImageIndex = 2;
            childNode.IsChecked = true;
            childNode.SelectedImageIndex = 2;
            childNode.Text = "aaa";
            childNode.IsChecked = true;
            node.Nodes.Add(childNode);

            childNode = new NTreeNode();
            childNode.ImageIndex = 2;
            childNode.SelectedImageIndex = 2;
            childNode.Text = "bbb";
            node.Nodes.Add(childNode);

            node = new NTreeNode();
            node.Text = "222";
            nTreeView1.Nodes.Add(node);

            childNode = new NTreeNode();
            childNode.ImageIndex = 2;
            childNode.SelectedImageIndex = 2;
            childNode.Text = "aaa";
            childNode.IsChecked = true;
            node.Nodes.Add(childNode);

            childNode = new NTreeNode();
            childNode.ImageIndex = 2;
            childNode.SelectedImageIndex = 2;
            childNode.Text = "bbb";
            node.Nodes.Add(childNode);

            childNode = new NTreeNode();
            childNode.Text = "ccc";
            childNode.ImageIndex = 2;
            childNode.SelectedImageIndex = 3;
            node.Nodes.Add(childNode);

            childNode = new NTreeNode();
            childNode.Text = "menu_page";
            node.Nodes.Add(childNode);

            childNode = new NTreeNode();
            childNode.Text = "menu_page";
            node.Nodes.Add(childNode);


            for (int i = 0; i < 20; i++)
            {
                NTreeNode childNode_childNode = new NTreeNode();
                childNode_childNode.Text = "ddd-ddd";
                childNode.Nodes.Add(childNode_childNode);

                for (int j = 0; j < 10; j++)
                {
                    NTreeNode child3 = new NTreeNode();
                    child3.Text = "usergroup_relation";
                    childNode_childNode.Nodes.Add(child3);
                }

               
            }
            
            //nTreeView1.Enabled = false;
           
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            LoadTreeView();
            LoadTreeViewNodes();            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var list = nTreeView1.Nodes;
        }
    }
}
