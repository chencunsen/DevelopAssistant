﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.GuiTest
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            System.Data.DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[] {
                new DataColumn("ID",typeof(int)),
                new DataColumn("Name",typeof(string)),
                new DataColumn("Age",typeof(int)),
                new DataColumn("Tel",typeof(string)),
                new DataColumn("Emial",typeof(string)),
                new DataColumn("Address",typeof(string))
            });

            for (int i = 1; i < 100; i++)
            {
                DataRow dr = dt.NewRow();
                dr[0] = i;
                dr[1] = "姓名" + i;
                dr[2] = i % 2;
                dr[3] = "130000000";
                dr[3] = "wxdaaftt@126.com";
                dr[4] = "中国人民万风中国人民万风";
                dt.Rows.Add(dr);
            }

            this.dataGridViewEx1.DataSource = dt;
        }
    }
}
