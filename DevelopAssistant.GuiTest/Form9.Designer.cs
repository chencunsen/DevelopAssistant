﻿namespace DevelopAssistant.GuiTest
{
    partial class Form9
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nComboBox1 = new ICSharpCode.WinFormsUI.Controls.NComboBox();
            this.SuspendLayout();
            // 
            // nComboBox1
            // 
            this.nComboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.nComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.nComboBox1.DropDownWidth = 160;
            this.nComboBox1.Font = new System.Drawing.Font("宋体", 12F);
            this.nComboBox1.FormattingEnabled = true;
            this.nComboBox1.ItemIcon = global::DevelopAssistant.GuiTest.Properties.Resources.gps;
            this.nComboBox1.Items.AddRange(new object[] {
            "111111111111",
            "222222222222",
            "333333333333",
            "444444444444",
            "555555555555"});
            this.nComboBox1.Location = new System.Drawing.Point(178, 57);
            this.nComboBox1.Name = "nComboBox1";
            this.nComboBox1.Size = new System.Drawing.Size(157, 27);
            this.nComboBox1.TabIndex = 0;
            // 
            // Form9
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 276);
            this.Controls.Add(this.nComboBox1);
            this.Name = "Form9";
            this.Text = "Form9";
            this.Load += new System.EventHandler(this.Form9_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ICSharpCode.WinFormsUI.Controls.NComboBox nComboBox1;
    }
}