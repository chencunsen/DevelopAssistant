﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopAssistant.GuiTest
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
            this.nListBox1.UpdateScrollBars();
            this.nListBox1.SizeChanged += NListBox1_SizeChanged;
        }

        private void NListBox1_SizeChanged(object sender, EventArgs e)
        {
            this.nListBox1.UpdateScrollBars();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 20; i++)
            {
                this.nListBox1.Items.Add("第 "+i +" 项 asdfasdfasdfasdfsadfasdf" + i);
            }

            this.nListBox1.UpdateScrollBars();

        }
    }
}
